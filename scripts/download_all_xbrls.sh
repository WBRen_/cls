#!/bin/sh

if [ $# -lt 3 ]; then
    echo "Usage: $0 <container_id> <sh|sz> <company_codes_file>"
    exit 1
fi

container_id=$1
exchange_code=$2
company_codes_file=$3

echo "download $exchange_code $company_codes_file"

echo "xbrls company code path: $company_codes_file"

if [ ! -z "$xbrlscompany_codes_file_path" ];then
  echo "Error: path $company_codes_file doesn't exist"
  exit 2
fi

docker cp $company_codes_file $container_id:/xbrls_${exchange_code}_codes.txt

docker exec $container_id mix aimidas.import.download_all_xbrls $exchange_code /xbrls_${exchange_code}_codes.txt