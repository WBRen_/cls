defmodule Aimidas.Umbrella.Mixfile do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      start_permanent: Mix.env == :prod,
      build_embedded: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options.
  #
  # Dependencies listed here are available only for this project
  # and cannot be accessed from applications inside the apps folder
  defp deps do
    [
      {:distillery, "~> 1.5"},
      {:credo, "~> 0.9", only: [:dev, :test], runtime: false},
      # {:appsignal, "~> 1.0"}
    ]
  end

  defp aliases do
    ['ecto.setup': [
        "ecto.create --quiet",
        "ecto.migrate",
        "run apps/aimidas/priv/repo/seeds.exs",
      ]]
  end  
end
