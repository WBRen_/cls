defmodule Admin.QnaTest do
  use Admin.DataCase

  alias Admin.Qna

  describe "pdf_pages" do
    alias Admin.Qna.PdfPage

    @valid_attrs %{company_code: "some company_code", content: "some content", end_page: 42, image_url: "some image_url", is_abstract: 42, quarter: 42, start_page: 42, year: 42}
    @update_attrs %{company_code: "some updated company_code", content: "some updated content", end_page: 43, image_url: "some updated image_url", is_abstract: 43, quarter: 43, start_page: 43, year: 43}
    @invalid_attrs %{company_code: nil, content: nil, end_page: nil, image_url: nil, is_abstract: nil, quarter: nil, start_page: nil, year: nil}

    def pdf_page_fixture(attrs \\ %{}) do
      {:ok, pdf_page} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Qna.create_pdf_page()

      pdf_page
    end

    test "list_pdf_pages/0 returns all pdf_pages" do
      pdf_page = pdf_page_fixture()
      assert Qna.list_pdf_pages() == [pdf_page]
    end

    test "get_pdf_page!/1 returns the pdf_page with given id" do
      pdf_page = pdf_page_fixture()
      assert Qna.get_pdf_page!(pdf_page.id) == pdf_page
    end

    test "create_pdf_page/1 with valid data creates a pdf_page" do
      assert {:ok, %PdfPage{} = pdf_page} = Qna.create_pdf_page(@valid_attrs)
      assert pdf_page.company_code == "some company_code"
      assert pdf_page.content == "some content"
      assert pdf_page.end_page == 42
      assert pdf_page.image_url == "some image_url"
      assert pdf_page.is_abstract == 42
      assert pdf_page.quarter == 42
      assert pdf_page.start_page == 42
      assert pdf_page.year == 42
    end

    test "create_pdf_page/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Qna.create_pdf_page(@invalid_attrs)
    end

    test "update_pdf_page/2 with valid data updates the pdf_page" do
      pdf_page = pdf_page_fixture()
      assert {:ok, pdf_page} = Qna.update_pdf_page(pdf_page, @update_attrs)
      assert %PdfPage{} = pdf_page
      assert pdf_page.company_code == "some updated company_code"
      assert pdf_page.content == "some updated content"
      assert pdf_page.end_page == 43
      assert pdf_page.image_url == "some updated image_url"
      assert pdf_page.is_abstract == 43
      assert pdf_page.quarter == 43
      assert pdf_page.start_page == 43
      assert pdf_page.year == 43
    end

    test "update_pdf_page/2 with invalid data returns error changeset" do
      pdf_page = pdf_page_fixture()
      assert {:error, %Ecto.Changeset{}} = Qna.update_pdf_page(pdf_page, @invalid_attrs)
      assert pdf_page == Qna.get_pdf_page!(pdf_page.id)
    end

    test "delete_pdf_page/1 deletes the pdf_page" do
      pdf_page = pdf_page_fixture()
      assert {:ok, %PdfPage{}} = Qna.delete_pdf_page(pdf_page)
      assert_raise Ecto.NoResultsError, fn -> Qna.get_pdf_page!(pdf_page.id) end
    end

    test "change_pdf_page/1 returns a pdf_page changeset" do
      pdf_page = pdf_page_fixture()
      assert %Ecto.Changeset{} = Qna.change_pdf_page(pdf_page)
    end
  end
end
