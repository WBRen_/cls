defmodule Admin.SynonymTest do
  use Admin.DataCase

  alias Admin.Synonym

  describe "syn_words" do
    alias Admin.Synonym.Word

    @valid_attrs %{synonym_words: "some synonym_words", word: "some word"}
    @update_attrs %{synonym_words: "some updated synonym_words", word: "some updated word"}
    @invalid_attrs %{synonym_words: nil, word: nil}

    def word_fixture(attrs \\ %{}) do
      {:ok, word} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Synonym.create_word()

      word
    end

    test "list_syn_words/0 returns all syn_words" do
      word = word_fixture()
      assert Synonym.list_syn_words() == [word]
    end

    test "get_word!/1 returns the word with given id" do
      word = word_fixture()
      assert Synonym.get_word!(word.id) == word
    end

    test "create_word/1 with valid data creates a word" do
      assert {:ok, %Word{} = word} = Synonym.create_word(@valid_attrs)
      assert word.synonym_words == "some synonym_words"
      assert word.word == "some word"
    end

    test "create_word/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Synonym.create_word(@invalid_attrs)
    end

    test "update_word/2 with valid data updates the word" do
      word = word_fixture()
      assert {:ok, word} = Synonym.update_word(word, @update_attrs)
      assert %Word{} = word
      assert word.synonym_words == "some updated synonym_words"
      assert word.word == "some updated word"
    end

    test "update_word/2 with invalid data returns error changeset" do
      word = word_fixture()
      assert {:error, %Ecto.Changeset{}} = Synonym.update_word(word, @invalid_attrs)
      assert word == Synonym.get_word!(word.id)
    end

    test "delete_word/1 deletes the word" do
      word = word_fixture()
      assert {:ok, %Word{}} = Synonym.delete_word(word)
      assert_raise Ecto.NoResultsError, fn -> Synonym.get_word!(word.id) end
    end

    test "change_word/1 returns a word changeset" do
      word = word_fixture()
      assert %Ecto.Changeset{} = Synonym.change_word(word)
    end
  end
end
