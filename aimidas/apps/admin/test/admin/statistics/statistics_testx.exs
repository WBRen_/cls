defmodule Admin.StatisticsTest do
  use Admin.DataCase

  alias Admin.Statistics

  describe "user_answers" do
    alias Admin.Statistics.UserAnswer

    @valid_attrs %{answer: "some answer", contras: 42, stars: 42, status: 42}
    @update_attrs %{answer: "some updated answer", contras: 43, stars: 43, status: 43}
    @invalid_attrs %{answer: nil, contras: nil, stars: nil, status: nil}

    def user_answer_fixture(attrs \\ %{}) do
      {:ok, user_answer} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Statistics.create_user_answer()

      user_answer
    end

    test "list_user_answers/0 returns all user_answers" do
      user_answer = user_answer_fixture()
      assert Statistics.list_user_answers() == [user_answer]
    end

    test "get_user_answer!/1 returns the user_answer with given id" do
      user_answer = user_answer_fixture()
      assert Statistics.get_user_answer!(user_answer.id) == user_answer
    end

    test "create_user_answer/1 with valid data creates a user_answer" do
      assert {:ok, %UserAnswer{} = user_answer} = Statistics.create_user_answer(@valid_attrs)
      assert user_answer.answer == "some answer"
      assert user_answer.contras == 42
      assert user_answer.stars == 42
      assert user_answer.status == 42
    end

    test "create_user_answer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Statistics.create_user_answer(@invalid_attrs)
    end

    test "update_user_answer/2 with valid data updates the user_answer" do
      user_answer = user_answer_fixture()
      assert {:ok, user_answer} = Statistics.update_user_answer(user_answer, @update_attrs)
      assert %UserAnswer{} = user_answer
      assert user_answer.answer == "some updated answer"
      assert user_answer.contras == 43
      assert user_answer.stars == 43
      assert user_answer.status == 43
    end

    test "update_user_answer/2 with invalid data returns error changeset" do
      user_answer = user_answer_fixture()
      assert {:error, %Ecto.Changeset{}} = Statistics.update_user_answer(user_answer, @invalid_attrs)
      assert user_answer == Statistics.get_user_answer!(user_answer.id)
    end

    test "delete_user_answer/1 deletes the user_answer" do
      user_answer = user_answer_fixture()
      assert {:ok, %UserAnswer{}} = Statistics.delete_user_answer(user_answer)
      assert_raise Ecto.NoResultsError, fn -> Statistics.get_user_answer!(user_answer.id) end
    end

    test "change_user_answer/1 returns a user_answer changeset" do
      user_answer = user_answer_fixture()
      assert %Ecto.Changeset{} = Statistics.change_user_answer(user_answer)
    end
  end

  describe "normalized_questions" do
    alias Admin.Statistics.NormalizedQuestion

    @valid_attrs %{ask_count: 42, md5: "some md5", otpq: %{}, query: "some query"}
    @update_attrs %{ask_count: 43, md5: "some updated md5", otpq: %{}, query: "some updated query"}
    @invalid_attrs %{ask_count: nil, md5: nil, otpq: nil, query: nil}

    def normalized_question_fixture(attrs \\ %{}) do
      {:ok, normalized_question} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Statistics.create_normalized_question()

      normalized_question
    end

    test "list_normalized_questions/0 returns all normalized_questions" do
      normalized_question = normalized_question_fixture()
      assert Statistics.list_normalized_questions() == [normalized_question]
    end

    test "get_normalized_question!/1 returns the normalized_question with given id" do
      normalized_question = normalized_question_fixture()
      assert Statistics.get_normalized_question!(normalized_question.id) == normalized_question
    end

    test "create_normalized_question/1 with valid data creates a normalized_question" do
      assert {:ok, %NormalizedQuestion{} = normalized_question} = Statistics.create_normalized_question(@valid_attrs)
      assert normalized_question.ask_count == 42
      assert normalized_question.md5 == "some md5"
      assert normalized_question.otpq == %{}
      assert normalized_question.query == "some query"
    end

    test "create_normalized_question/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Statistics.create_normalized_question(@invalid_attrs)
    end

    test "update_normalized_question/2 with valid data updates the normalized_question" do
      normalized_question = normalized_question_fixture()
      assert {:ok, normalized_question} = Statistics.update_normalized_question(normalized_question, @update_attrs)
      assert %NormalizedQuestion{} = normalized_question
      assert normalized_question.ask_count == 43
      assert normalized_question.md5 == "some updated md5"
      assert normalized_question.otpq == %{}
      assert normalized_question.query == "some updated query"
    end

    test "update_normalized_question/2 with invalid data returns error changeset" do
      normalized_question = normalized_question_fixture()
      assert {:error, %Ecto.Changeset{}} = Statistics.update_normalized_question(normalized_question, @invalid_attrs)
      assert normalized_question == Statistics.get_normalized_question!(normalized_question.id)
    end

    test "delete_normalized_question/1 deletes the normalized_question" do
      normalized_question = normalized_question_fixture()
      assert {:ok, %NormalizedQuestion{}} = Statistics.delete_normalized_question(normalized_question)
      assert_raise Ecto.NoResultsError, fn -> Statistics.get_normalized_question!(normalized_question.id) end
    end

    test "change_normalized_question/1 returns a normalized_question changeset" do
      normalized_question = normalized_question_fixture()
      assert %Ecto.Changeset{} = Statistics.change_normalized_question(normalized_question)
    end
  end
end
