defmodule Admin.FeedbackTest do
  use Admin.DataCase

  alias Admin.Feedback

  describe "advices" do
    alias Admin.Feedback.Advice

    @valid_attrs %{content: "some content"}
    @update_attrs %{content: "some updated content"}
    @invalid_attrs %{content: nil}

    def advice_fixture(attrs \\ %{}) do
      {:ok, advice} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Feedback.create_advice()

      advice
    end

    test "list_advices/0 returns all advices" do
      advice = advice_fixture()
      assert Feedback.list_advices() == [advice]
    end

    test "get_advice!/1 returns the advice with given id" do
      advice = advice_fixture()
      assert Feedback.get_advice!(advice.id) == advice
    end

    test "create_advice/1 with valid data creates a advice" do
      assert {:ok, %Advice{} = advice} = Feedback.create_advice(@valid_attrs)
      assert advice.content == "some content"
    end

    test "create_advice/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Feedback.create_advice(@invalid_attrs)
    end

    test "update_advice/2 with valid data updates the advice" do
      advice = advice_fixture()
      assert {:ok, advice} = Feedback.update_advice(advice, @update_attrs)
      assert %Advice{} = advice
      assert advice.content == "some updated content"
    end

    test "update_advice/2 with invalid data returns error changeset" do
      advice = advice_fixture()
      assert {:error, %Ecto.Changeset{}} = Feedback.update_advice(advice, @invalid_attrs)
      assert advice == Feedback.get_advice!(advice.id)
    end

    test "delete_advice/1 deletes the advice" do
      advice = advice_fixture()
      assert {:ok, %Advice{}} = Feedback.delete_advice(advice)
      assert_raise Ecto.NoResultsError, fn -> Feedback.get_advice!(advice.id) end
    end

    test "change_advice/1 returns a advice changeset" do
      advice = advice_fixture()
      assert %Ecto.Changeset{} = Feedback.change_advice(advice)
    end
  end
end
