defmodule Admin.ManagementTest do
  use Admin.DataCase

  alias Admin.Management

  describe "hot_questions" do
    alias Admin.Management.HotQuestion

    @valid_attrs %{content: "some content", level: 42}
    @update_attrs %{content: "some updated content", level: 43}
    @invalid_attrs %{content: nil, level: nil}

    def hot_question_fixture(attrs \\ %{}) do
      {:ok, hot_question} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Management.create_hot_question()

      hot_question
    end

    test "list_hot_questions/0 returns all hot_questions" do
      hot_question = hot_question_fixture()
      assert Management.list_hot_questions() == [hot_question]
    end

    test "get_hot_question!/1 returns the hot_question with given id" do
      hot_question = hot_question_fixture()
      assert Management.get_hot_question!(hot_question.id) == hot_question
    end

    test "create_hot_question/1 with valid data creates a hot_question" do
      assert {:ok, %HotQuestion{} = hot_question} = Management.create_hot_question(@valid_attrs)
      assert hot_question.content == "some content"
      assert hot_question.level == 42
    end

    test "create_hot_question/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Management.create_hot_question(@invalid_attrs)
    end

    test "update_hot_question/2 with valid data updates the hot_question" do
      hot_question = hot_question_fixture()
      assert {:ok, hot_question} = Management.update_hot_question(hot_question, @update_attrs)
      assert %HotQuestion{} = hot_question
      assert hot_question.content == "some updated content"
      assert hot_question.level == 43
    end

    test "update_hot_question/2 with invalid data returns error changeset" do
      hot_question = hot_question_fixture()
      assert {:error, %Ecto.Changeset{}} = Management.update_hot_question(hot_question, @invalid_attrs)
      assert hot_question == Management.get_hot_question!(hot_question.id)
    end

    test "delete_hot_question/1 deletes the hot_question" do
      hot_question = hot_question_fixture()
      assert {:ok, %HotQuestion{}} = Management.delete_hot_question(hot_question)
      assert_raise Ecto.NoResultsError, fn -> Management.get_hot_question!(hot_question.id) end
    end

    test "change_hot_question/1 returns a hot_question changeset" do
      hot_question = hot_question_fixture()
      assert %Ecto.Changeset{} = Management.change_hot_question(hot_question)
    end
  end
end
