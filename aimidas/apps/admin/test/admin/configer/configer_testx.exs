defmodule Admin.ConfigerTest do
  use Admin.DataCase

  alias Admin.Configer

  describe "configs" do
    alias Admin.Configer.Config

    @valid_attrs %{key_name: "some key_name", value: "some value"}
    @update_attrs %{key_name: "some updated key_name", value: "some updated value"}
    @invalid_attrs %{key_name: nil, value: nil}

    def config_fixture(attrs \\ %{}) do
      {:ok, config} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Configer.create_config()

      config
    end

    test "list_configs/0 returns all configs" do
      config = config_fixture()
      assert Configer.list_configs() == [config]
    end

    test "get_config!/1 returns the config with given id" do
      config = config_fixture()
      assert Configer.get_config!(config.id) == config
    end

    test "create_config/1 with valid data creates a config" do
      assert {:ok, %Config{} = config} = Configer.create_config(@valid_attrs)
      assert config.key_name == "some key_name"
      assert config.value == "some value"
    end

    test "create_config/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Configer.create_config(@invalid_attrs)
    end

    test "update_config/2 with valid data updates the config" do
      config = config_fixture()
      assert {:ok, config} = Configer.update_config(config, @update_attrs)
      assert %Config{} = config
      assert config.key_name == "some updated key_name"
      assert config.value == "some updated value"
    end

    test "update_config/2 with invalid data returns error changeset" do
      config = config_fixture()
      assert {:error, %Ecto.Changeset{}} = Configer.update_config(config, @invalid_attrs)
      assert config == Configer.get_config!(config.id)
    end

    test "delete_config/1 deletes the config" do
      config = config_fixture()
      assert {:ok, %Config{}} = Configer.delete_config(config)
      assert_raise Ecto.NoResultsError, fn -> Configer.get_config!(config.id) end
    end

    test "change_config/1 returns a config changeset" do
      config = config_fixture()
      assert %Ecto.Changeset{} = Configer.change_config(config)
    end
  end
end
