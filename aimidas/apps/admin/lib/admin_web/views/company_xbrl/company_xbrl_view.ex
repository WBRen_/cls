defmodule AdminWeb.Ontology.CompanyXbrlView do
  use AdminWeb, :view
  alias AdminWeb.Ontology.CompanyXbrlView

  def render("index.json", %{company_xbrls: page}) do
    %{
      list: render_many(page.entries, CompanyXbrlView, "company_xbrl.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{company_xbrl: company_xbrl}) do
    %{data: render_one(company_xbrl, CompanyXbrlView, "company_xbrl_single.json")}
  end

  def render("company_xbrl.json", %{company_xbrl: company_xbrl}) do
    {date1, {a1, b1, c1, _}} = company_xbrl.updated_at
    updated_at = {date1, {a1, b1, c1}} |> Ecto.DateTime.from_erl
    {date2, {a2, b2, c2, _}} = company_xbrl.inserted_at
    inserted_at = {date2, {a2, b2, c2}} |> Ecto.DateTime.from_erl
    %{
      id: company_xbrl.id,
      abbrev: company_xbrl.abbrev,
      company_code: company_xbrl.company_code,
      # details: company_xbrl.details, # size is toooooo large
      exchange_code: company_xbrl.exchange_code,
      full_name: company_xbrl.full_name,
      industry: company_xbrl.industry,
      src_updated_at: company_xbrl.src_updated_at |> Ecto.Date.from_erl,
      updated_at: updated_at,
      inserted_at: inserted_at
    }
  end

  def render("company_xbrl_single.json", %{company_xbrl: company_xbrl}) do
    %{
      id: company_xbrl.id,
      abbrev: company_xbrl.abbrev,
      company_code: company_xbrl.company_code,
      details: company_xbrl.details,
      exchange_code: company_xbrl.exchange_code,
      full_name: company_xbrl.full_name,
      industry: company_xbrl.industry,
      src_updated_at: company_xbrl.src_updated_at,
      updated_at: company_xbrl.updated_at,
      inserted_at: company_xbrl.inserted_at
    }
  end
end
