defmodule AdminWeb.Ontology.TermView do
  use AdminWeb, :view
  alias AdminWeb.Ontology.TermView

  def render("index.json", %{terms: page}) do
    %{
      list: render_many(page.entries, TermView, "term.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("list_all.json", %{terms: terms}) do
    %{list: render_many(terms, TermView, "term_simple.json")}
  end

  def render("term_simple.json", %{term: term}) do
    "#{term.name}"
  end

  def render("show.json", %{term: term}) do
    %{data: render_one(term, TermView, "term_single.json")}
  end

  def render("term_single.json", %{term: term}) do
    %{
      id: term.id,
      description: term.description,
      name: term.name,
      updated_at: term.updated_at,
      inserted_at: term.inserted_at
    }
  end

  def render("term.json", %{term: term}) do
    {date1, {a1, b1, c1, _}} = term.updated_at
    updated_at = {date1, {a1, b1, c1}} |> Ecto.DateTime.from_erl
    {date2, {a2, b2, c2, _}} = term.inserted_at
    inserted_at = {date2, {a2, b2, c2}} |> Ecto.DateTime.from_erl
    %{
      id: term.id,
      description: term.description,
      name: term.name,
      updated_at: updated_at,
      inserted_at: inserted_at
    }
  end
end
