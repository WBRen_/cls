defmodule AdminWeb.AdviceView do
  use AdminWeb, :view
  alias AdminWeb.AdviceView

  def render("index.json", %{advices: page}) do
    %{
      list: render_many(page.entries, AdviceView, "advice.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{advice: advice}) do
    %{data: render_one(advice, AdviceView, "advice.json")}
  end

  def render("advice.json", %{advice: advice}) do
    %{
      id: advice.id,
      content: advice.content,
      user: %{
        nickname: advice.user.nickname,
        avatar: advice.user.avatar
      },
      updated_at: advice.updated_at,
      inserted_at: advice.inserted_at
    }
  end
end
