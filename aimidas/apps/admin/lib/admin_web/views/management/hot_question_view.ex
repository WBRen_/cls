defmodule AdminWeb.HotQuestionView do
  use AdminWeb, :view
  alias AdminWeb.HotQuestionView

  def render("index.json", %{hot_questions: page}) do
    %{
      list: render_many(page.entries, HotQuestionView, "hot_question.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{hot_question: hot_question}) do
    %{data: render_one(hot_question, HotQuestionView, "hot_question.json")}
  end

  def render("hot_question.json", %{hot_question: hot_question}) do
    %{
      id: hot_question.id,
      content: hot_question.content,
      level: hot_question.level,
      updated_at: hot_question.updated_at,
      inserted_at: hot_question.inserted_at
    }
  end
end
