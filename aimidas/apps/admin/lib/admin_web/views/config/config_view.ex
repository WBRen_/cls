defmodule AdminWeb.ConfigView do
  use AdminWeb, :view
  alias AdminWeb.ConfigView

  def render("index.json", %{configs: configs}) do
    %{data: render_many(configs, ConfigView, "config.json")}
  end

  def render("show.json", %{config: config}) do
    %{data: render_one(config, ConfigView, "config.json")}
  end

  def render("config.json", %{config: config}) do
    %{id: config.id,
      key_name: config.key_name,
      value: config.value}
  end
end
