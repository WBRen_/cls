defmodule AdminWeb.Ontology.CompanyInfoView do
  use AdminWeb, :view
  alias AdminWeb.Ontology.CompanyInfoView

  def render("index.json", %{company_infos: page}) do
    %{
      list: render_many(page.entries, CompanyInfoView, "company_info.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{company_info: company_info}) do
    %{data: render_one(company_info, CompanyInfoView, "company_info_single.json")}
  end

  def render("company_info_single.json", %{company_info: company_info}) do
    %{
      id: company_info.id,
      abbrev_name: company_info.abbrev_name,
      company_code: company_info.company_code,
      details: company_info.details,
      full_name: company_info.full_name,
      industry: company_info.industry,
      season: company_info.season,
      source: company_info.source,
      info_updated_at: company_info.info_updated_at,
      year: company_info.year,
      updated_at: company_info.updated_at,
      inserted_at: company_info.inserted_at
    }
  end

  def render("company_info.json", %{company_info: company_info}) do
    {date1, {a1, b1, c1, _}} = company_info.updated_at
    updated_at = {date1, {a1, b1, c1}} |> Ecto.DateTime.from_erl
    {date2, {a2, b2, c2, _}} = company_info.inserted_at
    inserted_at = {date2, {a2, b2, c2}} |> Ecto.DateTime.from_erl
    %{
      id: company_info.id,
      abbrev_name: company_info.abbrev_name,
      company_code: company_info.company_code,
      details: company_info.details,
      full_name: company_info.full_name,
      industry: company_info.industry,
      season: company_info.season,
      source: company_info.source,
      info_updated_at: company_info.info_updated_at |> Ecto.Date.from_erl,
      year: company_info.year,
      updated_at: updated_at,
      inserted_at: inserted_at
    }
  end
end
