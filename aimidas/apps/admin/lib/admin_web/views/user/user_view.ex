defmodule AdminWeb.Accounts.UserView do
  use AdminWeb, :view
  alias AdminWeb.Accounts.UserView

  def render("index.json", %{users: page}) do
    %{
      # list: render_many(page.entries, UserView, "user.json"),
      list: render_many(page.entries, UserView, "user_single.json"), # 等完善sql后再增强
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user_single.json")}
  end

  def render("user_single.json", %{user: user}) do
    %{
      id: user.id,
      nickname: user.nickname,
      avatar: user.avatar,
      telephone: user.telephone,
      point: render_one(user.point, UserView, "user_point.json"),
      inserted_at: user.inserted_at,
      updated_at: user.updated_at
    }
  end

  def render("user.json", %{user: user}) do
    {date1, {a1, b1, c1, _}} = user.updated_at
    updated_at = {date1, {a1, b1, c1}} |> Ecto.DateTime.from_erl
    {date2, {a2, b2, c2, _}} = user.inserted_at
    inserted_at = {date2, {a2, b2, c2}} |> Ecto.DateTime.from_erl
    %{
      id: user.id,
      nickname: user.nickname,
      avatar: user.avatar,
      telephone: user.telephone,
      point: render_one(user.point, UserView, "user_point.json"),
      # point: %{
      #   point: user.point.point,
      #   quota: user.point.quota
      # },
      inserted_at: inserted_at,
      updated_at: updated_at
    }
  end

  def render("user_point.json", %{user: %Aimidas.Points.Point{} = point}) do
  # def render("user_point.json", %{user: point}) do
    # if point.__struct__ == Ecto.Association.NotLoaded do
    #   %{}
    # else
    %{
      point: point.point,
      quota: point.quota,
      current_quota_use: point.current_quota_use
    }
    # end
  end
  def render("user_point.json", %{user: point}) do
    %{}
  end

end
