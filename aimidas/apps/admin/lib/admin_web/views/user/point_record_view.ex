defmodule AdminWeb.Accounts.PointRecordView do
  use AdminWeb, :view
  alias AdminWeb.Accounts.PointRecordView

  def render("index.json", %{point_records: page}) do
    %{
      list: render_many(page.entries, PointRecordView, "point_record.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{point_record: point_record}) do
    %{data: render_one(point_record, PointRecordView, "point_record.json")}
  end

  def render("point_record.json", %{point_record: point_record}) do
    %{
      id: point_record.id,
      type: point_record.type,
      point: point_record.point,
      quota: point_record.quota,
      user_id: point_record.user_id,
      user: render_one(point_record.user, AdminWeb.Accounts.UserView, "user_single.json"),
      inserted_at: point_record.inserted_at,
      updated_at: point_record.updated_at
    }
  end
end
