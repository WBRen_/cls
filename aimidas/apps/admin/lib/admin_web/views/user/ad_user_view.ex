defmodule AdminWeb.Accounts.AdUserView do
  use AdminWeb, :view
  alias AdminWeb.Accounts.AdUserView

  def render("index.json", %{admin_users: page}) do
    %{
      list: render_many(page.entries, AdUserView, "ad_user.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{ad_user: ad_user}) do
    %{data: render_one(ad_user, AdUserView, "ad_user.json")}
  end

  def render("ad_user.json", %{ad_user: ad_user}) do
    %{
      id: ad_user.id,
      name: ad_user.name,
      # password: ad_user.password,
      level: ad_user.level,
      auths: ad_user.auths,
      inserted_at: ad_user.inserted_at,
      updated_at: ad_user.updated_at
    }
  end
end
