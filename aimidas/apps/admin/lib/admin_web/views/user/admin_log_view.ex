defmodule AdminWeb.Accounts.AdminLogView do
  use AdminWeb, :view
  alias AdminWeb.Accounts.AdminLogView

  def render("index.json", %{admin_logs: page}) do
    %{
      list: render_many(page.entries, AdminLogView, "admin_log.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{admin_log: admin_log}) do
    %{data: render_one(admin_log, AdminLogView, "admin_log.json")}
  end

  def render("admin_log.json", %{admin_log: admin_log}) do
    IO.inspect admin_log
    %{
      id: admin_log.id,
      type: admin_log.type,
      user_id: admin_log.user_id,
      user: render_one(admin_log.user, AdminWeb.Accounts.AdUserView, "ad_user.json"),
      params: admin_log.params,
      status: admin_log.status,
      inserted_at: admin_log.inserted_at,
      updated_at: admin_log.updated_at
    }
  end
end
