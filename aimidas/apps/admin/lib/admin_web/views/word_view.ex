defmodule AdminWeb.WordView do
  use AdminWeb, :view
  alias AdminWeb.WordView

  def render("index.json", %{syn_words: syn_words}) do
    %{data: render_many(syn_words, WordView, "word.json")}
  end

  def render("show.json", %{word: word}) do
    %{data: render_one(word, WordView, "word.json")}
  end

  def render("word.json", %{word: word}) do
    %{id: word.id,
      word: word.word,
      synonym_words: word.synonym_words}
  end
end
