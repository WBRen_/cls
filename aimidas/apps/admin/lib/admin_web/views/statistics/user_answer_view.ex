defmodule AdminWeb.Statistics.UserAnswerView do
  use AdminWeb, :view
  alias AdminWeb.Statistics.UserAnswerView

  def render("index.json", %{user_answers: page}) do
    %{
      # list: render_many(page.entries, UserAnswerView, "user_answer.json"),
      list: render_many(page.entries, UserAnswerView, "user_answer_single.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  # def render("index.json", %{user_answers: user_answers}) do
  #   %{data: render_many(user_answers, UserAnswerView, "user_answer.json")}
  # end

  def render("show.json", %{user_answer: user_answer}) do
    %{data: render_one(user_answer, UserAnswerView, "user_answer_single.json")}
  end

  def render("user_answer_single.json", %{user_answer: user_answer}) do
    %{
      id: user_answer.id,
      answer: user_answer.answer,
      stars: user_answer.stars,
      contras: user_answer.contras,
      status: user_answer.status,
      updated_at: user_answer.updated_at,
      inserted_at: user_answer.inserted_at,
      question_id: user_answer.question_id,
      user: user_answer.user
    }
  end

  def render("user_answer.json", %{user_answer: user_answer}) do
    {date1, {a1, b1, c1, _}} = user_answer.updated_at
    updated_at = {date1, {a1, b1, c1}} |> Ecto.DateTime.from_erl
    {date2, {a2, b2, c2, _}} = user_answer.inserted_at
    inserted_at = {date2, {a2, b2, c2}} |> Ecto.DateTime.from_erl
    %{
      id: user_answer.id,
      answer: user_answer.answer,
      stars: user_answer.stars,
      contras: user_answer.contras,
      status: user_answer.status,
      updated_at: updated_at,
      inserted_at: inserted_at
    }
  end
end
