defmodule AdminWeb.Statistics.NormalizedQuestionView do
  use AdminWeb, :view
  alias AdminWeb.Statistics.NormalizedQuestionView

  def render("index.json", %{normalized_questions: page}) do
    %{
      list: render_many(page.entries, NormalizedQuestionView, "normalized_question_with_need_apply.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  # def render("index.json", %{normalized_questions: normalized_questions}) do
  #   %{data: render_many(normalized_questions, NormalizedQuestionView, "normalized_question.json")}
  # end

  def render("show.json", %{normalized_question: normalized_question}) do
    %{data: render_one(normalized_question, NormalizedQuestionView, "normalized_question.json")}
  end

  def render("normalized_question.json", %{normalized_question: normalized_question}) do
    %{id: normalized_question.id,
      md5: normalized_question.md5,
      query: normalized_question.query,
      ask_count: normalized_question.ask_count,
      can_answer: normalized_question.can_answer,
      otpq: normalized_question.otpq}
  end

  def render("normalized_question_with_need_apply.json", %{normalized_question: normalized_question}) do
    %{id: normalized_question.id,
      md5: normalized_question.md5,
      query: normalized_question.query,
      ask_count: normalized_question.ask_count,
      otpq: normalized_question.otpq,
      can_answer: normalized_question.can_answer,
      stars: normalized_question.stars,
      contras: normalized_question.contras,
      need_apply: normalized_question.need_apply
    }
  end
end
