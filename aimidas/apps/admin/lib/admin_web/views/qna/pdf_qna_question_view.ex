defmodule AdminWeb.PdfQnaQuestionView do
  use AdminWeb, :view
  alias AdminWeb.PdfQnaQuestionView

  def render("index.json", %{pdf_qna_questions: page}) do
    %{
      # list: render_many(page.entries, PdfQnaQuestionView, "pdf_qna_question.json"), # 已经改成了非 SqlUtil 方式，所以废弃
      list: render_many(page.entries, PdfQnaQuestionView, "pdf_qna_question_single.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("list.json", %{pdf_qna_questions: pdf_qna_questions}) do
    %{
      data: render_many(pdf_qna_questions, PdfQnaQuestionView, "pdf_qna_question_single.json"),
    }
  end

  def render("show.json", %{pdf_qna_question: pdf_qna_question}) do
    %{data: render_one(pdf_qna_question, PdfQnaQuestionView, "pdf_qna_question_single.json")}
  end

  def render("pdf_qna_question_single.json", %{pdf_qna_question: qna_question}) do
    %{
      id: qna_question.id,
      md5: qna_question.md5,
      otpq: qna_question.otpq,
      answer: qna_question.answer,
      updated_at: qna_question.updated_at,
      inserted_at: qna_question.inserted_at
    }
  end

  def render("pdf_qna_question.json", %{pdf_qna_question: qna_question}) do
    {date1, {a1, b1, c1, _}} = qna_question.updated_at
    updated_at = {date1, {a1, b1, c1}} |> Ecto.DateTime.from_erl
    {date2, {a2, b2, c2, _}} = qna_question.inserted_at
    inserted_at = {date2, {a2, b2, c2}} |> Ecto.DateTime.from_erl
    %{
      id: qna_question.id,
      md5: qna_question.md5,
      otpq: qna_question.otpq,
      answer: qna_question.answer,
      updated_at: updated_at,
      inserted_at: inserted_at
    }
  end
end
