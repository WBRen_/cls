defmodule AdminWeb.PdfPageView do
  use AdminWeb, :view
  alias AdminWeb.PdfPageView

  def render("index.json", %{pdf_pages: page}) do
    %{
      list: render_many(page.entries, PdfPageView, "pdf_page.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

    def render("index_with_counts.json", %{pdf_pages: page}) do
    %{
      list: render_many(page.entries, PdfPageView, "pdf_page_with_counts.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("index_nlp.json", %{pdf_pages: pdf_pages}) do
    %{data: render_many(pdf_pages, PdfPageView, "pdf_page_nlp.json")}
  end

  def render("show.json", %{pdf_page: pdf_page}) do
    %{data: render_one(pdf_page, PdfPageView, "pdf_page.json")}
  end

  def render("pdf_page_nlp.json", %{pdf_page: pdf_page}) do
    "#{pdf_page.content}"
  end

  def render("pdf_page.json", %{pdf_page: pdf_page}) do
    %{
      id: pdf_page.id,
      content: pdf_page.content,
      start_page: pdf_page.start_page,
      end_page: pdf_page.end_page,
      image_url: pdf_page.image_url,
      company_code: pdf_page.company_code,
      year: pdf_page.year,
      src_updated_at: pdf_page.src_updated_at,
      type_code: pdf_page.type_code,
      detail_code: pdf_page.detail_code,
      inserted_at: pdf_page.inserted_at
    }
  end

    def render("pdf_page_with_counts.json", %{pdf_page: pdf_page}) do
    %{
      pages: pdf_page.pages,
      company_code: pdf_page.company_code,
      year: pdf_page.year,
      type_code: pdf_page.type_code,
      detail_code: pdf_page.detail_code,
      src_updated_at: pdf_page.src_updated_at,
      inserted_at: pdf_page.inserted_at
    }
  end
end
