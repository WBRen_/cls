defmodule AdminWeb.ProQnaQuestionView do
  use AdminWeb, :view
  alias AdminWeb.ProQnaQuestionView

  def render("index.json", %{pro_qna_questions: page}) do
    %{
      list: render_many(page.entries, ProQnaQuestionView, "pro_qna_question.json"),
      pagination: %{
        page_number: page.page_number,
        page_size: page.page_size,
        total_entries: page.total_entries,
        total_pages: page.total_pages
      }
    }
  end

  def render("show.json", %{pro_qna_question: pro_qna_question}) do
    %{data: render_one(pro_qna_question, ProQnaQuestionView, "pro_qna_question_single.json")}
  end

  def render("pro_qna_question_single.json", %{pro_qna_question: qna_question}) do
    %{
      id: qna_question.id,
      md5: qna_question.md5,
      query: qna_question.query,
      answer: qna_question.answer,
      updated_at: qna_question.updated_at,
      inserted_at: qna_question.inserted_at
    }
  end

  def render("pro_qna_question.json", %{pro_qna_question: qna_question}) do
    {date1, {a1, b1, c1, _}} = qna_question.updated_at
    updated_at = {date1, {a1, b1, c1}} |> Ecto.DateTime.from_erl
    {date2, {a2, b2, c2, _}} = qna_question.inserted_at
    inserted_at = {date2, {a2, b2, c2}} |> Ecto.DateTime.from_erl
    %{
      id: qna_question.id,
      md5: qna_question.md5,
      query: qna_question.query,
      answer: qna_question.answer,
      updated_at: updated_at,
      inserted_at: inserted_at
    }
  end
end
