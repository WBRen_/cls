defmodule AdminWeb.Router do
  use AdminWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  pipeline :api_auth do
    plug :ensure_authenticated
  end

  scope "/", AdminWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", AdminWeb do
    pipe_through [:api, :api_auth]
    get "/me", LoginController, :me
    put "/me/update", LoginController, :update
    post "/me/sign_out", LoginController, :sign_out
    get "/auths", Accounts.AdUserController, :list_auth_names
    post "/auths/:user_id/:auth_name", Accounts.AdUserController, :patch_auth
    post "/auths/renew_password", Accounts.AdUserController, :change_password

    get "/configs", ConfigController, :index
    put "/configs", ConfigController, :update
    resources "/qna/dongmis", DmQnaQuestionController, only: [:index, :show, :create, :update, :delete]
    resources "/qna/pros", ProQnaQuestionController, only: [:index, :show, :create, :update, :delete]
    resources "/qna/pdfs", PdfQnaQuestionController, only: [:index, :show, :create, :update, :delete]
    resources "/qna/pdf/pages", PdfPageController, only: [:index, :show, :create, :update, :delete]
    get "/pdfs", PdfPageController, :index_groupby_pdf # groupby company_code, etc
    resources "/company/xbrls", Ontology.CompanyXbrlController, only: [:index, :show, :create, :update, :delete]
    resources "/company/infos", Ontology.CompanyInfoController, only: [:index, :show, :create, :update, :delete]
    resources "/company/terms", Ontology.TermController, only: [:index, :show, :create, :update, :delete]
    resources "/accounts/users", Accounts.UserController, only: [:index, :show, :create, :update, :delete]
    resources "/accounts/users/:user_id/point_logs", Accounts.PointRecordController, only: [:index, :show, :create, :update, :delete]
    resources "/accounts/admins", Accounts.AdUserController, only: [:index, :show, :create, :update, :delete]
    resources "/accounts/admins_logs", Accounts.AdminLogController, only: [:index, :show, :delete]

    resources "/statistics/normalized_questions", Statistics.NormalizedQuestionController, only: [:index, :show, :create, :update, :delete]
    resources "/statistics/normalized_questions/:normalized_question_id/user_answers", Statistics.UserAnswerController, only: [:index, :show, :create, :update, :delete]
    put "/statistics/normalized_questions/:normalized_question_id/user_answers/:answer_id/apply", Statistics.UserAnswerController, :active_apply
    delete "/statistics/normalized_questions/:normalized_question_id/user_answers/:answer_id/apply", Statistics.UserAnswerController, :fail_apply
  
    resources "/synonym_words", WordController, only: [:index, :show, :create, :update, :delete]
    resources "/feedbacks", AdviceController, only: [:index, :show, :create, :update, :delete]
    resources "/managements/hot_questions", HotQuestionController, only: [:index, :show, :create, :update, :delete]
  end

  scope "/api/", AdminWeb do
    pipe_through :api
    post "/me/sign_in", LoginController, :sign_in
    # demo test, plz delete it
    post "/me/createme", Accounts.AdUserController, :create_demo_user

    # for nlp.python
    # PDF 问答接口
    post "/importer/pdfs/xlsx", Importer.PdfController, :import_xlsx_file
    post "/importer/pdfs/json", Importer.PdfController, :import_json_file
    post "/importer/pdfs/body", Importer.PdfController, :import_json
    get "/qna/pdflist", NLP.PdfQnaQuestionController, :index
    post "/qna/pdflist", NLP.PdfQnaQuestionController, :index
    # 董秘接口
    post "/importer/dms/xlsx", Importer.DmController, :import_xlsx_file
    post "/importer/dms/json", Importer.DmController, :import_json_file
    post "/importer/dms/body", Importer.DmController, :import_json
    get "/dms/dmlist", NLP.DmQnaQuestionController, :index
    post "/dms/dmlist", NLP.DmQnaQuestionController, :index
    # PDF 截图接口
    get "/qna/pdfpages", PdfPageController, :list_nlp
    post "/qna/pdfpages", PdfPageController, :list_nlp
    # 定义接口
    get "/company/termlist", NLP.TermController, :index
    post "/company/termlist", NLP.TermController, :index
    # 专家问答接口
    get "/qna/prolist", NLP.ProQnaQuestionController, :index
    post "/qna/prolist", NLP.ProQnaQuestionController, :index

    # 同义词PING
    post "/synonyms/ping", Importer.TongyiciController, :index
    # for admin import file 同义词
    post "/importer/tongyici", Importer.TongyiciController, :import

    # for executer.jar
    post "/importer/pdfs/pages", Importer.PdfPageController, :import
  end

  # 权限验证
  defp ensure_authenticated(conn, _opts) do
    current_user_id = get_session(conn, :current_user_id)

    if current_user_id do
      conn
    else
      conn |> refuse_render
    end
  end

  defp refuse_render(conn) do
    conn
    |> put_status(:unauthorized)
    |> json(%{message: "无权访问"})
    |> halt()
  end

end
