defmodule Admin.NLP.DmConnector do
  @host System.get_env("NLP_HOST") || "http://prod2.aimidas.com:18080"
  @url "#{@host}/importScretary/"

  def add(data) do
    post_data("ADD", data)
  end

  def update(data) do
    post_data("UPDATE", data)
  end

  def delete(data) do
    post_data("DELETE", data)
  end

  defp post_data(type, data_list) do
    data = data_list |> Enum.map(fn q ->
      %{
        "question" => q.question,
        "answer" => q.answer,
        "md5" => q.md5,
        "id" => q.id
      }
    end)
    # IO.inspect data

    json_string =
    %{
      "type" => type,
      "data" => data
    }
    |> Poison.encode!()
    try do
      response =
      @url
      |> HTTPotion.post([body: json_string, headers: headers()])
      if response.status_code == 200 do
        response.body |> Poison.decode!
      else
        %{error: "notification fail[#{response.status_code}] for type: #{type}, data size: #{data |> length}"}
      end
    rescue
      _ -> %{error: "notification error for type: #{type}, data size: #{data |> length}"}
    end
  end

  defp headers() do
    [
      "Content-Type": "application/json",
      "Accept": "application/json"
    ]
  end
end
