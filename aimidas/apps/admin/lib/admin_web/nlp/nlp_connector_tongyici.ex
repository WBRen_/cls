defmodule Admin.NLP.TongyiciConnector do
  # @url "http://prod2.aimidas.com:19090/importSynonym/"
  @host System.get_env("NLP_HOST") || "http://prod2.aimidas.com:18080"
  @url "#{@host}/importSynonym/"

  def post_data(data_list) do
    jx_string =
    data_list
    |> Enum.map(fn line ->
      strline = line |> Enum.map(fn x -> "'#{x}'" end) |> Enum.join(",")
      "[#{strline}]"
    end)
    |> Enum.join(",")
    # |> Poison.encode!()
    body = "\"[#{jx_string}]\"" #|> IO.puts
    # try do
    response =
    HTTPotion.post(@url, [body: body, headers: headers(), timeout: 60_000, recv_timeout: 60_000])
    # |> IO.inspect
    if 200 == response.status_code do
      # response.body #|> Poison.decode!
      %{ok: response.body}
    else
      %{error: "notification fail[#{response.status_code}] for data size: #{data_list |> length}"}
    end
    # rescue
    #   _ -> %{error: "notification error for data size: #{data_list |> length}"}
    # end
  end

  defp headers() do
    [
      "Content-Type": "application/json",
      "Accept": "application/json"
    ]
  end
end
