defmodule AdminWeb.Statistics.UserAnswerController do
  use AdminWeb, :controller

  alias Admin.Statistics
  alias Aimidas.Statistics.UserAnswer
  alias Admin.Points

  action_fallback AdminWeb.FallbackController

  def index(conn, %{"normalized_question_id" => normalized_question_id} = params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    user_answers = Statistics.page_user_answers(normalized_question_id, query_params, page)
    render(conn, "index.json", user_answers: user_answers)
  end

  def create(conn, params) do
    with {:ok, %UserAnswer{} = user_answer} <- Statistics.create_user_answer(params) do
      AdminWeb.LogHelper.log(conn, "添加小程序标准问答-用户答案", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", user_answer_path(conn, :show, user_answer))
      |> render("show.json", user_answer: user_answer)
    end
  end

  def show(conn, %{"id" => id}) do
    user_answer = Statistics.get_user_answer!(id)
    render(conn, "show.json", user_answer: user_answer)
  end

  def update(conn, %{"id" => id} = params) do
    user_answer = Statistics.get_user_answer!(id)

    with {:ok, %UserAnswer{} = user_answer} <- Statistics.update_user_answer(user_answer, params) do
      AdminWeb.LogHelper.log(conn, "修改小程序标准问答-用户答案", "success", params)
      render(conn, "show.json", user_answer: user_answer)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除用户贡献答案")
    user_answer = Statistics.get_user_answer!(id)
    with {:ok, %UserAnswer{}} <- Statistics.delete_user_answer(user_answer) do
      AdminWeb.LogHelper.log(conn, "删除小程序标准问答-用户答案", "success", params)
      send_resp(conn, :no_content, "")
    end
  end

  def active_apply(conn, %{"answer_id" => answer_id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "审核用户贡献答案")
    user_answer = Statistics.get_user_answer!(answer_id)
    # set status: 100, means apply success!
    with {:ok, %UserAnswer{} = user_answer} <- Statistics.update_user_answer(user_answer, %{status: 100}) do
      Points.add_point(user_answer.user_id, 10)
      Points.create_point_record(%{user_id: user_answer.user_id, type: "PassAnswer", quota: 0, point: Aimidas.Configer.fetch("PassAnswer")})
      AdminWeb.LogHelper.log(conn, "审核小程序标准问答-用户答案::审核通过", "success", params)
      # 通知用户
      Notification.Pusher.send(:answer_apply, user_answer.user_id, "问答审核结果", "恭喜您提供的回答审核通过！", %{question_id: user_answer.question_id, point: 10, answer: user_answer.answer})
      render(conn, "show.json", user_answer: user_answer)
    end
  end

  def fail_apply(conn, %{"answer_id" => answer_id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "审核用户贡献答案")
    user_answer = Statistics.get_user_answer!(answer_id)
    # set status: -1, means apply fail!
    with {:ok, %UserAnswer{} = user_answer} <- Statistics.update_user_answer(user_answer, %{status: -1}) do
      AdminWeb.LogHelper.log(conn, "审核小程序标准问答-用户答案::审核不通过", "success", params)
      # 通知用户
      Notification.Pusher.send(:answer_apply, user_answer.user_id, "问答审核结果", "很遗憾您提供的回答审核未通过", %{question_id: user_answer.question_id, point: 0, answer: user_answer.answer})
      render(conn, "show.json", user_answer: user_answer)
    end
  end
end
