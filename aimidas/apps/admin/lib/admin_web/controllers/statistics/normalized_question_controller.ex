defmodule AdminWeb.Statistics.NormalizedQuestionController do
  use AdminWeb, :controller

  alias Admin.Statistics
  alias Aimidas.Statistics.NormalizedQuestion

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    can_answer = params |> Map.get("can_answer", "all") # all, true, false
    status = params |> Map.get("status", "all") # all, true, false

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    normalized_questions = Statistics.page_normalized_questions(query_params, page, can_answer, status)
    render(conn, "index.json", normalized_questions: normalized_questions)
  end

  def create(conn, params) do
    with {:ok, %NormalizedQuestion{} = normalized_question} <- Statistics.create_normalized_question(params) do
      AdminWeb.LogHelper.log(conn, "添加小程序标准问答", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", normalized_question_path(conn, :show, normalized_question))
      |> render("show.json", normalized_question: normalized_question)
    end
  end

  def show(conn, %{"id" => id}) do
    normalized_question = Statistics.get_normalized_question!(id)
    render(conn, "show.json", normalized_question: normalized_question)
  end

  def update(conn, %{"id" => id} = params) do
    normalized_question = Statistics.get_normalized_question!(id)

    with {:ok, %NormalizedQuestion{} = normalized_question} <- Statistics.update_normalized_question(normalized_question, params) do
      AdminWeb.LogHelper.log(conn, "修改小程序标准问答", "success", params)
      render(conn, "show.json", normalized_question: normalized_question)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除小程序标准问答")
    normalized_question = Statistics.get_normalized_question!(id)
    with {:ok, %NormalizedQuestion{}} <- Statistics.delete_normalized_question(normalized_question) do
      AdminWeb.LogHelper.log(conn, "删除小程序标准问答", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
