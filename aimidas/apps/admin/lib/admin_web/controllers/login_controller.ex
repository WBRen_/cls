defmodule AdminWeb.LoginController do
  use AdminWeb, :controller

  alias Admin.AdminAccounts
  alias Admin.AdminAccounts.AdUser

  action_fallback AdminWeb.FallbackController

  def sign_in(conn, %{"name" => name, "password" => password}) do
    if user = AdminAccounts.get_ad_user_by_name(name) do
      if user.password == password do
        conn
        |> put_session(:current_user_id, user.id)     # 注入会话
        |> put_status(:ok)
        |> json(%{message: "success"})
      else
        conn
        |> delete_session(:current_user_id) # 如果出现登陆异常，直接删除会话
        |> put_status(400)
        |> json(%{message: "wrong password"})
      end
    else
      conn
      |> delete_session(:current_user_id) # 如果出现登陆异常，直接删除会话
      |> put_status(400)
      |> json(%{message: "login fail"})
    end
  end

  def sign_out(conn, _) do
    current_user_id = get_session(conn, :current_user_id)
    conn
    |> delete_session(:current_user_id) # 删除会话
    |> json(%{message: "注销成功"})
  end

  def me(conn, _) do
    current_user_id = get_session(conn, :current_user_id)
    if user = AdminAccounts.get_ad_user(current_user_id) do
      conn
      |> render(AdminWeb.Accounts.AdUserView, "show.json", ad_user: user)
    end
  end

  def update(conn, params) do
    # name = Map.get(params, "name", nil)
    # password = Map.get(params, "password", nil)
    current_user_id = get_session(conn, :current_user_id)
    user_params = params |> Map.delete("level") |> Map.delete("auths") # 去掉不可修改的参数
    if user = AdminAccounts.get_ad_user(current_user_id) do
      with {:ok, %AdUser{} = user} <- AdminAccounts.update_ad_user(user, user_params) do
        conn |> render(AdminWeb.Accounts.AdUserView, "show.json", ad_user: user)
      end
    end
  end

end
