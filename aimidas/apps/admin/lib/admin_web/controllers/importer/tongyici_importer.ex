defmodule AdminWeb.Importer.TongyiciController do
  use AdminWeb, :controller

  alias Admin.NLP.TongyiciConnector

  action_fallback AdminWeb.FallbackController

  @default_name "/var/synonym.csv"
  @default_path "/var/"

  def import_test(conn, %{"file" => file}) do
    data_list =
    File.stream!(file.path) 
    |> Stream.map(&String.strip/1)
    |> Stream.map(fn line ->
      line
      |> String.split(",")
      |> Enum.filter(fn x -> x != "" end)
    end)
    |> Stream.filter(fn x -> x != [] end)
    |> Enum.to_list

    jx_string =
    data_list
    |> Enum.map(fn line ->
      strline = line |> Enum.map(fn x -> "'#{x}'" end) |> Enum.join(",")
      "[#{strline}]"
    end)
    |> Enum.join(",")

    conn |> json(%{data: "#{jx_string}"})
  end

  # file: xxx.csv
  def import(conn, %{"file" => file}) do
    if file do
      # File.mkdir!(@default_path)
      File.cp(file.path, @default_name)
      result = extract_post(conn, file.path)
      case result do
        %{error: msg} -> conn |> put_status(400) |> json(%{message: "文件格式错误/服务器异常 #{msg}"})
        %{ok: _} -> conn |> put_status(200) |> json(%{message: "同义词表更新成功！"})
      end
    end

  end

  def index(conn, _) do
    result = extract_post(conn, @default_name)
    case result do
      %{error: msg} -> conn |> put_status(400) |> json(%{message: "同义词文件格式错误，请在管理员端进行更新 #{msg}"})
      %{ok: _} -> conn |> put_status(200) |> json(%{message: "同义词表通知触发成功！"})
    end
  end

  defp extract_post(conn, file_path) do
    list_data =
    File.stream!(file_path) 
    |> Stream.map(&String.strip/1)
    |> Stream.map(fn line ->
      line
      |> String.split(",")
      |> Enum.filter(fn x -> x != "" end)
    end)
    |> Stream.filter(fn x -> x != [] end)
    |> Enum.to_list
    # |> IO.inspect
    list_data |> TongyiciConnector.post_data |> IO.inspect
  end

end
