defmodule AdminWeb.Importer.PdfController do
  use AdminWeb, :controller

  alias Admin.Import.PdfQnaQuestions

  action_fallback AdminWeb.FallbackController

  def import_json(conn, %{"pdf_json" => json} = params) do
    # try do
      result =
      json
      # |> IO.inspect
      |> Enum.map(fn line ->
        # IO.inspect line
        {status, q} = line |> PdfQnaQuestions.import_pdf_qna_question # |> IO.inspect
      end)
      # added list
      add_list = result |> Enum.map(fn line ->
        {status, record} = line
        if status == :add do
          %{
            id: record.id,
            md5: record.md5,
            otpq: record.otpq,
            answer: record.answer
          }
        end
      end)
      |> Enum.filter(fn line -> line end)
      # updated list
      update_list = result |> Enum.map(fn line ->
        {status, record} = line
        if status == :update do
          %{
            id: record.id,
            md5: record.md5,
            otpq: record.otpq,
            answer: record.answer
          }
        end
      end)
      |> Enum.filter(fn line -> line end)
      # notification::
      r_add = Admin.NLP.Connector.add(add_list)
      r_update = Admin.NLP.Connector.update(update_list)
      # return message data prepare
      success_add = add_list |> length
      success_update = update_list |> length
      failure = length(result) - success_update - success_add

      conn
      |> put_status(:created)
      |> json(%{
        # message: "success, updated count: #{success_update}, inserted count: #{success_add}, fail count: #{failure} ",
        nlp_notifications: %{
          add_list: r_add,
          update_list: r_update
        },
        message: %{
          add_list: %{count: success_add},
          update_list: %{count: success_update},
          failure: %{count: failure}
        }
      })
    # rescue
    #   _ ->
    #     conn
    #     |> put_status(:bad_request)
    #     |> json(%{message: "格式异常！【body 格式必须为 json】"})
    # end
  end

  def import_json_file(conn, params) do
    if upload = params["file"] do
      try do
        result =
        upload.path
        |> File.read!()
        |> Poison.decode!()
        |> Enum.map(fn line ->
          # IO.inspect line
          {dada, _} = line |> PdfQnaQuestions.import_pdf_qna_question |> IO.inspect
          dada
        end)

        success = result |> Enum.filter(fn line -> line == :ok end) |> length
        failure = result |> Enum.filter(fn line -> line == :error end) |> length

        conn
        |> put_status(:created)
        |> json(%{message: "success, updated/inserted count: #{success}, fail count: #{failure} "})
      rescue
        _ ->
          conn
          |> put_status(:bad_request)
          |> json(%{message: "上传文件格式异常！【文件格式必须为 json】"})
      end
    else
      conn
      |> put_status(:bad_request)
      |> json(%{
        messgae: "upload fail"
      })
    end
  end

  def import_xlsx_file(conn, params) do
    # IO.inspect params
    if upload = params["file"] do
      # try do
        result =
        # upload.path
        # |> File.read!()
        # |> Poison.decode!()
        # |> PdfQnaQuestions.do_import()
        upload.path
        # "/Users/neo/Desktop/Workbook1.xlsx"
        |> Excelion.parse!(0, 2)
        |> Enum.map(fn line ->
          {dada, _} =
          %{
            "md5" => line |> at(0) |> xto_string,
            "otpq" => line |> at(1) |> xto_map,
            "answer" => line |> at(2) |> xto_string
          }
          # |> IO.inspect
          |> PdfQnaQuestions.import_pdf_qna_question()
          dada
        end)

        success = result |> Enum.filter(fn line -> line == :ok end) |> length
        failure = result |> Enum.filter(fn line -> line == :error end) |> length

        conn
        |> put_status(:created)
        |> json(%{message: "success, updated/inserted count: #{success}, fail count: #{failure} "})
      # rescue
      #   _ ->
      #     conn
      #     |> put_status(:bad_request)
      #     |> json(%{message: "上传文件格式异常！【文件格式必须为 .xlsx，且必须配表头信息，正文需在第2行开始】"})
      # end
    else
      conn
      |> put_status(:bad_request)
      |> json(%{
        messgae: "upload fail"
      })
    end
  end

  defp at(list, index) do
    {value, _} = list |> List.pop_at(index)
    value
  end

  defp xto_string(nil), do: nil
  defp xto_string(""), do: nil
  defp xto_string(str), do: str

  defp xto_map(nil), do: nil
  defp xto_map(""), do: nil
  defp xto_map(str), do: Poison.decode!(str)

end
