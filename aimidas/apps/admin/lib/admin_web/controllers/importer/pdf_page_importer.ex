defmodule AdminWeb.Importer.PdfPageController do
  use AdminWeb, :controller

  alias Admin.Qna
  alias Aimidas.Qna.PdfPage

  action_fallback AdminWeb.FallbackController

  # for executer.jar
  def import(conn, params) do
    %{"data" => data} = params
    # IO.puts data
    result =
    data
    |> Poison.decode!()
    |> Enum.map(fn page_params ->
      # if page_params.type in ["Y", "Q1", "H1", "Q3"] do
      if pdf_page = Qna.get_pdf_page(page_params) do
        with {:ok, %PdfPage{} = pdf_page} <- Qna.update_pdf_page(pdf_page, page_params) do
          true
        end
      else
        with {:ok, %PdfPage{} = pdf_page} <- Qna.create_pdf_page(page_params) do
          # IO.inspect pdf_page
          true
        end
      end
      # else
      #   with {:ok, %PdfPage{} = pdf_page} <- Qna.create_pdf_page(page_params) do
      #     # IO.inspect pdf_page
      #     true
      #   end
      # end
    end)
    |> Enum.filter(fn x -> x end)
    conn |> json(%{message: "success count: #{result |> length()}"})
  end

end
