defmodule AdminWeb.HotQuestionController do
  use AdminWeb, :controller

  alias Admin.Management
  alias Aimidas.Management.HotQuestion

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    hot_questions = Management.page_hot_questions(params, page)
    render(conn, "index.json", hot_questions: hot_questions)
  end

  def create(conn, params) do
    with {:ok, %HotQuestion{} = hot_question} <- Management.create_hot_question(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", hot_question_path(conn, :show, hot_question))
      |> render("show.json", hot_question: hot_question)
    end
  end

  def show(conn, %{"id" => id}) do
    hot_question = Management.get_hot_question!(id)
    render(conn, "show.json", hot_question: hot_question)
  end

  def update(conn, %{"id" => id} = params) do
    hot_question = Management.get_hot_question!(id)

    with {:ok, %HotQuestion{} = hot_question} <- Management.update_hot_question(hot_question, params) do
      render(conn, "show.json", hot_question: hot_question)
    end
  end

  def delete(conn, %{"id" => id}) do
    hot_question = Management.get_hot_question!(id)
    with {:ok, %HotQuestion{}} <- Management.delete_hot_question(hot_question) do
      send_resp(conn, :no_content, "")
    end
  end
end
