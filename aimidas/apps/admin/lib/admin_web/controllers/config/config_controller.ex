defmodule AdminWeb.ConfigController do
  use AdminWeb, :controller

  alias Admin.Configer
  alias Aimidas.Config.Config

  action_fallback AdminWeb.FallbackController

  def index(conn, _params) do
    configs = Configer.list_configs()
    render(conn, "index.json", configs: configs)
  end

  def update(conn, params) do
    keys = ["AddAnswer", "AskQuestion", "Like", "DisLike", "PassAnswer", "Share"]
    keys |> Enum.map(fn key ->
      value = params |> Map.get(key, 0)
      IO.puts key
      IO.puts value
      Aimidas.Configer.set(key, "#{value}")
    end)
    conn |> json(%{message: "updated!"})
  end
end
