defmodule AdminWeb.NLP.TermController do
  use AdminWeb, :controller

  alias Admin.Ontology
  alias Aimidas.Ontology.Term
  alias AdminWeb.Ontology.TermView

  action_fallback AdminWeb.FallbackController

  def index(conn, _) do
    terms = Ontology.list_terms
    render(conn, TermView, "list_all.json", terms: terms)
  end

end
