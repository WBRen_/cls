defmodule AdminWeb.NLP.PdfQnaQuestionController do
  use AdminWeb, :controller

  alias Admin.Qna
  alias Aimidas.Qa.PdfQnaQuestion
  alias AdminWeb.PdfQnaQuestionView

  action_fallback AdminWeb.FallbackController

  def index(conn, %{"timestamp" => timestamp} = params) do
    page_size = params |> Map.get("size", "10") |> String.to_integer
    time = DateTime.from_unix!(timestamp |> String.to_integer, :millisecond) # :second, :millisecond, :microsecond, :nanosecond
    pdf_qna_questions = Qna.timestamp_list_pdf(time, page_size)
    render(conn, PdfQnaQuestionView, "list.json", pdf_qna_questions: pdf_qna_questions)
  end

end
