defmodule AdminWeb.NLP.ProQnaQuestionController do
  use AdminWeb, :controller

  alias Admin.Qna
  alias Aimidas.Qa.ProQnaQuestion
  alias AdminWeb.ProQnaQuestionView

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    pro_qna_questions = Qna.pagePro(query_params, page)
    render(conn, ProQnaQuestionView, "index.json", pro_qna_questions: pro_qna_questions)
  end
end
