defmodule AdminWeb.NLP.DmQnaQuestionController do
  use AdminWeb, :controller

  alias Admin.Qna
  alias Aimidas.Qa.DmQnaQuestion
  alias AdminWeb.DmQnaQuestionView

  action_fallback AdminWeb.FallbackController

  def index(conn, %{"timestamp" => timestamp} = params) do
    page_size = params |> Map.get("size", "10") |> String.to_integer
    time = DateTime.from_unix!(timestamp |> String.to_integer, :millisecond) # :second, :millisecond, :microsecond, :nanosecond
    dm_qna_questions = Qna.timestamp_list_dm(time, page_size)
    render(conn, DmQnaQuestionView, "list.json", dm_qna_questions: dm_qna_questions)
  end

end
