defmodule AdminWeb.AdviceController do
  use AdminWeb, :controller

  alias Admin.Feedback
  alias Aimidas.Feedback.Advice

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    advices = Feedback.page_advices(params, page)
    render(conn, "index.json", advices: advices)
  end

  def create(conn, %{"advice" => advice_params}) do
    with {:ok, %Advice{} = advice} <- Feedback.create_advice(advice_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", advice_path(conn, :show, advice))
      |> render("show.json", advice: advice)
    end
  end

  def show(conn, %{"id" => id}) do
    advice = Feedback.get_advice!(id)
    render(conn, "show.json", advice: advice)
  end

  def update(conn, %{"id" => id, "advice" => advice_params}) do
    advice = Feedback.get_advice!(id)

    with {:ok, %Advice{} = advice} <- Feedback.update_advice(advice, advice_params) do
      render(conn, "show.json", advice: advice)
    end
  end

  def delete(conn, %{"id" => id}) do
    advice = Feedback.get_advice!(id)
    with {:ok, %Advice{}} <- Feedback.delete_advice(advice) do
      send_resp(conn, :no_content, "")
    end
  end
end
