defmodule AdminWeb.Accounts.PointRecordController do
  use AdminWeb, :controller

  alias Admin.Accounts
  alias Aimidas.Points.PointRecord

  action_fallback AdminWeb.FallbackController

  def index(conn, %{"user_id" => user_id} = params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    point_records = Accounts.page_point_records(user_id, query_params, page)
    render(conn, "index.json", point_records: point_records)
  end

  def create(conn, params) do
    with {:ok, %PointRecord{} = point_record} <- Accounts.create_point_record(params) do
      AdminWeb.LogHelper.log(conn, "新增用户积分记录", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", point_record_path(conn, :show, point_record))
      |> render("show.json", point_record: point_record)
    end
  end

  def show(conn, %{"id" => id}) do
    point_record = Accounts.get_point_record!(id)
    render(conn, "show.json", point_record: point_record)
  end

  def update(conn, %{"id" => id} = params) do
    point_record = Accounts.get_point_record!(id)

    with {:ok, %PointRecord{} = point_record} <- Accounts.update_point_record(point_record, params) do
      AdminWeb.LogHelper.log(conn, "修改用户积分记录", "success", params)
      render(conn, "show.json", point_record: point_record)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    point_record = Accounts.get_point_record!(id)
    with {:ok, %PointRecord{}} <- Accounts.delete_point_record(point_record) do
      AdminWeb.LogHelper.log(conn, "删除用户积分记录", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
