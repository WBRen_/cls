defmodule AdminWeb.Accounts.AdminLogController do
  use AdminWeb, :controller

  alias Admin.AdminAccounts
  alias Admin.AdminAccounts.AdminLog

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    admin_logs = AdminAccounts.page_admin_logs(query_params, page)
    render(conn, "index.json", admin_logs: admin_logs)
  end

  # def create(conn, %{"admin_log" => admin_log_params}) do
  #   with {:ok, %AdminLog{} = admin_log} <- AdminAccounts.create_admin_log(admin_log_params) do
  #     conn
  #     |> put_status(:created)
  #     |> put_resp_header("location", admin_log_path(conn, :show, admin_log))
  #     |> render("show.json", admin_log: admin_log)
  #   end
  # end

  def show(conn, %{"id" => id}) do
    admin_log = AdminAccounts.get_admin_log!(id)
    render(conn, "show.json", admin_log: admin_log)
  end

  # def update(conn, %{"id" => id, "admin_log" => admin_log_params}) do
  #   admin_log = AdminAccounts.get_admin_log!(id)

  #   with {:ok, %AdminLog{} = admin_log} <- AdminAccounts.update_admin_log(admin_log, admin_log_params) do
  #     render(conn, "show.json", admin_log: admin_log)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   admin_log = AdminAccounts.get_admin_log!(id)
  #   with {:ok, %AdminLog{}} <- AdminAccounts.delete_admin_log(admin_log) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
