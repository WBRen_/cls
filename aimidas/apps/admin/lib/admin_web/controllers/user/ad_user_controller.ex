defmodule AdminWeb.Accounts.AdUserController do
  use AdminWeb, :controller

  alias Admin.AdminAccounts
  alias Admin.AdminAccounts.AdUser

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    admin_users = AdminAccounts.page_admin_users(query_params, page)
    render(conn, "index.json", admin_users: admin_users)
  end

  def create(conn, params) do
    # 验权
    AdminWeb.AuthHelper.auth_it(conn, "ROOT")

    with {:ok, %AdUser{} = ad_user} <- AdminAccounts.create_ad_user(params) do
      AdminWeb.LogHelper.log(conn, "创建管理员", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", ad_user_path(conn, :show, ad_user))
      |> render("show.json", ad_user: ad_user)
    end
  end

  def show(conn, %{"id" => id}) do
    ad_user = AdminAccounts.get_ad_user!(id)
    render(conn, "show.json", ad_user: ad_user)
  end

  def update(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "ROOT")
    ad_user = AdminAccounts.get_ad_user!(id)
    with {:ok, %AdUser{} = ad_user} <- AdminAccounts.update_ad_user(ad_user, params) do
      AdminWeb.LogHelper.log(conn, "修改管理员", "success", params)
      render(conn, "show.json", ad_user: ad_user)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "ROOT")
    ad_user = AdminAccounts.get_ad_user!(id)
    with {:ok, %AdUser{}} <- AdminAccounts.delete_ad_user(ad_user) do
      AdminWeb.LogHelper.log(conn, "删除管理员", "success", params)
      send_resp(conn, :no_content, "")
    end
  end

  @auth_names [
    "修改用户信息",
    "删除用户",
    "增加公司XBRL信息",
    "修改公司XBRL信息",
    "删除公司XBRL信息",
    "增加公司基本信息",
    "修改公司基本信息",
    "删除公司基本信息",
    "增加公司定义信息",
    "修改公司定义信息",
    "删除公司定义信息",
    "增加董秘问答",
    "修改董秘问答",
    "删除董秘问答",
    "增加PDF问答",
    "修改PDF问答",
    "删除PDF问答",
    "删除PDF截图",
    "增加专家问答",
    "修改专家问答",
    "删除专家问答",
    "删除用户贡献答案",
    "审核用户贡献答案",
    "删除小程序标准问答"
  ]

  def list_auth_names(conn, _) do
    AdminWeb.AuthHelper.auth_it(conn, "ROOT")
    conn
    |> json(@auth_names)
  end

  def patch_auth(conn, %{"user_id" => user_id, "auth_name" => auth_name}) do
    AdminWeb.AuthHelper.auth_it(conn, "ROOT")
    auth = @auth_names |> Enum.find(auth_name)
    if auth do
      AdminWeb.AuthHelper.add_auth(user_id, auth)
      conn |> json(%{message: "授权成功"})
    else
      conn |> put_status(400) |> json(%{message: "权限不存在"})
    end
  end

  def create_demo_user(conn, _) do
    %{
      name: "aimidas",
      password: "aimidas2018",
      level: 7,
      auths: ["ROOT"]
    }
    |> Admin.AdminAccounts.create_ad_user()
    conn |> json(%{message: "create success"})
  end


  ###### 管理员自己操作
  def change_password(conn, %{"password" => password}) do
    if password do
      if current_user_id = get_session(conn, :current_user_id) do
        ad_user = AdminAccounts.get_ad_user!(current_user_id)
        params = %{password: password}
        with {:ok, %AdUser{} = ad_user} <- AdminAccounts.update_ad_user(ad_user, params) do
          AdminWeb.LogHelper.log(conn, "修改管理员【自己】密码", "success", params)
          render(conn, "show.json", ad_user: ad_user)
        end 
      end
    else
      conn |> put_status(400) |> json(%{message: "密码不能为空"})
    end
  end
end
