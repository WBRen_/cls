defmodule AdminWeb.Accounts.UserController do
  use AdminWeb, :controller

  alias Admin.Accounts
  alias Admin.Points
  alias Aimidas.Accounts.User

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    users = Accounts.page_users(query_params, page)
    render(conn, "index.json", users: users)
  end

  def create(conn, params) do
    with {:ok, %User{} = user} <- Accounts.create_user(params) do
      AdminWeb.LogHelper.log(conn, "创建用户", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", user_path(conn, :show, user))
      |> render("show.json", user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "修改用户信息")
    user = Accounts.get_user!(id)
    if params["point"] do
      # IO.puts ">>>>>>>>"
      AdminWeb.LogHelper.log(conn, "修改用户信息", "success", params)
      user.point |> Points.update_point(params["point"])
    end
    with {:ok, %User{} = user} <- Accounts.update_user(user, params) do
      render(conn, "show.json", user: Accounts.get_user!(id))
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除用户")
    user = Accounts.get_user!(id)
    with {:ok, %User{}} <- Accounts.delete_user(user) do
      AdminWeb.LogHelper.log(conn, "删除用户", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
