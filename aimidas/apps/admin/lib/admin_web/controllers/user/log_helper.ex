defmodule AdminWeb.LogHelper do
  use AdminWeb, :controller

  alias Admin.AdminAccounts

  @doc """
  AdminWeb.LogHelper.log(conn, "type", "status", params)
  """
  def log(conn, type, status, params) do
    current_user_id = get_session(conn, :current_user_id)
    %{
      user_id: current_user_id,
      type: type,
      status: status,
      params: params |> Poison.encode!()
    }
    |> AdminAccounts.create_admin_log()
  end

  def clean_log(user_id) do
    AdminAccounts.delete_all_admin_log(user_id)
  end
end
