defmodule AdminWeb.AuthHelper do
  @moduledoc """
  auth_names:
  [
    "删除其他管理员",
    "ROOT"
  ]
  """
  use AdminWeb, :controller

  alias Admin.AdminAccounts

  @doc """
  AdminWeb.AuthHelper.auth_it(conn, "删除其他管理员")
  """
  def auth_it(conn, auth_name) do
    if current_user_id = get_session(conn, :current_user_id) do
      auth_it(conn, current_user_id, auth_name)
    else
      conn |> put_status(401) |> json(%{message: "无权访问"}) |> halt()
    end
  end
  def auth_it(conn, admin_user_id, auth_name) do
    auth_names = AdminAccounts.load_auths(admin_user_id)
    if Enum.find(auth_names, fn auth ->
        auth == auth_name or auth == "ROOT" # 终极管理员可以操作一切
       end) do
      conn
    else
      conn |> put_status(401) |> json(%{message: "无权访问"}) |> halt()
    end
  end

  @doc """
  AdminWeb.AuthHelper.add_auth(1, "adad")
  """
  def add_auth(user_id, auth_name) do
    if user = AdminAccounts.get_ad_user(user_id) do
      auths = user.auths |> List.insert_at(0, auth_name) |> Enum.uniq
      {:ok, _} = user |> AdminAccounts.update_ad_user(%{auths: auths})
      :ok
    else
      :error
    end
  end

  @doc """
  AdminWeb.AuthHelper.remove_auth(1, "adad")
  """
  def remove_auth(user_id, auth_name) do
    if user = AdminAccounts.get_ad_user(user_id) do
      auths = user.auths |> Enum.uniq |> List.delete(auth_name)
      {:ok, _} = user |> AdminAccounts.update_ad_user(%{auths: auths})
      :ok
    else
      :error
    end
  end

  def delete_someone(my_id, his_id) do
    me = AdminAccounts.get_ad_user(my_id)
    him = AdminAccounts.get_ad_user(his_id)
    if me != nil and him != nil do
      if me.level > him.level do
        if Enum.find(me.auths, fn auth -> auth == "删除其他管理员" end) do
          # delete
          him |> AdminAccounts.delete_ad_user
        end
      end
    end
  end
end
