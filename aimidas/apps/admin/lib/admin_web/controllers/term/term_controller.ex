defmodule AdminWeb.Ontology.TermController do
  use AdminWeb, :controller

  alias Admin.Ontology
  alias Aimidas.Ontology.Term

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    terms = Ontology.page_terms(query_params, page)
    render(conn, "index.json", terms: terms)
  end

  def create(conn, params) do
    AdminWeb.AuthHelper.auth_it(conn, "增加公司定义信息")
    with {:ok, %Term{} = term} <- Ontology.create_term(params) do
      AdminWeb.LogHelper.log(conn, "添加公司定义", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", term_path(conn, :show, term))
      |> render("show.json", term: term)
    end
  end

  def show(conn, %{"id" => id}) do
    term = Ontology.get_term!(id)
    render(conn, "show.json", term: term)
  end

  def update(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "修改公司定义信息")
    term = Ontology.get_term!(id)

    with {:ok, %Term{} = term} <- Ontology.update_term(term, params) do
      AdminWeb.LogHelper.log(conn, "修改公司定义", "success", params)
      render(conn, "show.json", term: term)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除公司定义信息")
    term = Ontology.get_term!(id)
    with {:ok, %Term{}} <- Ontology.delete_term(term) do
      AdminWeb.LogHelper.log(conn, "删除公司定义", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
