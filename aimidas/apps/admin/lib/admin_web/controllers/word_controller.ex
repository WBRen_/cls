defmodule AdminWeb.WordController do
  use AdminWeb, :controller

  alias Admin.Synonym
  alias Admin.Synonym.Word

  action_fallback AdminWeb.FallbackController

  def index(conn, _params) do
    syn_words = Synonym.list_syn_words()
    render(conn, "index.json", syn_words: syn_words)
  end

  def create(conn, %{"word" => word_params}) do
    with {:ok, %Word{} = word} <- Synonym.create_word(word_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", word_path(conn, :show, word))
      |> render("show.json", word: word)
    end
  end

  def show(conn, %{"id" => id}) do
    word = Synonym.get_word!(id)
    render(conn, "show.json", word: word)
  end

  def update(conn, %{"id" => id, "word" => word_params}) do
    word = Synonym.get_word!(id)

    with {:ok, %Word{} = word} <- Synonym.update_word(word, word_params) do
      render(conn, "show.json", word: word)
    end
  end

  def delete(conn, %{"id" => id}) do
    word = Synonym.get_word!(id)
    with {:ok, %Word{}} <- Synonym.delete_word(word) do
      send_resp(conn, :no_content, "")
    end
  end
end
