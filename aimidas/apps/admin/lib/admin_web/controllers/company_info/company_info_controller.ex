defmodule AdminWeb.Ontology.CompanyInfoController do
  use AdminWeb, :controller

  alias Admin.Ontology
  alias Aimidas.Ontology.CompanyInfo

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    company_infos = Ontology.page_company_infos(query_params, page)
    render(conn, "index.json", company_infos: company_infos)
  end

  def create(conn, params) do
    AdminWeb.AuthHelper.auth_it(conn, "增加公司基本信息")
    with {:ok, %CompanyInfo{} = company_info} <- Ontology.create_company_info(params) do
      AdminWeb.LogHelper.log(conn, "添加公司信息", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", company_info_path(conn, :show, company_info))
      |> render("show.json", company_info: company_info)
    end
  end

  def show(conn, %{"id" => id}) do
    company_info = Ontology.get_company_info!(id)
    render(conn, "show.json", company_info: company_info)
  end

  def update(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "修改公司基本信息")
    company_info = Ontology.get_company_info!(id)
    AdminWeb.LogHelper.log(conn, "修改公司信息", "success", params)
    with {:ok, %CompanyInfo{} = company_info} <- Ontology.update_company_info(company_info, params) do
      render(conn, "show.json", company_info: company_info)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除公司基本信息")
    company_info = Ontology.get_company_info!(id)
    with {:ok, %CompanyInfo{}} <- Ontology.delete_company_info(company_info) do
      AdminWeb.LogHelper.log(conn, "删除公司信息", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
