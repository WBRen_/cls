defmodule AdminWeb.Ontology.CompanyXbrlController do
  use AdminWeb, :controller

  alias Admin.Ontology
  alias Aimidas.Ontology.CompanyXbrl

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    company_xbrls = Ontology.page_company_xbrls(query_params, page)
    render(conn, "index.json", company_xbrls: company_xbrls)
  end

  def create(conn, params) do
    AdminWeb.AuthHelper.auth_it(conn, "增加公司XBRL信息")
    with {:ok, %CompanyXbrl{} = company_xbrl} <- Ontology.create_company_xbrl(params) do
      AdminWeb.LogHelper.log(conn, "添加公司XBRL信息", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", company_xbrl_path(conn, :show, company_xbrl))
      |> render("show.json", company_xbrl: company_xbrl)
    end
  end

  def show(conn, %{"id" => id}) do
    company_xbrl = Ontology.get_company_xbrl!(id)
    render(conn, "show.json", company_xbrl: company_xbrl)
  end

  def update(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "修改公司XBRL信息")
    company_xbrl = Ontology.get_company_xbrl!(id)

    with {:ok, %CompanyXbrl{} = company_xbrl} <- Ontology.update_company_xbrl(company_xbrl, params) do
      AdminWeb.LogHelper.log(conn, "修改公司XBRL信息", "success", params)
      render(conn, "show.json", company_xbrl: company_xbrl)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除公司XBRL信息")
    company_xbrl = Ontology.get_company_xbrl!(id)
    with {:ok, %CompanyXbrl{}} <- Ontology.delete_company_xbrl(company_xbrl) do
      AdminWeb.LogHelper.log(conn, "删除公司XBRL信息", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
