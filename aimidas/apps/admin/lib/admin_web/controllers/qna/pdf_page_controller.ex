defmodule AdminWeb.PdfPageController do
  use AdminWeb, :controller

  alias Admin.Qna
  alias Aimidas.Qna.PdfPage

  action_fallback AdminWeb.FallbackController


  # for admin web, 查看pdf截图，一个pdf文件一条记录
  def index_groupby_pdf(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    pdf_pages = Qna.page_pdf_pages(page)
    render(conn, "index_with_counts.json", pdf_pages: pdf_pages)
  end

  # for normal
  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    pdf_pages = Qna.list_pdf_pages(page)
    render(conn, "index.json", pdf_pages: pdf_pages)
  end

  # for nlp
  def list_nlp(conn, %{"company_code" => company_code, "year" => year, "type_code" => type_code, "detail_code" => detail_code} = params) do
    updated_at = params |> Map.get("updated_at", nil)
    pdf_pages = if updated_at do
      Qna.list_pdf_pages(company_code, year, type_code, detail_code, updated_at)
    else
      Qna.list_pdf_pages(company_code, year, type_code, detail_code)
    end
    render(conn, "index_nlp.json", pdf_pages: pdf_pages)
  end

  def create(conn, params) do
    with {:ok, %PdfPage{} = pdf_page} <- Qna.create_pdf_page(params) do
      AdminWeb.LogHelper.log(conn, "添加PDF截图", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", pdf_page_path(conn, :show, pdf_page))
      |> render("show.json", pdf_page: pdf_page)
    end
  end

  def show(conn, %{"id" => id}) do
    pdf_page = Qna.get_pdf_page!(id)
    render(conn, "show.json", pdf_page: pdf_page)
  end

  def update(conn, %{"id" => id} = params) do
    pdf_page = Qna.get_pdf_page!(id)

    with {:ok, %PdfPage{} = pdf_page} <- Qna.update_pdf_page(pdf_page, params) do
      AdminWeb.LogHelper.log(conn, "修改PDF截图", "success", params)
      render(conn, "show.json", pdf_page: pdf_page)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除PDF截图")
    pdf_page = Qna.get_pdf_page!(id)
    with {:ok, %PdfPage{}} <- Qna.delete_pdf_page(pdf_page) do
      AdminWeb.LogHelper.log(conn, "删除PDF截图", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
