defmodule AdminWeb.ProQnaQuestionController do
  use AdminWeb, :controller

  alias Admin.Qna
  alias Aimidas.Qa.ProQnaQuestion

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    pro_qna_questions = Qna.pagePro(query_params, page)
    render(conn, "index.json", pro_qna_questions: pro_qna_questions)
  end

  def create(conn, params) do
    AdminWeb.AuthHelper.auth_it(conn, "增加专家问答")
    with {:ok, %ProQnaQuestion{} = pro_qna_question} <- Qna.create_pro_qna_question(params) do
      AdminWeb.LogHelper.log(conn, "添加专家问答", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", pro_qna_question_path(conn, :show, pro_qna_question))
      |> render("show.json", pro_qna_question: pro_qna_question)
    end
  end

  def show(conn, %{"id" => id}) do
    pro_qna_question = Qna.get_pro_qna_question!(id)
    render(conn, "show.json", pro_qna_question: pro_qna_question)
  end

  def update(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "修改专家问答")
    pro_qna_question = Qna.get_pro_qna_question!(id)

    with {:ok, %ProQnaQuestion{} = pro_qna_question} <- Qna.update_pro_qna_question(pro_qna_question, params) do
      AdminWeb.LogHelper.log(conn, "修改专家问答", "success", params)
      render(conn, "show.json", pro_qna_question: pro_qna_question)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除专家问答")
    pro_qna_question = Qna.get_pro_qna_question!(id)
    with {:ok, %ProQnaQuestion{}} <- Qna.delete_pro_qna_question(pro_qna_question) do
      AdminWeb.LogHelper.log(conn, "删除专家问答", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
