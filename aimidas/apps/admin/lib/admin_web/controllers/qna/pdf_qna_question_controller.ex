defmodule AdminWeb.PdfQnaQuestionController do
  use AdminWeb, :controller

  alias Admin.Qna
  alias Aimidas.Qa.PdfQnaQuestion

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    pdf_qna_questions = Qna.pagePdf(query_params, page)
    render(conn, "index.json", pdf_qna_questions: pdf_qna_questions)
  end

  def create(conn, params) do
    AdminWeb.AuthHelper.auth_it(conn, "增加PDF问答")
    with {:ok, %PdfQnaQuestion{} = pdf_qna_question} <- Qna.create_pdf_qna_question(params) do
      # notification nlp add
      Admin.NLP.Connector.add([pdf_qna_question])
      AdminWeb.LogHelper.log(conn, "添加PDF问答", "success", params)
      conn
      |> put_status(:created)
      |> put_resp_header("location", pdf_qna_question_path(conn, :show, pdf_qna_question))
      |> render("show.json", pdf_qna_question: pdf_qna_question)
    end
  end

  def show(conn, %{"id" => id}) do
    pdf_qna_question = Qna.get_pdf_qna_question!(id)
    render(conn, "show.json", pdf_qna_question: pdf_qna_question)
  end

  def update(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "修改PDF问答")
    pdf_qna_question = Qna.get_pdf_qna_question!(id)

    with {:ok, %PdfQnaQuestion{} = pdf_qna_question} <- Qna.update_pdf_qna_question(pdf_qna_question, params) do
      # notification nlp update
      Admin.NLP.Connector.update([pdf_qna_question])
      AdminWeb.LogHelper.log(conn, "修改PDF问答", "success", params)
      render(conn, "show.json", pdf_qna_question: pdf_qna_question)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除PDF问答")
    pdf_qna_question = Qna.get_pdf_qna_question!(id)
    with {:ok, %PdfQnaQuestion{}} <- Qna.delete_pdf_qna_question(pdf_qna_question) do
      # notification nlp delete
      Admin.NLP.Connector.delete([pdf_qna_question])
      AdminWeb.LogHelper.log(conn, "删除PDF问答", "success", params)
      send_resp(conn, :no_content, "")
    end
  end
end
