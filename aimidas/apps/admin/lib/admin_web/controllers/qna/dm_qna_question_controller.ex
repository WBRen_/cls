defmodule AdminWeb.DmQnaQuestionController do
  use AdminWeb, :controller

  alias Admin.Qna
  alias Aimidas.Qa.DmQnaQuestion

  action_fallback AdminWeb.FallbackController

  def index(conn, params) do
    search_word = params |> Map.get("search_word", nil)
    search_by = params |> Map.get("search_by", nil)
    sorter = params |> Map.get("sorter", nil)
    order = params |> Map.get("order", nil)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer

    page =
    %{
      page: page_number,
      page_size: page_size
    }
    query_params =
    %{
      search_word: search_word,
      search_by: search_by,
      sorter: sorter,
      order: order
    }
    dm_qna_questions = Qna.pageDm(query_params, page)
    render(conn, "index.json", dm_qna_questions: dm_qna_questions)
  end

  def create(conn, params) do
    AdminWeb.AuthHelper.auth_it(conn, "增加董秘问答")
    with {:ok, %DmQnaQuestion{} = dm_qna_question} <- Qna.create_dm_qna_question(params) do
      AdminWeb.LogHelper.log(conn, "添加董秘问答", "success", params)
      Admin.NLP.DmConnector.add([dm_qna_question])
      conn
      |> put_status(:created)
      |> put_resp_header("location", dm_qna_question_path(conn, :show, dm_qna_question))
      |> render("show.json", dm_qna_question: dm_qna_question)
    end
  end

  def show(conn, %{"id" => id}) do
    dm_qna_question = Qna.get_dm_qna_question!(id)
    render(conn, "show.json", dm_qna_question: dm_qna_question)
  end

  def update(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "修改董秘问答")
    dm_qna_question = Qna.get_dm_qna_question!(id)

    with {:ok, %DmQnaQuestion{} = dm_qna_question} <- Qna.update_dm_qna_question(dm_qna_question, params) do
      AdminWeb.LogHelper.log(conn, "修改董秘问答", "success", params)
      Admin.NLP.DmConnector.update([dm_qna_question])
      render(conn, "show.json", dm_qna_question: dm_qna_question)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    AdminWeb.AuthHelper.auth_it(conn, "删除董秘问答")
    dm_qna_question = Qna.get_dm_qna_question!(id)
    with {:ok, %DmQnaQuestion{}} <- Qna.delete_dm_qna_question(dm_qna_question) do
      AdminWeb.LogHelper.log(conn, "删除董秘问答", "success", params)
      Admin.NLP.DmConnector.delete([dm_qna_question])
      send_resp(conn, :no_content, "")
    end
  end
end
