defmodule Admin.AdminAccounts.AdminLog do
  use Ecto.Schema
  import Ecto.Changeset

  alias Admin.AdminAccounts.AdUser

  schema "admin_logs" do
    field :params, :string
    field :status, :string
    field :type, :string

    belongs_to :user, AdUser

    timestamps()
  end

  @doc false
  def changeset(admin_log, attrs) do
    admin_log
    |> cast(attrs, [:type, :user_id, :params, :status])
    |> validate_required([:type, :user_id, :params, :status])
  end
end
