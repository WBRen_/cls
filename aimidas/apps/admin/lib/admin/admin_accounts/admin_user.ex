defmodule Admin.AdminAccounts.AdUser do
  use Ecto.Schema
  import Ecto.Changeset


  schema "admin_users" do
    field :level, :integer
    field :name, :string
    field :password, :string
    field :auths, {:array, :string}

    timestamps()
  end

  @doc false
  def changeset(ad_user, attrs) do
    ad_user
    |> cast(attrs, [:name, :password, :level, :auths])
    |> validate_required([:name, :password, :level])
  end
end
