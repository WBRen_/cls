defmodule Admin.AdminAccounts do
  @moduledoc """
  The AdminAccounts context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Admin.AdminAccounts.AdUser
  alias Admin.AdminAccounts.AdminLog


  def page_admin_users(_params, page) do
    AdUser
    |> Repo.paginate(page)
  end
  def page_admin_logs(_params, page) do
    AdminLog
    |> preload([:user])
    |> Repo.paginate(page)
  end
  def page_admin_logs(user_id, _params, page) do
    AdminLog
    |> where(user_id: ^user_id)
    |> preload([:user])
    |> Repo.paginate(page)
  end


  @doc """
  Admin.AdminAccounts.load_auths(1)
  """
  def load_auths(user_id) do
    case Repo.get(AdUser, user_id) do
      nil -> []
      user -> user.auths
    end
  end

  @doc """
  Returns the list of admin_users.

  ## Examples

      iex> list_admin_users()
      [%AdUser{}, ...]

  """
  def list_admin_users do
    Repo.all(AdUser)
  end

  @doc """
  Gets a single ad_user.

  Raises `Ecto.NoResultsError` if the Ad user does not exist.

  ## Examples

      iex> Admin.AdminAccounts.get_ad_user!(123)
      %AdUser{}

      iex> get_ad_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_ad_user!(id), do: Repo.get!(AdUser, id)
  def get_ad_user(id), do: Repo.get(AdUser, id)
  def get_ad_user_by_name(name) do
    AdUser
    |> where(name: ^name)
    |> Repo.all
    |> List.first
  end

  @doc """
  Creates a ad_user.

  ## Examples

      iex> Admin.AdminAccounts.create_ad_user(%{
        name: "demo",
        password: "xxx",
        level: 7,
        auths: ["page", "dodo"]
      })
      {:ok, %AdUser{}}

      iex> create_ad_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_ad_user(attrs \\ %{}) do
    %AdUser{}
    |> AdUser.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a ad_user.

  ## Examples

      iex> update_ad_user(ad_user, %{field: new_value})
      {:ok, %AdUser{}}

      iex> update_ad_user(ad_user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_ad_user(%AdUser{} = ad_user, attrs) do
    ad_user
    |> AdUser.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a AdUser.

  ## Examples

      iex> delete_ad_user(ad_user)
      {:ok, %AdUser{}}

      iex> delete_ad_user(ad_user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_ad_user(%AdUser{} = ad_user) do
    Repo.delete(ad_user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking ad_user changes.

  ## Examples

      iex> change_ad_user(ad_user)
      %Ecto.Changeset{source: %AdUser{}}

  """
  def change_ad_user(%AdUser{} = ad_user) do
    AdUser.changeset(ad_user, %{})
  end

  @doc """
  Returns the list of admin_logs.

  ## Examples

      iex> list_admin_logs()
      [%AdminLog{}, ...]

  """
  def list_admin_logs do
    Repo.all(AdminLog)
  end

  @doc """
  Gets a single admin_log.

  Raises `Ecto.NoResultsError` if the Admin log does not exist.

  ## Examples

      iex> get_admin_log!(123)
      %AdminLog{}

      iex> get_admin_log!(456)
      ** (Ecto.NoResultsError)

  """
  def get_admin_log!(id), do: Repo.get!(AdminLog, id)

  @doc """
  Creates a admin_log.

  ## Examples

      iex> create_admin_log(%{field: value})
      {:ok, %AdminLog{}}

      iex> create_admin_log(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_admin_log(attrs \\ %{}) do
    %AdminLog{}
    |> AdminLog.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a admin_log.

  ## Examples

      iex> update_admin_log(admin_log, %{field: new_value})
      {:ok, %AdminLog{}}

      iex> update_admin_log(admin_log, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_admin_log(%AdminLog{} = admin_log, attrs) do
    admin_log
    |> AdminLog.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a AdminLog.

  ## Examples

      iex> delete_admin_log(admin_log)
      {:ok, %AdminLog{}}

      iex> delete_admin_log(admin_log)
      {:error, %Ecto.Changeset{}}

  """
  def delete_admin_log(%AdminLog{} = admin_log) do
    Repo.delete(admin_log)
  end

  def delete_all_admin_log(user_id) do
    AdminLog
    |> where(user_id: ^user_id)
    |> Repo.delete_all
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking admin_log changes.

  ## Examples

      iex> change_admin_log(admin_log)
      %Ecto.Changeset{source: %AdminLog{}}

  """
  def change_admin_log(%AdminLog{} = admin_log) do
    AdminLog.changeset(admin_log, %{})
  end
end
