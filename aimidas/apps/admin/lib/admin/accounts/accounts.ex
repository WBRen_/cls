defmodule Admin.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Accounts.User
  alias Aimidas.Points.PointRecord

  def page_users(_params, page) do
    User
    |> preload([:point])
    |> Repo.paginate(page)
  end
  @doc """
  Admin.Accounts.page_point_records(1, nil, %{page_number: 0, page_size: 10})
  """
  def page_point_records(user_id, _params, page) do
    PointRecord
    |> where(user_id: ^user_id)
    |> preload([:user])
    # |> SqlCreater.page_data("point_records", ["type"], params, page)
    |> Repo.paginate(page)
  end

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id) do
    User
    |> preload([:point])
    |> Repo.get!(id)
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end


  @doc """
  Returns the list of point_records.

  ## Examples

      iex> list_point_records()
      [%PointRecord{}, ...]

  """
  def list_point_records do
    Repo.all(PointRecord)
  end

  @doc """
  Gets a single point_record.

  Raises `Ecto.NoResultsError` if the Point record does not exist.

  ## Examples

      iex> get_point_record!(123)
      %PointRecord{}

      iex> get_point_record!(456)
      ** (Ecto.NoResultsError)

  """
  def get_point_record!(id), do: Repo.get!(PointRecord, id)

  @doc """
  Creates a point_record.

  ## Examples

      iex> create_point_record(%{field: value})
      {:ok, %PointRecord{}}

      iex> create_point_record(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_point_record(attrs \\ %{}) do
    %PointRecord{}
    |> PointRecord.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a point_record.

  ## Examples

      iex> update_point_record(point_record, %{field: new_value})
      {:ok, %PointRecord{}}

      iex> update_point_record(point_record, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_point_record(%PointRecord{} = point_record, attrs) do
    point_record
    |> PointRecord.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PointRecord.

  ## Examples

      iex> delete_point_record(point_record)
      {:ok, %PointRecord{}}

      iex> delete_point_record(point_record)
      {:error, %Ecto.Changeset{}}

  """
  def delete_point_record(%PointRecord{} = point_record) do
    Repo.delete(point_record)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking point_record changes.

  ## Examples

      iex> change_point_record(point_record)
      %Ecto.Changeset{source: %PointRecord{}}

  """
  def change_point_record(%PointRecord{} = point_record) do
    PointRecord.changeset(point_record, %{})
  end
end
