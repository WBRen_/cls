defmodule Admin.Qna do
  @moduledoc """
  The Qna context.
  """

  import Ecto.Query, warn: false
  # alias Admin.Repo
  alias Aimidas.Repo

  alias Aimidas.Qa.DmQnaQuestion
  alias Aimidas.Qa.ProQnaQuestion
  alias Aimidas.Qa.PdfQnaQuestion
  alias Admin.Utils.SqlCreater
  alias Aimidas.Qna.PdfPage

  ############ ==== ##############

  @doc """
  Admin.Qna.pageDm(%{search_word: "a", search_by: nil, sorter: nil, order: nil}, %{page: 1, page_size: 10})
  """
  def pageDm(params, page) do
    DmQnaQuestion
    |> SqlCreater.page_data("dm_qna_questions", ["query", "answer"], params, page)
  end

  def pagePro(params, page) do
    ProQnaQuestion
    |> SqlCreater.page_data("pro_qna_questions", ["query", "answer"], params, page)
  end

  def pagePdf(params, page) do
    if word = Map.get(params, :search_word, nil) do
      # up_word = "%#{word |> String.upcase}%"
      up_word = "%#{word}%"
      # IO.puts word
      PdfQnaQuestion |> where([p], like(p.answer, ^up_word)) |> Repo.paginate(page)
    else
      # IO.inspect params
      PdfQnaQuestion |> Repo.paginate(page)
    end
    # PdfQnaQuestion
    # |> where(like())
    # |> SqlCreater.page_data("pdf_qna_questions", ["answer"], params, page)
  end
  @doc """
  Admin.Qna.timestamp_list_pdf()
  """
  def timestamp_list_pdf(time, size) do
    PdfQnaQuestion
    |> where([p], p.inserted_at > ^time)
    |> order_by([asc: :inserted_at])
    |> limit(^size)
    |> Repo.all
  end

  @doc """
  Admin.Qna.timestamp_list_dm()
  """
  def timestamp_list_dm(time, size) do
    DmQnaQuestion
    |> where([p], p.inserted_at > ^time)
    |> order_by([asc: :inserted_at])
    |> limit(^size)
    |> Repo.all
  end
  @doc """
  Returns the list of pdf_qna_questions.

  ## Examples

      iex> list_pdf_qna_questions()
      [%PdfQnaQuestion{}, ...]

  """
  def list_pdf_qna_questions do
    Repo.all(PdfQnaQuestion)
  end

  @doc """
  Gets a single pdf_qna_question.

  Raises `Ecto.NoResultsError` if the Pdf qna question does not exist.

  ## Examples

      iex> get_pdf_qna_question!(123)
      %PdfQnaQuestion{}

      iex> get_pdf_qna_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pdf_qna_question!(id), do: Repo.get!(PdfQnaQuestion, id)

  @doc """
  Creates a pdf_qna_question.

  ## Examples

      iex> create_pdf_qna_question(%{field: value})
      {:ok, %PdfQnaQuestion{}}

      iex> create_pdf_qna_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pdf_qna_question(attrs \\ %{}) do
    %PdfQnaQuestion{}
    |> PdfQnaQuestion.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pdf_qna_question.

  ## Examples

      iex> update_pdf_qna_question(pdf_qna_question, %{field: new_value})
      {:ok, %PdfQnaQuestion{}}

      iex> update_pdf_qna_question(pdf_qna_question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pdf_qna_question(%PdfQnaQuestion{} = pdf_qna_question, attrs) do
    pdf_qna_question
    |> PdfQnaQuestion.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PdfQnaQuestion.

  ## Examples

      iex> delete_pdf_qna_question(pdf_qna_question)
      {:ok, %PdfQnaQuestion{}}

      iex> delete_pdf_qna_question(pdf_qna_question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pdf_qna_question(%PdfQnaQuestion{} = pdf_qna_question) do
    Repo.delete(pdf_qna_question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pdf_qna_question changes.

  ## Examples

      iex> change_pdf_qna_question(pdf_qna_question)
      %Ecto.Changeset{source: %PdfQnaQuestion{}}

  """
  def change_pdf_qna_question(%PdfQnaQuestion{} = pdf_qna_question) do
    PdfQnaQuestion.changeset(pdf_qna_question, %{})
  end

  @doc """
  Returns the list of pro_qna_questions.

  ## Examples

      iex> list_pro_qna_questions()
      [%ProQnaQuestion{}, ...]

  """
  def list_pro_qna_questions do
    Repo.all(ProQnaQuestion)
  end

  @doc """
  Gets a single pro_qna_question.

  Raises `Ecto.NoResultsError` if the Pro qna question does not exist.

  ## Examples

      iex> get_pro_qna_question!(123)
      %ProQnaQuestion{}

      iex> get_pro_qna_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pro_qna_question!(id), do: Repo.get!(ProQnaQuestion, id)

  @doc """
  Creates a pro_qna_question.

  ## Examples

      iex> create_pro_qna_question(%{field: value})
      {:ok, %ProQnaQuestion{}}

      iex> create_pro_qna_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pro_qna_question(attrs \\ %{}) do
    %ProQnaQuestion{}
    |> ProQnaQuestion.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pro_qna_question.

  ## Examples

      iex> update_pro_qna_question(pro_qna_question, %{field: new_value})
      {:ok, %ProQnaQuestion{}}

      iex> update_pro_qna_question(pro_qna_question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pro_qna_question(%ProQnaQuestion{} = pro_qna_question, attrs) do
    pro_qna_question
    |> ProQnaQuestion.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ProQnaQuestion.

  ## Examples

      iex> delete_pro_qna_question(pro_qna_question)
      {:ok, %ProQnaQuestion{}}

      iex> delete_pro_qna_question(pro_qna_question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pro_qna_question(%ProQnaQuestion{} = pro_qna_question) do
    Repo.delete(pro_qna_question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pro_qna_question changes.

  ## Examples

      iex> change_pro_qna_question(pro_qna_question)
      %Ecto.Changeset{source: %ProQnaQuestion{}}

  """
  def change_pro_qna_question(%ProQnaQuestion{} = pro_qna_question) do
    ProQnaQuestion.changeset(pro_qna_question, %{})
  end

  @doc """
  Returns the list of dm_qna_questions.

  ## Examples

      iex> list_dm_qna_questions()
      [%DmQnaQuestion{}, ...]

  """
  def list_dm_qna_questions do
    Repo.all(DmQnaQuestion)
  end

  @doc """
  Gets a single dm_qna_question.

  Raises `Ecto.NoResultsError` if the Dm qna question does not exist.

  ## Examples

      iex> get_dm_qna_question!(123)
      %DmQnaQuestion{}

      iex> get_dm_qna_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_dm_qna_question!(id), do: Repo.get!(DmQnaQuestion, id)

  @doc """
  Creates a dm_qna_question.

  ## Examples

      iex> create_dm_qna_question(%{field: value})
      {:ok, %DmQnaQuestion{}}

      iex> create_dm_qna_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_dm_qna_question(attrs \\ %{}) do
    %DmQnaQuestion{}
    |> DmQnaQuestion.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a dm_qna_question.

  ## Examples

      iex> update_dm_qna_question(dm_qna_question, %{field: new_value})
      {:ok, %DmQnaQuestion{}}

      iex> update_dm_qna_question(dm_qna_question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_dm_qna_question(%DmQnaQuestion{} = dm_qna_question, attrs) do
    dm_qna_question
    |> DmQnaQuestion.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a DmQnaQuestion.

  ## Examples

      iex> delete_dm_qna_question(dm_qna_question)
      {:ok, %DmQnaQuestion{}}

      iex> delete_dm_qna_question(dm_qna_question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_dm_qna_question(%DmQnaQuestion{} = dm_qna_question) do
    Repo.delete(dm_qna_question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking dm_qna_question changes.

  ## Examples

      iex> change_dm_qna_question(dm_qna_question)
      %Ecto.Changeset{source: %DmQnaQuestion{}}

  """
  def change_dm_qna_question(%DmQnaQuestion{} = dm_qna_question) do
    DmQnaQuestion.changeset(dm_qna_question, %{})
  end

  @doc """
  Admin.Qna.page_pdf_pages("000001", 2018, "M", "X", %{page_number: 1, page_size: 10})
  合并同一个pdf，若页码不同，其余信息一致，视为同一个pdf
  """
  def page_pdf_pages(page) do
    query = from(p in PdfPage,
            group_by: [p.company_code, p.year, p.type_code, p.detail_code],
            select: {p.company_code, p.year, p.type_code, p.detail_code, max(p.inserted_at), max(p.src_updated_at), count(p.id)})
    page_ = query |> Repo.paginate(page)
    %{
      page_number: page_.page_number,
      page_size: page_.page_size,
      total_entries: page_.total_entries,
      total_pages: page_.total_pages,
      entries: Enum.map(page_.entries, fn q ->
        { company_code, year, type_code, detail_code, inserted_at, src_updated_at, pages } = q
        %{
          company_code: company_code,
          year: year,
          type_code: type_code,
          detail_code: detail_code,
          inserted_at: inserted_at,
          src_updated_at: src_updated_at,
          pages: pages
        }
      end)
    }
  end
  def list_pdf_pages(company_code, year, type_code, detail_code) do
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    |> order_by([asc: :start_page])
    |> Repo.all
  end
  def list_pdf_pages(company_code, year, type_code, detail_code, updated_at) do
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    |> where(src_updated_at: ^updated_at)
    |> order_by([asc: :start_page])
    |> Repo.all
  end
  def list_pdf_pages do
    Repo.all(PdfPage)
  end
  def list_pdf_pages(page) do
    PdfPage |> Repo.paginate(page)
  end

  @doc """
  Gets a single pdf_page.

  Raises `Ecto.NoResultsError` if the Pdf page does not exist.

  ## Examples

      iex> get_pdf_page!(123)
      %PdfPage{}

      iex> get_pdf_page!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pdf_page!(id), do: Repo.get!(PdfPage, id)

  @doc """
  Creates a pdf_page.

  ## Examples

      iex> create_pdf_page(%{field: value})
      {:ok, %PdfPage{}}

      iex> create_pdf_page(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pdf_page(attrs \\ %{}) do
    %PdfPage{}
    |> PdfPage.changeset(attrs)
    |> Repo.insert()
  end

  def get_pdf_page(params) do
  # params |> IO.inspect
    %{
      "company_code" => company_code,
      "start_page" => start_page,
      "end_page" => end_page,
      "year" => year,
      "type_code" => type_code,
      "detail_code" => detail_code,
      "src_updated_at" => src_updated_at
    } = params
    # %{
    #   company_code: company_code,
    #   start_page: start_page,
    #   end_page: end_page,
    #   year: year,
    #   type_code: type_code,
    #   detail_code: detail_code
    # } = params
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    |> where(src_updated_at: ^src_updated_at)
    |> where(start_page: ^start_page)
    |> where(end_page: ^end_page)
    |> Repo.all
    |> List.first
  end

  @doc """
  Updates a pdf_page.

  ## Examples

      iex> update_pdf_page(pdf_page, %{field: new_value})
      {:ok, %PdfPage{}}

      iex> update_pdf_page(pdf_page, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pdf_page(%PdfPage{} = pdf_page, attrs) do
    pdf_page
    |> PdfPage.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PdfPage.

  ## Examples

      iex> delete_pdf_page(pdf_page)
      {:ok, %PdfPage{}}

      iex> delete_pdf_page(pdf_page)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pdf_page(%PdfPage{} = pdf_page) do
    Repo.delete(pdf_page)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pdf_page changes.

  ## Examples

      iex> change_pdf_page(pdf_page)
      %Ecto.Changeset{source: %PdfPage{}}

  """
  def change_pdf_page(%PdfPage{} = pdf_page) do
    PdfPage.changeset(pdf_page, %{})
  end
end
