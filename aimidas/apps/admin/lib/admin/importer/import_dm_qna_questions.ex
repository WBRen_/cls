defmodule Admin.Import.DmQnaQuestions do
  alias Aimidas.Qa

  def do_import(questions) when is_list(questions) do
    questions
    |> Enum.each(&import_dm_qna_question/1)
  end

  def import_dm_qna_question(question_raw) do
    question_raw["md5"]
    |> Qa.find_dm_qna_question
    |> get_question()
    |> import_dm_qna_question(question_raw)
  end

  def import_dm_qna_question(nil, %{"md5" => md5, "question" => question, "answer" => answer}) do
    case Qa.create_dm_qna_question(%{md5: md5, query: question, answer: answer}) do
      {:ok, q} -> {:add, q}
      _ -> {:error, "insert error"}
    end
  end

  def import_dm_qna_question(question, %{"question" => query, "answer" => answer} = _p) do
    case question |> Qa.update_dm_qna_question(%{query: query, answer: answer}) do
      {:ok, q} -> {:update, q}
      _ -> {:error, "update error"}
    end
  end

  def delete_dm_qna_questions(md5s) do
    md5s
    |> Qa.delete_dm_qna_questions()
  end

  defp get_question({:error, _}), do: nil
  defp get_question({:ok, question}), do: question
  defp to_map(nil), do: nil
  defp to_map(""), do: nil
  defp to_map(str) when is_map(str), do: str
  defp to_map(str) when is_list(str), do: str
  defp to_map(str), do: Poison.decode!(str)
end
