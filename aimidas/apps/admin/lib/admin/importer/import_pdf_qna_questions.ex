defmodule Admin.Import.PdfQnaQuestions do
  alias Aimidas.Qa

  def do_import(questions) when is_list(questions) do
    questions
    |> Enum.each(&import_pdf_qna_question/1)
  end

  def import_pdf_qna_question(question_raw) do
    question_raw["md5"]
    |> Qa.find_pdf_qna_question
    |> get_question()
    |> import_pdf_qna_question(question_raw)
  end

  def import_pdf_qna_question(nil, %{"md5" => md5, "otpq" => otpq, "answer" => answer}) do
    case Qa.create_pdf_qna_question(%{md5: md5, otpq: otpq |> to_map, answer: answer}) do
      {:ok, q} -> {:add, q}
      _ -> {:error, "insert error"}
    end
  end

  def import_pdf_qna_question(question, %{"otpq" => otpq, "answer" => answer}) do
    case question |> Qa.update_pdf_qna_question(%{otpq: otpq |> to_map, answer: answer}) do
      {:ok, q} -> {:update, q}
      _ -> {:error, "insert error"}
    end
  end

  def delete_pdf_qna_questions(md5s) do
    md5s
    |> Qa.delete_pdf_qna_questions()
  end

  defp get_question({:error, _}), do: nil
  defp get_question({:ok, question}), do: question
  defp to_map(nil), do: nil
  defp to_map(""), do: nil
  defp to_map(str) when is_map(str), do: str
  defp to_map(str) when is_list(str), do: str
  defp to_map(str), do: Poison.decode!(str)
end
