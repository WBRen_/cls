defmodule Admin.Utils.SqlCreater do
  alias Aimidas.Repo

  @doc """
    Admin.Ontology.page_company_infos(%{search_word: "a", search_by: nil, sorter: nil, order: nil}, %{page: 1, page_size: 10})
    Admin.Ontology.page_company_xbrls(%{search_word: "6", search_by: nil, sorter: nil, order: nil}, %{page: 1, page_size: 10})

    Admin.Utils.SqlCreater.page_data(Aimidas.Qa.DmQnaQuestion, "dm_qna_questions", ["query", "answer"], %{search_word: "a", search_by: nil, sorter: nil, order: nil}, %{page: 1, page_size: 10})
    table_name = "dm_qna_questions"
    table_cols = ["query", "answer"]
    Admin.Utils.SqlCreater.page_data(Aimidas.Qa.DmQnaQuestion, table_name, table_cols, %{search_word: "a", search_by: nil, sorter: nil, order: nil}, %{page: 0, page_size: 10})
    Admin.Utils.SqlCreater.page_data(Aimidas.Qa.DmQnaQuestion, table_name, table_cols, %{search_word: "a", search_by: "all", sorter: nil, order: nil}, %{page: 1, page_size: 10})
    Admin.Utils.SqlCreater.page_data(Aimidas.Qa.DmQnaQuestion, table_name, table_cols, %{search_word: "a", search_by: "query", sorter: "query", order: "descend"}, %{page: 1, page_size: 10})
  """
  def page_data(module_name, table_name, table_cols_string, %{search_word: word, search_by: search_by, sorter: sorter, order: order} = params, %{page: page_number_, page_size: page_size} = page) do
    page_data(module_name, table_name, table_cols_string, [], %{search_word: word, search_by: search_by, sorter: sorter, order: order} = params, %{page: page_number_, page_size: page_size} = page)
  end
  def page_data(module_name, table_name, table_cols_string, table_cols_integer, %{search_word: word, search_by: search_by, sorter: sorter, order: order} = params, %{page: page_number_, page_size: page_size} = page) do
    order_ = case order do
      "descend" -> "DESC"
      "ascend" -> "ASC"
      _ -> "ASC"

    end
    order_by_ = if sorter do
      "ORDER BY #{sorter} #{order_}"
    else
      "ORDER BY q.updated_at ASC"
    end

    like_statement_ = if word do
      convert_like_statements(search_by, table_cols_string, table_cols_integer, word)
    end

    limit_size = page_size
    page_number = if page_number_ < 1 do 1 else page_number_ end
    limit_start = (page_number - 1) * limit_size
    query_statement = "SELECT * FROM #{table_name} q #{like_statement_} #{order_by_} LIMIT #{limit_size} OFFSET #{limit_start}"

    query_result = Ecto.Adapters.SQL.query!(Repo, query_statement, []) # a
    model_cols = Enum.map query_result.columns, &(String.to_atom(&1)) # b
    entries = Enum.map query_result.rows, fn(row) ->
      # struct(module_name, Enum.zip(model_cols, row |> convert_record)) # c
      struct(module_name, Enum.zip(model_cols, row)) # c
    end
    # |> Enum.map(fn record ->
    #   record |> convert_record()
    # end)

    count_statement = "SELECT COUNT(*) AS counts FROM #{table_name} q #{like_statement_}"
    count_result = Ecto.Adapters.SQL.query!(Repo, count_statement, []) # a
    {total_entries, _} = count_result.rows |> List.first |> List.pop_at(0)


    %{
      page_number: page_number,
      page_size: page_size,
      total_entries: total_entries,
      total_pages: total_pages(total_entries, page_size),
      entries: entries
    }
  end

  def convert_like_statements(_, [], _), do: nil
  def convert_like_statements("all", cols, int_cols, word) do
    upcase_word = word |> word_up()
    statement_list_1 =
    cols
    |> Enum.map(fn col ->
      "UPPER(q.#{col}) LIKE '#{upcase_word}'"
    end)

    statement_list_2 =
    int_cols
    |> Enum.map(fn col ->
      create_like_sentence_for_int_col(col, word)
    end)

    statement =
    [statement_list_1 | statement_list_2]
    |> List.flatten
    |> Enum.filter(fn line -> line end)
    # |> IO.inspect
    |> Enum.join(" OR ")
    "WHERE #{statement}"
  end
  def convert_like_statements(col, cols, int_cols, word) do
    upcase_word = word |> word_up()
    contains_col_1 =
    cols
    |> Enum.filter(fn col_ ->
      col_ == col
    end)
    |> length()

    contains_col_2 =
    int_cols
    |> Enum.filter(fn col_ ->
      col_ == col
    end)
    |> length()

    if contains_col_1 > 0 do
      # 满足条件，即代表该字段为真实字段
      "WHERE UPPER(q.#{col}) LIKE '#{upcase_word}'"
    else
      if contains_col_2 > 0 do
        # 满足条件，即代表该字段为真实字段
        create_like_sentence_for_int_col(col, word)
      else
        convert_like_statements("all", cols, int_cols, word)
      end
    end
  end

  defp create_like_sentence_for_int_col(col, word) when is_integer(word), do: "WHERE q.#{col} = #{word}"
  defp create_like_sentence_for_int_col(col, word), do: nil
  # defp create_like_sentence(col, word) when is_float(word), do: "WHERE q.#{col} = '#{word}'"
  defp word_up(word) when is_integer(word), do: "%#{word}%"
  defp word_up(word) when is_binary(word) do
    word_r = word |> String.replace("'", "") |> String.replace("\"", "") |> String.upcase
    "%#{word_r}%"
  end
  defp word_up(word), do: "%#{word |> String.upcase}%"
  # defp word_up(word) when is_float(word), do: "%#{word}%"

  defp total_pages(total_entries, page_size) do
    IO.inspect({total_entries, page_size})
    (total_entries / page_size) |> Float.ceil |> round
  end

  def convert_record(record) do
    record
    |> convert_record_date("src_updated_at")
    |> convert_record_datetime("inserted_at")
    |> convert_record_datetime("updated_at")
  end
  defp convert_record_date(record, col) do
    if Map.has_key?(record, col) do
      src_updated_at = Map.fetch!(record, col) |> Ecto.Date.from_erl
      record |> Map.replace!(col, src_updated_at)
    else
      record
    end
  end
  defp convert_record_datetime(record, col) do
    if Map.has_key?(record, col) do
      {date, {a, b, c, d}} = Map.fetch!(record, col)
      datetime = {date, {a, b, c}} |> Ecto.DateTime.from_erl
      record |> Map.replace!(col, datetime)
    else
      record
    end
  end

end
