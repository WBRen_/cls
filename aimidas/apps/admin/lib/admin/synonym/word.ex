defmodule Admin.Synonym.Word do
  use Ecto.Schema
  import Ecto.Changeset


  schema "syn_words" do
    field :synonym_words, :string
    field :word, :string

    timestamps()
  end

  @doc false
  def changeset(word, attrs) do
    word
    |> cast(attrs, [:word, :synonym_words])
    |> validate_required([:word, :synonym_words])
  end
end
