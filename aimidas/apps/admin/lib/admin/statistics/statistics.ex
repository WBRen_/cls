defmodule Admin.Statistics do
  @moduledoc """
  The Statistics context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Statistics.UserAnswer
  alias Aimidas.Statistics.NormalizedQuestion


  @doc """
  Admin.Statistics.page_normalized_questions(%{search_word: nil}, %{page_number: 1, page_size: 2}, "all", "all")
  有 3X3 种筛选：
  1. can_answer: true, false, all
  2. status: true, false, all
  所以：
  [all, all]
  [all, true]
  [all, false]
  [true, all]
  [true, true]
  [true, false]
  [false, all]
  [false, true]
  [false, false]
  """
  def page_normalized_questions(%{search_word: word} = _params, page, "all", "all") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end
  def page_normalized_questions(%{search_word: word} = _params, page, "all", "true") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: not is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: not is_nil(u.status),
      group_by: [q.id, u.status],
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end
  def page_normalized_questions(%{search_word: word} = _params, page, "all", "false") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end
  def page_normalized_questions(%{search_word: word} = _params, page, "true", "all") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == true,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == true,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end
  def page_normalized_questions(%{search_word: word} = _params, page, "true", "true") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == true,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: not is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == true,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: not is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end
  def page_normalized_questions(%{search_word: word} = _params, page, "true", "false") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == true,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == true,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end
  def page_normalized_questions(%{search_word: word} = _params, page, "false", "all") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == false,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == false,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end
  def page_normalized_questions(%{search_word: word} = _params, page, "false", "true") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == false,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: not is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == false,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: not is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end
  def page_normalized_questions(%{search_word: word} = _params, page, "false", "false") do
    query = if word do
      word_up = "%#{word}%" |> String.upcase
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == false,
      where: like(fragment("upper(?)", q.query), ^word_up),
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    else
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == false,
      left_join: u in UserAnswer, on: (u.question_id == q.id and u.status != -1 and u.status < 100),
      where: is_nil(u.status),
      group_by: [q.id, u.status],
      select: {q, u.status, fragment("SUM(?) as stars", u.stars), fragment("SUM(?) as contras", u.contras)},
      order_by: [desc: q.ask_count]
    end
    # query |> Repo.paginate(page)
    query |> Repo.paginate(page) |> page_it()
  end

  defp page_it(page_) do
    %{
      entries: page_.entries |> actived_filter,
      page_number: page_.page_number,
      page_size: page_.page_size,
      total_entries: page_.total_entries,
      total_pages: page_.total_pages
    }
  end
  # 过滤问题是否有答案需要审核
  defp actived_filter([]), do: []
  defp actived_filter(normalized_questions) do
    normalized_questions
    |> Enum.map(fn entrie ->
      # entrie question should like this: {{question}, u.status}
      {question, status, stars, contras} = entrie
      %{
        id: question.id,
        md5: question.md5,
        query: question.query,
        ask_count: question.ask_count,
        otpq: question.otpq,
        can_answer: question.can_answer,
        # answer: question.answer, # can not load this
        inserted_at: question.inserted_at,
        updated_at: question.updated_at,
        stars: if stars == nil do 0 else stars end,
        contras: if contras == nil do 0 else contras end,
        need_apply: status != nil
      }
    end)
  end
  # 该答案组是否需要审核
  # defp need_active_apply([]), do: false
  # defp need_active_apply(user_answers) do
  #   not_applied_answer_count =
  #   user_answers
  #   |> Enum.filter(fn answer ->
  #     # status > 99 / status == -1 代表审核通过/不通过，都表示已经审核了的答案
  #     answer.status < 100 and answer.status == -1
  #   end)
  #   length(not_applied_answer_count) > 0
  # end
  def page_user_answers(normalized_question_id, _params, page) do
    UserAnswer
    |> where(question_id: ^normalized_question_id)
    |> preload([:user])
    |> order_by([desc: :id])
    |> Repo.paginate(page)
  end

  @doc """
  Returns the list of user_answers.

  ## Examples

      iex> list_user_answers()
      [%UserAnswer{}, ...]

  """
  def list_user_answers do
    Repo.all(UserAnswer)
  end

  @doc """
  Gets a single user_answer.

  Raises `Ecto.NoResultsError` if the User answer does not exist.

  ## Examples

      iex> get_user_answer!(123)
      %UserAnswer{}

      iex> get_user_answer!(456)
      ** (Ecto.NoResultsError)

  """
  # def get_user_answer!(id), do: Repo.get!(UserAnswer, id)
  def get_user_answer!(id) do
    UserAnswer
    |> preload([:user])
    |> Repo.get!(id)
  end

  @doc """
  Creates a user_answer.

  ## Examples

      iex> create_user_answer(%{field: value})
      {:ok, %UserAnswer{}}

      iex> create_user_answer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_answer(attrs \\ %{}) do
    %UserAnswer{}
    |> UserAnswer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user_answer.

  ## Examples

      iex> update_user_answer(user_answer, %{field: new_value})
      {:ok, %UserAnswer{}}

      iex> update_user_answer(user_answer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_answer(%UserAnswer{} = user_answer, attrs) do
    user_answer
    |> UserAnswer.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a UserAnswer.

  ## Examples

      iex> delete_user_answer(user_answer)
      {:ok, %UserAnswer{}}

      iex> delete_user_answer(user_answer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_answer(%UserAnswer{} = user_answer) do
    Repo.delete(user_answer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_answer changes.

  ## Examples

      iex> change_user_answer(user_answer)
      %Ecto.Changeset{source: %UserAnswer{}}

  """
  def change_user_answer(%UserAnswer{} = user_answer) do
    UserAnswer.changeset(user_answer, %{})
  end

  @doc """
  Returns the list of normalized_questions.

  ## Examples

      iex> list_normalized_questions()
      [%NormalizedQuestion{}, ...]

  """
  def list_normalized_questions do
    Repo.all(NormalizedQuestion)
  end

  @doc """
  Gets a single normalized_question.

  Raises `Ecto.NoResultsError` if the Normalized question does not exist.

  ## Examples

      iex> get_normalized_question!(123)
      %NormalizedQuestion{}

      iex> get_normalized_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_normalized_question!(id), do: Repo.get!(NormalizedQuestion, id)

  @doc """
  Creates a normalized_question.

  ## Examples

      iex> create_normalized_question(%{field: value})
      {:ok, %NormalizedQuestion{}}

      iex> create_normalized_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_normalized_question(attrs \\ %{}) do
    %NormalizedQuestion{}
    |> NormalizedQuestion.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a normalized_question.

  ## Examples

      iex> update_normalized_question(normalized_question, %{field: new_value})
      {:ok, %NormalizedQuestion{}}

      iex> update_normalized_question(normalized_question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_normalized_question(%NormalizedQuestion{} = normalized_question, attrs) do
    normalized_question
    |> NormalizedQuestion.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a NormalizedQuestion.

  ## Examples

      iex> delete_normalized_question(normalized_question)
      {:ok, %NormalizedQuestion{}}

      iex> delete_normalized_question(normalized_question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_normalized_question(%NormalizedQuestion{} = normalized_question) do
    Repo.delete(normalized_question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking normalized_question changes.

  ## Examples

      iex> change_normalized_question(normalized_question)
      %Ecto.Changeset{source: %NormalizedQuestion{}}

  """
  def change_normalized_question(%NormalizedQuestion{} = normalized_question) do
    NormalizedQuestion.changeset(normalized_question, %{})
  end
end
