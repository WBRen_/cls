defmodule Admin.Management do
  @moduledoc """
  The Management context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Management.HotQuestion

  def page_hot_questions(_params, page) do
    HotQuestion
    |> order_by([desc: :inserted_at])
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of hot_questions.

  ## Examples

      iex> list_hot_questions()
      [%HotQuestion{}, ...]

  """
  def list_hot_questions do
    Repo.all(HotQuestion)
  end

  @doc """
  Gets a single hot_question.

  Raises `Ecto.NoResultsError` if the Hot question does not exist.

  ## Examples

      iex> get_hot_question!(123)
      %HotQuestion{}

      iex> get_hot_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_hot_question!(id), do: Repo.get!(HotQuestion, id)

  @doc """
  Creates a hot_question.

  ## Examples

      iex> create_hot_question(%{field: value})
      {:ok, %HotQuestion{}}

      iex> create_hot_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_hot_question(attrs \\ %{}) do
    %HotQuestion{}
    |> HotQuestion.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a hot_question.

  ## Examples

      iex> update_hot_question(hot_question, %{field: new_value})
      {:ok, %HotQuestion{}}

      iex> update_hot_question(hot_question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_hot_question(%HotQuestion{} = hot_question, attrs) do
    hot_question
    |> HotQuestion.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a HotQuestion.

  ## Examples

      iex> delete_hot_question(hot_question)
      {:ok, %HotQuestion{}}

      iex> delete_hot_question(hot_question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_hot_question(%HotQuestion{} = hot_question) do
    Repo.delete(hot_question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking hot_question changes.

  ## Examples

      iex> change_hot_question(hot_question)
      %Ecto.Changeset{source: %HotQuestion{}}

  """
  def change_hot_question(%HotQuestion{} = hot_question) do
    HotQuestion.changeset(hot_question, %{})
  end
end
