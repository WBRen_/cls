use Mix.Config

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :fast_db, FastDb.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_FASTDB_USER") || "postgres",
  password: System.get_env("POSTGRES_FASTDB_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_FASTDB_DB") || "aimidas_fast_db_test",
  hostname: System.get_env("POSTGRES_FASTDB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox