# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :fast_db,
  namespace: FastDb,
  ecto_repos: [FastDb.Repo]

# Configures the endpoint
config :fast_db, FastDbWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XSkMZBRfA+PPQt6d0OwQcSAm7Z5J3QR9q7cjSeUsP4FyGZyAYPbCI+Acfds9pXPQ",
  render_errors: [view: FastDbWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: FastDb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]
  
# http
config :fast_db, FastDbWeb.Endpoint,
  http: [port: 14040],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../assets", __DIR__)]]
# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
