use Mix.Config

# Configure your database
config :fast_db, FastDb.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_FASTDB_DB_HOST") || "aim-postgresql",
  username: System.get_env("AIMIDAS_FASTDB_DB_USER") || "postgres",
  password: System.get_env("AIMIDAS_FASTDB_DB_PASSWORD") || "postgres",
  database: System.get_env("AIMIDAS_FASTDB_DB_NAME") || "aimidas_fast_db_citest",
  pool_size: 10,
  pool_timeout: 20_000,
  timeout: 20_000
