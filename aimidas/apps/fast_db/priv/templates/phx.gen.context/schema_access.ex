
  alias <%= inspect schema.module %>

  @doc """
  Returns the page of <%= schema.plural %>.

  ## Examples

      iex> search_<%= schema.plural %>(%{}, %{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%<%= inspect schema.alias %>{}, ...]
      }

  """
  def search_<%= schema.plural %>(allowed_lookup \\ %{}, page) do
    allowed_lookup
    |> Enum.reduce(<%= inspect schema.alias %>,
      fn {x, y}, query ->
        if is_integer(y) or is_float(y) do
          query |> where(^[{x, y}])
        else
          x = if is_atom(x) do x else String.to_atom(x) end
          case x do
            <%= for {k, v} <- schema.types do %><%= inspect k %> -> query |> where([c], like(c.<%= k %>, ^y))
            <% end %>_ -> query |> where(^[{x, y}]) # equal
          end
        end
      end)
    |> order_by(^[{page.order, page.by}])
    |> Repo.paginate(page)
  end
  @doc """
  Returns the page of <%= schema.plural %>.

  ## Examples

      iex> page_<%= schema.plural %>(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%<%= inspect schema.alias %>{}, ...]
      }

  """
  def page_<%= schema.plural %>(page) do
    <%= inspect schema.alias %>
    |> order_by(^[{page.order, page.by}])
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of <%= schema.plural %>.

  ## Examples

      iex> list_<%= schema.plural %>()
      [%<%= inspect schema.alias %>{}, ...]

  """
  def list_<%= schema.plural %> do
    Repo.all(<%= inspect schema.alias %>)
  end

  @doc """
  Gets a single <%= schema.singular %>.

  Raises `Ecto.NoResultsError` if the <%= schema.human_singular %> does not exist.

  ## Examples

      iex> get_<%= schema.singular %>!(123)
      %<%= inspect schema.alias %>{}

      iex> get_<%= schema.singular %>!(456)
      ** (Ecto.NoResultsError)

  """
  def get_<%= schema.singular %>!(id), do: Repo.get!(<%= inspect schema.alias %>, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_<%= schema.singular %>(id) do
    <%= inspect schema.alias %>
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a <%= schema.singular %>.

  ## Examples

      iex> create_<%= schema.singular %>(%{field: value})
      {:ok, %<%= inspect schema.alias %>{}}

      iex> create_<%= schema.singular %>(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_<%= schema.singular %>(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %<%= inspect schema.alias %>{}
    |> <%= inspect schema.alias %>.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a <%= schema.singular %>.

  ## Examples

      iex> update_<%= schema.singular %>(<%= schema.singular %>, %{field: new_value})
      {:ok, %<%= inspect schema.alias %>{}}

      iex> update_<%= schema.singular %>(<%= schema.singular %>, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_<%= schema.singular %>(%<%= inspect schema.alias %>{} = <%= schema.singular %>, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    <%= schema.singular %>
    |> <%= inspect schema.alias %>.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a <%= inspect schema.alias %>.

  ## Examples

      iex> delete_<%= schema.singular %>(<%= schema.singular %>)
      {:ok, %<%= inspect schema.alias %>{}}

      iex> delete_<%= schema.singular %>(<%= schema.singular %>)
      {:error, %Ecto.Changeset{}}

  """
  def delete_<%= schema.singular %>(%<%= inspect schema.alias %>{} = <%= schema.singular %>) do
    Repo.delete(<%= schema.singular %>)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking <%= schema.singular %> changes.

  ## Examples

      iex> change_<%= schema.singular %>(<%= schema.singular %>)
      %Ecto.Changeset{source: %<%= inspect schema.alias %>{}}

  """
  def change_<%= schema.singular %>(%<%= inspect schema.alias %>{} = <%= schema.singular %>) do
    <%= inspect schema.alias %>.changeset(<%= schema.singular %>, %{})
  end
