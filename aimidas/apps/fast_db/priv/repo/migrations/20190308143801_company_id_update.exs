defmodule FastDb.Repo.Migrations.CompanyIdUpdate do
  use Ecto.Migration

  def change do
    alter table(:secretary_datas) do
      add :cid, :integer
      remove :company_code
    end
    create index(:secretary_datas, [:cid, :md5], unique: true)

    alter table(:individual_researchs) do
      add :cid, :integer
      remove :company_code
    end

    alter table(:individual_research_pdfs) do
      add :cid, :integer
      remove :company_code
    end
    create index(:individual_research_pdfs, [:cid, :md5], unique: true)

    alter table(:other_pdf_pictures) do
      add :cid, :integer
      remove :company_code
    end

    alter table(:other_pdfs) do
      add :cid, :integer
      remove :company_code
    end
    
    alter table(:summaries) do
      add :cid, :integer
      remove :company_code
    end

    alter table(:md5_list) do
      add :cid, :integer
      remove :company_code
    end
  end
end
