defmodule FastDb.Repo.Migrations.CreateCompanyXbrl do
  use Ecto.Migration

  def change do
    create table(:company_xbrls) do
      add :company_code, :string
      add :abbrev, :string
      add :exchange_code, :string
      add :full_name, :string
      add :industry, :string
      add :src_updated_at, :date
      add :details, :map

      timestamps()
    end

    create index(:company_xbrls, [:company_code, :exchange_code])
  end
end
