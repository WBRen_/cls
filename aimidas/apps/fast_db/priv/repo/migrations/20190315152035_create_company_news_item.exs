defmodule FastDb.Repo.Migrations.CreateCompanyNewsItem do
  use Ecto.Migration

  def change do
    create table(:company_news_item) do
      add :title, :string
      add :publish_time, :naive_datetime
      add :source, :string
      add :tag, :string
      add :content, :text

      timestamps()
    end
    
    create index(:company_news_item, [:title, :publish_time], unique: true)
  end
end
