defmodule FastDb.Repo.Migrations.CompanyInfosAdd do
  use Ecto.Migration

  def up do
    alter table(:company_infos) do
      add :exchange_code, :string
    end

    drop index(:company_infos, [:company_code, :year])
    create index(:company_infos, [:company_code, :exchange_code, :year], unique: true)
  end

  def down do
    drop index(:company_infos, [:company_code, :exchange_code, :year])

    alter table(:company_infos) do
      remove :exchange_code
    end

    create index(:company_infos, [:company_code, :year], unique: true)
  end
end
