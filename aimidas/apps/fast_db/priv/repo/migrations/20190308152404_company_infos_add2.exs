defmodule FastDb.Repo.Migrations.CompanyInfosAdd2 do
  use Ecto.Migration

  def change do
    alter table(:company_infos) do
      add :stock_name, :string
      add :other_names, :text
    end
  end
end
