defmodule FastDb.Repo.Migrations.CreateIndividualResearchs do
  use Ecto.Migration

  def change do
    create table(:individual_researchs) do
      add :company_code, :integer
      add :name, :string
      add :title, :string
      add :date, :naive_datetime
      add :src_level, :string
      add :level_change, :string
      add :institution, :string
      add :content, :text
      add :pdf_link, :string
      add :pdf_name, :string

      timestamps()
    end

  end
end
