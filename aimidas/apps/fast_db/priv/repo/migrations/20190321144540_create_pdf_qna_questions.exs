defmodule FastDb.Repo.Migrations.CreatePdfQnaQuestions do
  use Ecto.Migration

  def change do
    create table(:pdf_qna_questions) do
      add :md5, :string
      add :answer, :text
      add :otpq, :map

      timestamps()
    end
    create index(:pdf_qna_questions, [:md5])
  end
end
