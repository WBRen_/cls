defmodule FastDb.Repo.Migrations.CreateCompanyNewsFirm do
  use Ecto.Migration

  def change do
    create table(:company_news_firm) do
      add :cid, :integer
      add :name, :string
      add :company_news_item_id, :integer

      timestamps()
    end

  end
end
