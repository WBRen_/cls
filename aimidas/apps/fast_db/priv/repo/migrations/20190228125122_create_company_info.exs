defmodule FastDb.Repo.Migrations.CreateCompanyInfo do
  use Ecto.Migration

  def change do
    create table(:company_infos) do
      add :company_code, :string
      add :year, :integer
      add :season, :integer
      add :abbrev_name, :string
      add :full_name, :string
      add :details, :map
      add :industry, :string
      add :source, :string
      add :info_updated_at, :date

      timestamps()
    end
    create index(:company_infos, [:company_code, :year], unique: true)

  end
end
