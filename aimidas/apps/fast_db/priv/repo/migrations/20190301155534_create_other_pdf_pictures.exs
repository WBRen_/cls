defmodule FastDb.Repo.Migrations.CreateOtherPdfPictures do
  use Ecto.Migration

  def change do
    create table(:other_pdf_pictures) do
      add :company_code, :integer
      add :name, :string
      add :report_title, :string
      add :report_date, :naive_datetime
      add :pdf_name, :string
      add :exchange_code, :string
      
      timestamps()
    end

  end
end
