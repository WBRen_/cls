defmodule FastDb.Repo.Migrations.CreatePdfPages do
  use Ecto.Migration

  def change do
    create table(:pdf_pages) do
      add :company_code, :string
      add :content, :text
      add :end_page, :integer
      add :image_url, :string
      add :start_page, :integer
      add :year, :integer
      add :src_updated_at, :integer
      add :type_code, :string
      add :detail_code, :string

      timestamps()
    end
    create index(:pdf_pages, [:company_code, :year, :src_updated_at, :type_code, :detail_code, :start_page, :end_page], unique: true)

  end
end
