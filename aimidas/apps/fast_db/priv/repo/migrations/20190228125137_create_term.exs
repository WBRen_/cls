defmodule FastDb.Repo.Migrations.CreateTerm do
  use Ecto.Migration

  def change do
    create table(:terms) do
      add :name, :string
      add :description, :text

      timestamps()
    end

    create index(:terms, [:name], unique: true)

  end
end
