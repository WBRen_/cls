defmodule FastDb.Repo.Migrations.CreateOtherPdfs do
  use Ecto.Migration

  def change do
    create table(:other_pdfs) do
      add :company_code, :integer
      add :name, :string
      add :report_title, :string
      add :report_date, :naive_datetime
      add :content, :text
      add :pdf_name, :string
      add :exchange_code, :string

      timestamps()
    end

  end
end
