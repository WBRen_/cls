defmodule FastDb.Repo.Migrations.CreateSummaries do
  use Ecto.Migration

  def change do
    create table(:summaries) do
      add :company_code, :integer
      add :name, :string
      add :report_title, :string
      add :report_date, :naive_datetime
      add :abstract, :text

      timestamps()
    end

  end
end
