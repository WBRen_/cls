defmodule FastDb.Repo.Migrations.CreateIndividualResearchPdfs do
  use Ecto.Migration

  def change do
    create table(:individual_research_pdfs) do
      add :company_code, :integer
      add :otpq, :map
      add :md5, :string
      add :answer, :text
      add :date, :naive_datetime
      add :institution, :string

      timestamps()
    end

  end
end
