defmodule FastDb.Repo.Migrations.CreateNews do
  use Ecto.Migration

  def change do
    create table(:news) do
      add :cid, :integer
      add :name, :string
      add :title, :string
      add :publish_time, :naive_datetime
      add :source, :string
      add :tag, :string
      add :content, :text

      timestamps()
    end

    create index(:news, [:title, :publish_time], unique: true)
  end
end
