defmodule FastDb.Repo.Migrations.CreateSecretaryDatas do
  use Ecto.Migration

  def change do
    create table(:secretary_datas) do
      add :company_code, :integer
      add :year, :integer
      add :md5, :string
      add :question, :text
      add :question_time, :naive_datetime
      add :answer, :text
      add :answer_time, :naive_datetime

      timestamps()
    end

  end
end
