defmodule FastDb.Repo.Migrations.CreateMd5List do
  use Ecto.Migration

  def change do
    create table(:md5_list) do
      add :company_code, :integer
      add :md5, :string

      timestamps()
    end

  end
end
