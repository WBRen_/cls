defmodule FastDbWeb.Router do
  use FastDbWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FastDbWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/db/fast_db", FastDbWeb do
    pipe_through :api
    resources "/individual_researchs", IndividualResearchController, only: [:index, :show, :create, :update, :delete]
    resources "/individual_research_pdfs", IndividualResearchPdfController, only: [:index, :show, :create, :update, :delete]
    resources "/md5_lists", MD5ListController, only: [:index, :show, :create, :update, :delete]
    resources "/other_pdfs", OtherPdfController, only: [:index, :show, :create, :update, :delete]
    resources "/other_pdf_pictures", OtherPdfPictureController, only: [:index, :show, :create, :update, :delete]
    resources "/secretary_datas", SecretaryDataController, only: [:index, :show, :create, :update, :delete]
    resources "/summaries", SummaryController, only: [:index, :show, :create, :update, :delete]
    get "/company_infos/all", CompanyInfoController, :all
    resources "/company_infos", CompanyInfoController, only: [:index, :show, :create, :update, :delete]
    resources "/company_xbrls", CompanyXbrlController, only: [:index, :show, :create, :update, :delete]
    resources "/terms", TermController, only: [:index, :show, :create, :update, :delete]
    resources "/company_news", CompanyNewsItemController, only: [:index, :show, :create, :update, :delete]

    resources "/pdf_pages", PdfPageController, only: [:index, :show, :create, :update, :delete]
    resources "/qna_pdfs", PdfQnaQuestionController, only: [:index, :show, :create, :update, :delete]

  end
end
