defmodule FastDbWeb.IndividualResearchView do
  use FastDbWeb, :view
  alias FastDbWeb.IndividualResearchView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, IndividualResearchView, "individual_research.json")
    }
  end

  def render("index.json", %{individual_researchs: individual_researchs}) do
    %{data: render_many(individual_researchs, IndividualResearchView, "individual_research.json")}
  end

  def render("show.json", %{individual_research: individual_research}) do
    %{data: render_one(individual_research, IndividualResearchView, "individual_research.json")}
  end

  def render("individual_research.json", %{individual_research: individual_research}) do
    %{
      id: individual_research.id,
      cid: individual_research.cid,
      name: individual_research.name,
      title: individual_research.title,
      date: individual_research.date,
      src_level: individual_research.src_level,
      level_change: individual_research.level_change,
      institution: individual_research.institution,
      content: individual_research.content,
      pdf_link: individual_research.pdf_link,
      pdf_name: individual_research.pdf_name,
      updated_at: individual_research.updated_at,
      inserted_at: individual_research.inserted_at
    }
  end
end
