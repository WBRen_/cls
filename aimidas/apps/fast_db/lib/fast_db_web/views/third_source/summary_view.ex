defmodule FastDbWeb.SummaryView do
  use FastDbWeb, :view
  alias FastDbWeb.SummaryView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, SummaryView, "summary.json")
    }
  end

  def render("index.json", %{summaries: summaries}) do
    %{data: render_many(summaries, SummaryView, "summary.json")}
  end

  def render("show.json", %{summary: summary}) do
    %{data: render_one(summary, SummaryView, "summary.json")}
  end

  def render("summary.json", %{summary: summary}) do
    %{
      id: summary.id,
      cid: summary.cid,
      name: summary.name,
      report_title: summary.report_title,
      report_date: summary.report_date,
      abstract: summary.abstract,
      updated_at: summary.updated_at,
      inserted_at: summary.inserted_at
    }
  end
end
