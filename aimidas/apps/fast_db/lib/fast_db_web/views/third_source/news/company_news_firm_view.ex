defmodule FastDbWeb.CompanyNewsFirmView do
  use FastDbWeb, :view
  alias FastDbWeb.CompanyNewsFirmView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, CompanyNewsFirmView, "company_news_firm.json")
    }
  end

  def render("index.json", %{company_news_firm: company_news_firm}) do
    %{data: render_many(company_news_firm, CompanyNewsFirmView, "company_news_firm.json")}
  end

  def render("show.json", %{company_news_firm: company_news_firm}) do
    %{data: render_one(company_news_firm, CompanyNewsFirmView, "company_news_firm.json")}
  end

  def render("company_news_firm.json", %{company_news_firm: company_news_firm}) do
    %{
      id: company_news_firm.id,
      cid: company_news_firm.cid,
      name: company_news_firm.name,
      company_news_item_id: company_news_firm.company_news_item_id,
      updated_at: company_news_firm.updated_at,
      inserted_at: company_news_firm.inserted_at
    }
  end
end
