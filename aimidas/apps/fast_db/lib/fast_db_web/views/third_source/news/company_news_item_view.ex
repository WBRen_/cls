defmodule FastDbWeb.CompanyNewsItemView do
  use FastDbWeb, :view
  alias FastDbWeb.CompanyNewsItemView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, CompanyNewsItemView, "company_news_item.json")
    }
  end

  def render("index.json", %{company_news_item: company_news_item}) do
    %{data: render_many(company_news_item, CompanyNewsItemView, "company_news_item.json")}
  end

  def render("show.json", %{company_news_item: company_news_item}) do
    %{data: render_one(company_news_item, CompanyNewsItemView, "company_news_item.json")}
  end

  def render("company_news_item.json", %{company_news_item: company_news_item}) do
    %{
      id: company_news_item.id,
      title: company_news_item.title,
      publish_time: company_news_item.publish_time,
      source: company_news_item.source,
      tag: company_news_item.tag,
      content: company_news_item.content,
      updated_at: company_news_item.updated_at,
      inserted_at: company_news_item.inserted_at,
      firms: company_news_item.firms |> render_firms()
    }
  end

  defp render_firms(nil), do: []
  defp render_firms(_anything = %Ecto.Association.NotLoaded{}), do: []
  defp render_firms(firms) do
    firms
    |> Enum.map(fn x ->
      %{
        cid: x.cid,
        name: x.name
      }
    end)
  end
end
