defmodule FastDbWeb.IndividualResearchPdfView do
  use FastDbWeb, :view
  alias FastDbWeb.IndividualResearchPdfView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, IndividualResearchPdfView, "individual_research_pdf.json")
    }
  end

  def render("index.json", %{individual_research_pdfs: individual_research_pdfs}) do
    %{data: render_many(individual_research_pdfs, IndividualResearchPdfView, "individual_research_pdf.json")}
  end

  def render("show.json", %{individual_research_pdf: individual_research_pdf}) do
    %{data: render_one(individual_research_pdf, IndividualResearchPdfView, "individual_research_pdf.json")}
  end

  def render("individual_research_pdf.json", %{individual_research_pdf: individual_research_pdf}) do
    %{
      id: individual_research_pdf.id,
      cid: individual_research_pdf.cid,
      otpq: individual_research_pdf.otpq,
      md5: individual_research_pdf.md5,
      answer: individual_research_pdf.answer,
      date: individual_research_pdf.date,
      institution: individual_research_pdf.institution,
      updated_at: individual_research_pdf.updated_at,
      inserted_at: individual_research_pdf.inserted_at
    }
  end
end
