defmodule FastDbWeb.OtherPdfView do
  use FastDbWeb, :view
  alias FastDbWeb.OtherPdfView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, OtherPdfView, "other_pdf.json")
    }
  end

  def render("index.json", %{other_pdfs: other_pdfs}) do
    %{data: render_many(other_pdfs, OtherPdfView, "other_pdf.json")}
  end

  def render("show.json", %{other_pdf: other_pdf}) do
    %{data: render_one(other_pdf, OtherPdfView, "other_pdf.json")}
  end

  def render("other_pdf.json", %{other_pdf: other_pdf}) do
    %{
      id: other_pdf.id,
      cid: other_pdf.cid,
      name: other_pdf.name,
      report_title: other_pdf.report_title,
      report_date: other_pdf.report_date,
      content: other_pdf.content,
      pdf_name: other_pdf.pdf_name,
      exchange_code: other_pdf.exchange_code,
      updated_at: other_pdf.updated_at,
      inserted_at: other_pdf.inserted_at
    }
  end
end
