defmodule FastDbWeb.OtherPdfPictureView do
  use FastDbWeb, :view
  alias FastDbWeb.OtherPdfPictureView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, OtherPdfPictureView, "other_pdf_picture.json")
    }
  end

  def render("index.json", %{other_pdf_pictures: other_pdf_pictures}) do
    %{data: render_many(other_pdf_pictures, OtherPdfPictureView, "other_pdf_picture.json")}
  end

  def render("show.json", %{other_pdf_picture: other_pdf_picture}) do
    %{data: render_one(other_pdf_picture, OtherPdfPictureView, "other_pdf_picture.json")}
  end

  def render("other_pdf_picture.json", %{other_pdf_picture: other_pdf_picture}) do
    %{
      id: other_pdf_picture.id,
      cid: other_pdf_picture.cid,
      name: other_pdf_picture.name,
      report_title: other_pdf_picture.report_title,
      report_date: other_pdf_picture.report_date,
      pdf_name: other_pdf_picture.pdf_name,
      updated_at: other_pdf_picture.updated_at,
      inserted_at: other_pdf_picture.inserted_at
    }
  end
end
