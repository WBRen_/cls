defmodule FastDbWeb.SecretaryDataView do
  use FastDbWeb, :view
  alias FastDbWeb.SecretaryDataView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, SecretaryDataView, "secretary_data.json")
    }
  end

  def render("index.json", %{secretary_datas: secretary_datas}) do
    %{data: render_many(secretary_datas, SecretaryDataView, "secretary_data.json")}
  end

  def render("show.json", %{secretary_data: secretary_data}) do
    %{data: render_one(secretary_data, SecretaryDataView, "secretary_data.json")}
  end

  def render("secretary_data.json", %{secretary_data: secretary_data}) do
    %{
      id: secretary_data.id,
      cid: secretary_data.cid,
      year: secretary_data.year,
      md5: secretary_data.md5,
      question: secretary_data.question,
      question_time: secretary_data.question_time,
      answer: secretary_data.answer,
      answer_time: secretary_data.answer_time,
      updated_at: secretary_data.updated_at,
      inserted_at: secretary_data.inserted_at
    }
  end
end
