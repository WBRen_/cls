defmodule FastDbWeb.MD5ListView do
  use FastDbWeb, :view
  alias FastDbWeb.MD5ListView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, MD5ListView, "md5_list.json")
    }
  end

  def render("index.json", %{md5_list: md5_list}) do
    %{data: render_many(md5_list, MD5ListView, "md5_list.json")}
  end

  def render("show.json", %{md5_list: md5_list}) do
    %{data: render_one(md5_list, MD5ListView, "md5_list.json")}
  end

  def render("md5_list.json", %{md5_list: md5_list}) do
    %{
      id: md5_list.id,
      cid: md5_list.cid,
      md5: md5_list.md5,
      updated_at: md5_list.updated_at,
      inserted_at: md5_list.inserted_at
    }
  end
  
  def render("md5_list.json", %{m_d5_list: md5_list}) do
    %{
      id: md5_list.id,
      cid: md5_list.cid,
      md5: md5_list.md5,
      updated_at: md5_list.updated_at,
      inserted_at: md5_list.inserted_at
    }
  end
end
