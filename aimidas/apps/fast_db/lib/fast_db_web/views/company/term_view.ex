defmodule FastDbWeb.TermView do
  use FastDbWeb, :view
  alias FastDbWeb.TermView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, TermView, "term.json")
    }
  end

  def render("index.json", %{terms: terms}) do
    %{data: render_many(terms, TermView, "term.json")}
  end

  def render("show.json", %{term: term}) do
    %{data: render_one(term, TermView, "term.json")}
  end

  def render("term.json", %{term: term}) do
    %{
      id: term.id,
      description: term.description,
      name: term.name,
      updated_at: term.updated_at,
      inserted_at: term.inserted_at
    }
  end
end
