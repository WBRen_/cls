defmodule FastDbWeb.CompanyInfoView do
  use FastDbWeb, :view
  alias FastDbWeb.CompanyInfoView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, CompanyInfoView, "company_info.json")
    }
  end

  def render("index.json", %{company_infos: company_infos}) do
    %{data: render_many(company_infos, CompanyInfoView, "company_info_simple.json")}
  end

  def render("show.json", %{company_info: company_info}) do
    %{data: render_one(company_info, CompanyInfoView, "company_info.json")}
  end

  def render("company_info.json", %{company_info: company_info}) do
    %{
      cid: company_info.id,
      company_code: company_info.company_code,
      abbrev_name: company_info.abbrev_name,
      full_name: company_info.full_name,
      stock_name: company_info.stock_name,
      other_names: company_info.other_names,
      industry: company_info.industry,
      year: company_info.year,
      season: company_info.season,
      source: company_info.source,
      info_updated_at: company_info.info_updated_at,
      details: company_info.details,
      updated_at: company_info.updated_at,
      inserted_at: company_info.inserted_at
    }
  end
  def render("company_info_simple.json", %{company_info: company_info}) do
    %{
      cid: company_info.id,
      company_code: company_info.company_code,
      abbrev_name: company_info.abbrev_name,
      full_name: company_info.full_name,
      stock_name: company_info.stock_name,
      other_names: company_info.other_names,
      industry: company_info.industry,
      year: company_info.year,
      season: company_info.season,
      source: company_info.source,
      info_updated_at: company_info.info_updated_at
    }
  end
end
