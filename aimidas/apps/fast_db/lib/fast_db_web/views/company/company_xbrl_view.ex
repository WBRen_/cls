defmodule FastDbWeb.CompanyXbrlView do
  use FastDbWeb, :view
  alias FastDbWeb.CompanyXbrlView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, CompanyXbrlView, "company_xbrl.json")
    }
  end

  def render("index.json", %{company_xbrls: company_xbrls}) do
    %{data: render_many(company_xbrls, CompanyXbrlView, "company_xbrl.json")}
  end

  def render("show.json", %{company_xbrl: company_xbrl}) do
    %{data: render_one(company_xbrl, CompanyXbrlView, "company_xbrl.json")}
  end

  def render("company_xbrl.json", %{company_xbrl: company_xbrl}) do
    %{
      id: company_xbrl.id,
      abbrev: company_xbrl.abbrev,
      company_code: company_xbrl.company_code,
      details: company_xbrl.details,
      exchange_code: company_xbrl.exchange_code,
      full_name: company_xbrl.full_name,
      industry: company_xbrl.industry,
      src_updated_at: company_xbrl.src_updated_at,
      updated_at: company_xbrl.updated_at,
      inserted_at: company_xbrl.inserted_at
    }
  end
end
