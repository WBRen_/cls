defmodule FastDbWeb.PdfPageView do
  use FastDbWeb, :view
  alias FastDbWeb.PdfPageView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, PdfPageView, "pdf_page.json")
    }
  end

  def render("index.json", %{pdf_pages: pdf_pages}) do
    %{data: render_many(pdf_pages, PdfPageView, "pdf_page.json")}
  end

  def render("show.json", %{pdf_page: pdf_page}) do
    %{data: render_one(pdf_page, PdfPageView, "pdf_page.json")}
  end

  def render("pdf_page.json", %{pdf_page: pdf_page}) do
    %{
      id: pdf_page.id,
      company_code: pdf_page.company_code,
      content: pdf_page.content,
      end_page: pdf_page.end_page,
      image_url: pdf_page.image_url,
      start_page: pdf_page.start_page,
      year: pdf_page.year,
      src_updated_at: pdf_page.src_updated_at,
      type_code: pdf_page.type_code,
      detail_code: pdf_page.detail_code,
      updated_at: pdf_page.updated_at,
      inserted_at: pdf_page.inserted_at
    }
  end
end
