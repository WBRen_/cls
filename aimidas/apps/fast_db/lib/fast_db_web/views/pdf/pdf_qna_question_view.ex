defmodule FastDbWeb.PdfQnaQuestionView do
  use FastDbWeb, :view
  alias FastDbWeb.PdfQnaQuestionView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, PdfQnaQuestionView, "pdf_qna_question.json")
    }
  end

  def render("index.json", %{pdf_qna_questions: pdf_qna_questions}) do
    %{data: render_many(pdf_qna_questions, PdfQnaQuestionView, "pdf_qna_question.json")}
  end

  def render("show.json", %{pdf_qna_question: pdf_qna_question}) do
    %{data: render_one(pdf_qna_question, PdfQnaQuestionView, "pdf_qna_question.json")}
  end

  def render("pdf_qna_question.json", %{pdf_qna_question: pdf_qna_question}) do
    %{
      id: pdf_qna_question.id,
      md5: pdf_qna_question.md5,
      answer: pdf_qna_question.answer,
      otpq: pdf_qna_question.otpq,
      updated_at: pdf_qna_question.updated_at,
      inserted_at: pdf_qna_question.inserted_at
    }
  end
end
