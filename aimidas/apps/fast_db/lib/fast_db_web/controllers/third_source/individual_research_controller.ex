defmodule FastDbWeb.IndividualResearchController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.IndividualResearch
  alias FastDb.ThirdSource.Querier

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    # cid = params |> Map.get("cid", nil)
    # title = params |> Map.get("title", "")
    # pdf_name = params |> Map.get("pdf_name", "")
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    # individual_researchs = if cid do
    #   ThirdSource.page_individual_researchs(page, cid, title, pdf_name)
    # else
    #   ThirdSource.page_individual_researchs(page)
    # end
    individual_researchs = Querier.find(FastDb.ThirdSource.IndividualResearch, params, page)
    render(conn, "page.json", page: individual_researchs)
  end

  def create(conn, %{"individual_research" => individual_research_params}) do
    with {:ok, %IndividualResearch{} = individual_research} <- ThirdSource.create_individual_research(individual_research_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", individual_research_path(conn, :show, individual_research))
      |> render("show.json", individual_research: individual_research)
    end
  end

  def show(conn, %{"id" => id}) do
    individual_research = ThirdSource.get_individual_research!(id)
    render(conn, "show.json", individual_research: individual_research)
  end

  def update(conn, %{"id" => id, "individual_research" => individual_research_params}) do
    individual_research = ThirdSource.get_individual_research!(id)

    with {:ok, %IndividualResearch{} = individual_research} <- ThirdSource.update_individual_research(individual_research, individual_research_params) do
      render(conn, "show.json", individual_research: individual_research)
    end
  end

  def delete(conn, %{"id" => id}) do
    individual_research = ThirdSource.get_individual_research!(id)
    with {:ok, %IndividualResearch{}} <- ThirdSource.delete_individual_research(individual_research) do
      send_resp(conn, :no_content, "")
    end
  end

  # def selective_update(conn, %{"individual_research" => individual_research_params}) do
  #   case ThirdSource.create_individual_research(individual_research_params) do
  #     {:ok, %IndividualResearch{} = individual_research} ->
  #       conn
  #       |> put_status(:created)
  #       |> put_resp_header("location", individual_research_path(conn, :show, individual_research))
  #       |> render("show.json", individual_research: individual_research)
  #     _ -> 
  #   end
  # end
end
