defmodule FastDbWeb.IndividualResearchPdfController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.IndividualResearchPdf
  alias FastDb.ThirdSource.Querier

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    # cid = params |> Map.get("cid", nil)
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    # individual_research_pdfs = if cid do
    #   ThirdSource.page_individual_research_pdfs(page, cid)
    # else
    #   ThirdSource.page_individual_research_pdfs(page)
    # end
    individual_research_pdfs = Querier.find(IndividualResearchPdf, params, page)
    render(conn, "page.json", page: individual_research_pdfs)
  end

  def create(conn, %{"individual_research_pdf" => individual_research_pdf_params}) do
    with {:ok, %IndividualResearchPdf{} = individual_research_pdf} <- ThirdSource.create_individual_research_pdf(individual_research_pdf_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", individual_research_pdf_path(conn, :show, individual_research_pdf))
      |> render("show.json", individual_research_pdf: individual_research_pdf)
    end
  end

  def show(conn, %{"id" => id}) do
    individual_research_pdf = ThirdSource.get_individual_research_pdf!(id)
    render(conn, "show.json", individual_research_pdf: individual_research_pdf)
  end

  def update(conn, %{"id" => id, "individual_research_pdf" => individual_research_pdf_params}) do
    individual_research_pdf = ThirdSource.get_individual_research_pdf!(id)

    with {:ok, %IndividualResearchPdf{} = individual_research_pdf} <- ThirdSource.update_individual_research_pdf(individual_research_pdf, individual_research_pdf_params) do
      render(conn, "show.json", individual_research_pdf: individual_research_pdf)
    end
  end

  def delete(conn, %{"id" => id}) do
    individual_research_pdf = ThirdSource.get_individual_research_pdf!(id)
    with {:ok, %IndividualResearchPdf{}} <- ThirdSource.delete_individual_research_pdf(individual_research_pdf) do
      send_resp(conn, :no_content, "")
    end
  end
end
