defmodule FastDbWeb.OtherPdfController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.OtherPdf
  alias FastDb.ThirdSource.Querier

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    # cid = params |> Map.get("cid", nil)
    # title = params |> Map.get("title", "")
    # pdf_name = params |> Map.get("pdf_name", "")
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    # other_pdfs = if cid do
    #   ThirdSource.page_other_pdfs(page, cid, title, pdf_name)
    # else
    #   ThirdSource.page_other_pdfs(page)
    # end
    other_pdfs = Querier.find(OtherPdf, params, page)
    render(conn, "page.json", page: other_pdfs)
  end

  def create(conn, %{"other_pdf" => other_pdf_params}) do
    with {:ok, %OtherPdf{} = other_pdf} <- ThirdSource.create_other_pdf(other_pdf_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", other_pdf_path(conn, :show, other_pdf))
      |> render("show.json", other_pdf: other_pdf)
    end
  end

  def show(conn, %{"id" => id}) do
    other_pdf = ThirdSource.get_other_pdf!(id)
    render(conn, "show.json", other_pdf: other_pdf)
  end

  def update(conn, %{"id" => id, "other_pdf" => other_pdf_params}) do
    other_pdf = ThirdSource.get_other_pdf!(id)

    with {:ok, %OtherPdf{} = other_pdf} <- ThirdSource.update_other_pdf(other_pdf, other_pdf_params) do
      render(conn, "show.json", other_pdf: other_pdf)
    end
  end

  def delete(conn, %{"id" => id}) do
    other_pdf = ThirdSource.get_other_pdf!(id)
    with {:ok, %OtherPdf{}} <- ThirdSource.delete_other_pdf(other_pdf) do
      send_resp(conn, :no_content, "")
    end
  end
end
