defmodule FastDbWeb.SecretaryDataController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.SecretaryData
  alias FastDb.ThirdSource.Querier

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    # cid = params |> Map.get("cid", nil)
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    # secretary_datas = if cid do
    #   ThirdSource.page_secretary_datas(page, cid)
    # else
    #   ThirdSource.page_secretary_datas(page)
    # end
    secretary_datas = Querier.find(SecretaryData, params, page)
    render(conn, "page.json", page: secretary_datas)
  end

  def create(conn, %{"secretary_data" => secretary_data_params}) do
    with {:ok, %SecretaryData{} = secretary_data} <- ThirdSource.create_secretary_data(secretary_data_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", secretary_data_path(conn, :show, secretary_data))
      |> render("show.json", secretary_data: secretary_data)
    end
  end

  def show(conn, %{"id" => id}) do
    secretary_data = ThirdSource.get_secretary_data!(id)
    render(conn, "show.json", secretary_data: secretary_data)
  end

  def update(conn, %{"id" => id, "secretary_data" => secretary_data_params}) do
    secretary_data = ThirdSource.get_secretary_data!(id)

    with {:ok, %SecretaryData{} = secretary_data} <- ThirdSource.update_secretary_data(secretary_data, secretary_data_params) do
      render(conn, "show.json", secretary_data: secretary_data)
    end
  end

  def delete(conn, %{"id" => id}) do
    secretary_data = ThirdSource.get_secretary_data!(id)
    with {:ok, %SecretaryData{}} <- ThirdSource.delete_secretary_data(secretary_data) do
      send_resp(conn, :no_content, "")
    end
  end
end
