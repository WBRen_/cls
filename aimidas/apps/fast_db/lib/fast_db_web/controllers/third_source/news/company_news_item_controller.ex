defmodule FastDbWeb.CompanyNewsItemController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.CompanyNewsItem

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    company_news_item = ThirdSource.page_company_news_item(page) # |> IO.inspect
    render(conn, "page.json", page: company_news_item)
  end

  def create(conn, %{"company_news" => company_news_item_params}) do
    with {:ok, %CompanyNewsItem{} = company_news_item} <- ThirdSource.create_company_news_item(company_news_item_params) do
      # create firms
      firms = company_news_item_params |> Map.get("firms", [])
      ThirdSource.create_company_news_firms(firms)
      # render
      conn
      |> put_status(:created)
      |> put_resp_header("location", company_news_item_path(conn, :show, company_news_item))
      |> render("show.json", company_news_item: company_news_item)
    end
  end

  def show(conn, %{"id" => id}) do
    company_news_item = ThirdSource.get_company_news_item!(id)
    render(conn, "show.json", company_news_item: company_news_item)
  end

  def update(conn, %{"id" => id, "company_news" => company_news_item_params}) do
    company_news_item = ThirdSource.get_company_news_item!(id)
    # update firms start (transaction)
    firms = company_news_item_params |> Map.get("firms", [])
    if firms != [] do
      ThirdSource.update_company_news_firms_by_nid(id, firms)
    end
    # update firms end
    with {:ok, %CompanyNewsItem{} = company_news_item} <- ThirdSource.update_company_news_item(company_news_item, company_news_item_params) do
      render(conn, "show.json", company_news_item: company_news_item)
    end
  end

  def delete(conn, %{"id" => id}) do
    company_news_item = ThirdSource.get_company_news_item!(id)
    ThirdSource.delete_company_news_firms_by_nid(id)
    with {:ok, %CompanyNewsItem{}} <- ThirdSource.delete_company_news_item(company_news_item) do
      send_resp(conn, :no_content, "")
    end
  end
end
