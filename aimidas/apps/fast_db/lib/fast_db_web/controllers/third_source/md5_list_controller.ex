defmodule FastDbWeb.MD5ListController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.MD5List
  alias FastDb.ThirdSource.Querier

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    # cid = params |> Map.get("cid", nil)
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    # md5_list = if cid do
    #   ThirdSource.page_md5_list(page, cid)
    # else
    #   ThirdSource.page_md5_list(page)
    # end
    md5_list = Querier.find(MD5List, params, page)
    render(conn, "page.json", page: md5_list)
  end

  def create(conn, %{"md5_list" => md5_list_params}) do
    with {:ok, %MD5List{} = md5_list} <- ThirdSource.create_md5_list(md5_list_params) do
      conn
      |> put_status(:created)
      # |> put_resp_header("location", md5_list_path(conn, :show, md5_list))
      |> render("show.json", md5_list: md5_list)
    end
  end

  def show(conn, %{"id" => id}) do
    md5_list = ThirdSource.get_md5_list!(id)
    render(conn, "show.json", md5_list: md5_list)
  end

  def update(conn, %{"id" => id, "md5_list" => md5_list_params}) do
    md5_list = ThirdSource.get_md5_list!(id)

    with {:ok, %MD5List{} = md5_list} <- ThirdSource.update_md5_list(md5_list, md5_list_params) do
      render(conn, "show.json", md5_list: md5_list)
    end
  end

  def delete(conn, %{"id" => id}) do
    md5_list = ThirdSource.get_md5_list!(id)
    with {:ok, %MD5List{}} <- ThirdSource.delete_md5_list(md5_list) do
      send_resp(conn, :no_content, "")
    end
  end
end
