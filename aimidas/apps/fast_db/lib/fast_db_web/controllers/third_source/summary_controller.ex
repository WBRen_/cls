defmodule FastDbWeb.SummaryController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.Summary
  alias FastDb.ThirdSource.Querier

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    # cid = params |> Map.get("cid", nil)
    # title = params |> Map.get("title", "")
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    # summaries = if cid do
    #   ThirdSource.page_summaries(page, cid, title)
    # else
    #   ThirdSource.page_summaries(page)
    # end
    summaries = Querier.find(Summary, params, page)
    render(conn, "page.json", page: summaries)
  end

  def create(conn, %{"summary" => summary_params}) do
    with {:ok, %Summary{} = summary} <- ThirdSource.create_summary(summary_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", summary_path(conn, :show, summary))
      |> render("show.json", summary: summary)
    end
  end

  def show(conn, %{"id" => id}) do
    summary = ThirdSource.get_summary!(id)
    render(conn, "show.json", summary: summary)
  end

  def update(conn, %{"id" => id, "summary" => summary_params}) do
    summary = ThirdSource.get_summary!(id)

    with {:ok, %Summary{} = summary} <- ThirdSource.update_summary(summary, summary_params) do
      render(conn, "show.json", summary: summary)
    end
  end

  def delete(conn, %{"id" => id}) do
    summary = ThirdSource.get_summary!(id)
    with {:ok, %Summary{}} <- ThirdSource.delete_summary(summary) do
      send_resp(conn, :no_content, "")
    end
  end
end
