defmodule FastDbWeb.OtherPdfPictureController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.OtherPdfPicture
  alias FastDb.ThirdSource.Querier

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    # cid = params |> Map.get("cid", nil)
    # title = params |> Map.get("title", "")
    # pdf_name = params |> Map.get("pdf_name", "")
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    # other_pdf_pictures = if cid do
    #   ThirdSource.page_other_pdf_pictures(page, cid, title, pdf_name)
    # else
    #   ThirdSource.page_other_pdf_pictures(page)
    # end
    other_pdf_pictures = Querier.find(OtherPdfPicture, params, page)
    render(conn, "page.json", page: other_pdf_pictures)
  end

  def create(conn, %{"other_pdf_picture" => other_pdf_picture_params}) do
    with {:ok, %OtherPdfPicture{} = other_pdf_picture} <- ThirdSource.create_other_pdf_picture(other_pdf_picture_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", other_pdf_picture_path(conn, :show, other_pdf_picture))
      |> render("show.json", other_pdf_picture: other_pdf_picture)
    end
  end

  def show(conn, %{"id" => id}) do
    other_pdf_picture = ThirdSource.get_other_pdf_picture!(id)
    render(conn, "show.json", other_pdf_picture: other_pdf_picture)
  end

  def update(conn, %{"id" => id, "other_pdf_picture" => other_pdf_picture_params}) do
    other_pdf_picture = ThirdSource.get_other_pdf_picture!(id)

    with {:ok, %OtherPdfPicture{} = other_pdf_picture} <- ThirdSource.update_other_pdf_picture(other_pdf_picture, other_pdf_picture_params) do
      render(conn, "show.json", other_pdf_picture: other_pdf_picture)
    end
  end

  def delete(conn, %{"id" => id}) do
    other_pdf_picture = ThirdSource.get_other_pdf_picture!(id)
    with {:ok, %OtherPdfPicture{}} <- ThirdSource.delete_other_pdf_picture(other_pdf_picture) do
      send_resp(conn, :no_content, "")
    end
  end
end
