defmodule FastDbWeb.TermController do
  use FastDbWeb, :controller

  alias FastDb.Ontology
  alias FastDb.Ontology.Term

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    terms = Ontology.page_terms(page)
    render(conn, "page.json", page: terms)
  end

  def create(conn, %{"term" => term_params}) do
    with {:ok, %Term{} = term} <- Ontology.create_term(term_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", term_path(conn, :show, term))
      |> render("show.json", term: term)
    end
  end

  def show(conn, %{"id" => id}) do
    term = Ontology.get_term!(id)
    render(conn, "show.json", term: term)
  end

  def update(conn, %{"id" => id, "term" => term_params}) do
    term = Ontology.get_term!(id)

    with {:ok, %Term{} = term} <- Ontology.update_term(term, term_params) do
      render(conn, "show.json", term: term)
    end
  end

  def delete(conn, %{"id" => id}) do
    term = Ontology.get_term!(id)
    with {:ok, %Term{}} <- Ontology.delete_term(term) do
      send_resp(conn, :no_content, "")
    end
  end
end
