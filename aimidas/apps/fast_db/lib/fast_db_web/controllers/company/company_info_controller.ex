defmodule FastDbWeb.CompanyInfoController do
  use FastDbWeb, :controller

  alias FastDb.Ontology
  alias FastDb.Ontology.CompanyInfo

  action_fallback FastDbWeb.FallbackController

  def all(conn, params) do
    company_infos = Ontology.list_company_infos()
    render(conn, "index.json", company_infos: company_infos)
  end

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    company_code = params |> Map.get("company_code", nil)
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    company_infos = if company_code do
      Ontology.page_company_infos(page, company_code)
    else
      Ontology.page_company_infos(page)
    end
    render(conn, "page.json", page: company_infos)
  end

  def create(conn, %{"company_info" => company_info_params}) do
    with {:ok, %CompanyInfo{} = company_info} <- Ontology.create_company_info(company_info_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", company_info_path(conn, :show, company_info))
      |> render("show.json", company_info: company_info)
    end
  end

  def show(conn, %{"id" => id}) do
    company_info = Ontology.get_company_info!(id)
    render(conn, "show.json", company_info: company_info)
  end

  def update(conn, %{"id" => id, "company_info" => company_info_params}) do
    company_info = Ontology.get_company_info!(id)

    with {:ok, %CompanyInfo{} = company_info} <- Ontology.update_company_info(company_info, company_info_params) do
      render(conn, "show.json", company_info: company_info)
    end
  end

  def delete(conn, %{"id" => id}) do
    company_info = Ontology.get_company_info!(id)
    with {:ok, %CompanyInfo{}} <- Ontology.delete_company_info(company_info) do
      send_resp(conn, :no_content, "")
    end
  end
end
