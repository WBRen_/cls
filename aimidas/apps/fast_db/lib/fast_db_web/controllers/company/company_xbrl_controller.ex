defmodule FastDbWeb.CompanyXbrlController do
  use FastDbWeb, :controller

  alias FastDb.Ontology
  alias FastDb.Ontology.CompanyXbrl

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    company_code = params |> Map.get("company_code", nil)
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    company_xbrls = if company_code do
      Ontology.page_company_xbrls(page, company_code)
    else
      Ontology.page_company_xbrls(page)
    end
    render(conn, "page.json", page: company_xbrls)
  end

  def create(conn, %{"company_xbrl" => company_xbrl_params}) do
    with {:ok, %CompanyXbrl{} = company_xbrl} <- Ontology.create_company_xbrl(company_xbrl_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", company_xbrl_path(conn, :show, company_xbrl))
      |> render("show.json", company_xbrl: company_xbrl)
    end
  end

  def show(conn, %{"id" => id}) do
    company_xbrl = Ontology.get_company_xbrl!(id)
    render(conn, "show.json", company_xbrl: company_xbrl)
  end

  def update(conn, %{"id" => id, "company_xbrl" => company_xbrl_params}) do
    company_xbrl = Ontology.get_company_xbrl!(id)

    with {:ok, %CompanyXbrl{} = company_xbrl} <- Ontology.update_company_xbrl(company_xbrl, company_xbrl_params) do
      render(conn, "show.json", company_xbrl: company_xbrl)
    end
  end

  def delete(conn, %{"id" => id}) do
    company_xbrl = Ontology.get_company_xbrl!(id)
    with {:ok, %CompanyXbrl{}} <- Ontology.delete_company_xbrl(company_xbrl) do
      send_resp(conn, :no_content, "")
    end
  end
end
