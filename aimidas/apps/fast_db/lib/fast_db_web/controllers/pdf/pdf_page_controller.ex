defmodule FastDbWeb.PdfPageController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.PdfPage

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    pdf_pages = ThirdSource.search_pdf_pages(params, page)
    render(conn, "page.json", page: pdf_pages)
  end

  def create(conn, %{"pdf_page" => pdf_page_params}) do
    with {:ok, %PdfPage{} = pdf_page} <- ThirdSource.create_pdf_page(pdf_page_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", pdf_page_path(conn, :show, pdf_page))
      |> render("show.json", pdf_page: pdf_page)
    end
  end

  def show(conn, %{"id" => id}) do
    pdf_page = ThirdSource.get_pdf_page!(id)
    render(conn, "show.json", pdf_page: pdf_page)
  end

  def update(conn, %{"id" => id, "pdf_page" => pdf_page_params}) do
    pdf_page = ThirdSource.get_pdf_page!(id)

    with {:ok, %PdfPage{} = pdf_page} <- ThirdSource.update_pdf_page(pdf_page, pdf_page_params) do
      render(conn, "show.json", pdf_page: pdf_page)
    end
  end

  def delete(conn, %{"id" => id}) do
    pdf_page = ThirdSource.get_pdf_page!(id)
    with {:ok, %PdfPage{}} <- ThirdSource.delete_pdf_page(pdf_page) do
      send_resp(conn, :no_content, "")
    end
  end
end
