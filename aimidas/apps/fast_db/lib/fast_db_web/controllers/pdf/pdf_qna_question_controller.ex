defmodule FastDbWeb.PdfQnaQuestionController do
  use FastDbWeb, :controller

  alias FastDb.ThirdSource
  alias FastDb.ThirdSource.PdfQnaQuestion

  action_fallback FastDbWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    by = params |> Map.get("orderby", "inserted_at") |> String.to_atom
    order = params |> Map.get("order", "desc") |> String.downcase |> String.to_atom
    params = params |> Map.drop(["page", "size", "orderby", "order"])
    page =
    %{
      page: page_number,
      page_size: page_size,
      order: order,
      by: by
    }
    pdf_qna_questions = ThirdSource.search_pdf_qna_questions(params, page)
    render(conn, "page.json", page: pdf_qna_questions)
  end

  def create(conn, %{"pdf_qna_question" => pdf_qna_question_params}) do
    with {:ok, %PdfQnaQuestion{} = pdf_qna_question} <- ThirdSource.create_pdf_qna_question(pdf_qna_question_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", pdf_qna_question_path(conn, :show, pdf_qna_question))
      |> render("show.json", pdf_qna_question: pdf_qna_question)
    end
  end

  def show(conn, %{"id" => id}) do
    pdf_qna_question = ThirdSource.get_pdf_qna_question!(id)
    render(conn, "show.json", pdf_qna_question: pdf_qna_question)
  end

  def update(conn, %{"id" => id, "pdf_qna_question" => pdf_qna_question_params}) do
    pdf_qna_question = ThirdSource.get_pdf_qna_question!(id)

    with {:ok, %PdfQnaQuestion{} = pdf_qna_question} <- ThirdSource.update_pdf_qna_question(pdf_qna_question, pdf_qna_question_params) do
      render(conn, "show.json", pdf_qna_question: pdf_qna_question)
    end
  end

  def delete(conn, %{"id" => id}) do
    pdf_qna_question = ThirdSource.get_pdf_qna_question!(id)
    with {:ok, %PdfQnaQuestion{}} <- ThirdSource.delete_pdf_qna_question(pdf_qna_question) do
      send_resp(conn, :no_content, "")
    end
  end
end
