defmodule FastDbWeb.PageController do
  use FastDbWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
