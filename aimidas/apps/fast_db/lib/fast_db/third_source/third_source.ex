defmodule FastDb.ThirdSource do
  @moduledoc """
  The ThirdSource context.
  """

  import Ecto.Query, warn: false
  alias FastDb.Repo

  alias FastDb.ThirdSource.SecretaryData

  ## private function
  defp page_2(module_name, page, cid, title) do
    query = if title == "" do
      from q in module_name,
      where: q.cid == ^cid,
      order_by: [desc: q.inserted_at]
    else
      title_ = "%#{title}%"
      from q in module_name,
      where: (q.cid == ^cid)
              or like(q.title, ^title_),
      order_by: [desc: q.inserted_at]
    end
    query |> Repo.paginate(page)
  end
  defp page_3(module_name, page, cid, title, pdf_name) do
    query = if title == "" and pdf_name == "" do
      from q in module_name,
      where: q.cid == ^cid,
      order_by: [desc: q.inserted_at]
    else
      title_ = "%#{title}%"
      pdf_name_ = "%#{pdf_name}%"
      from q in module_name,
      where: (q.cid == ^cid)
              or like(q.title, ^title_)
              or like(q.pdf_name, ^pdf_name_),
      order_by: [desc: q.inserted_at]
    end
    query |> Repo.paginate(page)
  end

  @doc """
  Returns the page of secretary_datas.

  ## Examples

      iex> page_secretary_datas(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%SecretaryData{}, ...]
      }

  """
  def page_secretary_datas(page) do
    SecretaryData
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  def page_secretary_datas(page, cid) do
    SecretaryData
    |> where(cid: ^cid)
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of secretary_datas.

  ## Examples

      iex> list_secretary_datas()
      [%SecretaryData{}, ...]

  """
  def list_secretary_datas do
    Repo.all(SecretaryData)
  end

  @doc """
  Gets a single secretary_data.

  Raises `Ecto.NoResultsError` if the Secretary data does not exist.

  ## Examples

      iex> get_secretary_data!(123)
      %SecretaryData{}

      iex> get_secretary_data!(456)
      ** (Ecto.NoResultsError)

  """
  def get_secretary_data!(id), do: Repo.get!(SecretaryData, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_secretary_data(id) do
    SecretaryData
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a secretary_data.

  ## Examples

      iex> create_secretary_data(%{field: value})
      {:ok, %SecretaryData{}}

      iex> create_secretary_data(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_secretary_data(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %SecretaryData{}
    |> SecretaryData.changeset(attrs_with_time)
    # |> unsafe_validate_unique([:cid, :md5], Repo, message: "")
    |> Repo.insert()
  end

  @doc """
  Updates a secretary_data.

  ## Examples

      iex> update_secretary_data(secretary_data, %{field: new_value})
      {:ok, %SecretaryData{}}

      iex> update_secretary_data(secretary_data, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_secretary_data(%SecretaryData{} = secretary_data, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    secretary_data
    |> SecretaryData.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a SecretaryData.

  ## Examples

      iex> delete_secretary_data(secretary_data)
      {:ok, %SecretaryData{}}

      iex> delete_secretary_data(secretary_data)
      {:error, %Ecto.Changeset{}}

  """
  def delete_secretary_data(%SecretaryData{} = secretary_data) do
    Repo.delete(secretary_data)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking secretary_data changes.

  ## Examples

      iex> change_secretary_data(secretary_data)
      %Ecto.Changeset{source: %SecretaryData{}}

  """
  def change_secretary_data(%SecretaryData{} = secretary_data) do
    SecretaryData.changeset(secretary_data, %{})
  end

  alias FastDb.ThirdSource.IndividualResearch


  @doc """
  Returns the page of individual_researchs.

  ## Examples

      iex> page_individual_researchs(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%IndividualResearch{}, ...]
      }

  """
  def page_individual_researchs(page) do
    IndividualResearch
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  # FastDb.ThirdSource.page_individual_researchs(%{page: 0, page_size: 10}, 600036, "dsa", "sda")
  def page_individual_researchs(page, cid, title, pdf_name) do
    page_3(IndividualResearch, page, cid, title, pdf_name)
  end
  @doc """
  Returns the list of individual_researchs.

  ## Examples

      iex> list_individual_researchs()
      [%IndividualResearch{}, ...]

  """
  def list_individual_researchs do
    Repo.all(IndividualResearch)
  end

  @doc """
  Gets a single individual_research.

  Raises `Ecto.NoResultsError` if the Individual research does not exist.

  ## Examples

      iex> get_individual_research!(123)
      %IndividualResearch{}

      iex> get_individual_research!(456)
      ** (Ecto.NoResultsError)

  """
  def get_individual_research!(id), do: Repo.get!(IndividualResearch, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_individual_research(id) do
    IndividualResearch
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a individual_research.

  ## Examples

      iex> create_individual_research(%{field: value})
      {:ok, %IndividualResearch{}}

      iex> create_individual_research(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_individual_research(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %IndividualResearch{}
    |> IndividualResearch.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a individual_research.

  ## Examples

      iex> update_individual_research(individual_research, %{field: new_value})
      {:ok, %IndividualResearch{}}

      iex> update_individual_research(individual_research, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_individual_research(%IndividualResearch{} = individual_research, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    individual_research
    |> IndividualResearch.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a IndividualResearch.

  ## Examples

      iex> delete_individual_research(individual_research)
      {:ok, %IndividualResearch{}}

      iex> delete_individual_research(individual_research)
      {:error, %Ecto.Changeset{}}

  """
  def delete_individual_research(%IndividualResearch{} = individual_research) do
    Repo.delete(individual_research)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking individual_research changes.

  ## Examples

      iex> change_individual_research(individual_research)
      %Ecto.Changeset{source: %IndividualResearch{}}

  """
  def change_individual_research(%IndividualResearch{} = individual_research) do
    IndividualResearch.changeset(individual_research, %{})
  end

  alias FastDb.ThirdSource.IndividualResearchPdf


  @doc """
  Returns the page of individual_research_pdfs.

  ## Examples

      iex> page_individual_research_pdfs(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%IndividualResearchPdf{}, ...]
      }

  """
  def page_individual_research_pdfs(page) do
    IndividualResearchPdf
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  def page_individual_research_pdfs(page, cid) do
    IndividualResearchPdf
    |> where(cid: ^cid)
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of individual_research_pdfs.

  ## Examples

      iex> list_individual_research_pdfs()
      [%IndividualResearchPdf{}, ...]

  """
  def list_individual_research_pdfs do
    Repo.all(IndividualResearchPdf)
  end

  @doc """
  Gets a single individual_research_pdf.

  Raises `Ecto.NoResultsError` if the Individual research pdf does not exist.

  ## Examples

      iex> get_individual_research_pdf!(123)
      %IndividualResearchPdf{}

      iex> get_individual_research_pdf!(456)
      ** (Ecto.NoResultsError)

  """
  def get_individual_research_pdf!(id), do: Repo.get!(IndividualResearchPdf, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_individual_research_pdf(id) do
    IndividualResearchPdf
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a individual_research_pdf.

  ## Examples

      iex> create_individual_research_pdf(%{field: value})
      {:ok, %IndividualResearchPdf{}}

      iex> create_individual_research_pdf(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_individual_research_pdf(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %IndividualResearchPdf{}
    |> IndividualResearchPdf.changeset(attrs_with_time)
    # |> unsafe_validate_unique([:cid, :md5], Repo)
    |> Repo.insert()
  end

  @doc """
  Updates a individual_research_pdf.

  ## Examples

      iex> update_individual_research_pdf(individual_research_pdf, %{field: new_value})
      {:ok, %IndividualResearchPdf{}}

      iex> update_individual_research_pdf(individual_research_pdf, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_individual_research_pdf(%IndividualResearchPdf{} = individual_research_pdf, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    individual_research_pdf
    |> IndividualResearchPdf.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a IndividualResearchPdf.

  ## Examples

      iex> delete_individual_research_pdf(individual_research_pdf)
      {:ok, %IndividualResearchPdf{}}

      iex> delete_individual_research_pdf(individual_research_pdf)
      {:error, %Ecto.Changeset{}}

  """
  def delete_individual_research_pdf(%IndividualResearchPdf{} = individual_research_pdf) do
    Repo.delete(individual_research_pdf)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking individual_research_pdf changes.

  ## Examples

      iex> change_individual_research_pdf(individual_research_pdf)
      %Ecto.Changeset{source: %IndividualResearchPdf{}}

  """
  def change_individual_research_pdf(%IndividualResearchPdf{} = individual_research_pdf) do
    IndividualResearchPdf.changeset(individual_research_pdf, %{})
  end

  alias FastDb.ThirdSource.OtherPdfPicture


  @doc """
  Returns the page of other_pdf_pictures.

  ## Examples

      iex> page_other_pdf_pictures(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%OtherPdfPicture{}, ...]
      }

  """
  def page_other_pdf_pictures(page) do
    OtherPdfPicture
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  def page_other_pdf_pictures(page, cid, title, pdf_name) do
    page_3(OtherPdfPicture, page, cid, title, pdf_name)
  end
  @doc """
  Returns the list of other_pdf_pictures.

  ## Examples

      iex> list_other_pdf_pictures()
      [%OtherPdfPicture{}, ...]

  """
  def list_other_pdf_pictures do
    Repo.all(OtherPdfPicture)
  end

  @doc """
  Gets a single other_pdf_picture.

  Raises `Ecto.NoResultsError` if the Other pdf picture does not exist.

  ## Examples

      iex> get_other_pdf_picture!(123)
      %OtherPdfPicture{}

      iex> get_other_pdf_picture!(456)
      ** (Ecto.NoResultsError)

  """
  def get_other_pdf_picture!(id), do: Repo.get!(OtherPdfPicture, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_other_pdf_picture(id) do
    OtherPdfPicture
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a other_pdf_picture.

  ## Examples

      iex> create_other_pdf_picture(%{field: value})
      {:ok, %OtherPdfPicture{}}

      iex> create_other_pdf_picture(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_other_pdf_picture(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %OtherPdfPicture{}
    |> OtherPdfPicture.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a other_pdf_picture.

  ## Examples

      iex> update_other_pdf_picture(other_pdf_picture, %{field: new_value})
      {:ok, %OtherPdfPicture{}}

      iex> update_other_pdf_picture(other_pdf_picture, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_other_pdf_picture(%OtherPdfPicture{} = other_pdf_picture, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    other_pdf_picture
    |> OtherPdfPicture.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a OtherPdfPicture.

  ## Examples

      iex> delete_other_pdf_picture(other_pdf_picture)
      {:ok, %OtherPdfPicture{}}

      iex> delete_other_pdf_picture(other_pdf_picture)
      {:error, %Ecto.Changeset{}}

  """
  def delete_other_pdf_picture(%OtherPdfPicture{} = other_pdf_picture) do
    Repo.delete(other_pdf_picture)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking other_pdf_picture changes.

  ## Examples

      iex> change_other_pdf_picture(other_pdf_picture)
      %Ecto.Changeset{source: %OtherPdfPicture{}}

  """
  def change_other_pdf_picture(%OtherPdfPicture{} = other_pdf_picture) do
    OtherPdfPicture.changeset(other_pdf_picture, %{})
  end

  alias FastDb.ThirdSource.OtherPdf


  @doc """
  Returns the page of other_pdfs.

  ## Examples

      iex> page_other_pdfs(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%OtherPdf{}, ...]
      }

  """
  def page_other_pdfs(page) do
    OtherPdf
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end

  def page_other_pdfs(page, cid, title, pdf_name) do
    page_3(OtherPdf, page, cid, title, pdf_name)
  end
  @doc """
  Returns the list of other_pdfs.

  ## Examples

      iex> list_other_pdfs()
      [%OtherPdf{}, ...]

  """
  def list_other_pdfs do
    Repo.all(OtherPdf)
  end

  @doc """
  Gets a single other_pdf.

  Raises `Ecto.NoResultsError` if the Other pdf does not exist.

  ## Examples

      iex> get_other_pdf!(123)
      %OtherPdf{}

      iex> get_other_pdf!(456)
      ** (Ecto.NoResultsError)

  """
  def get_other_pdf!(id), do: Repo.get!(OtherPdf, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_other_pdf(id) do
    OtherPdf
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a other_pdf.

  ## Examples

      iex> create_other_pdf(%{field: value})
      {:ok, %OtherPdf{}}

      iex> create_other_pdf(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_other_pdf(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %OtherPdf{}
    |> OtherPdf.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a other_pdf.

  ## Examples

      iex> update_other_pdf(other_pdf, %{field: new_value})
      {:ok, %OtherPdf{}}

      iex> update_other_pdf(other_pdf, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_other_pdf(%OtherPdf{} = other_pdf, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    other_pdf
    |> OtherPdf.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a OtherPdf.

  ## Examples

      iex> delete_other_pdf(other_pdf)
      {:ok, %OtherPdf{}}

      iex> delete_other_pdf(other_pdf)
      {:error, %Ecto.Changeset{}}

  """
  def delete_other_pdf(%OtherPdf{} = other_pdf) do
    Repo.delete(other_pdf)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking other_pdf changes.

  ## Examples

      iex> change_other_pdf(other_pdf)
      %Ecto.Changeset{source: %OtherPdf{}}

  """
  def change_other_pdf(%OtherPdf{} = other_pdf) do
    OtherPdf.changeset(other_pdf, %{})
  end

  alias FastDb.ThirdSource.Summary


  @doc """
  Returns the page of summaries.

  ## Examples

      iex> page_summaries(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%Summary{}, ...]
      }

  """
  def page_summaries(page) do
    Summary
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  
  def page_summaries(page, cid, title) do
    page_2(Summary, page, cid, title)
  end
  @doc """
  Returns the list of summaries.

  ## Examples

      iex> list_summaries()
      [%Summary{}, ...]

  """
  def list_summaries do
    Repo.all(Summary)
  end

  @doc """
  Gets a single summary.

  Raises `Ecto.NoResultsError` if the Summary does not exist.

  ## Examples

      iex> get_summary!(123)
      %Summary{}

      iex> get_summary!(456)
      ** (Ecto.NoResultsError)

  """
  def get_summary!(id), do: Repo.get!(Summary, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_summary(id) do
    Summary
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a summary.

  ## Examples

      iex> create_summary(%{field: value})
      {:ok, %Summary{}}

      iex> create_summary(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_summary(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %Summary{}
    |> Summary.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a summary.

  ## Examples

      iex> update_summary(summary, %{field: new_value})
      {:ok, %Summary{}}

      iex> update_summary(summary, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_summary(%Summary{} = summary, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    summary
    |> Summary.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a Summary.

  ## Examples

      iex> delete_summary(summary)
      {:ok, %Summary{}}

      iex> delete_summary(summary)
      {:error, %Ecto.Changeset{}}

  """
  def delete_summary(%Summary{} = summary) do
    Repo.delete(summary)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking summary changes.

  ## Examples

      iex> change_summary(summary)
      %Ecto.Changeset{source: %Summary{}}

  """
  def change_summary(%Summary{} = summary) do
    Summary.changeset(summary, %{})
  end

  alias FastDb.ThirdSource.MD5List


  @doc """
  Returns the page of md5_list.

  ## Examples

      iex> page_md5_list(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%MD5List{}, ...]
      }

  """
  def page_md5_list(page) do
    MD5List
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
    
  def page_md5_list(page, cid) do
    MD5List
    |> where(cid: ^cid)
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of md5_list.

  ## Examples

      iex> list_md5_list()
      [%MD5List{}, ...]

  """
  def list_md5_list do
    Repo.all(MD5List)
  end

  @doc """
  Gets a single md5_list.

  Raises `Ecto.NoResultsError` if the M d5 list does not exist.

  ## Examples

      iex> get_md5_list!(123)
      %MD5List{}

      iex> get_md5_list!(456)
      ** (Ecto.NoResultsError)

  """
  def get_md5_list!(id), do: Repo.get!(MD5List, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_md5_list(id) do
    MD5List
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a md5_list.

  ## Examples

      iex> create_md5_list(%{field: value})
      {:ok, %MD5List{}}

      iex> create_md5_list(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_md5_list(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %MD5List{}
    |> MD5List.changeset(attrs_with_time)
    # |> unsafe_validate_unique([:md5], Repo)
    |> Repo.insert()
  end

  @doc """
  Updates a md5_list.

  ## Examples

      iex> update_md5_list(md5_list, %{field: new_value})
      {:ok, %MD5List{}}

      iex> update_md5_list(md5_list, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_md5_list(%MD5List{} = md5_list, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    md5_list
    |> MD5List.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a MD5List.

  ## Examples

      iex> delete_md5_list(md5_list)
      {:ok, %MD5List{}}

      iex> delete_md5_list(md5_list)
      {:error, %Ecto.Changeset{}}

  """
  def delete_md5_list(%MD5List{} = md5_list) do
    Repo.delete(md5_list)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking md5_list changes.

  ## Examples

      iex> change_md5_list(md5_list)
      %Ecto.Changeset{source: %MD5List{}}

  """
  def change_md5_list(%MD5List{} = md5_list) do
    MD5List.changeset(md5_list, %{})
  end

  alias FastDb.ThirdSource.CompanyNewsFirm


  @doc """
  Returns the page of company_news_firm.

  ## Examples

      iex> page_company_news_firm(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%CompanyNewsFirm{}, ...]
      }

  """
  def page_company_news_firm(page) do
    CompanyNewsFirm
    # |> order_by(desc: :inserted_at)
    |> order_by(^[{page.order, page.by}])
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of company_news_firm.

  ## Examples

      iex> list_company_news_firm()
      [%CompanyNewsFirm{}, ...]

  """
  def list_company_news_firm do
    Repo.all(CompanyNewsFirm)
  end

  @doc """
  Gets a single company_news_firm.

  Raises `Ecto.NoResultsError` if the Company news firm does not exist.

  ## Examples

      iex> get_company_news_firm!(123)
      %CompanyNewsFirm{}

      iex> get_company_news_firm!(456)
      ** (Ecto.NoResultsError)

  """
  def get_company_news_firm!(id), do: Repo.get!(CompanyNewsFirm, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_company_news_firm(id) do
    CompanyNewsFirm
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a company_news_firm.

  ## Examples

      iex> create_company_news_firm(%{field: value})
      {:ok, %CompanyNewsFirm{}}

      iex> create_company_news_firm(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_company_news_firm(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %CompanyNewsFirm{}
    |> CompanyNewsFirm.changeset(attrs_with_time)
    |> Repo.insert()
  end

  def create_company_news_firms(attrs \\ []) do
    attrs
    |> Enum.map(fn x ->
      create_company_news_firm(x)
    end)
  end

  @doc """
  Updates a company_news_firm.

  ## Examples

      iex> update_company_news_firm(company_news_firm, %{field: new_value})
      {:ok, %CompanyNewsFirm{}}

      iex> update_company_news_firm(company_news_firm, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_company_news_firm(%CompanyNewsFirm{} = company_news_firm, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    company_news_firm
    |> CompanyNewsFirm.changeset(attrs_with_time)
    |> Repo.update()
  end

  def update_company_news_firms_by_nid(nid, attrs \\ []) do
    Repo.transaction(fn ->
      delete_company_news_firms_by_nid(nid)
      create_company_news_firms(attrs)
    end)
  end

  @doc """
  Deletes a CompanyNewsFirm.

  ## Examples

      iex> delete_company_news_firm(company_news_firm)
      {:ok, %CompanyNewsFirm{}}

      iex> delete_company_news_firm(company_news_firm)
      {:error, %Ecto.Changeset{}}

  """
  def delete_company_news_firm(%CompanyNewsFirm{} = company_news_firm) do
    Repo.delete(company_news_firm)
  end

  def delete_company_news_firms_by_nid(nid) do
    CompanyNewsFirm
    |> where(company_news_item_id: ^nid)
    |> Repo.delete_all
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking company_news_firm changes.

  ## Examples

      iex> change_company_news_firm(company_news_firm)
      %Ecto.Changeset{source: %CompanyNewsFirm{}}

  """
  def change_company_news_firm(%CompanyNewsFirm{} = company_news_firm) do
    CompanyNewsFirm.changeset(company_news_firm, %{})
  end

  alias FastDb.ThirdSource.CompanyNewsItem


  @doc """
  Returns the page of company_news_item.

  ## Examples

      iex> page_company_news_item(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%CompanyNewsItem{}, ...]
      }
  FastDb.ThirdSource.page_company_news_item(%{page: 1, page_size: 10})
  """
  def page_company_news_item(page) do
    CompanyNewsItem
    |> preload([:firms])
    |> order_by(^[{page.order, page.by}])
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of company_news_item.

  ## Examples

      iex> list_company_news_item()
      [%CompanyNewsItem{}, ...]

  """
  def list_company_news_item do
    CompanyNewsItem
    |> preload([:firms])
    |> Repo.all()
  end

  @doc """
  Gets a single company_news_item.

  Raises `Ecto.NoResultsError` if the Company news item does not exist.

  ## Examples

      iex> get_company_news_item!(123)
      %CompanyNewsItem{}

      iex> get_company_news_item!(456)
      ** (Ecto.NoResultsError)

  """
  def get_company_news_item!(id), do: Repo.get!(CompanyNewsItem, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_company_news_item(id) do
    CompanyNewsItem
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a company_news_item.

  ## Examples

      iex> create_company_news_item(%{field: value})
      {:ok, %CompanyNewsItem{}}

      iex> create_company_news_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_company_news_item(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %CompanyNewsItem{}
    |> CompanyNewsItem.changeset(attrs_with_time)
    # |> unsafe_validate_unique([:title, :publish_time], Repo)
    |> Repo.insert()
  end

  @doc """
  Updates a company_news_item.

  ## Examples

      iex> update_company_news_item(company_news_item, %{field: new_value})
      {:ok, %CompanyNewsItem{}}

      iex> update_company_news_item(company_news_item, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_company_news_item(%CompanyNewsItem{} = company_news_item, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    company_news_item
    |> CompanyNewsItem.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a CompanyNewsItem.

  ## Examples

      iex> delete_company_news_item(company_news_item)
      {:ok, %CompanyNewsItem{}}

      iex> delete_company_news_item(company_news_item)
      {:error, %Ecto.Changeset{}}

  """
  def delete_company_news_item(%CompanyNewsItem{} = company_news_item) do
    Repo.delete(company_news_item)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking company_news_item changes.

  ## Examples

      iex> change_company_news_item(company_news_item)
      %Ecto.Changeset{source: %CompanyNewsItem{}}

  """
  def change_company_news_item(%CompanyNewsItem{} = company_news_item) do
    CompanyNewsItem.changeset(company_news_item, %{})
  end

  alias FastDb.ThirdSource.PdfPage

  @doc """
  Returns the page of pdf_pages.

  ## Examples

      iex> search_pdf_pages(%{}, %{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%PdfPage{}, ...]
      }

  """
  def search_pdf_pages(allowed_lookup \\ %{}, page) do
    allowed_lookup
    |> Enum.reduce(PdfPage,
      fn {x, y}, query ->
        if is_integer(y) or is_float(y) do
          query |> where(^[{x, y}])
        else
          x = if is_atom(x) do x else String.to_atom(x) end
          case x do
            :company_code -> query |> where([c], like(c.company_code, ^y))
            :content -> query |> where([c], like(c.content, ^y))
            :detail_code -> query |> where([c], like(c.detail_code, ^y))
            :end_page -> query |> where([c], like(c.end_page, ^y))
            :image_url -> query |> where([c], like(c.image_url, ^y))
            :src_updated_at -> query |> where([c], like(c.src_updated_at, ^y))
            :start_page -> query |> where([c], like(c.start_page, ^y))
            :type_code -> query |> where([c], like(c.type_code, ^y))
            :year -> query |> where([c], like(c.year, ^y))
            _ -> query |> where(^[{x, y}]) # equal
          end
        end
      end)
    |> order_by(^[{page.order, page.by}])
    |> Repo.paginate(page)
  end
  @doc """
  Returns the page of pdf_pages.

  ## Examples

      iex> page_pdf_pages(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%PdfPage{}, ...]
      }

  """
  def page_pdf_pages(page) do
    PdfPage
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of pdf_pages.

  ## Examples

      iex> list_pdf_pages()
      [%PdfPage{}, ...]

  """
  def list_pdf_pages do
    Repo.all(PdfPage)
  end

  @doc """
  Gets a single pdf_page.

  Raises `Ecto.NoResultsError` if the Pdf page does not exist.

  ## Examples

      iex> get_pdf_page!(123)
      %PdfPage{}

      iex> get_pdf_page!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pdf_page!(id), do: Repo.get!(PdfPage, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_pdf_page(id) do
    PdfPage
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a pdf_page.

  ## Examples

      iex> create_pdf_page(%{field: value})
      {:ok, %PdfPage{}}

      iex> create_pdf_page(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pdf_page(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %PdfPage{}
    |> PdfPage.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a pdf_page.

  ## Examples

      iex> update_pdf_page(pdf_page, %{field: new_value})
      {:ok, %PdfPage{}}

      iex> update_pdf_page(pdf_page, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pdf_page(%PdfPage{} = pdf_page, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    pdf_page
    |> PdfPage.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a PdfPage.

  ## Examples

      iex> delete_pdf_page(pdf_page)
      {:ok, %PdfPage{}}

      iex> delete_pdf_page(pdf_page)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pdf_page(%PdfPage{} = pdf_page) do
    Repo.delete(pdf_page)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pdf_page changes.

  ## Examples

      iex> change_pdf_page(pdf_page)
      %Ecto.Changeset{source: %PdfPage{}}

  """
  def change_pdf_page(%PdfPage{} = pdf_page) do
    PdfPage.changeset(pdf_page, %{})
  end

  alias FastDb.ThirdSource.PdfQnaQuestion

  @doc """
  Returns the page of pdf_qna_questions.

  ## Examples

      iex> search_pdf_qna_questions(%{}, %{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%PdfQnaQuestion{}, ...]
      }

  """
  def search_pdf_qna_questions(allowed_lookup \\ %{}, page) do
    allowed_lookup
    |> Enum.reduce(PdfQnaQuestion,
      fn {x, y}, query ->
        if is_integer(y) or is_float(y) do
          query |> where(^[{x, y}])
        else
          x = if is_atom(x) do x else String.to_atom(x) end
          case x do
            :answer -> query |> where([c], like(c.answer, ^y))
            :md5 -> query |> where([c], like(c.md5, ^y))
            :otpq -> query |> where([c], like(c.otpq, ^y))
            _ -> query |> where(^[{x, y}]) # equal
          end
        end
      end)
    |> order_by(^[{page.order, page.by}])
    |> Repo.paginate(page)
  end
  @doc """
  Returns the page of pdf_qna_questions.

  ## Examples

      iex> page_pdf_qna_questions(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%PdfQnaQuestion{}, ...]
      }

  """
  def page_pdf_qna_questions(page) do
    PdfQnaQuestion
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of pdf_qna_questions.

  ## Examples

      iex> list_pdf_qna_questions()
      [%PdfQnaQuestion{}, ...]

  """
  def list_pdf_qna_questions do
    Repo.all(PdfQnaQuestion)
  end

  @doc """
  Gets a single pdf_qna_question.

  Raises `Ecto.NoResultsError` if the Pdf qna question does not exist.

  ## Examples

      iex> get_pdf_qna_question!(123)
      %PdfQnaQuestion{}

      iex> get_pdf_qna_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pdf_qna_question!(id), do: Repo.get!(PdfQnaQuestion, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_pdf_qna_question(id) do
    PdfQnaQuestion
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a pdf_qna_question.

  ## Examples

      iex> create_pdf_qna_question(%{field: value})
      {:ok, %PdfQnaQuestion{}}

      iex> create_pdf_qna_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pdf_qna_question(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %PdfQnaQuestion{}
    |> PdfQnaQuestion.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a pdf_qna_question.

  ## Examples

      iex> update_pdf_qna_question(pdf_qna_question, %{field: new_value})
      {:ok, %PdfQnaQuestion{}}

      iex> update_pdf_qna_question(pdf_qna_question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pdf_qna_question(%PdfQnaQuestion{} = pdf_qna_question, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    pdf_qna_question
    |> PdfQnaQuestion.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a PdfQnaQuestion.

  ## Examples

      iex> delete_pdf_qna_question(pdf_qna_question)
      {:ok, %PdfQnaQuestion{}}

      iex> delete_pdf_qna_question(pdf_qna_question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pdf_qna_question(%PdfQnaQuestion{} = pdf_qna_question) do
    Repo.delete(pdf_qna_question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pdf_qna_question changes.

  ## Examples

      iex> change_pdf_qna_question(pdf_qna_question)
      %Ecto.Changeset{source: %PdfQnaQuestion{}}

  """
  def change_pdf_qna_question(%PdfQnaQuestion{} = pdf_qna_question) do
    PdfQnaQuestion.changeset(pdf_qna_question, %{})
  end
end
