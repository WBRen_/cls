defmodule FastDb.ThirdSource.Querier do
  import Ecto.Query, warn: false
  alias FastDb.Repo

  alias FastDb.ThirdSource # Repo
  alias FastDb.ThirdSource.SecretaryData
  alias FastDb.ThirdSource.IndividualResearch
  alias FastDb.ThirdSource.IndividualResearchPdf
  alias FastDb.ThirdSource.OtherPdfPicture
  alias FastDb.ThirdSource.OtherPdf
  alias FastDb.ThirdSource.Summary
  alias FastDb.ThirdSource.MD5List
  alias FastDb.ThirdSource.CompanyNewsFirm
  alias FastDb.ThirdSource.CompanyNewsItem

  @doc """
  FastDb.ThirdSource.Querier.find(FastDb.ThirdSource.IndividualResearchPdf, %{"answer": "这是%"}, %{page: 1, page_size: 10})
  """
  def find(module, allowed_lookup \\ %{}, page) do
    # allowed_lookup = %{coordinate: "15.0", id: 1}
    if allowed_lookup == %{} do
      module
    else
      allowed_lookup
      |> Enum.reduce(module,
        fn {x,y}, query ->
          # IO.puts "#{inspect x} , #{inspect y}"
          # field_query = [{x, y}] #dynamic keyword list
          # query |> where(^field_query)
          ## iex> [{:a, 1}] == [a: 1] # true
          if is_integer(y) or is_float(y) do
            query |> where(^[{x, y}])
          else
            # x =
            # if is_atom(x) do
            #   x
            # else
            #   String.to_atom(x)
            # end
            x = if is_atom(x) do x else String.to_atom(x) end
            # if String.downcase(y) == "null" do
            #   query |> where(^[{x, "null"}]) # equal
            # else
            #   y = "%#{y}%"
            case x do
              :report_title -> query |> where([c], like(c.report_title, ^y)) # like
              :title -> query |> where([c], like(c.title, ^y)) # like
              :pdf_name -> query |> where([c], like(c.pdf_name, ^y)) # like
              :tag -> query |> where([c], like(c.tag, ^y)) # like
              :answer -> query |> where([c], like(c.answer, ^y)) # like
              _ -> query |> where(^[{x, y}]) # equal
            end
            # end
          end
      end)
    end
    |> order_by(^[{page.order, page.by}])
    |> Repo.paginate(page)
  end

end