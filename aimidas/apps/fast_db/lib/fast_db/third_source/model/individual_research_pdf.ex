defmodule FastDb.ThirdSource.IndividualResearchPdf do
  use Ecto.Schema
  import Ecto.Changeset

  # update rate: 2 hours
  schema "individual_research_pdfs" do
    field :answer, :string
    field :cid, :integer
    field :date, :naive_datetime
    field :institution, :string
    field :md5, :string
    field :otpq, :map

    timestamps()
  end

  @doc false
  def changeset(individual_research_pdf, attrs) do
    individual_research_pdf
    |> cast(attrs, [:cid, :otpq, :md5, :answer, :date, :institution, :inserted_at, :updated_at])
    |> validate_required([:cid, :md5, :answer, :date, :institution])
    # |> unique_constraint(:duplicated_value!, :individual_research_pdfs_cid_md5_index)
    |> unsafe_validate_unique([:cid, :md5], FastDb.Repo, message: "cid and md5 must be unique")
  end
end
