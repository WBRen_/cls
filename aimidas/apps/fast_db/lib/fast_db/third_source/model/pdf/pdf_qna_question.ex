defmodule FastDb.ThirdSource.PdfQnaQuestion do
  use Ecto.Schema
  import Ecto.Changeset


  schema "pdf_qna_questions" do
    field :answer, :string
    field :md5, :string
    field :otpq, :map

    timestamps()
  end

  @doc false
  def changeset(pdf_qna_question, attrs) do
    pdf_qna_question
    |> cast(attrs, [:md5, :answer, :otpq, :inserted_at, :updated_at])
    |> validate_required([:md5, :answer, :otpq])
  end
end
