defmodule FastDb.ThirdSource.PdfPage do
  use Ecto.Schema
  import Ecto.Changeset


  schema "pdf_pages" do
    field :company_code, :string
    field :content, :string
    field :detail_code, :string
    field :end_page, :integer
    field :image_url, :string
    field :src_updated_at, :integer
    field :start_page, :integer
    field :type_code, :string
    field :year, :integer

    timestamps()
  end

  @doc false
  def changeset(pdf_page, attrs) do
    pdf_page
    |> cast(attrs, [:company_code, :content, :end_page, :image_url, :start_page, :year, :src_updated_at, :type_code, :detail_code, :inserted_at, :updated_at])
    |> validate_required([:company_code, :content, :end_page, :image_url, :start_page, :year, :src_updated_at, :type_code, :detail_code])
  end
end
