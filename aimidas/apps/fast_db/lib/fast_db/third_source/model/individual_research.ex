defmodule FastDb.ThirdSource.IndividualResearch do
  use Ecto.Schema
  import Ecto.Changeset

  # update rate: 2 hours
  schema "individual_researchs" do
    field :cid, :integer
    field :content, :string
    field :date, :naive_datetime
    field :level_change, :string
    field :institution, :string
    field :name, :string
    field :pdf_link, :string
    field :pdf_name, :string
    field :src_level, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(individual_research, attrs) do
    individual_research
    |> cast(attrs, [:cid, :name, :title, :date, :src_level, :level_change, :institution, :content, :pdf_link, :pdf_name, :inserted_at, :updated_at])
    |> validate_required([:cid, :name, :title, :date])
  end
end
