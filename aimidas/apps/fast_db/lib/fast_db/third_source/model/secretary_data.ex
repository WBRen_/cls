defmodule FastDb.ThirdSource.SecretaryData do
  use Ecto.Schema
  import Ecto.Changeset

  # update rate: 2 hours
  schema "secretary_datas" do
    field :answer, :string
    field :answer_time, :naive_datetime
    field :cid, :integer
    field :md5, :string
    field :question, :string
    field :question_time, :naive_datetime
    field :year, :integer

    timestamps()
  end

  @doc false
  def changeset(secretary_data, attrs) do
    secretary_data
    |> cast(attrs, [:cid, :year, :md5, :question, :question_time, :answer, :answer_time, :inserted_at, :updated_at])
    |> validate_required([:cid, :year, :md5, :question, :question_time, :answer, :answer_time])
    # |> unique_constraint(:duplicated_value!, :secretary_datas_cid_md5_index)
    |> unsafe_validate_unique([:cid, :md5], FastDb.Repo, message: "cid and md5 must be unique")
  end
end
