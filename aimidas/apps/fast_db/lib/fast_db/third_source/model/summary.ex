defmodule FastDb.ThirdSource.Summary do
  use Ecto.Schema
  import Ecto.Changeset


  schema "summaries" do
    field :abstract, :string
    field :cid, :integer
    field :name, :string
    field :report_date, :naive_datetime
    field :report_title, :string

    timestamps()
  end

  @doc false
  def changeset(summary, attrs) do
    summary
    |> cast(attrs, [:cid, :name, :report_title, :report_date, :abstract, :inserted_at, :updated_at])
    |> validate_required([:cid, :name, :report_title, :report_date])
  end
end
