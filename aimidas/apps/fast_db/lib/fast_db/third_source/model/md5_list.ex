defmodule FastDb.ThirdSource.MD5List do
  use Ecto.Schema
  import Ecto.Changeset


  schema "md5_list" do
    field :cid, :integer
    field :md5, :string

    timestamps()
  end

  @doc false
  def changeset(md5_list, attrs) do
    md5_list
    |> cast(attrs, [:cid, :md5, :inserted_at, :updated_at])
    |> validate_required([:cid, :md5])
    # |> unique_constraint(:md5)
    |> unsafe_validate_unique([:md5], FastDb.Repo, message: "must be unique")
  end
end
