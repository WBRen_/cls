defmodule FastDb.ThirdSource.CompanyNewsItem do
  use Ecto.Schema
  import Ecto.Changeset

  alias FastDb.ThirdSource.CompanyNewsFirm

  schema "company_news_item" do
    field :content, :string
    field :publish_time, :naive_datetime
    field :source, :string
    field :tag, :string
    field :title, :string

    has_many :firms, CompanyNewsFirm

    timestamps()
  end

  @doc false
  def changeset(company_news_item, attrs) do
    company_news_item
    |> cast(attrs, [:title, :publish_time, :source, :tag, :content, :inserted_at, :updated_at])
    |> validate_required([:title, :publish_time])
    |> unsafe_validate_unique([:title, :publish_time], FastDb.Repo, message: "title and publish_time must be unique")
  end
end
