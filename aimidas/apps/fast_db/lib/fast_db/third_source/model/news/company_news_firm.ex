defmodule FastDb.ThirdSource.CompanyNewsFirm do
  use Ecto.Schema
  import Ecto.Changeset


  schema "company_news_firm" do
    field :cid, :integer
    field :name, :string
    field :company_news_item_id, :integer

    timestamps()
  end

  @doc false
  def changeset(company_news_firm, attrs) do
    company_news_firm
    |> cast(attrs, [:cid, :name, :company_news_item_id, :inserted_at, :updated_at])
    |> validate_required([:cid, :name, :company_news_item_id])
  end
end
