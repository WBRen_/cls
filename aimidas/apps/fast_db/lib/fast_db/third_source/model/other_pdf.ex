defmodule FastDb.ThirdSource.OtherPdf do
  use Ecto.Schema
  import Ecto.Changeset


  schema "other_pdfs" do
    field :cid, :integer
    field :content, :string
    field :exchange_code, :string
    field :name, :string
    field :pdf_name, :string
    field :report_date, :naive_datetime
    field :report_title, :string

    timestamps()
  end

  @doc false
  def changeset(other_pdf, attrs) do
    other_pdf
    |> cast(attrs, [:cid, :name, :report_title, :report_date, :content, :pdf_name, :exchange_code, :inserted_at, :updated_at])
    |> validate_required([:cid, :name, :report_title, :report_date, :pdf_name])
  end
end
