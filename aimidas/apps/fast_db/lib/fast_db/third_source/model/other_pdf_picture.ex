defmodule FastDb.ThirdSource.OtherPdfPicture do
  use Ecto.Schema
  import Ecto.Changeset


  schema "other_pdf_pictures" do
    field :cid, :integer
    field :name, :string
    field :pdf_name, :string
    field :report_date, :naive_datetime
    field :report_title, :string
    field :exchange_code, :string

    timestamps()
  end

  @doc false
  def changeset(other_pdf_picture, attrs) do
    other_pdf_picture
    |> cast(attrs, [:cid, :name, :report_title, :report_date, :pdf_name, :exchange_code, :inserted_at, :updated_at])
    |> validate_required([:cid, :name, :report_title, :report_date, :pdf_name])
  end
end
