defmodule FastDb.Ontology do
  @moduledoc """
  The Ontology context.
  """

  import Ecto.Query, warn: false
  alias FastDb.Repo

  alias FastDb.Ontology.CompanyInfo


  @doc """
  Returns the page of company_infos.

  ## Examples

      iex> page_company_infos(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%CompanyInfo{}, ...]
      }

  """
  def page_company_infos(page) do
    CompanyInfo
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  def page_company_infos(page, company_code) do
    CompanyInfo
    |> where(company_code: ^company_code)
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of company_infos.

  ## Examples

      iex> list_company_infos()
      [%CompanyInfo{}, ...]

  """
  def list_company_infos do
    Repo.all(CompanyInfo)
  end

  @doc """
  Gets a single company_info.

  Raises `Ecto.NoResultsError` if the Company info does not exist.

  ## Examples

      iex> get_company_info!(123)
      %CompanyInfo{}

      iex> get_company_info!(456)
      ** (Ecto.NoResultsError)

  """
  def get_company_info!(id), do: Repo.get!(CompanyInfo, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_company_info(id) do
    CompanyInfo
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a company_info.

  ## Examples

      iex> create_company_info(%{field: value})
      {:ok, %CompanyInfo{}}

      iex> create_company_info(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_company_info(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %CompanyInfo{}
    |> CompanyInfo.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a company_info.

  ## Examples

      iex> update_company_info(company_info, %{field: new_value})
      {:ok, %CompanyInfo{}}

      iex> update_company_info(company_info, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_company_info(%CompanyInfo{} = company_info, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    company_info
    |> CompanyInfo.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a CompanyInfo.

  ## Examples

      iex> delete_company_info(company_info)
      {:ok, %CompanyInfo{}}

      iex> delete_company_info(company_info)
      {:error, %Ecto.Changeset{}}

  """
  def delete_company_info(%CompanyInfo{} = company_info) do
    Repo.delete(company_info)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking company_info changes.

  ## Examples

      iex> change_company_info(company_info)
      %Ecto.Changeset{source: %CompanyInfo{}}

  """
  def change_company_info(%CompanyInfo{} = company_info) do
    CompanyInfo.changeset(company_info, %{})
  end

  alias FastDb.Ontology.CompanyXbrl


  @doc """
  Returns the page of company_xbrls.

  ## Examples

      iex> page_company_xbrls(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%CompanyXbrl{}, ...]
      }

  """
  def page_company_xbrls(page) do
    CompanyXbrl
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end

  def page_company_xbrls(page, company_code) do
    CompanyXbrl
    |> where(company_code: ^company_code)
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end

  @doc """
  Returns the list of company_xbrls.

  ## Examples

      iex> list_company_xbrls()
      [%CompanyXbrl{}, ...]

  """
  def list_company_xbrls do
    Repo.all(CompanyXbrl)
  end

  @doc """
  Gets a single company_xbrl.

  Raises `Ecto.NoResultsError` if the Company xbrl does not exist.

  ## Examples

      iex> get_company_xbrl!(123)
      %CompanyXbrl{}

      iex> get_company_xbrl!(456)
      ** (Ecto.NoResultsError)

  """
  def get_company_xbrl!(id), do: Repo.get!(CompanyXbrl, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_company_xbrl(id) do
    CompanyXbrl
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a company_xbrl.

  ## Examples

      iex> create_company_xbrl(%{field: value})
      {:ok, %CompanyXbrl{}}

      iex> create_company_xbrl(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_company_xbrl(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %CompanyXbrl{}
    |> CompanyXbrl.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a company_xbrl.

  ## Examples

      iex> update_company_xbrl(company_xbrl, %{field: new_value})
      {:ok, %CompanyXbrl{}}

      iex> update_company_xbrl(company_xbrl, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_company_xbrl(%CompanyXbrl{} = company_xbrl, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    company_xbrl
    |> CompanyXbrl.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a CompanyXbrl.

  ## Examples

      iex> delete_company_xbrl(company_xbrl)
      {:ok, %CompanyXbrl{}}

      iex> delete_company_xbrl(company_xbrl)
      {:error, %Ecto.Changeset{}}

  """
  def delete_company_xbrl(%CompanyXbrl{} = company_xbrl) do
    Repo.delete(company_xbrl)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking company_xbrl changes.

  ## Examples

      iex> change_company_xbrl(company_xbrl)
      %Ecto.Changeset{source: %CompanyXbrl{}}

  """
  def change_company_xbrl(%CompanyXbrl{} = company_xbrl) do
    CompanyXbrl.changeset(company_xbrl, %{})
  end

  alias FastDb.Ontology.Term


  @doc """
  Returns the page of terms.

  ## Examples

      iex> page_terms(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%Term{}, ...]
      }

  """
  def page_terms(page) do
    Term
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of terms.

  ## Examples

      iex> list_terms()
      [%Term{}, ...]

  """
  def list_terms do
    Repo.all(Term)
  end

  @doc """
  Gets a single term.

  Raises `Ecto.NoResultsError` if the Term does not exist.

  ## Examples

      iex> get_term!(123)
      %Term{}

      iex> get_term!(456)
      ** (Ecto.NoResultsError)

  """
  def get_term!(id), do: Repo.get!(Term, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_term(id) do
    Term
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a term.

  ## Examples

      iex> create_term(%{field: value})
      {:ok, %Term{}}

      iex> create_term(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_term(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %Term{}
    |> Term.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a term.

  ## Examples

      iex> update_term(term, %{field: new_value})
      {:ok, %Term{}}

      iex> update_term(term, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_term(%Term{} = term, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    term
    |> Term.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a Term.

  ## Examples

      iex> delete_term(term)
      {:ok, %Term{}}

      iex> delete_term(term)
      {:error, %Ecto.Changeset{}}

  """
  def delete_term(%Term{} = term) do
    Repo.delete(term)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking term changes.

  ## Examples

      iex> change_term(term)
      %Ecto.Changeset{source: %Term{}}

  """
  def change_term(%Term{} = term) do
    Term.changeset(term, %{})
  end
end
