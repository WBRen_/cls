defmodule FastDb.Ontology.CompanyXbrl do
  use Ecto.Schema
  import Ecto.Changeset


  schema "company_xbrls" do
    field :abbrev, :string
    field :company_code, :string
    field :details, :map
    field :exchange_code, :string
    field :full_name, :string
    field :industry, :string
    field :src_updated_at, :date

    timestamps()
  end

  @doc false
  def changeset(company_xbrl, attrs) do
    company_xbrl
    |> cast(attrs, [:company_code, :abbrev, :exchange_code, :full_name, :industry, :src_updated_at, :details])
    |> validate_required([:company_code, :exchange_code, :src_updated_at, :details])
    # |> unique_constraint(:duplicated_value!, name: :company_xbrls_company_code_exchange_code_index)
    |> unsafe_validate_unique([:company_code, :exchange_code], FastDb.Repo, message: "company_code and exchange_code must be unique")
  end
end
