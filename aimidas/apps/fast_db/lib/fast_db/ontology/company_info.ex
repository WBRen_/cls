defmodule FastDb.Ontology.CompanyInfo do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder,
          only: [:id, :abbrev_name, :company_code, :details, :full_name,
                 :industry, :source, :info_udpated_at, :inserted_at,
                 :updated_at]}
  schema "company_infos" do
    field :abbrev_name, :string
    field :company_code, :string
    field :exchange_code, :string
    field :details, :map
    field :full_name, :string
    field :industry, :string
    field :season, :integer
    field :source, :string
    field :info_updated_at, :date
    field :year, :integer
    field :stock_name, :string
    field :other_names, :string

    timestamps()
  end

  @doc false
  def changeset(company_info, attrs) do
    company_info
    |> cast(attrs, [:company_code, :year, :season, :industry, :source, :exchange_code,
                    :info_updated_at, :abbrev_name, :full_name, :details])
    |> validate_required([:company_code, :year, :abbrev_name, :full_name])
    # |> unique_constraint(:duplicated_value!, name: :company_infos_company_code_exchange_code_year_index)
    |> unsafe_validate_unique([:company_code, :exchange_code, :year], FastDb.Repo, message: "company_code, exchange_code and year must be unique")
  end
end
