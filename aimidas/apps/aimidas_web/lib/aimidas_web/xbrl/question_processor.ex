defmodule AimidasWeb.Xbrl.QuestionProcessor do
  alias AimidasWeb.Xbrl.DataKeyMapping
  alias AimidasWeb.UnitTransform.UnitProcessor
  alias AimidasWeb.UnitTransform.UnitKeyMapping
  import Number.Delimit

  defp season_map(season) do
    table =
      %{
        "0" => "年报",
        "1" => "一季报",
        "2" => "半年报",
        "3" => "三季报"
      }
    table[season]
  end

  defguard is_blank(value) when value == "0" or
                                value == "-" or
                                value == "" or
                                is_nil(value)

  defguard is_scale(value) when value == "总资产" or
                                value == "总负债" or
                                value == "利润总额"

  def build_answer(content, context) do
    %{
      answer: content,
      otpq: context
    }
  end

  def process_xbrl_question(%{type: "OPQ"} = params, xbrl) do
    year =
      xbrl.details
      |> get_in(["年报", "latest_year"])
      |> String.to_integer()

    params
    |> Map.put(:T, [%{text: "#{year}年", year: year, nlp_reason: "exact", quarter: 0}])
    |> Map.put(:type, "OTPQ")
    |> process_xbrl_question(xbrl)
  end
  def process_xbrl_question(%{type: "OTP", Q: [%{value: "quantitive"}]} = params, xbrl) do
    params
    |> Map.put(:type, "OTPQ")
    |> process_xbrl_question(xbrl)
  end

  def process_xbrl_question(%{type: "OTPQ", P: [%{value: "股价"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code, code: code}],
      T: [%{year: year, quarter: quarter, time: time}],
      P: [%{value: p_key}]
    } = params
      # |> IO.inspect(label: ">>>> GET IN:")
      {:ok, result} = AimidasWeb.Stock.Stock.analyze(ex_code, code)
      result
      |> assemble_xbrl_answer(params, xbrl)
  end

  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: "诚信情况"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        "年报",
        "诚信情况",
        to_string(year)
      ]

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl)
  end

  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: p_value}], Q: [%{value: "quantitive"}]} = params, xbrl)
    when is_scale(p_value) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    time_path =
      [
        "年报",
        "公司规模",
        "update_time"
      ]

    time =
      xbrl.details
      |> get_in(time_path)

    key_path =
      [
        "年报",
        "公司规模",
        p_value
      ]

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl, time)
  end

  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: "十大股东"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        "年报",
        "十大股东",
        "十大股东"
      ]
    time =
      xbrl.details
      |> get_in(["年报", "十大股东", "update_time"])

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl, time)
  end

  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SH"}], P: [%{value: "十大流通股东"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    params =
      params
      |> Map.put(:P, [%{value: "十大股东"}])
      |> process_xbrl_question(xbrl)
  end

  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SH"}], P: [%{value: "十大股东"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        season_map("#{quarter}"),
        "十大股东",
        to_string(year)
      ]

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl)
  end
  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: "十大流通股东"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        "年报",
        "十大股东",
        "十大流通股东"
      ]
    time =
      xbrl.details
      |> get_in(["年报", "十大股东", "update_time"])

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl, time)
  end
  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: "分红"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        season_map("#{quarter}"),
        "分红",
        to_string(year)
      ]

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl)
  end
  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SH"}], P: [%{value: "分红"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        season_map("#{quarter}"),
        "分红",
        to_string(year)
      ]
      |> IO.inspect(label: ">>>>path:")

    xbrl.details
      |> get_in(key_path)
      |> IO.inspect(label: ">>>>OTPQ GET IN:")
      |> assemble_xbrl_answer(params, xbrl)
  end
  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: "再融资"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        season_map("#{quarter}"),
        "融资情况",
        to_string(year)
      ]

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl)
  end
  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: "利润"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        "年报",
        "利润",
        to_string(year),
        season_map("#{quarter}")
      ]

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl)
  end
  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: "营业收入"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        "年报",
        "营业收入",
        season_map("#{quarter}"),
        to_string(year)
      ]

    xbrl.details
      |> get_in(key_path)
      # |> IO.inspect(label: ">>>> GET IN:")
      |> assemble_xbrl_answer(params, xbrl)
  end
  def process_xbrl_question(%{type: "OTPQ", O: [%{exchange_code: "SZ"}], P: [%{value: "净利润"}], Q: [%{value: "quantitive"}]} = params, xbrl) do
    params =
      params
      |> Map.put(:P, [%{value: "利润"}])
      |> process_xbrl_question(xbrl)
  end
  def process_xbrl_question(%{type: "OTPQ", Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}],
      P: [%{value: p_key}]
    } = params

    key_path =
      [
        season_map("#{quarter}"),
        DataKeyMapping.key_map(ex_code, p_key),
        to_string(year),
        p_key
      ]

    # IO.inspect(key_path, label: "key_path", pretty: true)

    xbrl.details
    |> get_in(key_path)
    # |> IO.inspect(label: ">>>> GET IN:")
    |> assemble_xbrl_answer(params, xbrl)
  end
  def process_xbrl_question(%{type: "OTPQ", Q: [%{value: "yoy_ratio"}]} = params, xbrl) do
    %{
      O: [%{exchange_code: ex_code}],
      T: [%{year: year, quarter: quarter}|_],
      P: [%{value: p_key}]
    } = params

    key_path_1 =
      [
        season_map("#{quarter}"),
        DataKeyMapping.key_map(ex_code, p_key),
        to_string(year),
        p_key
      ]

    key_path_2 =
      [
        season_map("#{quarter}"),
        DataKeyMapping.key_map(ex_code, p_key),
        to_string(year-1),
        p_key
      ]

    # IO.inspect(key_path_1, label: "key_path_1", pretty: true)
    # IO.inspect(key_path_2, label: "key_path_2", pretty: true)

    result1 =
    xbrl.details
    |> get_in(key_path_1)

    result2 =
    xbrl.details
    |> get_in(key_path_2)

    assemble_xbrl_answer(result1, result2, params, xbrl)

  end

  #拼装答案
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: p_value}]} = params, xbrl, time)
  when is_scale(p_value) and is_blank(result) do
  %{
    O: [%{value: o_value, exchange_code: ex_code, code: code}],
    T: [%{year: t_year}]
  } = params

  answer =
    "#{o_value}的#{p_value}数据暂无，暂时无法回答该问题\n交易所公示数据最新更新于#{xbrl.src_updated_at}"

  {:ok,
    %{
      answer: answer,
      otpq: params
    }
  }
end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: p_value}]} = params, xbrl, time)
    when is_scale(p_value) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    answer =
      "截止于#{time}，#{o_value}的#{p_value}为：#{result}(元)\n交易所公示数据最新更新于#{xbrl.src_updated_at}"

    {:ok,
      %{
        answer: answer,
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{P: [%{value: "股价"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    stock =

    {:ok,
      %{
        answer: "#{o_value}在昨日收盘于#{result["昨日收盘价"]}元，今日开盘价#{result["今日开盘价"]}元，当前价格#{result["当前价格"]}元，涨幅#{result["涨幅"]}\n数据更新于#{result["日期"]} #{result["时间"]}。",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(nil, %{P: [%{value: "股价"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{o_value}股价信息暂无",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(nil, %{O: [%{exchange_code: "SZ"}], P: [%{value: "诚信情况"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{o_value}在#{t_year}诚信情况无异常\n交易所公示数据最新更新于#{xbrl.src_updated_at}。",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "诚信情况"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "在#{result["公告日期"]}，#{o_value}的诚信处罚信息如下：\n处罚内容：#{result["处罚内容"]}\n处罚原因：#{result["处罚原因"]}\n处罚对象：#{result["处罚对象"]}\n处罚类型：#{result["处罚类型"]}\n处罚部门：#{result["处罚部门"]}\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(nil, %{O: [%{exchange_code: "SZ"}], P: [%{value: "十大股东"}]} = params, xbrl, _time) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{o_value}十大股东信息未公布\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "十大股东"}]} = params, xbrl, time) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    text =
      ""
      |> assemble_sz_shareholder(result, 1)
      # |> IO.inspect(label: ">>>>>>>>>>>十大股东", pretty: true)

    {:ok,
      %{
        answer: "截止于#{time}，#{o_value}的十大股东信息如下：#{text}\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SH"}], P: [%{value: "十大股东"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year, quarter: quarter}],
      P: [%{value: p_value}]
    } = params

    season = season_map("#{quarter}")

    text =
      ""
      |> assemble_sh_shareholder(result, 1)
      # |> IO.inspect(label: ">>>>>>>>>>>十大股东", pretty: true)

    {:ok,
      %{
        answer: "#{o_value}在#{t_year}年的#{season}中十大股东信息如下：#{text}\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(nil, %{O: [%{exchange_code: "SZ"}], P: [%{value: "十大流通股东"}]} = params, xbrl, _time) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{o_value}十大流通股东信息未公布\n交易所公示数据最新更新于#{xbrl.src_updated_at}。",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "十大流通股东"}]} = params, xbrl, time) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    text =
      ""
      |> assemble_sz_shareholder(result, 1)
      # |> IO.inspect(label: ">>>>>>>>>>>十大流通股东", pretty: true)
    {:ok,
      %{
        answer: "截止于#{time}，#{o_value}的十大流通股东信息如下：#{text}\n交易所公示数据最新更新于#{xbrl.src_updated_at}。",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(nil, %{O: [%{exchange_code: "SZ"}], P: [%{value: "分红"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{o_value}#{t_year}年未进行分红\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "分红"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{result["分红年度"]}，#{o_value}以#{result["分红方案"]}进行分红，新增股份于#{result["新增股份上市日"]}上市。股权登记日为#{result["股权登记日"]}，除权基准日为#{result["除权基准日"]}。\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(nil, %{O: [%{exchange_code: "SH"}], P: [%{value: "分红"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{o_value}#{t_year}年未进行分红\n交易所公示数据最新更新于#{xbrl.src_updated_at}。",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SH"}], P: [%{value: "分红"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{result["分红年度"]}，#{o_value}以#{result["分红方案"]}进行分红，新增股份于#{result["新增股份上市日"]}上市。股权登记日为#{result["股权登记日"]}，除权基准日为#{result["除权基准日"]}。\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(nil, %{O: [%{exchange_code: "SZ"}], P: [%{value: "再融资"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{o_value}#{t_year}年未进行融资\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "再融资"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{result["公告日期"]}，#{o_value}以发行价格#{result["发行价格(元/股)"]}(元/股)进行融资，再融资类型为#{result["再融资类型"]}，实际募集资金#{result["实际募集资金(万元)"]}(万元)，总发行数量#{result["总发行数量(万股)"]}(万股)。#{result["发行对象"]}\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "利润"}]} = params, xbrl)
    when is_blank(result) do

    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year, quarter: quarter}],
      P: [%{value: p_value}]
    } = params

    season = DataKeyMapping.key_map("SZ", "#{quarter}")

    {:ok,
      %{
        answer: "#{o_value}在#{t_year}年#{season}中的利润暂时未收录，暂时无法回答该问题\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "利润"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year, quarter: quarter}],
      P: [%{value: p_value}]
    } = params

    season = DataKeyMapping.key_map("SZ", "#{quarter}")

    {:ok,
      %{
        answer: "#{o_value}在#{t_year}年#{season}中的归属于上市公司股东净利润为#{result}亿元\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "营业收入"}]} = params, xbrl)
  when is_blank(result) do

  %{
    O: [%{value: o_value, exchange_code: ex_code, code: code}],
    T: [%{year: t_year, quarter: quarter}],
    P: [%{value: p_value}]
  } = params

  season = DataKeyMapping.key_map("SZ", "#{quarter}")

  {:ok,
    %{
      answer: "#{o_value}在#{t_year}年#{season}中的营业收入暂时未收录，暂时无法回答该问题\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
      otpq: params
    }
  }
end
def assemble_xbrl_answer(result, %{O: [%{exchange_code: "SZ"}], P: [%{value: "营业收入"}]} = params, xbrl) do
  %{
    O: [%{value: o_value, exchange_code: ex_code, code: code}],
    T: [%{year: t_year, quarter: quarter}],
    P: [%{value: p_value}]
  } = params

  season = DataKeyMapping.key_map("SZ", "#{quarter}")

  {:ok,
    %{
      answer: "#{o_value}在#{t_year}年#{season}中的营业收入为#{result}亿元\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
      otpq: params
    }
  }
end
  def assemble_xbrl_answer(result, %{} = params, xbrl)
    when is_blank(result) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year}]
    } = params

    {:ok,
      %{
        answer: "#{o_value}#{t_year}年暂无此数据，暂时无法回答该问题\n交易所公示数据最新更新于#{xbrl.src_updated_at}",
        otpq: params
      }
    }
  end
  def assemble_xbrl_answer(result, %{Q: [%{value: "quantitive"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{text: t_year}],
      P: [%{value: p_value}]
    } = params

    result =
      result
      |> UnitProcessor.form_value(ex_code, p_value)

    answer =
      "#{o_value}#{t_year}的#{p_value}是 #{result}\n交易所公示数据最新更新于#{xbrl.src_updated_at}"
      |> build_answer(params)

    {:ok, answer}
  end

  def assemble_xbrl_answer(result1, _result2, %{Q: [%{value: "yoy_ratio"}]} = params, xbrl)
    when is_blank(result1) do
      %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year, quarter: quarter}|_],
      P: [%{value: p_value}]
      } = params

      season = season_map("#{quarter}")

      answer =
        "#{o_value}#{t_year}年#{season}的#{p_value}不存在，同比无法计算。\n交易所公示数据最新更新于#{xbrl.src_updated_at}"
        |> build_answer(params)

      {:ok, answer}
  end

  def assemble_xbrl_answer(result1, result2, %{Q: [%{value: "yoy_ratio"}]} = params, xbrl)
    when is_blank(result2) do
      %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year, quarter: quarter}|_],
      P: [%{value: p_value}]
      } = params

      season = season_map("#{quarter}")

      result1 =
        result1
        |> UnitProcessor.form_value(ex_code, p_value)

      answer =
        "#{o_value}#{t_year}年#{season}的#{p_value}是#{result1}，#{o_value}#{t_year-1}年#{season}的#{p_value}不存在，同比无法计算。\n交易所公示数据最新更新于#{xbrl.src_updated_at}"
        |> build_answer(params)

      {:ok, answer}
  end

  def assemble_xbrl_answer(result1, result2, %{Q: [%{value: "yoy_ratio"}]} = params, xbrl) do
    %{
      O: [%{value: o_value, exchange_code: ex_code, code: code}],
      T: [%{year: t_year, quarter: quarter}|_],
      P: [%{value: p_value}]
    } = params

    season = season_map("#{quarter}")

    result_num1 =
      result1
      # # |> String.replace(",", "")
      # |> to_string()
      # |> String.to_integer()

    result_num2 =
      result2
      # # |> String.replace(",", "")
      # |> to_string()
      # |> String.to_integer()

    {num1, num1_type, field_unit} =
      result_num1
      |> UnitProcessor.to_value_tuple(ex_code, p_value)
    {num2, _num2_type, _} =
      result_num2
      |> UnitProcessor.to_value_tuple(ex_code, p_value)
    # field_unit = UnitKeyMapping.key_map(ex_code, p_value)
    #绘图数据
    data =
      %{
        "type": "yty",
        "title": "#{o_value}#{p_value}同比表",
        "keys": ["x", "y"],
        "units": ["Year", "#{num1_type}#{field_unit}"],
        "values": %{
            "x": [t_year-1, t_year],
            "y": [num2, num1]
        }
      }

    ratio =
      ((result_num1 - result_num2) / result_num2) * 100
      |> Float.round(2)

    result1 =
      result1
      |> UnitProcessor.form_value(ex_code, p_value)

    result2 =
      result2
      |> UnitProcessor.form_value(ex_code, p_value)

    answer =
      "#{o_value}#{t_year},#{t_year-1}年#{season}的#{p_value}分别是 #{result1},#{result2}, 同比增长#{ratio}%\n交易所公示数据最新更新于#{xbrl.src_updated_at}"
      |> build_answer(params)

    {:ok, answer, data}
  end

  def assemble_sz_shareholder(text, value, item) do
    if item <= 10 do
      now = value["#{item}"]
      text =
        text<>"\n#{item}-股东名称:#{now["股东名称"]}，持股数量:#{now["持股数量"]}股，持股比例:#{now["持股比例"]}，股份性质:#{now["股份性质"]}。"
        |> assemble_sz_shareholder(value, item + 1)
      else
        text
    end
  end

  def assemble_sh_shareholder(text, value, item) do
    if item <= 10 do
      now = value["#{item}"]
      text =
        text<>"\n#{item}-股东名称:#{now["股东名称"]}，持股数量:#{now["持股数量"]}股，持股比例:#{now["持股比例"]}%。"
        |> assemble_sh_shareholder(value, item + 1)
      else
        text
    end
  end
end
