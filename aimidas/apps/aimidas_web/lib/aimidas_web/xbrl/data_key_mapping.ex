defmodule AimidasWeb.Xbrl.DataKeyMapping do
  
  alias Aimidas.Xbrl.AllFields, as: XbrlFields

  def key_map(exchange_code, key) do
    XbrlFields.field_infos()
    |> get_in([exchange_code, key, :group])
  end
  
  def field_info(exchange_code, field) do
    XbrlFields.field_infos()
    |> get_in([exchange_code, field])
  end

  def sh_field_info(field), do: field_info("SH", field)
  def sz_field_info(field), do: field_info("SZ", field)
end
