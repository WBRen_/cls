defmodule AimidasWeb.UnitTransform.UnitProcessor do
  alias AimidasWeb.Xbrl.DataKeyMapping
  alias Aimidas.Xbrl.OutConverter, as: Converter

  def num_transform(num) do
    Converter.to_smart_float(num)
  end

  def to_value_tuple(value, exchange_code, p) do
    field_info = DataKeyMapping.field_info(exchange_code, p)

    to_value(field_info, value)
  end

  def form_value(value, exchange_code, p) do
    {converted_value, value_unit, field_unit} = 
      to_value_tuple(value, exchange_code, p)
    "#{converted_value}#{value_unit}#{field_unit}"
  end

  def to_value(nil, value) do
    {value, "", ""}
  end
  def to_value(field_info, value) do
    {converted_value, value_unit} =
      Converter
      |> apply(field_info.out_converter, [value])

    {converted_value, value_unit, field_info.unit}
  end
end
