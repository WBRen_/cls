defmodule AimidasWeb.Router do
  use AimidasWeb, :router
  use Plug.ErrorHandler

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    # plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :ueberauth do
    plug Ueberauth
  end

  pipeline :guardian do
    # plug Guardian.Plug.Pipeline, module: Aimdimas.Guardian, error_handler: AimidasWeb.PageController
    # plug Guardian.Plug.VerifyHeader, claims: %{typ: "access"}
  end

  pipeline :graph do
    plug AimidasWeb.Graph.Context
  end

  pipeline :api do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    plug :put_secure_browser_headers
  end

  # pipeline :auth do
  #   Ueberauth.plug "/auth"
  # end

  scope "/", AimidasWeb do
    pipe_through :browser # Use the default browser stack

    # get "/", PageController, :index

    # resources "/users", UserController
  end

  scope "/auth", AimidasWeb do
    pipe_through [:browser, :ueberauth]

    get "/logout", AuthenticationController, :delete, as: :auth
    get "/:provider", AuthenticationController, :request, as: :auth
    get "/:provider/callback", AuthenticationController, :callback, as: :auth
    post "/:provider/callback", AuthenticationController, :callback, as: :auth
  end

  scope "/graphql" do
    pipe_through [:api, :guardian, :graph]

    forward "/", Absinthe.Plug, schema: AimidasWeb.Schema
  end

  scope "/graphiql" do
    pipe_through [:api, :guardian, :graph]

    forward "/", Absinthe.Plug.GraphiQL, schema: AimidasWeb.Schema
  end

  # 微信网页验证
  scope "/xy0acNufHl.txt", AimidasWeb do
    pipe_through :browser
    get "", WechatConfigController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", AimidasWeb do
  #   pipe_through :api
  # end

  defp handle_errors(conn, %{kind: kind, reason: reason, stack: stacktrace}) do
    conn =
      conn
      |> Plug.Conn.fetch_cookies()
      |> Plug.Conn.fetch_query_params()

    params =
      case conn.params do
        %Plug.Conn.Unfetched{aspect: :params} -> "unfetched"
        other -> other
      end

    occurrence_data = %{
      "request" => %{
        "cookies" => conn.req_cookies,
        "url" => "#{conn.scheme}://#{conn.host}:#{conn.port}#{conn.request_path}",
        "user_ip" => List.to_string(:inet.ntoa(conn.remote_ip)),
        "headers" => Enum.into(conn.req_headers, %{}),
        "method" => conn.method,
        "params" => params,
      }
    }

    Rollbax.report(kind, reason, stacktrace, _custom_data = %{}, occurrence_data)
  end
end
