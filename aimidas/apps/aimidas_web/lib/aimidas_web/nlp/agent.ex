defmodule AimidasWeb.NLP.Agent do
  # use Appsignal.Instrumentation.Decorators

  @nlp_url Application.get_env(:aimidas_web, :nlp_url)
  # @nlp_url "http://prod2.aimidas.com:19090/analyze/"

  # @decorate transaction_event("nlp_agent")
  def analyze(query, context) do
    # IO.inspect(query, label: "[AimidasWeb.NLP.Agent.analyze] query ", pretty: true)
    @nlp_url
    # |> IO.inspect(label: "[AimidasWeb.NLP.Agent.analyze] url ", pretty: true)
    |> HTTPotion.post([body: query_to_json(query, context), headers: headers()])
    |> handle_response()
    # |> IO.inspect(label: "[AimidasWeb.NLP.Agent.analyze] resposne ", pretty: true)
  end

  def build_query(query, context) do
    %{
      question: query,
      context: context
    }
  end

  def query_to_json(query, context) do
    query
    |> build_query(context)
    |> Poison.encode!()
    # |> IO.inspect(label: "[发送给NLP端的数据] ： ", pretty: true)

  end

  def headers do
    [
      "Content-Type": "application/json",
      "Accept": "application/json"
    ]
  end

  # @decorate transaction_event()
  def handle_response(%HTTPotion.ErrorResponse{} = response) do
    Rollbax.report_message(:error,
      "NLP Server Error",
      _custom_data = %{},
      %{"HTTPotion.ErrorMessage" => response.message})

    {
      :error,
      response
    }
  end

  # @decorate transaction_event()
  def handle_response(%{} = response) do
    {
      :ok,
      response.body
      |> Poison.decode!()
      |> to_atom_map
    }
  end

  # @decorate transaction_event()
  def to_atom_map(%{} = json_map) do
    json_map
    |> Map.new(fn {k, v} -> {String.to_atom(k), to_atom_map(v)} end)
  end
  def to_atom_map([]), do: []
  def to_atom_map([head|tail]) do
    [to_atom_map(head)] ++ to_atom_map(tail)
  end
  def to_atom_map(data), do: data
end
