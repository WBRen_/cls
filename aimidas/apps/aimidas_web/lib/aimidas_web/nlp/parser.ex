defmodule AimidasWeb.NLP.Parser do

  alias AimidasWeb.NLP.Agent, as: NAgent

  def parse(query, context) do
    {:ok, nlp} =
      query
      |> NAgent.analyze(context)

    nlp
  end
end
