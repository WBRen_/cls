defmodule AimidasWeb.Stock.Stock do
  # use Appsignal.Instrumentation.Decorators

  @stock_url "http://hq.sinajs.cn/?list="
  # @nlp_url "http://prod2.aimidas.com:19090/analyze/"

  # @decorate transaction_event("nlp_agent")
  def analyze(ex_code, code) do
    ex_code = String.downcase(ex_code)
    # IO.inspect(query, label: "[AimidasWeb.NLP.Agent.analyze] query ", pretty: true)
    @stock_url <> ex_code <> code
    # |> IO.inspect(label: "[AimidasWeb.NLP.Agent.analyze] url ", pretty: true)
    |> HTTPotion.get([headers: headers()])
    |> handle_response()
    # |> IO.inspect(label: "[AimidasWeb.NLP.Agent.analyze] resposne ", pretty: true)
  end

  def get_last_px(ex_code, code) do
    IO.inspect(ex_code, label: ">>>>>> ex_code")
    IO.inspect(code, label: ">>>>>> code")
    ex_code = String.downcase(ex_code)
    # IO.inspect(query, label: "[AimidasWeb.NLP.Agent.analyze] query ", pretty: true)
    @stock_url <> ex_code <> code
    # |> IO.inspect(label: "[AimidasWeb.NLP.Agent.analyze] url ", pretty: true)
    |> HTTPotion.get([headers: headers()])
    |> IO.inspect(label: "获取股价 ", pretty: true)
    |> handle_response_without()
    # |> IO.inspect(label: "[AimidasWeb.NLP.Agent.analyze] resposne ", pretty: true)
  end

  def craw(ex_code, code) do
    ex_code = String.downcase(ex_code)
    url = @stock_url <> ex_code <> code
    # IO.inspect("BasicInfo Download >> URL: #{url}")
    response = HTTPotion.get url
    response
    |> handle_response()
    # |> IO.inspect(label: "BasicInfo Download >> RESPONSE:")
  end

  def build_query(query, context) do
    %{
      question: query,
      context: context
    }
  end

  def query_to_json(query, context) do
    query
    |> build_query(context)
    |> Poison.encode!()
    # |> IO.inspect(label: "[发送给NLP端的数据] ： ", pretty: true)

  end

  def headers do
    [
      "Content-Type": "application/json",
      "Accept": "application/json"
    ]
  end

  # @decorate transaction_event()
  def handle_response(%HTTPotion.ErrorResponse{} = response) do
    Rollbax.report_message(:error,
      "Stock Server Error",
      _custom_data = %{},
      %{"HTTPotion.ErrorMessage" => response.message})

    {
      :error,
      response
    }
  end

  # @decorate transaction_event()
  def handle_response(%{} = response) do
    {
      :ok,
      response.body
      # |> Poison.decode!()
      |> String.chunk(:valid)
      |> to_normal
    }
  end

  def handle_response_without(%{} = response) do
    {
      :ok,
      response.body
      # |> Poison.decode!()
      |> String.chunk(:valid)
      |> to_normal_without
    }
  end

  def to_normal_without(value) do
    col_values =
    String.split(List.last(value), ",")
    names = ["股票名字", "今日开盘价", "昨日收盘价", "当前价格", "今日最高价", "今日最低价", "竞买价，即“买一”报价", "竞卖价，即“卖一”报价", "成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百", "成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万",
    "“买一”申请股数",
    "“买一”报价",
    "“买二”申请股数",
    "“买二”报价",
    "“买三”申请股数",
    "“买三”报价",
    "“买四”申请股数",
    "“买四”报价",
    "“买五”申请股数",
    "“买五”报价",
    "“卖一”申报股数",
    "“卖一”报价",
    "“卖二”申报股数",
    "“卖二”报价",
    "“卖三”申报股数",
    "“卖三”报价",
    "“卖四”申报股数",
    "“卖四”报价",
    "“卖五”申报股数",
    "“卖五”报价",
    "日期",
    "时间",
    "空"]

    values = Stream.zip(names, col_values)
              |> Enum.into(%{})
    if values["当前价格"] == "0.000" do
      values
      |> Map.replace!("当前价格", values["昨日收盘价"])
    end
    values
  end



  # @decorate transaction_event()
  def to_normal(value) do
    col_values =
    String.split(List.last(value), ",")
    names = ["股票名字", "今日开盘价", "昨日收盘价", "当前价格", "今日最高价", "今日最低价", "竞买价，即“买一”报价", "竞卖价，即“卖一”报价", "成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百", "成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万",
    "“买一”申请股数",
    "“买一”报价",
    "“买二”申请股数",
    "“买二”报价",
    "“买三”申请股数",
    "“买三”报价",
    "“买四”申请股数",
    "“买四”报价",
    "“买五”申请股数",
    "“买五”报价",
    "“卖一”申报股数",
    "“卖一”报价",
    "“卖二”申报股数",
    "“卖二”报价",
    "“卖三”申报股数",
    "“卖三”报价",
    "“卖四”申报股数",
    "“卖四”报价",
    "“卖五”申报股数",
    "“卖五”报价",
    "日期",
    "时间",
    "空"]

    values = Stream.zip(names, col_values)
              |> Enum.into(%{})
    if values["当前价格"] == "0.000" do
      values
      |> Map.replace!("当前价格", values["昨日收盘价"])
    end
    {now_price, _} = values["当前价格"] |> Float.parse
    {end_price, _} = values["昨日收盘价"] |> Float.parse
    ratio = 0
    if end_price != 0 do
      ratio = ((now_price - end_price) / end_price) * 100
        |> Float.round(2)
    end
    values = Map.put(values, "涨幅", "#{ratio}%")
  end

  def to_atom_map(%{} = json_map) do
    json_map
    |> Map.new(fn {k, v} -> {String.to_atom(k), to_atom_map(v)} end)
  end
  def to_atom_map([]), do: []
  def to_atom_map([head|tail]) do
    [to_atom_map(head)] ++ to_atom_map(tail)
  end
  def to_atom_map(data), do: data
end
