defmodule AimidasWeb.Graph.Middlewares.LoginRequired do
  @moduledoc """
  LogiinRequired 登录验证的GraphQL中间件
  用于需要已登录后才能使用的接口的保护
  """

  @behaviour Absinthe.Middleware
  alias Absinthe.Resolution

  def call(resolution, _config) do
    case resolution.context do
      %{current_user: _} ->
        resolution
      _ ->
        resolution
        |> Resolution.put_result({:error, "unauthenticated"})
    end
  end
end
