defmodule AimidasWeb.Graph.Mutations.PortfolioMutation do
  @moduledoc """
  投资组合修改服务
  """

  alias Aimidas.Stocks

  import AimidasWeb.ErrorHelpers
  import Ecto.Changeset, only: [traverse_errors: 2]
  alias Ecto.Changeset

  def add_portfolio(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]

    %{
      user_id: current_user.id,
      name: input[:name] |> String.trim()
    }
    |> Stocks.create_portfolio()
    |> response()
  end

  def update_portfolio(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]

    update_attrs = %{name: input[:name]}
    user_id = current_user.id
    id = input[:id]

    result = with {:ok, portfolio} <- Stocks.get_user_portfolio(user_id, id) do
                  Stocks.update_portfolio(portfolio, update_attrs)
    end

    response(result)
  end

  def delete_portfolio(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]

    user_id = current_user.id
    id = input[:id]

    result = with {:ok, portfolio} <- Stocks.get_user_portfolio(user_id, id) do
                  Stocks.delete_portfolio(portfolio)
    end

    response(result)
  end

  def add_portfolio_item(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]

    user_id = current_user.id
    p_id = input[:portfolio_id]
    stock_id = input[:stock_id]

    result = with {:ok, portfolio} <- Stocks.get_user_portfolio(user_id, p_id),
                  {:ok, stock} <- Stocks.get_stock(stock_id)
      do
      %{
        user_id: current_user.id,
        portfolio_id: portfolio.id,
        stock_id: stock.id
      }
      |> Stocks.create_portfolio_item()
    end

    response(result)
  end

  def delete_portfolio_item(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]

    user_id = current_user.id
    portfolio_id = input[:portfolio_id]
    stock_id = input[:stock_id]

    result = with {:ok, portfolio_item} <-
              Stocks.get_user_portfolio_item(user_id, portfolio_id, stock_id)
    do
      Stocks.delete_portfolio_item(portfolio_item)
    end

    response(result)
  end

  def response({:ok, _} = result) do
    result
  end
  def response({:error, %Changeset{} = changeset}) do
    {:error, message: changeset |> traverse_errors(&translate_error/1)}
  end
  def response({:error, message}) do
    {:error, message: message}
  end
end
