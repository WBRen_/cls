defmodule AimidasWeb.Graph.Mutations.ImportProQnaQuestions do
  @doc """
  这是个临时开给小赖的接口，记得适时删除
  """

  alias Mix.Tasks.Aimidas.Import.ProQnaQuestions


  def import(_parent, args, _resolution) do
    input = args[:input]
    data = input[:data] # string

    data
    # |> IO.inspect
    |> Poison.decode!()
    |> ProQnaQuestions.do_import()
    {
      :ok,
      %{message: "success"}
    }
  end

  def delete(_parent, args, _resolution) do
    input = args[:input]
    md5s = input[:md5s] # string

    {num, _} =
    md5s
    # |> IO.inspect
    |> Poison.decode!()
    |> ProQnaQuestions.delete_pro_qna_questions()
    # |> IO.inspect

    {
      :ok,
      %{message: "delete success, count: #{num}"}
    }
  end
end
