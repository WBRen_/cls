defmodule AimidasWeb.Graph.Types.Question do
  @moduledoc """
  问答
  """

#  use AimidasWeb, :schema

  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo

  alias AimidasWeb.Graph.Resolvers.QuestionResolver
  alias AimidasWeb.Graph.Middlewares.LoginRequired

  @desc "问题"
  object :question do
    @desc "问题唯一ID（不针对用户）"
    field :statistics_question_id, :integer

    @desc "问提ID（用户唯一）"
    field :id, :id

    @desc "提问用户"
    field :user, :user, resolve: assoc(:user)

    @desc "问题组"
    field :question_group, :question_group, resolve: assoc(:question_group)

    @desc "问题内容"
    field :query, :string

    @desc "问题回答"
    field :answer, :answer

    @desc "绘图数据"
    field :data, :string

    @desc "上下文"
    field :context, :string

    @desc "相关问题"
    field(:related_question, list_of(:string))
  end

  @desc "问题输入"
  input_object :question_input do
    @desc "问题 ID"
    field :id, :integer

    @desc "所在会话 ID"
    field :question_group_id, :integer

    @desc "所属用户ID"
    field :user_id, :integer

    @desc "提问"
    field :query, non_null(:string)

    @desc "上下文"
    field :context, :string
  end

  @desc "回答"
  object :answer do
    field :id, :id
    field :content, :string
    field :content_for_show, :string
    field :pdf_image_urls, list_of(:string)
    field :pdf_image_urls_detail, list_of(:string)
  end


  object :question_queries do
    field(:question, :question) do
      arg :input, non_null(:question_input)
      middleware LoginRequired
      resolve(&QuestionResolver.question/3)
    end
  end
end
