defmodule AimidasWeb.Graph.Types.QuestionGroupList do
  @moduledoc """
  QuestionGroup 话题的GraphQL类型定义
  """
  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo

  alias AimidasWeb.Graph.Resolvers.QuestionListResolver
  alias AimidasWeb.Graph.Middlewares.LoginRequired


  @desc "话题列表"
  object :question_group do
    @desc "话题ID"
    field :id, :id

    @desc "所属用户"
    field :user, :user, resolve: assoc(:user)

    @desc "话题名称"
    field :name, :string

    # @desc "更新时间"
    # field :updated_at, :integer do
    #   IO.inspect(:updated_at, label: ">>>>>>>>>>> :updated_at")
    #   resolve fn %{updated_at: updated_at}, _ ->
    #     IO.inspect(updated_at, label: ">>>>>>>>>>> updated_at")
    #     {:ok, DateTime.to_unix(updated_at)}
    #   end
    # end

    @desc "更新时间"
    field :updated_at, :integer, resolve: &QuestionListResolver.to_timestamp/3

    @desc "话题下问题"
    field(:items, list_of(:question_group_item)) do
      resolve(assoc(:questions))
    end
  end

  @desc "问答Item"
  object :question_group_item do
    @desc "问提ID"
    field :id, :id

    @desc "提问用户"
    field :user, :user, resolve: assoc(:user)

    @desc "投资组合"
    field :question_group, :question_group, resolve: assoc(:question_group)

    @desc "问题内容"
    field :query, :string

    @desc "问题回答"
    field :answer, :string

    @desc "问题等级"
    field :answer_level, :integer

    @desc "上下文"
    field :otpq, :string

    # @desc "上一个问题的上下文"
    # field :last_otpq, :string
  end

  @desc "话题输入"
  input_object :question_group_list_input do
    @desc "时间戳（返回该时间前的数据）"
    field :timestamp, :integer

    @desc "列表长度"
    field :size, :integer
  end

  object :question_group_queries do
    field(:question_group_list, list_of(:question_group)) do
      arg :input, non_null(:question_group_list_input)
      middleware LoginRequired
      resolve(&QuestionListResolver.list_question_groups/3)
    end
  end
end
