defmodule AimidasWeb.Graph.Types.HotQuestions do
  @moduledoc """
  """

  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo

  import Ecto.Query
  alias AimidasWeb.Graph.Resolvers.ManagementResolver

  @desc "热点问题"
  object :hot_question do
    @desc "热点问题"
    field :content, :string
    field :level, :integer
  end

  @desc "管理员更新热点问题"
  object :hot_questions do
    @desc "获取热门问题"
    field(:hot_question_list, list_of(:hot_question)) do
      resolve(&ManagementResolver.find_hot_questions/3)
    end
  end
end
