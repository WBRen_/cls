defmodule AimidasWeb.Graph.Types.Portfolio do
  @moduledoc """
  Portfolio 投资组合的GraphQL类型定义
  """
  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo
  alias AimidasWeb.Graph.Resolvers.TradeResolver
  alias AimidasWeb.Graph.Resolvers.StockResolver

  @desc "投资组合"
  object :portfolio do
    @desc "投资组合ID"
    field :id, :id

    @desc "所属用户"
    field :user, :user, resolve: assoc(:user)

    @desc "投资组合名称"
    field :name, :string

    @desc "投资组合交易"
    field :portfolio_trade, :portfolio_trade, resolve: &TradeResolver.get_portfolio_price/3

    @desc "相关问题"
    field :questions, list_of(:string), resolve: &StockResolver.get_portfolio_questions/3

    @desc "公告"
    field :content, list_of(:content), resolve: &StockResolver.get_portfolio_content/3

    @desc "投资组合明细"
    field(:items, list_of(:portfolio_item)) do
      resolve(assoc(:portfolio_items))
    end
  end

  @desc "投资组合交易"
  object :portfolio_trade do

    @desc "总持有股数"
    field :total, :integer

    @desc "总收益"
    field :income, :float

    @desc "总成本"
    field :price, :float

    @desc "rate"
    field :rate, :float
  end

  @desc "投资组合输入"
  input_object :portfolio_input do
    @desc "投资组合 ID"
    field :id, :integer

    @desc "所属用户ID"
    field :user_id, :integer

    @desc "投资组合名称"
    field :name, :string
  end

  object :get_portfolio do
    @desc "获取ID"
    field(:portfolio, :portfolio) do
      arg :input, non_null(:portfolio_input)
      resolve(&TradeResolver.get_portfolio/3)
    end
  end
end
