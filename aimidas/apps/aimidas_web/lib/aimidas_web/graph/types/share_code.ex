defmodule AimidasWeb.Graph.Types.ShareCode do
  @moduledoc """
  Stock 股票的GraphQL类型定义
  """

  use AimidasWeb, :type
  alias AimidasWeb.Graph.Resolvers.CodeResolver


  @desc "小程序码信息"
  object :code_info do

    @desc "生成的图片base64"
    field :url, :string

    @desc "参数"
    field :scene, :string
  end

  @desc "生成二维码输入"
  input_object :share_code_input do
    @desc "问题ID，参数"
    field :scene, :string
  end

  object :code_create do
    @desc "生成推荐码"
    field(:code_info, :code_info) do
      arg :input, non_null(:share_code_input)
      resolve(&CodeResolver.get_code/3)
    end
  end
end
