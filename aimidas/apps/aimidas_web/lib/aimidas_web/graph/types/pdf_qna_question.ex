defmodule AimidasWeb.Graph.Types.ImportPdfQnaQuestions do
  @moduledoc """
  """

  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo
  alias AimidasWeb.Graph.Mutations.ImportPdfQnaQuestions

  @desc "导入数据的返回信息"
  object :pdf_qna_questions_import_result do
    @desc "messge，若为 success 则表示成功"
    field :message, :string
  end

  @desc "import input"
  input_object :pdf_qna_questions_import_input do
    @desc "导入数据"
    field :data, :string
  end

  @desc "导入PDF问答"
  object :import_pdf_qna_questions do
    @desc "导入PDF问答"
    field(:pdf_qna_questions_import_result, :pdf_qna_questions_import_result) do
      arg :input, non_null(:pdf_qna_questions_import_input)
      resolve(&ImportPdfQnaQuestions.import/3)
    end
  end

  @desc "删除数据的返回信息"
  object :pdf_qna_questions_delete_result do
    @desc "messge，若含有 success 字样则表示成功"
    field :message, :string
  end

  @desc "delete imports"
  input_object :pdf_qna_questions_delete_input do
    @desc "删除数据（md5 array）"
    field :md5s, :string
  end

  @desc "删除PDF问答"
  object :delete_pdf_qna_questions do
    @desc "删除PDF问答"
    field(:pdf_qna_questions_delete_result, :pdf_qna_questions_delete_result) do
      arg :input, non_null(:pdf_qna_questions_delete_input)
      resolve(&ImportPdfQnaQuestions.delete/3)
    end
  end
end
