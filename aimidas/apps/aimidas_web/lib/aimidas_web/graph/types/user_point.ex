defmodule AimidasWeb.Graph.Types.UserPoint do
  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo

  import Ecto.Query

  alias AimidasWeb.Graph.Middlewares.LoginRequired
  alias AimidasWeb.Graph.Resolvers.PointResolver

  @desc "积分变更"
  object :user_point_result do
    @desc "message"
    field :message, :string
  end

  @desc "积分变更输入"
  input_object :point_input do
    @desc "类型"
    field :type, :string
    @desc "更新的额度"
    field :quota, :integer
    @desc "更新的积分"
    field :point, :integer
    @desc "使用的额度"
    field :use_quota, :integer
  end

  object :user_points do
    @desc "更新积分"
    field(:user_point_result, :user_point_result) do
      arg :input, :point_input
      resolve(&PointResolver.update_point/3)
    end
  end
end
