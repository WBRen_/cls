defmodule AimidasWeb.Graph.Types.PortfolioItem do
  @moduledoc """
  PortfolioItem 投资组合明细的GraphQL类型定义
  """

  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo
  import Ecto.Query
  alias AimidasWeb.Graph.Resolvers.TradeResolver


  @desc "投资组合项"
  object :portfolio_item do
    @desc "ID"
    field :id, :id

    @desc "所属用户"
    field :user, :user, resolve: assoc(:user)

    @desc "投资组合"
    field :portfolio, :portfolio, resolve: assoc(:portfolio)

    @desc "股票"
    field :stock, :stock, resolve: assoc(:stock)

    # @desc "投资组合交易记录"
    # field :portfolio_item_trades,
    #       list_of(:portfolio_item_trade),
    #       resolve: assoc(:portfolio_item_trade, fn(query, _, _) ->
    #                 from p in query, order_by: [desc: p.inserted_at]
    #                 end)

    @desc "投资组合交易记录"
    field :portfolio_item_trades,list_of(:portfolio_item_trade),resolve: &TradeResolver.get_item_list/3

    @desc "价格信息"
    field :portfolio_item_price, :portfolio_item_price, resolve: &TradeResolver.get_item_price/3
  end

  @desc "投资组合项交易"
  object :portfolio_item_price do
    @desc "持有数量"
    field :total_quantity, :integer

    @desc "加权平均价"
    field :average_price, :float

    @desc "总成本"
    field :total_price, :float

    @desc "当前价格-加权价格"
    field :income, :float

    @desc "收益率"
    field :income_rate, :float

    @desc "总收益"
    field :total_income, :float

    @desc "总收益/总成本"
    field :total_rate, :float

    @desc "净值"
    field :net_price, :float

  end

  @desc "投资组合项交易"
  object :portfolio_item_trade do
    @desc "ID"
    field :id, :id

    @desc "持有数量"
    field :quantity, :integer

    @desc "价格"
    field :price, :decimal

    @desc "type"
    field :type, :string
  end

  @desc "投资组合项输入"
  input_object :portfolio_item_input do
    @desc "ID"
    field :id, :integer

    @desc "所属投资组合id"
    field :portfolio_id, :integer

    @desc "股票id"
    field :stock_id, :integer
  end

  @desc "投资组合项输入"
  input_object :portfolio_item_trade_add_input do

    @desc "所属投资组合id"
    field :portfolio_item_id, :integer

    @desc "数量"
    field :quantity, :integer

    @desc "类型"
    field :type, :string

    @desc "数量"
    field :price, :string
  end

  @desc "投资组合项更新输入"
  input_object :portfolio_item_trade_update_input do

    @desc "交易记录id"
    field :id, :integer

    @desc "所属投资组合id"
    field :portfolio_item_id, :integer

    @desc "数量"
    field :quantity, :integer

    @desc "类型"
    field :type, :string

    @desc "数量"
    field :price, :string
  end

  @desc "投资组合项输入"
  input_object :portfolio_item_trade_delete_input do
    @desc "所属投资组合id"
    field :id, :integer

    @desc "所属投资组合id"
    field :portfolio_item_id, :integer
  end

  object :get_portfolio_item do
    @desc "获取ID"
    field(:portfolio_item, :portfolio_item) do
      arg :input, non_null(:portfolio_item_input)
      resolve(&TradeResolver.get_portfolio_item/3)
    end
  end
end
