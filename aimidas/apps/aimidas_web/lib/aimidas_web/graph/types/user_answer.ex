defmodule AimidasWeb.Graph.Types.UserAnswer do
  @moduledoc """
  GraphQL User type
  用户类型
  """

  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo

  import Ecto.Query

  alias AimidasWeb.Graph.Resolvers.StatisticsResolver
  alias AimidasWeb.Graph.Middlewares.LoginRequired
  alias AimidasWeb.Graph.Resolvers.RelatedAnswerResolver

  # 临时更新积分接口
  @desc "更新积分"
  input_object :quota_input do
    @desc "更新的额度"
    field :quota, :integer
  end

  @desc "更新积分"
  object :quota_result do
    @desc "message"
    field :message, :string
  end

  @desc "点赞"
  input_object :user_answer_like_input do
    @desc "点赞的回答id"
    field :answer_id, :integer
  end

  @desc "反对"
  input_object :user_answer_contra_input do
    @desc "点赞的回答id"
    field :answer_id, :integer
  end

  @desc "提供回答"
  input_object :user_answer_input do
    @desc "问题id"
    field :question_id, :integer
    field :answer, :string
  end

  @desc "点赞"
  object :user_answer_like_result do
    @desc "message"
    field :message, :string
  end

  @desc "反对"
  object :user_answer_contra_result do
    @desc "message"
    field :message, :string
  end

  @desc "提供回答"
  object :user_answer_result do
    @desc "message"
    field :message, :string
  end

  @desc "分享小程序"
  object :user_share_result do
    @desc "message"
    field :message, :string
  end

  @desc "反馈建议"
  object :feedback_result do
    @desc "message"
    field :message, :string
  end

  @desc "答案信息"
  object :user_answer do
    @desc "ID"
    field :id, :id

    @desc "答案内容"
    field :answer, :string

    @desc "点赞次数"
    field :stars, :integer

    @desc "反对次数"
    field :contras, :integer

    @desc "答案状态"
    field :status, :integer

    @desc "所属用户"
    field :user, :user, resolve: assoc(:user)

    @desc "所属问题"
    field :question, :question, resolve: assoc(:question)

    @desc "更新时间"
    field :updated_at, :integer, resolve: &StatisticsResolver.to_timestamp/3

    end

    @desc "问题"
    object :statistics_question do
      @desc "问题唯一ID（不针对用户）"
      field :id, :id

      @desc "问题组"
      field :md5, :string

      @desc "问题内容"
      field :query, :string
    end

  object :user_answers do
    # 临时更新积分
    @desc "更新积分"
    field(:quota_result, :quota_result) do
      arg :input, non_null(:quota_input)
      resolve(&StatisticsResolver.quota/3)
    end

    @desc "点赞"
    field(:user_answer_like_result, :user_answer_like_result) do
      arg :input, non_null(:user_answer_like_input)
      middleware LoginRequired
      resolve(&StatisticsResolver.like_answer/3)
    end

    @desc "反对"
    field(:user_answer_contra_result, :user_answer_contra_result) do
      arg :input, non_null(:user_answer_contra_input)
      middleware LoginRequired
      resolve(&StatisticsResolver.contra_answer/3)
    end

    @desc "反对"
    field(:user_share_result, :user_share_result) do
      middleware LoginRequired
      resolve(&StatisticsResolver.share/3)
    end

    @desc "提供回答"
    field(:user_answer_result, :user_answer_result) do
      arg :input, non_null(:user_answer_input)
      middleware LoginRequired
      resolve(&StatisticsResolver.contribute_answer/3)
    end

    @desc "获取问题"
    field(:user_answer, :user_answer) do
      arg :answer_id, :integer
      resolve(&StatisticsResolver.answer/3)
    end

    @desc "获取相关问题"
    field(:user_answer_list, list_of(:user_answer)) do
      arg :statistics_question_id, :integer
      resolve(&RelatedAnswerResolver.find_related_answer/3)
    end

    @desc "获取统计问题"
    field(:statistics_question, :statistics_question) do
      arg :statistics_question_id, :integer
      resolve(&StatisticsResolver.find_question/3)
    end

    @desc "获取热门问题"
    field(:statistics_question_list, list_of(:statistics_question)) do
      resolve(&StatisticsResolver.find_hot_question/3)
    end

    @desc "反馈建议"
    field(:feedback_result, :feedback_result) do
      arg :content, :string
      middleware LoginRequired
      resolve(&StatisticsResolver.feedback/3)
    end
  end
end
