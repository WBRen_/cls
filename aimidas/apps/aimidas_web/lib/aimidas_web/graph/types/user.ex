defmodule AimidasWeb.Graph.Types.User do
  @moduledoc """
  GraphQL User type
  用户类型
  """

  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo

  import Ecto.Query

  alias AimidasWeb.Graph.Resolvers.UserResolver
  alias AimidasWeb.Graph.Middlewares.LoginRequired
  alias AimidasWeb.Graph.Resolvers.PointResolver

  @desc "用户信息"
  object :user do
    @desc "ID"
    field :id, :id

    @desc "用户昵称"
    field :nickname, :string

    @desc "头像"
    field :avatar, :string

    @desc "手机号码"
    field :telephone, :string

    @desc "积分"
    # field :point, :point, resolve: assoc(:point)
    field :point, :point, resolve: &PointResolver.fetch_point/3


    @desc "oauth认证授权"
    field :authentications, list_of(:authentication) do
      resolve(assoc(:authentications))
    end

    @desc "投资组合"
    field :portfolios,
          list_of(:portfolio),
          resolve: assoc(:portfolios, fn(query, _, _) ->
                    from p in query, order_by: [desc: p.inserted_at]
                    end)

    @desc "积分变动"
    field :point_records,
          list_of(:point_record),
          resolve: assoc(:point_records, fn(query, _, _) ->
            from p in query, order_by: [desc: p.inserted_at]
            end)

    @desc "用户答案"
    field :user_answers,
          list_of(:user_answer),
          resolve: assoc(:user_answers, fn(query, _, _) ->
            from p in query, order_by: [desc: p.inserted_at]
            end)

  end

  @desc "用户信息"
  input_object :user_input do
    @desc "ID"
    field :id, :id

    @desc "用户昵称"
    field :nickname, :string

    @desc "头像"
    field :avatar, :string

    @desc "手机号码"
    field :telephone, :string
  end

  @desc "用户积分"
  object :point do
    @desc "积分"
    field :point, :integer
    @desc "限额"
    field :quota, :integer
    @desc "今日使用量"
    field :current_quota_use, :integer

  end

  @desc "用户积分变动"
  object :point_record do
    @desc "ID"
    field :id, :id
    @desc "积分变动"
    field :point, :integer
    @desc "限额变动"
    field :quota, :integer
    @desc "类型"
    field :type, :string
    @desc "更新时间"
    field :updated_at, :integer, resolve: &PointResolver.to_timestamp/3
    @desc "关联用户"
    field :user, :user, resolve: assoc(:user)

  end


  @desc "OAuth登录授权"
  object :authentication do
    @desc "ID"
    field :id, :id

    @desc "oauth uid"
    field :uid, :string

    @desc "邮箱地址"
    field :email, :string

    @desc "头像"
    field :image, :string

    @desc "名称"
    field :name, :string

    @desc "昵称"
    field :nickname, :string

    @desc "oauth提供者"
    field :provider, :string

    @desc "token刷新"
    field :refresh_token, :string

    @desc "token"
    field :token, :string

    @desc "token secret"
    field :token_secret, :string

    @desc "关联用户"
    field :user, :user, resolve: assoc(:user)
  end

  object :user_queries do
    @desc "当前登录用户"
    field(:me, :user) do
      middleware LoginRequired
      resolve(&UserResolver.me/3)
    end

    @desc "当前登录用户"
    field(:users, list_of(:user)) do
      middleware LoginRequired
      resolve(&UserResolver.users/3)
    end

    @desc "获取单个用户"
    field(:user, :user) do
      arg :user_id, :integer
      middleware LoginRequired
      resolve(&UserResolver.user/3)
    end
  end

  object :user_update do
    @desc "修改用户信息"
    field(:update_user, :user) do
      arg :input, non_null(:user_input)
      middleware LoginRequired
      resolve(&UserResolver.update/3)
    end
  end
end
