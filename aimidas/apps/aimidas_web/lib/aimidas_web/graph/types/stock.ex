defmodule AimidasWeb.Graph.Types.Stock do
  @moduledoc """
  Stock 股票的GraphQL类型定义
  """

  use AimidasWeb, :type
  use Absinthe.Ecto, repo: Aimidas.Repo

  alias AimidasWeb.Graph.Resolvers.StockResolver

  @desc "股票信息"
  object :stock do
    @desc "ID"
    field :id, :id

    @desc "股票代码"
    field :code, :string

    @desc "股票名称"
    field :name, :string

    @desc "最近报价"
    field :last_px, :decimal, resolve: &StockResolver.get_last_px/3

    @desc "报价时间"
    field :last_px_at, :datetime

    @desc "浮动"
    field :ratio, :string, resolve: &StockResolver.get_ratio/3

    @desc "换手率"
    field :px_exchange_rate, :decimal

    @desc "股票交易所"
    field :exchange_type_code, :string
  end

  @desc "股票详情"
  object :stock_detail do
    @desc "ID"
    field :id, :id

    @desc "股票代码"
    field :code, :string

    @desc "股票名称"
    field :name, :string

    @desc "最近报价"
    field :last_px, :decimal, resolve: &StockResolver.get_last_px/3

    @desc "报价时间"
    field :last_px_at, :datetime

    @desc "换手率"
    field :px_exchange_rate, :decimal

    @desc "股票交易所"
    field :exchange_type_code, :string

    @desc "公司主页"
    field :web, :string

    @desc "公司主页"
    field :questions, list_of(:string)

    @desc "公告"
    field :content, list_of(:content)
  end

  @desc "公告"
  object :content do
    @desc "日期"
    field :date, :string

    @desc "标题"
    field :title, :string

    @desc "内容"
    field :content, :string

    @desc "类型"
    field :type, :string

    @desc "url"
    field :url, :string

    @desc "source"
    field :source, :string

    @desc "institution"
    field :institution, :string

    @desc "rating"
    field :rating, :string

    @desc "rating_change"
    field :rating_change, :string

    @desc "pdf_screenshot"
    field :pdf_screenshot, list_of(:string)
  end



  @desc "更新股票"
  object :stock_result do
    @desc "message"
    field :message, :string
  end

  @desc "股票搜索查询输入"
  input_object :stock_search_input do
    @desc "股票代码关键词"
    field :word, :string
  end
end
