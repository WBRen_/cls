defmodule AimidasWeb.Graph.Resolvers.PointResolver do
  @moduledoc """
  问答列表
  """

  alias Aimidas.Points

  def fetch_point(parent, args, resolution) do
    # IO.inspect(parent, label: ">>>>>> parent")
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    # {:ok, point} = parent.user_id |> Points.find_point
    parent.id |> Points.find_point
  end

  def to_timestamp(parent, args, resolution) do
    # IO.inspect(parent, label: ">>>>>> parent")
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    {:ok, n_time} =
      parent.updated_at
      |> DateTime.from_naive("Etc/UTC")
      # |> IO.inspect(label: ">>>>>> updated_at")

    timestamp =
      n_time
      |> DateTime.to_unix()

    {:ok, timestamp}
  end

  def update_point(_parent, args, %{context: %{current_user: current_user}}) do
    input = args[:input]
    type = input[:type]
    user_id = current_user.id

    # 增加积分（异步）
    update_point_by_type(type, input, user_id)
    {:ok, %{message: "积分更新成功"}}
  end

  def update_point_by_type("AskQuestion", input, user_id) do
    Points.add_quota_use_today(user_id, 0 - Aimidas.Configer.fetch("AskQuestion"))
    Points.create_point_record(%{user_id: user_id, type: "AskQuestion", quota: Aimidas.Configer.fetch("AskQuestion"), point: 0})
  end

  def update_point_by_type("AskQuestionOver", input, user_id) do
    Points.add_point(user_id, Aimidas.Configer.fetch("AskQuestion"))
    Points.create_point_record(%{user_id: user_id, type: "AskQuestion", quota: 0, point: Aimidas.Configer.fetch("AskQuestion")})
  end

  def update_point_by_type("UpdateQuota", input, user_id) do
    quota = input[:quota]
    Points.set_quota(user_id, quota)
    Points.create_point_record(%{user_id: user_id, type: "UpdateQuota", quota: quota, point: 0})
  end

  def update_point_by_type(type, input, user_id) do
    quota = input[:quota]
    point = input[:point]
    Points.add_point(user_id, Aimidas.Configer.fetch(type))
    Points.create_point_record(%{user_id: user_id, type: type, quota: quota, point: Aimidas.Configer.fetch(type)})
  end
end
