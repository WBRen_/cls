defmodule AimidasWeb.Graph.Resolvers.StockResolver do
  # @url "http://prod2.aimidas.com:18080/returnOtherPdfContent/"
  @host System.get_env("NLP_HOST") || "http://prod2.aimidas.com:18080"
  @url "#{@host}/returnOtherPdfContent/"
  @url_question "#{@host}/returnRecommendQuestion/"
  @moduledoc """
  股票解释器
  """
#AimidasWeb.Graph.Resolvers.StockResolver.put_pic(%{company_code: "000001",detail_code: "A1",type_code: "UC",year: "2018"})
  alias Aimidas.Stocks
  alias Aimidas.Ontology
  alias Aimidas.Qa

  alias AimidasWeb.Stock.Stock

  def get_last_px(parent, args, resolution) do
    IO.inspect(parent, label: ">>>>>> parent")
    IO.inspect(parent.exchange_type_code, label: ">>>>>> EX CODE")
    IO.inspect(parent.code, label: ">>>>>> CODE")
    {:ok, stock} = Stock.get_last_px(parent.exchange_type_code, parent.code)
    stock |> IO.inspect(label: ">>>>>> updated_at")

      last_px = Decimal.new(stock["当前价格"])

    {:ok, last_px}
  end

  def get_ratio(parent, args, resolution) do
    IO.inspect(parent, label: ">>>>>> parent")
    IO.inspect(parent.exchange_type_code, label: ">>>>>> EX CODE")
    IO.inspect(parent.code, label: ">>>>>> CODE")
    {:ok, stock} = Stock.analyze(parent.exchange_type_code, parent.code)
    stock |> IO.inspect(label: ">>>>>> updated_at")

      ratio = stock["涨幅"]

    {:ok, ratio}
  end

  @doc """
  获取股票列表
  """
  def list_stocks(_parent, _args, _resolution) do
    {:ok, Stocks.list_stocks()}
  end

  @doc """
  搜索股票列表
  """
  def list_stocks_by_search_word(_parent, args, _resolution) do
    input = args[:input]
    word = input[:word]
    {:ok, stocks} = Stocks.list_stocks_by_search_word(word)
    IO.puts "[testAddPI] response: #{inspect(stocks, pretty: true)}"
    {:ok, stocks}
  end

  def get_stock_by_search_word(_parent, args, _resolution) do
    input = args[:input]
    word = input[:word]
    {:ok, stock} = Stocks.get_stock_by_search_word(word)
    {:ok, info} = Ontology.find_company_info_by_code(stock.code)
    key_path =
      [
        "基本信息",
        "公司网址"
      ]
    url =
        info.details
        |> get_in(key_path)

    questions = get_question(stock.code)
    {:ok, content} =
      @url
      |> HTTPotion.post([body: query_to_json(stock.code), headers: headers()])
      |> handle_response()
    IO.puts "[testAddPI] response: #{inspect(stock, pretty: true)}"
    content =
      content
      |> Enum.map(fn item ->
        item |> rebuild()
      end)
    stock =
      stock
      |> Map.put(:content, content)
      |> Map.put(:web, url)
      |> Map.put(:questions, questions)
    {:ok, stock}
  end

  def test(code) do
    response =
      @url_question
      |> HTTPotion.post([body: query_to_json(code), headers: headers()])
    # String.split(response.body)
      |> handle_response()
    # content =
    #     content
    #     |> Enum.map(fn item ->
    #       item |> rebuild()
    #     end)
  end

  def get_question(code) do
    {:ok, result} =
      @url_question
      |> HTTPotion.post([body: query_to_json(code), headers: headers()])
      |> handle_response()
    result
  end

  def get_portfolio_questions(parent, args, resolution) do
    case Stocks.get_portfolio_items(parent.id) do
      {:ok, list} ->
        stock_list =
          list
          |> Enum.map(fn item ->
            item.stock_id |> get_stock_code()
          end)
        {:ok, result} =
          @url_question
          |> HTTPotion.post([body: query_to_json(stock_list), headers: headers()])
          |> handle_response()
        {:ok, result}
      {:error, _} ->
        {:ok, []}
    end
  end

  def get_portfolio_content(parent, args, resolution) do
    case Stocks.get_portfolio_items(parent.id) do
    {:ok, list} ->
      stock_list =
        list
        |> Enum.map(fn item ->
            item.stock_id |> get_stock_code()
          end)
      {:ok, content} =
        @url
        |> HTTPotion.post([body: query_to_json(stock_list), headers: headers()])
        |> handle_response()
      content =
        content
        |> Enum.map(fn item ->
          item |> rebuild()
        end)
      {:ok, content}
    {:error, _} ->
      {:ok, []}
    end
  end

  def get_stock_code(stock_id) do
    item = Stocks.get_stock!(stock_id)
    item.code
  end

  def rebuild(item) do
    pdf_info = item.pdf_screenshot
    pdf_screenshot = put_pic(pdf_info)
    item
    |> Map.replace!(:pdf_screenshot, pdf_screenshot)

  end

  def put_pic(%{company_code: company_code, year: year, type_code: type_code, detail_code: detail_code, updated_at: updated_at}) do
    Qa.list_all_pdf_page_pictures(company_code, year, type_code, detail_code, updated_at)
    |> Enum.map(fn url ->
      url <> "?x-oss-process=image/watermark,text_6L%2BI5aSn5biI,image_cGEucG5nP3gtb3NzLXByb2Nlc3M9aW1hZ2UvcmVzaXplLFBfMTI,t_60,g_se,x_10,y_10,order_1,align_1"
    end)
    # Aimidas.Qa.list_all_pdf_page_pictures("000001", "2018", "UC", "A0")
  end
  def put_pic(%{company_code: company_code, year: year, type_code: type_code, detail_code: detail_code}) do
    Qa.list_all_pdf_page_pictures(company_code, year, type_code, detail_code)
    |> Enum.map(fn url ->
      url <> "?x-oss-process=image/watermark,text_6L%2BI5aSn5biI,image_cGEucG5nP3gtb3NzLXByb2Nlc3M9aW1hZ2UvcmVzaXplLFBfMTI,t_60,g_se,x_10,y_10,order_1,align_1"
    end)
    # Aimidas.Qa.list_all_pdf_page_pictures("000001", "2018", "UC", "A0")
  end
  def put_pic(%{}) do
    []
  end

  def create_stocks(_parent, _args, _resolution) do
    Task.async(fn -> update_stocks() end)
    # update_stocks()
    {:ok, %{message: "股票更新成功"}}
  end
  def update_stocks() do
    Ontology.list_company_infos
    |> Enum.map(fn item ->
      item.company_code |> do_update(item.abbrev_name)
    end)
  end

  def do_update(code, name) do
    case Stocks.get_stock_by_code(code) do
      {:error, _} ->
        {:ok, xbrl} = Ontology.find_company_xbrl_by_code(code)
        Stocks.create_stock(%{exchange_type_code: xbrl.exchange_code, code: code, name: name})
      {:ok, stock} ->
        {:ok, xbrl} = Ontology.find_company_xbrl_by_code(code)
        Stocks.update_stock(stock, %{exchange_type_code: xbrl.exchange_code, code: code, name: name})
    end
  end

  def build_query(code) do
    %{
      company_code: code
    }
  end

  def query_to_json(code) do
    build_query(code)
    |> Poison.encode!()

  end

  def headers do
    [
      "Content-Type": "application/json",
      "Accept": "application/json"
    ]
  end

  # @decorate transaction_event()
  def handle_response(%HTTPotion.ErrorResponse{} = response) do
    Rollbax.report_message(:error,
      "NLP Server Error",
      _custom_data = %{},
      %{"HTTPotion.ErrorMessage" => response.message})

    {
      :error,
      response
    }
  end

  # @decorate transaction_event()
  def handle_response(%{} = response) do
    {
      :ok,
      response.body
      |> Poison.decode!()
      |> to_atom_map
    }
  end

  # @decorate transaction_event()
  def to_atom_map(%{} = json_map) do
    json_map
    |> Map.new(fn {k, v} -> {String.to_atom(k), to_atom_map(v)} end)
  end
  def to_atom_map([]), do: []
  def to_atom_map([head|tail]) do
    [to_atom_map(head)] ++ to_atom_map(tail)
  end
  def to_atom_map(data), do: data
end
