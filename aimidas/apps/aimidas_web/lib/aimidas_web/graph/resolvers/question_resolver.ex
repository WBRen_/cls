defmodule AimidasWeb.Graph.Resolvers.QuestionResolver do
  @moduledoc """
  问答处理器
  """

  alias Aimidas.Ontology
  alias AimidasWeb.NLP.Parser, as: NLPParser
  alias Aimidas.Qa

  alias Aimidas.Points
  alias AimidasWeb.StatisticsProcesser
  alias Aimidas.Statistics

  import AimidasWeb.Xbrl.QuestionProcessor, only: [process_xbrl_question: 2]

  @doc """
  处理问题
  """

  def question(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]
    query = input[:query]
    # 从数据库查询用户上次的 otpq 和 question 信息
    last_question =
      current_user.id
      |> context_from_last_question()

    # 预处理好的数据，为填充当前问答记录做准备
    pre_question_attrs =
    %{
      question_group_id: last_question.question_group_id,
      user_id: current_user.id,
      query: query,
      answer_level: 1
      # answer: < from nlp
      # otpq: < from nlp
    }

    # 当前用户使用该问答服务，+1 积分
    # Points.add_point(current_user.id, 1)

    # 当前用户使用该问答服务，+1 积分 异步待解决
    # Points.add_quota(current_user.id, -1)
    # Points.add_quota_use_today(current_user.id, 1)
    # Points.create_point_record(%{user_id: current_user.id, type: "AskQuestion", quota: -1, point: 0})
    # Points.update_quota_count(current_user.id, -1)

    # nlp_result = {
    #   type,
    #   question,
    #   question_md5,
    #   context_tag,
    #   param_holder,
    #   context
    # }
    query
      # |> IO.inspect(label: ">>>> QUERY:")
      |> NLPParser.parse(last_question.otpq)
      # 统计该类问题的次数
      |> count_question(query) # 系统的标准问答记录，同一问题只有一条记录
      # |> IO.inspect(label: ">>>> NLP RESULT:")
      |> dispatcher() # 会返回当前问答的 otpq 和 answer
      # |> IO.inspect(label: ">>>> DISPATCH RESULT:")
      |> create_question(pre_question_attrs) # 用户自己的记录，每问一次都会有一条
      # |> IO.inspect(label: ">>>> QUESTION RESULT:")
      |> response()
      # |> IO.inspect(label: ">>>> RESPONSE:")
  end

  @doc """
  问题持久化，如果已经持久，则计数器 +1
  """
  def count_question(%{question_md5: md5, context: context, question: question} = param, query) do
    # 该过程需要特殊处理，避免并发时持久化重复的问题
    {:ok, q_record} = StatisticsProcesser.question_plus(%{md5: md5, query: question, otpq: context})
    # 不做任何其他处理，将此参数传给下一步
    param |> Map.put(:statistics_question_id, q_record.id)
  end

  def count_question(%{question_md5: md5, context: context} = param, query) do
    # 该过程需要特殊处理，避免并发时持久化重复的问题
    {:ok, q_record} = StatisticsProcesser.question_plus(%{md5: md5, query: query, otpq: context})
    # 不做任何其他处理，将此参数传给下一步
    param |> Map.put(:statistics_question_id, q_record.id)
  end
  def count_question(%{context: context} = param, query) do
    md5 = query # 下下策，暂时未引入 md5 计算的包，如果 NLP 没有返回 md5 值，则出此下策
    {:ok, q_record} = StatisticsProcesser.question_plus(%{md5: md5, query: query, otpq: context})
    param |> Map.put(:statistics_question_id, q_record.id)
  end

  @doc """
    专家问答
    {
      :ok,
      %{
        answer: answer,
        otpq: context
      }
    }
  """
  def dispatcher(%{type: "pro_qna",
                   # question: _question, # string 存在，但未使用
                   question_md5: md5,
                   context: context,
                   context_tag: context_tag,
                   related_question: related_question,
                   statistics_question_id: statistics_question_id
                  } = _param) do
    md5
    |> Qa.find_pro_qna_question
    # |> IO.inspect(label: ">>>> ProQnaQuestion")
    |> build_qna_answer(context)
    # |> IO.inspect(label: ">>>> ProQnaQuestion Build Answer")
    |> put_context_tag(context_tag)
    # |> IO.inspect(label: ">>>> ProQnaQuestion Add Tag")
    |> put_related_question(related_question)
    |> put_statistics_question_id(statistics_question_id)
    # |> put_related_answer(statistics_question_id)
    |> put_pdf_page_screen_pictures("no_pdf_image")
  end

  @doc """
  董秘问答
    {
      :ok,
      %{
        answer: answer,
        otpq: context
      }
    }
  """
  def dispatcher(%{type: "secretary_qna",
                  #  question: _question, # string 存在，但未使用
                   question_md5: md5,
                   context: context,
                   context_tag: context_tag,
                   related_question: related_question,
                   statistics_question_id: statistics_question_id
                  } = _param) do
    md5
    |> Qa.find_dm_qna_question
    # |> IO.inspect(label: ">>>> DmQnaQuestion")
    |> build_qna_answer(context)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Build Answer")
    |> put_context_tag(context_tag)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Add Tag")
    |> put_related_question(related_question)
    |> put_statistics_question_id(statistics_question_id)
    # |> put_related_answer(statistics_question_id)
    |> put_pdf_page_screen_pictures("no_pdf_image")
  end

  @doc """
  PDF问答
    {
      :ok,
      %{
        answer: answer,
        otpq: context
      }
    }
  """

  def dispatcher(%{ type: "pdf_qna",
                    # question: _question, # string 存在，但未使用
                    question_md5: md5,
                    context: context,
                    context_tag: context_tag,
                    related_question: related_question,
                    statistics_question_id: statistics_question_id,
                    pdf_screenshot: pdf_screenshot
                  } = _param) do
    md5
    |> Qa.find_pdf_qna_question
    # |> IO.inspect(label: ">>>> DmQnaQuestion")
    |> build_qna_answer(context)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Build Answer")
    |> put_context_tag(context_tag)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Add Tag")
    |> put_related_question(related_question)
    |> put_statistics_question_id(statistics_question_id)
    # |> put_related_answer(statistics_question_id)
    |> put_pdf_page_screen_pictures(pdf_screenshot)
  end
  def dispatcher(%{ type: "pdf_qna",
                    # question: _question, # string 存在，但未使用
                    question_md5: md5,
                    context: context,
                    context_tag: context_tag,
                    related_question: related_question,
                    statistics_question_id: statistics_question_id
                  } = _param) do
    md5
    |> Qa.find_pdf_qna_question
    # |> IO.inspect(label: ">>>> DmQnaQuestion")
    |> build_qna_answer(context)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Build Answer")
    |> put_context_tag(context_tag)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Add Tag")
    |> put_related_question(related_question)
    |> put_statistics_question_id(statistics_question_id)
    # |> put_related_answer(statistics_question_id)
    |> put_pdf_page_screen_pictures("no_pdf_image")
  end

  def dispatcher(%{type: "pdf_rich",
                   # question: _question, # removed
                   # question_md5: md5, # removed
                   context: context,
                   context_tag: context_tag,
                   related_question: related_question,
                   statistics_question_id: statistics_question_id,
                   pdf_text: pdf_text,
                   pdf_screenshot: pdf_screenshot
                  } = _param) do
    # md5
    # |> Qa.find_pdf_qna_question
    # |> IO.inspect(label: ">>>> DmQnaQuestion")
    pdf_text # 直接放进去，成为答案内容
    |> build_qna_answer(context)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Build Answer")
    |> put_context_tag(context_tag)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Add Tag")
    |> put_related_question(related_question)
    |> put_statistics_question_id(statistics_question_id)
    # |> put_related_answer(statistics_question_id)
    |> put_pdf_page_screen_pictures(pdf_screenshot)
  end

  @doc """
  反问机制
    {
      :ok,
      %{
        answer: answer,
        otpq: context
      }
    }
  """
  def dispatcher(%{type: "ask", ask_reply: ask_reply, question_md5: md5, context: context, context_tag: context_tag, related_question: related_question, statistics_question_id: statistics_question_id} = _param) do
    ask_reply
    |> build_qna_answer(context)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Build Answer")
    |> put_context_tag(context_tag)
    # |> IO.inspect(label: ">>>> DmQnaQuestion Add Tag")
    |> put_related_question(related_question)
    |> put_statistics_question_id(statistics_question_id)
    |> put_pdf_page_screen_pictures("no_pdf_image")
  end


  @doc """
    XBRL、公司基本信息、定义查询
    {
      :ok,
      %{
        answer: answer,
        otpq: context,
        data: data # 图表[可选]
      }
    }
  """
  def dispatcher(%{context: context, context_tag: context_tag, related_question: related_question, statistics_question_id: statistics_question_id, pdf_screenshot: pdf_screenshot} = _param) do
    # IO.inspect(context, label: ">>>> NLP Context")
    context
    |> run_question_default()
    |> put_context_tag(context_tag)
    |> put_related_question(related_question)
    |> put_statistics_question_id(statistics_question_id)
    |> put_pdf_page_screen_pictures(pdf_screenshot)
  end

  def dispatcher(%{context: context, context_tag: context_tag, related_question: related_question, statistics_question_id: statistics_question_id} = _param) do
    # IO.inspect(context, label: ">>>> NLP Context")
    context
    |> run_question_default()
    |> put_context_tag(context_tag)
    |> put_related_question(related_question)
    |> put_statistics_question_id(statistics_question_id)
    |> put_pdf_page_screen_pictures("no_pdf_image")
  end

  @doc """
  以下是具体的详情 ==========================================================
  """

  @doc """
    put_related_question
  """
  defp put_related_question({:ok, map}, related_question) do
    {
      :ok,
      map |> Map.put(:related_question, related_question)
    }
  end
  defp related_question({:error, msg}, _related_question) do
    {:error, msg}
  end

  @doc """
    put_context_tag
  """
  defp put_context_tag({:ok, map}, context_tag) do
    {
      :ok,
      map |> Map.put(:context_tag, context_tag)
    }
  end

  # data 图表数据
  defp put_context_tag({:ok, map, data}, context_tag) do
    {
      :ok,
      map
      |> Map.put(:context_tag, context_tag)
      |> Map.put(:data, data)
    }
  end

  defp put_context_tag({:error, msg}, _context_tag) do
    {:error, msg}
  end

  @doc """
    put_statistics_question_id(statistics_question_id)
  """
  defp put_statistics_question_id({:ok, map}, statistics_question_id) do
    {
      :ok,
      map |> Map.put(:statistics_question_id, statistics_question_id)
    }
  end
  defp put_statistics_question_id({:error, msg}, _statistics_question_id) do
    {:error, msg}
  end

  @doc """
    put_related_answer(statistics_question_id)
  """
  defp put_related_answer({:ok, map}, statistics_question_id) do
    {
      :ok,
      map |> Map.put(:related_answer, Statistics.find_answer_list_by_question_id(statistics_question_id))
    }
  end
  defp put_related_answer({:error, msg}, _statistics_question_id) do
    {:error, msg}
  end

  @doc """
    put_pdf_page_screen_pictures([])
  """
  defp put_pdf_page_screen_pictures({:ok, map}, "no_pdf_image") do
    {
      :ok,
      map |> Map.put(:pdf_image_urls, [])
          |> Map.put(:pdf_image_urls_detail, [])
    }
  end
  defp put_pdf_page_screen_pictures({:ok, map}, %{company_code: company_code, year: year, type_code: type_code, detail_code: detail_code, updated_at: updated_at, page_num: page_nums}) do
    image_urls = Qa.list_pdf_page_pictures("#{company_code}", "#{year}", "#{type_code}", "#{detail_code}", updated_at, page_nums)
    image_urls_group = Qa.list_pdf_group("#{company_code}", "#{year}", "#{type_code}", "#{detail_code}", updated_at, page_nums) |> List.first()
    {
      :ok,
      map |> Map.put(:pdf_image_urls, image_urls)
          |> Map.put(:pdf_image_urls_detail, image_urls_group)
    }
  end
  defp put_pdf_page_screen_pictures({:ok, map}, %{company_code: company_code, year: year, type_code: type_code, detail_code: detail_code, page_num: page_nums}) do
    image_urls = Qa.list_pdf_page_pictures("#{company_code}", "#{year}", "#{type_code}", "#{detail_code}", page_nums)
    image_urls_group = Qa.list_pdf_group("#{company_code}", "#{year}", "#{type_code}", "#{detail_code}", page_nums) |> List.first()
    {
      :ok,
      map |> Map.put(:pdf_image_urls, image_urls)
          |> Map.put(:pdf_image_urls_detail, image_urls_group)
    }
  end
  defp put_pdf_page_screen_pictures({:error, msg}, _params) do
    {:error, msg}
  end

  @doc """
  加载上一个问题
  """
  def context_from_last_question(user_id) do
    user_id
    |> Qa.get_user_last_question()
    |> handle_context()
  end

  def handle_context({:error, _msg}) do
    %{
      question_group_id: nil,
      otpq: %{}
    }
  end
  def handle_context({:ok, last_question}) do
    last_question
  end

  @doc """
  解析上下文参数
  """
  def parse_context(nil), do: %{}
  def parse_context(""), do: %{}
  def parse_context(context), do: context |> Poison.decode!()

  @doc """
  处理响应
  """
  # def response({:ok, _} = result), do: result
  def response({:ok, question_record, related_question, statistics_question_id, pdf_image_urls, pdf_image_urls_detail} = _) do
    # （异步，建议事务）持久化回答，根据问题的 statistics_question_id 来持久化（如果 user_answer 表存在该 id，说明已经添加过标准回答了，反之不可能被添加答案）
    {:ok, statistics_answer} = Statistics.append_standard_answer(statistics_question_id, question_record.answer)
    # 不要问我为什么要加载一遍，不加载就报错，手动加载很有必要。。
    group = Qa.get_question_group!(question_record.question_group_id)
    # 记录该问题是否有回答 updated 2018/11/09
    if question_record.answer =~ ~R/暂时无法回答该问题/ do
      StatisticsProcesser.mark_me_by_can_answer(statistics_question_id, false)
      case StatisticsProcesser.get_user_answer(statistics_question_id) do
        {:ok, answer} ->
          response_answer = "用户贡献答案：" <> answer.answer
        {:error, _} ->
          response_answer = question_record.answer <> "\n快点击详情来贡献您的答案吧"
      end
    else
      StatisticsProcesser.mark_me_by_can_answer(statistics_question_id, true)
      response_answer = question_record.answer
    end
    stock = question_record.otpq[:O]
    |> IO.inspect(label: "stock")
    [stock_answer, stock_answer_for_show] = get_stock(stock)
    {:ok,
      %{
        statistics_question_id: statistics_question_id,
        question_group: group,
        question_group_id: question_record.question_group_id,
        id: question_record.id,
        answer: %{
          id: statistics_answer.id,
          content: stock_answer <> response_answer,
          content_for_show:  stock_answer_for_show <> response_answer,
          pdf_image_urls: pdf_image_urls,
          pdf_image_urls_detail: pdf_image_urls_detail
        },
        context: question_record.otpq |> Poison.encode!(),
        data: question_record.data |> Poison.encode!(),
        related_question: related_question
      }
    }
    # |> IO.inspect(label: ">>>> Response")
  end
  def response({:error, msg}), do: {:error, message: msg}

  def get_stock([]), do: ["", ""]
  def get_stock(params) do
    [%{value: o_value, exchange_code: ex_code, code: code}|_] = params
    {:ok, stock} = AimidasWeb.Stock.Stock.analyze(ex_code, code)
    result = "#{o_value}（当前价格#{stock["当前价格"]}元，涨幅#{stock["涨幅"]}）\n"
    number = Float.parse(stock["涨幅"])
    if number < 0 do
      result_for_show = "#{o_value}（当前价格<div style='color: blue'>#{stock["当前价格"]}</div>元，涨幅<div style='color: green'>#{stock["涨幅"]}</div>）\n"
    else
      result_for_show = "#{o_value}（当前价格<div style='color: blue'>#{stock["当前价格"]}</div>元，涨幅<div style='color: red'>#{stock["涨幅"]}</div>）\n"
    end
    [result, result_for_show]
  end


  @doc """
  存问题进数据库 ==============================================================
  """
  # 存问题进数据库 >> data: 图表数据
  def create_question({:ok, %{statistics_question_id: q_id, answer: answer, otpq: otpq, data: data, "context_tag": context_tag, related_question: related_question, pdf_image_urls: pdf_image_urls, pdf_image_urls_detail: pdf_image_urls_detail} = _} = _result, pre_question_attrs) do
    {:ok, group_id}  = find_question_group_id("#{context_tag}", pre_question_attrs, otpq)
    pre_question_attrs
    |> Map.replace!(:question_group_id, group_id)
    |> Map.put(:answer, answer)
    |> Map.put(:otpq, otpq)
    |> Map.put(:data, data) # now exist this column
    |> Qa.create_question
    |> Tuple.append(related_question)
    |> Tuple.append(q_id)
    |> Tuple.append(pdf_image_urls)
    |> Tuple.append(pdf_image_urls_detail)
  end
  # 存问题进数据库 >> 默认情况
  def create_question({:ok, %{statistics_question_id: q_id, answer: answer, otpq: otpq, "context_tag": context_tag, related_question: related_question, pdf_image_urls: pdf_image_urls, pdf_image_urls_detail: pdf_image_urls_detail} = _} = _result, pre_question_attrs) do
    {:ok, group_id} = find_question_group_id("#{context_tag}", pre_question_attrs, otpq)
    pre_question_attrs
    |> Map.replace!(:question_group_id, group_id)
    |> Map.put(:answer, answer)
    |> Map.put(:otpq, otpq)
    |> Qa.create_question
    |> Tuple.append(related_question)
    |> Tuple.append(q_id)
    |> Tuple.append(pdf_image_urls)
    |> Tuple.append(pdf_image_urls_detail)
  end

  # 存问题进数据库 >> 异常情况，重新创建 group
  def create_question({:error, _msg}, pre_question_attrs) do
    {:ok, group_id} = find_question_group_id(context_tag = "0", pre_question_attrs, nil)
    pre_question_attrs
    |> Map.replace!(:question_group_id, group_id)
    |> Map.put(:answer, "暂时无法回答该问题") # 此处可修改为 msg，但不友好
    |> Map.put(:otpq, %{})
    |> Qa.create_question
  end

  @doc """
  查找 QuestionGroup，如果需要断开则会新建一个 QuestionGroup
  """
  defp find_question_group_id("0",
      %{question_group_id: nil, user_id: user_id} = _pre_questions_attrs,
      current_otpq) do
    # IO.inspect(">>>> FIND QUESTION GROUP ID#1")
    {:ok, question_group} =
    %{
      user_id: user_id,
      name: extract_group_name(current_otpq)
    }
    |> Qa.create_question_group
    {:ok, question_group.id}
  end

  defp find_question_group_id("1",
      %{question_group_id: last_group_id, user_id: _user_id} = _pre_questions_attrs,
      current_otpq) when last_group_id != nil do
    # IO.inspect(">>>> FIND QUESTION GROUP ID#2")
    {:ok, last_group_id}
  end

  defp find_question_group_id("0",
      %{question_group_id: last_group_id, user_id: user_id} = _pre_questions_attrs,
      current_otpq) do
    # IO.inspect(">>>> FIND QUESTION GROUP ID#3")
    # 判断该 group 是否为 ungroup，如果是，则不需要再重建 group
    group = Qa.get_question_group!(last_group_id)
    id = fetch_question_group_id(group.id, group.name, user_id, current_otpq)
    {:ok, id}
  end

  # 如果 ungroup 则不创建新话题
  defp fetch_question_group_id(group_id, "默认分组", _user_id, %{O: []}) do
    group_id
  end

  # 创建新话题
  defp fetch_question_group_id(_group_id, _name, user_id, current_otpq) do
    {:ok, question_group} =
      %{
        user_id: user_id,
        name: extract_group_name(current_otpq)
      }
      |> Qa.create_question_group
    question_group.id
  end

  # 获取 group name
  defp extract_group_name(otpq) when otpq == nil do
    "默认分组"
  end

  defp extract_group_name(otpq) do
    otpq_o =
    otpq[:O]
    |> List.first()

    otpq_o[:value] || "默认分组"
    # |> IO.inspect(label: ">>>> GROUP NAME")
  end

  @doc """
  处理问题 - 定义、公司基本信息 ================================================
  """
  def run_question_default(%{type: "P", P: [%{value: _name}]} = params) do
    %{params | type: "PQ"}
    |> run_question_default
  end
  def run_question_default(%{type: "PQ", P: [%{value: name}]} = params) do
    with {:ok, term} <- Ontology.find_term_by_name(name) do
      answer =
        term.description
        |> build_answer(params)

      {:ok, answer}
    end
  end
  def run_question_default(%{type: "O", O: [%{code: _code}], P: [],} = params) do
    %{params | type: "OP", P: [%{text: "简介", value: "简介", nlp_reason: "implied"}], Q: [%{value: "definition", nlp_reason: "implied"}]}
    |> run_question_default
  end
  def run_question_default(%{type: "OP", O: [%{code: code, exchange_code: "SZ"}], P: [%{value: "公司规模"}], Q: [%{value: "definition"}]} = params) do
    %{
      O: [%{value: o_value}]
    } = params
    with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
      data =
        xbrl.details
        |> get_in(["年报", "公司规模"])
      answer =
        "截止于#{data["update_time"]}，#{o_value}的公司规模：\n总资产：#{data["总资产"]}(元)\n总负债：#{data["总负债"]}(元)\n总股本：#{data["总股本"]}(万股)\n利润总额：#{data["利润总额"]}(元)"
        |> build_answer(params)

      {:ok, answer}
    end
  end
  def run_question_default(%{type: "OP", O: [%{code: code}], P: [%{value: p_value}], Q: [%{value: "definition"}]} = params) do
    with {:ok, company} <- Ontology.find_company_info_by_code(code) do
      answer =
        company.details["基本信息"][p_value]
        |> build_answer(params)
        # |> IO.inspect(label: "基本信息的处理", pretty: true)

      {:ok, answer}
    end
  end

  @doc """
  处理问题 - XBRL
  """
  def run_question_default(%{type: "OP", Q: [%{value: "quantitive"}]} = params) do
    # IO.inspect(">>>> type: OP")
    %{O: [%{code: code}]} = params
    with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
      process_xbrl_question(%{params | type: "OPQ"}, xbrl)
    end
  end
  def run_question_default(%{type: "OPQ"} = params) do
    # IO.inspect(">>>> type: OPQ")
    %{O: [%{code: code}]} = params
    with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
      process_xbrl_question(params, xbrl)
    end
  end
  def run_question_default(%{type: "OTP", Q: [%{value: "quantitive"}]} = params) do
    # IO.inspect(">>>> type: OTP")
     %{O: [%{code: code}]} = params
    with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
      process_xbrl_question(params, xbrl)
    end
  end
  def run_question_default(%{type: "OTPQ"} = params) do
    # IO.inspect(">>>> type: OTPQ")
    %{O: [%{code: code}]} = params
    with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
      process_xbrl_question(params, xbrl)
    end
  end
  def run_question_default(params) do
    # IO.inspect(">>>> type: DEFAULT")
    Rollbax.report_message(
      :warning,
      "Unable to identify question",
      _custom_data = %{},
      params
    )

    {
      :ok,
      %{
        answer: "暂时无法回答该问题",
        otpq: params
      }
    }
  end


  @doc """
  构造专家问答/董秘问答的回答结果
  """
  defp build_qna_answer(answer, context) when is_binary(answer) do
    {
      :ok,
      %{
        answer: answer,
        otpq: context
      }
    }
  end

  defp build_qna_answer({:ok, pro_qna_question_record} = _param, context) do
    {
      :ok,
      %{
        answer: pro_qna_question_record.answer,
        otpq: context
      }
    }
  end

  defp build_qna_answer({:error, _} = params, context) do
    Rollbax.report_message(
      :warning,
      "Unable to answer question",
      %{},
      params
    )

    {
      :ok,
      %{
        answer: "专家问答异常",
        otpq: context
      }
    }
  end

  @doc """
  构造问题内容，公司基本信息 + 定义，非 xbrl 等
  """

  def build_answer(content, context) do
    %{
      answer: content,
      otpq: context
    }
  end

end
