defmodule AimidasWeb.Graph.Resolvers.RelatedAnswerResolver do
  alias Aimidas.Statistics
  def find_related_answer(_, args, _) do
    statistics_question_id = args[:statistics_question_id]
    # {:ok, Statistics.find_answer_list_by_question_id!(statistics_question_id)}
    {:ok, Statistics.find_answers_by_question_id!(statistics_question_id)}
  end
end
