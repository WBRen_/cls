defmodule AimidasWeb.Graph.Resolvers.ManagementResolver do
  alias Aimidas.Management
  def find_hot_questions(_, _, _) do
    {:ok, Aimidas.Management.find_hot_question!()}
  end
end
