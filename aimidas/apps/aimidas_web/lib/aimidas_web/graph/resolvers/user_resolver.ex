defmodule AimidasWeb.Graph.Resolvers.UserResolver do
  @moduledoc """
  用户解释器
  """
  import AimidasWeb.ErrorHelpers
  import Ecto.Changeset, only: [traverse_errors: 2]
  alias Ecto.Changeset

  @doc """
  返回当前登录用户
  """
  def me(_parent, _args, %{context: %{current_user: current_user}}) do
    {:ok, current_user}
  end

  @doc """
  给当前用户改信息
  """
  def update(_parent, args, %{context: %{current_user: current_user}}) do
    input = args[:input]

    update_attrs = %{nickname: input[:nickname]}
    user_id = current_user.id
    result = with user <- Aimidas.Accounts.get_user!(user_id) do
      Aimidas.Accounts.update_user(user, update_attrs)
    end

    response(result)
  end

  @doc"""
  返回所有用户
  """
  def users(_, _, _) do
    {:ok, Aimidas.Accounts.list_users()}
  end

  @doc"""
  返回用户
  """
  def user(_, args, _) do
    user_id = args[:user_id]
    {:ok, Aimidas.Accounts.get_user!(user_id)}
  end

  def response({:ok, _} = result) do
    result
  end
  def response({:error, %Changeset{} = changeset}) do
    {:error, message: changeset |> traverse_errors(&translate_error/1)}
  end
  def response({:error, message}) do
    {:error, message: message}
  end
end
