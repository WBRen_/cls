# defmodule AimidasWeb.Graph.Resolvers.QuestionResolver do
#   @moduledoc """
#   问答处理器
#   """

#   alias Aimidas.Ontology
#   alias AimidasWeb.NLP.Parser, as: NLPParser
#   alias Aimidas.Qa

#   import AimidasWeb.Xbrl.QuestionProcessor, only: [process_xbrl_question: 2]

#   @doc """
#   处理问题
#   """

#   def question(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
#     input = args[:input]
#     query = input[:query]
#     # 从数据库查询用户上次的 otpq 和 question 信息
#     {otpq, last_question} =
#       current_user.id
#       |> context_from_last_question()

#     question_attrs =
#     %{
#       user_id: current_user.id,
#       query: query,
#       last_otpq: otpq
#     }


#     # {
#     #   type,
#     #   question,
#     #   question_md5,
#     #   context_tag,
#     #   param_holder,
#     #   context
#     # }
#     query
#       |> NLPParser.parse(otpq)
#       |> IO.inspect(label: ">>>> From NLP")
#       |> dispatcher()
#       # |> run_question_xbrl()
#       |> create_question(question_attrs, last_question)
#       |> response()
#       # |> IO.inspect(label: "发送给前端的 resposne ", pretty: true)
#   end

#   @doc """
#     专家问答
#   """
#   def dispatcher(%{type: "pro_qna", question: _question, question_md5: md5, context: context} = _param) do
#     md5
#     |> Qa.find_pro_qna_question
#     |> build_qna_answer(context)
#   end

#   defp build_qna_answer({:ok, answer} = _param, context) do
#     {
#       :ok,
#       %{
#         answer: answer,
#         otpq: context
#       }
#     }
#   end

#   defp build_qna_answer({:error} = _param, context) do
#     {
#       :ok,
#       %{
#         answer: "暂时无法回答此类问题",
#         otpq: context
#       }
#     }
#   end

#   @doc """
#     XBRL
#   """
#   def dispatcher(%{context: context} = _param) do
#     # IO.inspect(context, label: ">>>> NLP Context")
#     context
#     |> run_question_xbrl()
#   end

#   def context_from_last_question(user_id) do
#     user_id
#     |> Qa.get_user_last_question()
#     |> handle_context()
#   end

#   def handle_context({:error, _msg}) do
#     {%{}, %{}}
#   end
#   def handle_context({:ok, last_question}) do
#     {last_question.otpq, last_question}
#   end

#   @doc """
#   解析上下文参数
#   """

#   def parse_context(nil), do: %{}
#   def parse_context(""), do: %{}
#   def parse_context(context), do: context |> Poison.decode!()

#   @doc """
#   处理响应
#   """

#   def response({:ok, _} = result), do: result
#   def response({:error, msg}), do: {:error, message: msg}


#   @doc """
#   存问题进数据库
#   """
#   def create_question({:ok, answer, data}, question_attrs, last_question) do
#     {:ok, question, question_group} = append_into_question_group(answer, last_question, question_attrs)

#     result=
#       %{
#         question_group: question_group,
#         question_group_id: question.question_group_id,
#         id: question.id,
#         answer: %{
#           content: question.answer
#         },
#         context: question.otpq |> Poison.encode!(),
#         data: data |> Poison.encode!()
#     }

#     {:ok, result}
#   end

#   def create_question({:ok, answer}, question_attrs, last_question) do
#     # IO.inspect(question_attrs, label: ">>>> CREATE QUESTION question_attrs")
#     # IO.inspect(answer, label: ">>>> CREATE QUESTION         answer")
#     # IO.inspect(last_question, label: ">>>> CREATE QUESTION  last_question")
#     {:ok, question, question_group} = append_into_question_group(answer, last_question, question_attrs)

#     result=
#       %{
#         question_group: question_group,
#         question_group_id: question.question_group_id,
#         id: question.id,
#         answer: %{
#           content: question.answer
#         },
#         context: question.otpq |> Poison.encode!()
#     }

#     {:ok, result}
#   end

#   def create_question({:error, msg}, question_attrs, last_question) do
#     answer=
#     %{
#       answer: msg,
#       otpq: %{context: 0}
#     }
#     {:ok, question, question_group} = append_into_question_group(answer, last_question, question_attrs)

#     result=
#       %{
#         question_group: question_group,
#         question_group_id: question.question_group_id,
#         id: question.id,
#         answer: %{
#           content: question.answer
#         },
#         context: question.otpq |> Poison.encode!()
#     }

#     {:ok, result}
#   end

#   # when break group
#   def append_into_question_group(%{otpq: %{context: 0}} = answer, _, question_attrs) do
#     {:ok, question_group_attrs} =
#       answer
#       |> convert_group_attrs(question_attrs)
#     {:ok, question_group} =
#       question_group_attrs
#       |> Qa.create_question_group
#     {:ok, question} =
#       question_attrs
#       |> Map.put(:answer, answer.answer)
#       |> Map.put(:otpq, answer.otpq)
#       |> Map.put(:answer_level, 1)
#       |> Map.put(:question_group_id, question_group.id)
#       |> Qa.create_question

#     {:ok, question, question_group}
#   end

#   # when continue group
#   def append_into_question_group(%{otpq: %{context: 1}} = answer, last_question, question_attrs) do
#     question_group =
#       last_question.question_group_id
#       |> Qa.get_question_group!()
#     {:ok, question} =
#       question_attrs
#       |> Map.put(:answer, answer.answer)
#       |> Map.put(:otpq, answer.otpq)
#       |> Map.put(:answer_level, 1)
#       |> Map.put(:question_group_id, question_group.id)
#       |> Qa.create_question

#     {:ok, question, question_group}
#   end

#   # when error from nlp
#   def append_into_question_group(%{otpq: {:error, _}} = answer, %{} = _last_question, question_attrs) do
#     {:ok, question_group_attrs} =
#       answer
#       |> convert_group_attrs(question_attrs)
#     {:ok, question_group} =
#       question_group_attrs
#       |> Qa.create_question_group
#     {:ok, question} =
#       question_attrs
#       |> Map.put(:answer, answer.answer)
#       |> Map.put(:otpq, %{})
#       |> Map.put(:answer_level, 1)
#       |> Map.put(:question_group_id, question_group.id)
#       |> Qa.create_question
#       # |> IO.inspect(label: ">>>> FETCH QUESTION GROUP")

#     {:ok, question, question_group}
#   end

#   # when last question otpq is empty
#   def append_into_question_group(answer, %{} = _last_question, question_attrs) do
#     {:ok, question_group_attrs} =
#       answer
#       |> convert_group_attrs(question_attrs)
#     {:ok, question_group} =
#       question_group_attrs
#       |> Qa.create_question_group
#     {:ok, question} =
#       question_attrs
#       |> Map.put(:answer, answer.answer)
#       |> Map.put(:otpq, answer.otpq)
#       |> Map.put(:answer_level, 1)
#       |> Map.put(:question_group_id, question_group.id)
#       # |> IO.inspect(label: ">>>> FETCH QUESTION GROUP")
#       |> Qa.create_question

#     {:ok, question, question_group}
#   end

#   # default, append into last group
#   def append_into_question_group(answer, last_question, question_attrs) do
#     question_group =
#       last_question.question_group_id
#       |> Qa.get_question_group!()
#     {:ok, question} =
#       question_attrs
#       |> Map.put(:answer, answer.answer)
#       |> Map.put(:otpq, answer.otpq)
#       |> Map.put(:answer_level, 1)
#       |> Map.put(:question_group_id, question_group.id)
#       |> Qa.create_question

#     {:ok, question, question_group}
#   end

#   @doc """
#     convert group 参数
#   """
#   def convert_group_attrs(%{otpq: %{O: [%{value: name}]}} = _answer, question_attrs) do
#     question_group_attrs =
#       %{
#         name: name,
#         user_id: question_attrs.user_id
#       }

#     {:ok, question_group_attrs}
#   end
#   def convert_group_attrs(_, question_attrs) do
#     question_group_attrs =
#       %{
#         name: "ungroup",
#         user_id: question_attrs.user_id
#       }

#     {:ok, question_group_attrs}
#   end

#   @doc """
#   处理问题 - XBRL
#   """

#   def run_question_xbrl(%{type: "P", P: [%{value: _name}]} = params) do
#     %{params | type: "PQ"}
#     |> run_question_xbrl
#   end
#   def run_question_xbrl(%{type: "PQ", P: [%{value: name}]} = params) do
#     with {:ok, term} <- Ontology.find_term_by_name(name) do
#       answer =
#         term.description
#         |> build_answer(params)

#       {:ok, answer}
#     end
#   end
#   def run_question_xbrl(%{type: "O", O: [%{code: _code}], P: [],} = params) do
#     %{params | type: "OP", P: [%{text: "简介", value: "简介", nlp_reason: "implied"}], Q: [%{value: "definition", nlp_reason: "implied"}]}
#     |> run_question_xbrl
#   end
#   def run_question_xbrl(%{type: "OP", O: [%{code: code}], P: [%{value: p_value}], Q: [%{value: "definition"}]} = params) do
#     with {:ok, company} <- Ontology.find_company_info_by_code(code) do
#       answer =
#         company.details["基本信息"][p_value]
#         |> build_answer(params)
#         # |> IO.inspect(label: "基本信息的处理", pretty: true)

#       {:ok, answer}
#     end
#   end
#   def run_question_xbrl(%{type: "OP", Q: [%{value: "quantitive"}]} = params) do
#     %{O: [%{code: code}]} = params
#     with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
#       process_xbrl_question(%{params | type: "OPQ"}, xbrl)
#     end
#   end
#   def run_question_xbrl(%{type: "OPQ"} = params) do
#     %{O: [%{code: code}]} = params
#     with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
#       process_xbrl_question(params, xbrl)
#     end
#   end
#   def run_question_xbrl(%{type: "OTP", Q: [%{value: "quantitive"}]} = params) do
#      %{O: [%{code: code}]} = params
#     with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
#       process_xbrl_question(params, xbrl)
#     end
#   end
#   def run_question_xbrl(%{type: "OTPQ"} = params) do
#     %{O: [%{code: code}]} = params
#     with {:ok, xbrl} <- Ontology.find_company_xbrl_by_code(code) do
#       process_xbrl_question(params, xbrl)
#     end
#   end
#   def run_question_xbrl(params) do
#     {
#       :ok,
#       %{
#         answer: "暂时无法回答此类问题",
#         otpq: params
#       }
#     }
#   end


#   @doc """
#   构造问题内容，公司基本信息 + 定义，非 xbrl 等
#   """

#   def build_answer(content, context) do
#     %{
#       answer: content,
#       otpq: context
#     }
#   end

# end
