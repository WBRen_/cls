defmodule AimidasWeb.Graph.Resolvers.CodeResolver do
  @moduledoc """
  生成图片
  """

  # @nlp_url Application.get_env(:aimidas_web, :nlp_url)
  @url_token "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxd907a379de9ccf45&secret=5f469b08c9dde9eddb9e8e4ab65b6589"

  @url_code "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="
  # @decorate transaction_event("nlp_agent")
  def get_code(_parent, args, _) do
    input = args[:input]
    scene = input[:scene]
    file_name = "code_" <> scene <>".jpg"
    case Aliyun.OSS.CLI.object_exists?("aimidas", file_name) do
      true ->
        return_url = "https://dev.aimidas.com/resource/" <> file_name
        {:ok, %{url: return_url, scene: scene}}
      false ->
        {:ok, data} = get_access_token
        url = @url_code <> data.access_token

        response =
        url
        |> HTTPotion.post([body: data_to_json(scene), headers: headers()])
        |> IO.inspect(label: "二维码", pretty: true)

        File.write!("code_" <> scene <>".jpg", response.body, [])
        Aliyun.OSS.CLI.put_object("aimidas", file_name, file_name)
        File.rm!(file_name)
        # File.open
        return_url = "https://dev.aimidas.com/resource/" <> file_name
        {:ok, %{url: return_url, scene: scene}}
    end
  end

  def get_access_token do
    # IO.inspect(query, label: "[AimidasWeb.NLP.Agent.analyze] query ", pretty: true)
    @url_token
    # |> IO.inspect(label: "[AimidasWeb.NLP.Agent.analyze] url ", pretty: true)
    |> HTTPotion.get([headers: headers()])
    |> handle_response()
    # |> IO.inspect(label: "[AimidasWeb.NLP.Agent.analyze] resposne ", pretty: true)
  end

  def build_data(scene) do
    %{
      scene: scene,
      width: 280,
      page: "pages/index/index",
    }
  end

  def data_to_json(scene) do
    scene
    |> build_data()
    |> Poison.encode!()
    # |> IO.inspect(label: "[发送给NLP端的数据] ： ", pretty: true)
  end

  def headers do
    [
      "Content-Type": "application/json",
      "Accept": "application/json"
    ]
  end

  # @decorate transaction_event()
  def handle_response(%HTTPotion.ErrorResponse{} = response) do
    Rollbax.report_message(:error,
      "Server Error",
      _custom_data = %{},
      %{"HTTPotion.ErrorMessage" => response.message})

    {
      :error,
      response
    }
  end

  # @decorate transaction_event()
  def handle_response(%{} = response) do
    {
      :ok,
      response.body
      |> Poison.decode!()
      |> to_atom_map
    }
  end


  # @decorate transaction_event()
  def to_atom_map(%{} = json_map) do
    json_map
    |> Map.new(fn {k, v} -> {String.to_atom(k), to_atom_map(v)} end)
  end
  def to_atom_map([]), do: []
  def to_atom_map([head|tail]) do
    [to_atom_map(head)] ++ to_atom_map(tail)
  end
  def to_atom_map(data), do: data
end
