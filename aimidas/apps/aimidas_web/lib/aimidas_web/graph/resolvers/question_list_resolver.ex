defmodule AimidasWeb.Graph.Resolvers.QuestionListResolver do
  @moduledoc """
  问答列表
  """

  alias Aimidas.Qa

  def list_question_groups(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]
    user_id = current_user.id
    timestamp = input[:timestamp]
    size = input[:size]

    # IO.inspect(timestamp, label: "timestamp")
    # IO.inspect(size, label: "size")
    # IO.inspect(user_id, label: "user_id")

    {:ok, time} = DateTime.from_unix(timestamp)
    # IO.inspect(time, label: "TimeStamp")
    Qa.find_question_groups(user_id, time, size)
    # |> IO.inspect(label: "List")
  end

  @doc """
    only provided for question_list.ex:33 #resolver
  """
  def to_timestamp(parent, args, resolution) do
    # IO.inspect(parent, label: ">>>>>> parent")
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    {:ok, n_time} =
      parent.updated_at
      |> DateTime.from_naive("Etc/UTC")
      # |> IO.inspect(label: ">>>>>> updated_at")

    timestamp =
      n_time
      |> DateTime.to_unix()

    {:ok, timestamp}
  end

end
