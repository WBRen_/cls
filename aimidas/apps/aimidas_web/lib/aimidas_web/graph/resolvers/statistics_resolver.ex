defmodule AimidasWeb.Graph.Resolvers.StatisticsResolver do
  @moduledoc """
  用户贡献答案、点赞、反对答案
  """

  alias AimidasWeb.StatisticsProcesser
  alias Aimidas.Points
  alias Aimidas.Feedback
  alias Mix.Tasks.Aimidas.Import.Quota
  @doc """
  返回当前登录用户
  """
  def contribute_answer(_parent, args, %{context: %{current_user: current_user}}) do
    input = args[:input]
    question_id = input[:question_id]
    answer = input[:answer]
    user_id = current_user.id
    StatisticsProcesser.append_answer(%{question_id: question_id, answer: answer, user_id: user_id})

    # # 增加积分（异步）
    # Points.add_point(user_id, 1)
    # Points.create_point_record(%{user_id: user_id, type: "AddAnswer", quota: 0, point: 1})
    {:ok, %{message: "感谢您的回答"}}
  end

  def to_timestamp(parent, args, resolution) do
    # IO.inspect(parent, label: ">>>>>> parent")
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    {:ok, n_time} =
      parent.updated_at
      |> DateTime.from_naive("Etc/UTC")
      # |> IO.inspect(label: ">>>>>> updated_at")

    timestamp =
      n_time
      |> DateTime.to_unix()

    {:ok, timestamp}
  end


  # 废弃
  def quota(_parent, args, _) do
    input = args[:input]
    quota = input[:quota]
    # 增加限额（异步）
    Quota.run(quota)
    {:ok, %{message: "更新成功"}}
  end

  def like_answer(_parent, args, %{context: %{current_user: current_user}}) do
    input = args[:input]
    answer_id = input[:answer_id]
    user_id = current_user.id
    # 增加积分（异步）
    # Points.add_point(user_id, 1)
    # Points.create_point_record(%{user_id: user_id, type: "Like", quota: 0, point: 1})
    case StatisticsProcesser.answer_star_plus(answer_id) do
      {:ok, _} -> {:ok, %{message: "点赞成功"}}
      {:error, msg} -> {:error, %{message: msg}}
    end
  end

  def contra_answer(_parent, args, %{context: %{current_user: current_user}}) do
    input = args[:input]
    answer_id = input[:answer_id]
    user_id = current_user.id
    # 增加积分（异步）
    # Points.add_point(user_id, 1)
    # Points.create_point_record(%{user_id: user_id, type: "DisLike", quota: 0, point: 1})
    case StatisticsProcesser.answer_contra_plus(answer_id) do
      {:ok, _} -> {:ok, %{message: "反对成功"}}
      {:error, msg} -> {:error, %{message: msg}}
    end
  end

  def share(_parent, _args, %{context: %{current_user: current_user}}) do
    user_id = current_user.id
    # 增加积分（异步）
    Points.add_point(user_id, 1)
    Points.create_point_record(%{user_id: user_id, type: "Share", quota: 0, point: 1})
    {:ok, %{message: "分享成功"}}
  end

  @doc"""
  返回问题
  """
  def answer(_, args, _) do
    id = args[:answer_id]
    {:ok, Aimidas.Statistics.find_answer!(id)}
  end

  def find_question(_, args, _) do
    statistics_question_id = args[:statistics_question_id]
    # {:ok, Statistics.find_answer_list_by_question_id!(statistics_question_id)}
    {:ok, Aimidas.Statistics.find_question!(statistics_question_id)}
  end

  def find_hot_question(_, _, _) do
    # {:ok, Statistics.find_answer_list_by_question_id!(statistics_question_id)}
    {:ok, Aimidas.Statistics.find_hot_question!()}
  end

  def feedback(_, args, %{context: %{current_user: current_user}}) do
    content = args[:content]
    # {:ok, Statistics.find_answer_list_by_question_id!(statistics_question_id)}
    case Feedback.create_advice(%{content: content, user_id: current_user.id}) do
      {:ok, _} ->
        {:ok, %{message: "提交成功"}}
      {:error, _} ->
        {:ok, %{message: "提交失败"}}
    end
  end

end
