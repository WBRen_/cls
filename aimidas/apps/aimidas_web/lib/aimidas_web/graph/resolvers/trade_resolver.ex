defmodule AimidasWeb.Graph.Resolvers.TradeResolver do
  @moduledoc """
  问答列表
  """

  alias Aimidas.Stocks
  alias AimidasWeb.Stock.Stock
  alias AimidasWeb.Graph.Resolvers.StockResolver
  import AimidasWeb.ErrorHelpers
  import Ecto.Changeset, only: [traverse_errors: 2]
  alias Ecto.Changeset

  def get_portfolio_price(parent, args, resolution) do
    case Stocks.get_portfolio_items(parent.id) do
    {:ok, list} ->
      item_list =
        list
          |> Enum.map(fn item ->
            item.id |> get_item_price()
          end)
      {total_quantity, total_price, total_income, total_out_quantity, total_out_price} = sum_list(item_list, 0, 0, 0, 0, 0)
      # IO.inspect(parent, label: ">>>>>> parent")
      # IO.inspect(args, label: ">>>>>> args")
      # IO.inspect(resolution, label: ">>>>>> resolution")
      # {:ok, point} = parent.user_id |> Points.find_point
      # {:ok, result} =
      #   parent.id |> Stocks.get_user_portfolio_price
      income_rate = 0
      total = 0
      income = 0
      price = 0
      if total_price != 0 do
        price = total_price |> Float.round(3)
      end
      if total_quantity != 0 do
        total = total_quantity
      end
      if total_income != 0 do
        income = total_income |> Float.round(3)
        if total_out_price != 0 do
          income_rate = (total_income / total_out_price) * 100 |> Float.round(2)
        end
      end
      {:ok, %{total: total,
              income: income,
              price: price,
              rate: income_rate}}
    {:error, _} ->
      {:ok, %{total: 0,
              income: 0,
              price: 0,
              rate: 0}}
    end
  end

  def get_item_price(parent, args, resolution) do
    IO.inspect(parent, label: ">>>>>> parent")
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    # {:ok, point} = parent.user_id |> Points.find_point
    case parent.id |> Stocks.get_portfolio_item_trades do
      {:error, _} ->
        attr = %{
          total_quantity: 0,
          average_price: 0,
          total_price: 0
        }
        item = Stocks.get_portfolio_item!(parent.id)
        {:ok, item} = item|> Stocks.update_portfolio_item(attr)
        {:ok, %{
          total_quantity: 0,
          average_price: 0,
          total_price: 0,
          income: 0,
          total_income: 0,
          income_rate: 0,
          total_rate: 0,
          net_price: 0
          }
        }
      {:ok, records} ->
        total_quantity = 0
        average_price = 0
        total_price = 0
        total_income = 0
        out_quantity = 0
        out_price= 0
        {total_quantity, total_price, average_price, total_income, out_quantity, out_price} = sum_list(records, 0, 0, 0, 0, 0, 0)
        attr = %{
          total_quantity: total_quantity,
          average_price: average_price,
          total_price: total_price
        }
        item = Stocks.get_portfolio_item!(parent.id)
        {:ok, item} = item|> Stocks.update_portfolio_item(attr)
        stock = Stocks.get_stock!(parent.stock_id)
        {:ok, value} = Stock.analyze(stock.exchange_type_code, stock.code)
        {now_price, _} = value["当前价格"] |> Float.parse

        total_quantity_out = total_quantity
        average_price_out = 0
        total_price_out = 0
        total_income_out = 0
        income = 0
        income_rate = 0
        total_rate = 0
        net_price = 0

        if average_price != 0 do
          average_price_out = average_price |> Float.round(3)
          income = (now_price - average_price) |> Float.round(3)
          income_rate = (income / average_price) * 100 |> Float.round(2)
        end
        if total_price != 0 do
          total_price_out = total_price |> Float.round(3)
        end
        if total_income != 0 do
          total_income_out = total_income |> Float.round(3)
          if out_price != 0 do
            total_rate = (total_income / out_price) * 100 |> Float.round(2)
          end
        end
        net_price = total_quantity * now_price |> Float.round(3)

        # |> IO.inspect(label: ">>>>>> updated_at")
        # income = now_price - average_price
        #  |> Float.round(3)
        # income_rate = 0
        # if average_price != 0 do
        #   income_rate = (income / average_price) * 100 |> Float.round(2)
        # end
        # total_income = (income * total_quantity)  |> Float.round(3)
        {:ok, %{
          total_quantity: total_quantity_out,
          total_price: total_price_out,
          total_income: total_income_out,
          total_rate: total_rate,
          average_price: average_price_out,
          income: income,
          income_rate: income_rate,
          net_price: net_price
          }
        }
    end
  end

  def get_item_price(id) do
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    # {:ok, point} = parent.user_id |> Points.find_point
    case id |> Stocks.get_portfolio_item_trades do
      {:error, _} ->
        attr = %{
          total_quantity: 0,
          average_price: Decimal.new("0"),
          total_price: Decimal.new("0")
        }
        item = Stocks.get_portfolio_item!(id)
        {:ok, item} = item|> Stocks.update_portfolio_item(attr)
        %{
          total_quantity: 0,
          average_price: 0,
          total_price: 0,
          income: 0,
          total_income: 0,
          out_quantity: 0,
          out_price: 0
          }
      {:ok, records} ->
        total_quantity = 0
        average_price = 0
        total_price = 0
        total_income = 0
        out_quantity = 0
        out_price= 0
        {total_quantity, total_price, average_price, total_income, out_quantity, out_price} = sum_list(records, 0, 0, 0, 0, 0, 0)
        attr = %{
          total_quantity: total_quantity,
          average_price: average_price,
          total_price: total_price
        }
        item = Stocks.get_portfolio_item!(id)
        {:ok, item} = item|> Stocks.update_portfolio_item(attr)
        stock = Stocks.get_stock!(item.stock_id)
        {:ok, value} = Stock.get_last_px(stock.exchange_type_code, stock.code)
        {now_price, _} = value["当前价格"] |> Float.parse
        %{
          total_quantity: total_quantity,
          average_price: average_price,
          total_price: total_price,
          income: now_price - average_price,
          total_income: total_income,
          income_rate: 0,
          out_quantity: out_quantity,
          out_price: out_price
          }
    end
  end

  def get_item_list(parent, args, resolution) do
    IO.inspect(parent, label: ">>>>>> parent")
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    # {:ok, point} = parent.user_id |> Points.find_point
    case parent.id |> Stocks.get_portfolio_item_trades do
      {:error, _} ->
        {:ok, nil}
      {:ok, records} ->
        IO.inspect(records, label: ">>>>>> records")
        {:ok, records}
    end
  end

  def get_portfolio_item(parent, args, resolution) do
    input = args[:input]
    portfolio_item_id = input[:id]
    item = Stocks.get_portfolio_item!(portfolio_item_id)
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    # {:ok, point} = parent.user_id |> Points.find_point
    {:ok, item}
  end

  def get_portfolio(parent, args, resolution) do
    input = args[:input]
    portfolio_id = input[:id]
    item = Stocks.get_portfolio!(portfolio_id)
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    # {:ok, point} = parent.user_id |> Points.find_point
    {:ok, item}
  end

  def sum_list([head | tail], total_quantity, total_price, average_price, total_income, out_quantity, out_price) do
    price = head.price |> Decimal.to_float()
    case head.type do
      "买入" ->
        now_quantity = total_quantity + head.quantity
        now_price = total_price + price * head.quantity
        if now_quantity != 0 do
          now_average_price = now_price/now_quantity
        else
          now_average_price = average_price
        end
        now_income = total_income
        now_out_quantity = out_quantity
        now_out_price = out_price
        sum_list(tail, now_quantity, now_price, now_average_price, now_income, now_out_quantity, now_out_price)
      "卖出" ->
        now_quantity = total_quantity - head.quantity
        now_price = total_price - average_price * head.quantity
        now_average_price = average_price
        now_income = total_income + (price - average_price) * head.quantity
        now_out_quantity = out_quantity + head.quantity
        now_out_price = out_price + average_price * head.quantity
        sum_list(tail, now_quantity, now_price, now_average_price, now_income, now_out_quantity, now_out_price)
      "股息" ->
        now_quantity = total_quantity - head.quantity
        now_price = total_price - average_price * head.quantity
        now_average_price = average_price
        now_income = total_income + price
        now_out_quantity = out_quantity
        now_out_price = out_price
        sum_list(tail, now_quantity, now_price, now_average_price, now_income, now_out_quantity, now_out_price)
      "红股" ->
        now_quantity = total_quantity + head.quantity
        now_price = total_price
        if now_quantity != 0 do
          now_average_price = now_price/now_quantity
        else
          now_average_price = average_price
        end
        now_income = total_income
        now_out_quantity = out_quantity
        now_out_price = out_price
        sum_list(tail, now_quantity, now_price, now_average_price, now_income, now_out_quantity, now_out_price)
    end
  end

  def sum_list([head | tail], total_quantity, total_price, total_income, total_out_quantity, total_out_price) do
    price = head.total_price
    income = head.total_income
    out_quantity = head.out_quantity
    out_price = head.out_price
    sum_list(tail, head.total_quantity + total_quantity, price + total_price, income + total_income, out_quantity + total_out_quantity, out_price + total_out_price)
  end

  def sum_list([], total_quantity, total_price, average_price, total_income, out_quantity, out_price) do
    {total_quantity, total_price, average_price, total_income, out_quantity, out_price}
  end

  def sum_list([], total_quantity, total_price, total_income, total_out_quantity, total_out_price) do
    {total_quantity, total_price, total_income, total_out_quantity, total_out_price}
  end

  def add_portfolio_trade(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]

    user_id = current_user.id
    portfolio_item_id = input[:portfolio_item_id]
    price = input[:price]
    quantity = input[:quantity]
    type = input[:type]

    result =
      %{
        portfolio_item_id: portfolio_item_id,
        price: price,
        quantity: quantity,
        type: type
      }
      |> Stocks.create_portfolio_item_trade

    response(result)

    item = Stocks.get_portfolio_item!(portfolio_item_id)
    {:ok, item}
  end

  def update_portfolio_trade(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]

    user_id = current_user.id
    id = input[:id]
    portfolio_item_id = input[:portfolio_item_id]
    price = input[:price]
    quantity = input[:quantity]
    type = input[:type]

    attr =
      %{
        price: price,
        quantity: quantity,
        type: type
      }

    result = with {:ok, portfolio_item_trade} <-
        Stocks.get_portfolio_item_trade(id)
      do
        Stocks.update_portfolio_item_trade(portfolio_item_trade, attr)
      end
    response(result)

    item = Stocks.get_portfolio_item!(portfolio_item_id)
    {:ok, item}
  end

  def delete_portfolio_trade(_parent, args, %{context: %{current_user: current_user}} = _resolution) do
    input = args[:input]

    user_id = current_user.id
    portfolio_item_id = input[:portfolio_item_id]
    id = input[:id]

    result = with {:ok, portfolio_item_trade} <-
              Stocks.get_portfolio_item_trade(id)
    do
      Stocks.delete_portfolio_item_trade(portfolio_item_trade)
    end
    response(result)

    item = Stocks.get_portfolio_item!(portfolio_item_id)
    {:ok, item}
  end

  def response({:ok, _} = result) do
    result
  end
  def response({:error, %Changeset{} = changeset}) do
    {:error, message: changeset |> traverse_errors(&translate_error/1)}
  end
  def response({:error, message}) do
    {:error, message: message}
  end


  def to_timestamp(parent, args, resolution) do
    # IO.inspect(parent, label: ">>>>>> parent")
    # IO.inspect(args, label: ">>>>>> args")
    # IO.inspect(resolution, label: ">>>>>> resolution")
    {:ok, n_time} =
      parent.updated_at
      |> DateTime.from_naive("Etc/UTC")
      # |> IO.inspect(label: ">>>>>> updated_at")

    timestamp =
      n_time
      |> DateTime.to_unix()

    {:ok, timestamp}
  end
end
