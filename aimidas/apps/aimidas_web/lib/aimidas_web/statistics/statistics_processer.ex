defmodule AimidasWeb.StatisticsProcesser do

  alias Aimidas.Statistics

  @doc """
  问题被询问次数 +1
  """
  def question_plus(%{id: id}) do
    Statistics.add_count(id)
  end
  # def question_plus(%{md5: md5}) do
  #   {:ok, question} = Statistics.find_question(md5)
  #   Statistics.add_count(question.id)
  # end
  def question_plus(%{md5: md5, query: query, otpq: otpq}) do
    {:ok, question} = Statistics.find_question(md5, query, otpq)
    # question |> IO.inspect(label: ">> question")
    Statistics.add_count(question.id)
  end

  @doc """
  问题是否能被回答，statistics_question_id 必须有记录，否则报错
  """
  def mark_me_by_can_answer(statistics_question_id, can_answer) do
    case question = Statistics.find_question_by_id(statistics_question_id) do
      nil -> nil
      question ->
        question |> Statistics.update_question(%{can_answer: can_answer})
    end
  end

  @doc """
  答案被点赞一次
  """
  def answer_star_plus(id) do
    Statistics.add_answer_star_count(id)
  end

  @doc """
  答案被反对一次
  """
  def answer_contra_plus(id) do
    Statistics.add_answer_contra_count(id)
  end

  @doc """
  用户添加答案
  """
  def append_answer(%{question_id: question_id, answer: answer, user_id: user_id}) do
    %{
      question_id: question_id,
      answer: answer,
      user_id: user_id,
      stars: 0,
      contras: 0,
      status: 0
    }
    |> Statistics.create_answer()
  end

  def get_user_answer(statistics_question_id) do
    case Statistics.find_user_answer(statistics_question_id) do
      {:ok, answer} ->
        {:ok, answer}
      {error, _} ->
        {error, "No User Answer"}
    end
  end

  # 如果问题不存在，则会创建该问题记录
  def append_answer(%{question_md5: question_md5, question: question, otpq: otpq, answer: answer, user_id: user_id}) do
    attr = {question_md5, question, otpq}
    with {:ok, question} <- Statistics.find_question(attr) do
      append_answer(%{question_id: question.id, answer: answer, user_id: user_id})
    end
  end

end
