defmodule AimidasWeb.AuthenticationController do
  use AimidasWeb, :controller
  alias Aimidas.Accounts.Authenticator

  alias AimidasWeb.Guardian

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    {:ok, _, user} = Authenticator.authenticate(auth_params(auth))
    conn
    |> put_flash(:info, "Successfully authenticated.")
    |> put_session(:current_user, user)
    |> assign(:current_user, user)
    |> handle_response(auth, user)
  end

  def callback(%{assigns: %{} = auth} = conn, params) do
    fails = auth |> Map.get(:ueberauth_failure)
    IO.puts("ueberauth_failure: #{inspect(fails)}")

    conn
    |> put_flash(:error, "Failed to authenticate")
    |> handle_failure(auth, params)
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "You have been logged out.")
    |> configure_session(drop: true)
    |> redirect(to: "/")
  end

  def handle_failure(conn, auth, %{"provider" => "wechat_miniapp"}) do
    conn
    |> json(auth[:errors])
  end

  def handle_failure(conn, _, _) do
    conn
    |> redirect("/")
  end

  def handle_response(conn, %{provider: :wechat_miniapp}, user) do
    {:ok, token, _full_claims} = Guardian.encode_and_sign(user)

    conn
    |> json(%{result: :ok, token: token})
  end

  def handle_response(conn, _, _) do
    conn
    |> redirect(to: "/")
  end

  def auth_params(%{provider: :github} = auth) do
    %{
      uid: to_string(auth.uid),
      name: auth.info.name || auth.info.nickname,
      nickname: auth.info.nickname,
      image: auth.info.image,
      provider: to_string(auth.provider),
      strategy: to_string(auth.strategy),
      union_id: "",
      token: auth.credentials.token,
      refresh_token: auth.credentials.refresh_token,
      token_secret: auth.credentials.secret
    }
  end

  def auth_params(%{provider: :wechat} = auth) do
    wechat_params(auth)
  end
  def auth_params(%{provider: :wechat_miniapp} = auth) do
    wechat_params(auth)
  end

  defp wechat_params(auth) do
    %{
      uid: auth.uid,
      name: auth.info.name || auth.info.nickname,
      nickname: auth.info.nickname,
      image: auth.info.image,
      provider: to_string(auth.provider),
      strategy: to_string(auth.strategy),
      union_id: Map.get(auth.extra.raw_info.user, "unionid"),
      token: auth.credentials.token,
      refresh_token: auth.credentials.refresh_token,
      token_secret: auth.credentials.secret
    }
  end
end
