defmodule AimidasWeb.Schema do
  @moduledoc """
  GraphQL Schema
  定义GraphQL的查询与修改接口
  """

  use AimidasWeb, :schema

  alias AimidasWeb.Graph.Resolvers.UserResolver
  alias AimidasWeb.Graph.Resolvers.StockResolver
  alias AimidasWeb.Graph.Resolvers.TradeResolver
  alias AimidasWeb.Graph.Mutations.PortfolioMutation
  alias Aimidas.Stocks

  alias AimidasWeb.Graph.Types

  alias AimidasWeb.Graph.Middlewares.LoginRequired

  @desc "GraphQL查询"
  query name: "Query" do
    import_fields :user_queries

    @desc "股票列表"
    field(:stocks, list_of(:stock)) do
      resolve(&StockResolver.list_stocks/3)
    end

    @desc "股票列表（搜索）"
    field(:stocks_search, list_of(:stock)) do
      arg :input, non_null(:stock_search_input)
      resolve(&StockResolver.list_stocks_by_search_word/3)
    end

    @desc "股票（搜索）"
    field(:stock_search, :stock_detail) do
      arg :input, non_null(:stock_search_input)
      resolve(&StockResolver.get_stock_by_search_word/3)
    end

    import_fields :question_queries
    import_fields :question_group_queries
    import_fields :code_create
    import_fields :hot_questions
    import_fields :get_portfolio_item
    import_fields :get_portfolio
  end

  @desc "GraphQL修改请求"
  mutation name: "Mutation" do

    @desc "添加投资组合"
    field(:add_portfolio, :portfolio) do
      arg :input, non_null(:portfolio_input)
      middleware LoginRequired
      resolve &PortfolioMutation.add_portfolio/3
    end

    @desc "修改投资组合"
    field(:update_portfolio, :portfolio) do
      arg :input, non_null(:portfolio_input)
      middleware LoginRequired
      resolve &PortfolioMutation.update_portfolio/3
    end

    @desc "删除投资组合"
    field(:delete_portfolio, :portfolio) do
      arg :input, non_null(:portfolio_input)
      middleware LoginRequired
      resolve &PortfolioMutation.delete_portfolio/3
    end

    @desc "更新投资组合明细"
    field(:stock_result, :stock_result) do
      resolve(&StockResolver.create_stocks/3)
    end

    @desc "添加投资组合明细"
    field(:add_portfolio_item, :portfolio_item) do
      arg :input, non_null(:portfolio_item_input)
      middleware LoginRequired
      resolve &PortfolioMutation.add_portfolio_item/3
    end

    @desc "删除投资组合明细"
    field(:delete_portfolio_item, :portfolio_item) do
      arg :input, non_null(:portfolio_item_input)
      middleware LoginRequired
      resolve &PortfolioMutation.delete_portfolio_item/3
    end

    @desc "添加投资组合交易明细"
    field(:add_portfolio_item_trade, :portfolio_item) do
      arg :input, non_null(:portfolio_item_trade_add_input)
      middleware LoginRequired
      resolve &TradeResolver.add_portfolio_trade/3
    end

    @desc "添加投资组合交易明细"
    field(:update_portfolio_item_trade, :portfolio_item) do
      arg :input, non_null(:portfolio_item_trade_update_input)
      middleware LoginRequired
      resolve &TradeResolver.update_portfolio_trade/3
    end

    @desc "删除投资组合交易明细"
    field(:delete_portfolio_item_trade, :portfolio_item) do
      arg :input, non_null(:portfolio_item_trade_delete_input)
      middleware LoginRequired
      resolve &TradeResolver.delete_portfolio_trade/3
    end

    import_fields :import_pro_qna_questions
    import_fields :delete_pro_qna_questions
    import_fields :import_dm_qna_questions
    import_fields :delete_dm_qna_questions
    import_fields :import_pdf_qna_questions
    import_fields :delete_pdf_qna_questions
    import_fields :user_answers
    import_fields :user_update
    import_fields :user_points
  end
end
