defmodule AimidasWeb.Types do
  @moduledoc """
  GraphQL 类型
  """

  use AimidasWeb, :type

  alias AimidasWeb.Graph.Types

  import_types Types.User
  import_types Types.Portfolio
  import_types Types.PortfolioItem
  import_types Types.Stock
  import_types Types.Question
  import_types Types.QuestionGroupList
  import_types Types.ImportProQnaQuestions
  import_types Types.ImportDmQnaQuestions
  import_types Types.ImportPdfQnaQuestions
  import_types Types.UserAnswer
  import_types Types.ShareCode
  import_types Types.UserPoint
  import_types Types.HotQuestions
end
