defmodule AimidasWeb.Mixfile do
  use Mix.Project

  def project do
    [
      app: :aimidas_web,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {AimidasWeb.Application, []},
      extra_applications: [
        # :appsignal,
        :logger,
        :runtime_tools,
        :ueberauth,
        :ueberauth_github,
        :ueberauth_wechat,
        :ueberauth_wechat_miniapp,
        :httpotion,
        :parse_trans,
        :ssl
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.3.2"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_ecto, "~> 3.2"},
      {:phoenix_html, "~> 2.10"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:gettext, "~> 0.11"},
      {:aimidas, in_umbrella: true},
      {:ueberauth, "~> 0.5"},
      {:ueberauth_github, github: "edwardzhou/ueberauth_github"},
      {:ueberauth_wechat, github: "edwardzhou/ueberauth_wechat"},
      {:ueberauth_wechat_miniapp, github: "edwardzhou/ueberauth_wechat_miniapp"},
      {:guardian, "~> 1.0"},
      {:absinthe, "~> 1.4"},
      {:absinthe_plug, "~> 1.4"},
      {:absinthe_ecto, "~> 0.1.3"},
      {:absinthe_relay, "~> 1.4"},
      {:poison, "~> 3.1"},
      {:credo, "~> 0.9", only: [:dev, :test], runtime: false},
      {:httpotion, "~> 3.1.0"},
      {:exvcr, github: "edwardzhou/exvcr", only: [:dev, :test]},
      {:cowboy, "~> 1.0"},
      {:rollbax, ">= 0.0.0"},
      {:html_sanitize_ex, "~> 1.3.0-rc3"},
      {:number, "~> 0.5.7"},
      {:aliyun, "~> 1.0"},
      {:plug_cowboy, "~> 1.0"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, we extend the test task to create and migrate the database.
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["test": ["ecto.create --quiet", "ecto.migrate", "test"]]
  end
end
