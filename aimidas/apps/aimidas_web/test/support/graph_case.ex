defmodule AimidasWeb.GraphCase do
  @moduledoc ~S"""
  This module defines the test case to be used by tests
  that requires running GraphQL queries and mutations.

  ## Example:
      @tag document: \"\"\"
      query get_user($id: ID!) {
        user(id: $id) {
          id,
          nickname,
          email
        }
      }
      \"\"\"
      test "gets a single user", tags do
        data = execute_document(tags)[:data]
      end

  """

  use ExUnit.CaseTemplate

  using do
    quote do
      defp execute_document(tags, status_code \\ 200) do
        {schema, tags}      = Map.pop(tags, :schema, AimidasWeb.Schema)
        {query, tags}       = Map.pop(tags, :query)
        {context, tags}     = Map.pop(tags, :context, %{})
        {variables, _tags}    = Map.pop(tags, :variables, %{})

        Absinthe.run!(query, schema, [context: context, variables: variables])
      end
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Aimidas.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Aimidas.Repo, {:shared, self()})
    end

    # cleanup. maybe this should be a transaction rollback?
    Aimidas.Repo.delete_all(Aimidas.Accounts.Authentication)
    Aimidas.Repo.delete_all(Aimidas.Accounts.User)

    :ok
  end
end
