defmodule AimidasWeb.UnitTransform.UnitTransformTest do
  use ExUnit.Case, async: true
  use ExVCR.Mock
  alias AimidasWeb.UnitTransform.UnitKeyMapping
  alias AimidasWeb.UnitTransform.UnitProcessor
  describe "unit_transform" do
    # test "单位映射_1", _ do
    #   assert {"元", :convert_float} == UnitKeyMapping.key_map("SH", "应付职工薪酬")
    # end
    # #这里是P不存在的情况，由于查不到数据为空时是无法调用该方法的，该测试是无必要的。
    # test "单位映射_2(不存在)", _ do
    #   assert nil == UnitKeyMapping.key_map("SH", "hahaha")
    # end

    test "单位转换_1", _ do
      assert "8195元" == UnitProcessor.form_value(8195, "SH", "应付职工薪酬")
    end
    #这里是P不存在的情况，由于查不到数据为空时是无法调用该方法的，该测试是无必要的。
    test "单位转换_2(不存在)", _ do
      assert "12312321321" == UnitProcessor.form_value(12312321321, "SH", "hahaha")
    end

    test "单位转换_3", _ do
      assert "1.231百万元" == UnitProcessor.form_value(1231231, "SH", "应付职工薪酬")
    end

    test "单位转换_4", _ do
      assert "123131231.312亿元" == UnitProcessor.form_value(12313123131231312, "SH", "应付职工薪酬")
    end

    test "数字转换_1", _ do
      assert {123, ""} == UnitProcessor.num_transform(123)
    end

    test "数字转换_2", _ do
      assert {12.312, "万"} == UnitProcessor.num_transform(123123)
    end

    test "数字转换_3", _ do
      assert {123131231.312, "亿"} == UnitProcessor.num_transform(12313123131231312)
    end
  end
end
