defmodule AimidasWeb.Graph.Mutations.PortfolioMutationTest do

  @moduledoc """
  投资组合修改接口测试
  """

  use AimidasWeb.GraphCase
  alias Aimidas.Accounts
  alias Aimidas.Stocks

  @portfolio_name "the existing portfolio"

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  @another_user_attrs %{
    nickname: "another_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  @portfolio_attrs %{
    user_id: 0,
    name: @portfolio_name
  }

  @another_portfolio_attrs %{
    user_id: 0,
    name: "another portfolio"
  }

  @portfolio_item_stock_id 0

  @stock_attrs %{
    #    user_id: 0,
    id: @portfolio_item_stock_id,
    code: "000001",
    name: "中国平安银行",
    exchange_type_code: "HK",
    last_px: 1232.12,
    last_px_at: DateTime.utc_now,
    px_exchange_rate: 32
  }

  def fixture(:user), do: fixture(:user, @user_attrs)
  def fixture(:stock), do: fixture(:stock, @stock_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  def fixture(:portfolio, user) do
    fixture(:portfolio, user, @portfolio_attrs)
  end

  def fixture(:stock, stock) do
    stock
    |> Stocks.create_stock()
  end

  def fixture(:portfolio, user, attrs) do
    %{attrs | user_id: user.id}
    |> Stocks.create_portfolio()
  end

  def fixture(:portfolio_item, user_id, portfolio_id, stock_id) do
    %{
      user_id: user_id,
      portfolio_id: portfolio_id,
      stock_id: stock_id
    }
    |> Stocks.create_portfolio_item()
  end

  defp with_add_portfolio_mutation(_tags) do
    mutation =
      """
      mutation test_add_portfolio($input: PortfolioInput!) {
        addPortfolio(input: $input) {
          id
          name

          user {
            id
            nickname
          }
        }
      }
      """

    {:ok, query: mutation}
  end

  defp with_update_portfolio_mutation(_tags) do
    mutation =
      """
      mutation test_update_portfolio($input: PortfolioInput!) {
        updatePortfolio(input: $input) {
          id
          name

          user {
            id
            nickname
          }
        }
      }
      """

    {:ok, query: mutation}
  end

  defp with_delete_portfolio_mutation(_tags) do
    mutation =
      """
      mutation test_delete_portfolio($input: PortfolioInput!) {
        deletePortfolio(input: $input) {
          id
          name

          user {
            id
            nickname
          }
        }
      }
      """

    {:ok, query: mutation}
  end

  defp with_add_portfolio_item_mutation(_tags) do
    mutation =
      """
      mutation test_add_portfolio_item($input: PortfolioItemInput!) {
        addPortfolioItem(input: $input) {
          stock{
            id
            code
            name
          }

          portfolio{
            id
            name
          }

          user {
            id
            nickname
          }
        }
      }
      """

    {:ok, query: mutation}
  end

  defp with_delete_portfolio_item_mutation(_tags) do
    mutation =
      """
      mutation test_delete_portfolio_item($input: PortfolioItemInput!) {
        deletePortfolioItem(input: $input) {
          id

          stock{
            id
            code
            name
          }

          portfolio{
            id
            name
          }

          user {
            id
            nickname
          }
        }
      }
      """

    {:ok, query: mutation}
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  # """
  # 创建测试投资组合
  # """
  defp with_portfolio(tags) do
    {:ok, new_portfolio} = fixture(:portfolio, tags.user)
    {:ok, portfolio: new_portfolio}
  end

  describe "创建新的投资组合" do
    setup [:with_add_portfolio_mutation, :with_user]
    test "返回新的投资组合", tags do
      context = %{current_user: tags.user}
      portfolio_input = %{
        "input" => %{
          "name" => @portfolio_name
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      portfolio = response[:data]["addPortfolio"]
      assert portfolio["name"] == @portfolio_name
      assert portfolio["user"]["id"] == tags.user.id |> to_string()
    end
  end

  describe "添加该用户中已存在的投资组合名称" do
    setup [:with_add_portfolio_mutation, :with_user, :with_portfolio]

    test "返回名称已存在错误", tags do
      context = %{current_user: tags.user}
      portfolio_input = %{
        "input" => %{
          "name" => @portfolio_name
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_input)
        |> Map.put(:context, context)

      response = execute_document(tags)
      errors = response[:errors]
      assert errors != nil
      assert [%{message: %{name: ["has already been taken"]}}] = errors
    end
  end

  describe "需要登录" do
    setup [:with_add_portfolio_mutation]

    test "返回需要登录错误", tags do
      context = %{}
      portfolio_input = %{
        "input" => %{
          "name" => "the new portfolio"
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_input)
        |> Map.put(:context, context)

      response = execute_document(tags)
      errors = response[:errors]
      assert errors != nil
      assert [%{message: "unauthenticated"}] = errors
    end
  end

  describe "修改投资组合名称" do
    setup [:with_update_portfolio_mutation, :with_user, :with_portfolio]

    test "返回修改后的投资组合", tags do
      context = %{current_user: tags.user}

      portfolio_input = %{
        "input" => %{
          "id" => tags.portfolio.id,
          "name" => "updated portfolio"
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      portfolio = response[:data]["updatePortfolio"]
      assert portfolio != nil
      assert portfolio["name"] == "updated portfolio"
    end

    test "修改他人的投资组合", tags do

      {:ok, another_user} = fixture(:user, @another_user_attrs)
      {:ok, another_portfolio} = fixture(
        :portfolio,
        another_user,
        %{@another_portfolio_attrs | user_id: another_user.id}
      )

      context = %{current_user: tags.user}

      portfolio_input = %{
        "input" => %{
          "id" => another_portfolio.id,
          "name" => "updated portfolio"
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      errors = response[:errors]
      assert errors != nil
      assert [%{message: "portfolio not found"}] = errors
    end
  end

  describe "删除投资组合" do
    setup [:with_delete_portfolio_mutation, :with_user, :with_portfolio]

    test "返回删除后的投资组合", tags do
      context = %{current_user: tags.user}

      portfolio_input = %{
        "input" => %{
          "id" => tags.portfolio.id,
          "name" => tags.portfolio.name
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      {:error, _} = Stocks.get_user_portfolio(tags.user.id, tags.portfolio.id)
      portfolio = response[:data]["deletePortfolio"]
      assert portfolio["id"] == tags.portfolio.id |> to_string()
      errors = response[:errors]
      assert errors == nil
    end

    test "删除他人的投资组合", tags do

      {:ok, another_user} = fixture(:user, @another_user_attrs)
      {:ok, another_portfolio} = fixture(
        :portfolio,
        another_user,
        %{@another_portfolio_attrs | user_id: another_user.id}
      )

      context = %{current_user: tags.user}

      portfolio_input = %{
        "input" => %{
          "id" => another_portfolio.id,
          "name" => "updated portfolio"
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      errors = response[:errors]
      assert errors != nil
      assert [%{message: "portfolio not found"}] = errors
    end

  end

  describe "增加投资组合 Item" do

    setup [:with_add_portfolio_item_mutation, :with_user, :with_portfolio]

    test "返回一个新的投资组合项", tags do
      {:ok, stock} = fixture(:stock)

      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => tags.portfolio.id,
          "stock_id" => stock.id
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      portfolio_item = response[:data]["addPortfolioItem"]
      assert portfolio_item["portfolio"]["id"] == tags.portfolio.id |> to_string()
      assert portfolio_item["stock"]["id"] == stock.id |> to_string()
      assert portfolio_item["user"]["id"] == tags.user.id |> to_string()
    end

    test "stock_id 不存在", tags do
      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => tags.portfolio.id,
          "stock_id" => 1
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      errors = response[:errors]
      assert errors != nil
      assert [%{message: "stock not found"}] = errors
    end

    test "向 portfolio 添加重复到 stock_id", tags do
      {:ok, stock} = fixture(:stock)

      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => tags.portfolio.id,
          "stock_id" => stock.id
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      portfolio_item = response[:data]["addPortfolioItem"]
      assert portfolio_item["portfolio"]["id"] == tags.portfolio.id |> to_string()
      assert portfolio_item["stock"]["id"] == stock.id |> to_string()
      assert portfolio_item["user"]["id"] == tags.user.id |> to_string()

      portfolio_item_input2 = %{
        "input" => %{
          "portfolio_id" => tags.portfolio.id,
          "stock_id" => stock.id
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input2)
        |> Map.put(:context, context)

      response2 = execute_document(tags)

      errors = response2[:errors]
      assert errors != nil
      assert [%{message: %{stock: ["has already been taken"]}}] = errors
    end
  end

  describe "增加投资组合 Item（portfolio 不存在时）" do

    setup [:with_add_portfolio_item_mutation, :with_user]
    test "投资组合项 portfolio_id 不存在", tags do
      {:ok, stock} = fixture(:stock)

      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => 1,
          "stock_id" => stock.id
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      errors = response[:errors]
      assert errors != nil
      assert [%{message: "portfolio not found"}] = errors
    end

    test "portfolio_id 指向到是其他人到 portfolio", tags do
      {:ok, another_user} = fixture(:user, @another_user_attrs)
      {:ok, another_portfolio} = fixture(
        :portfolio,
        another_user,
        %{@another_portfolio_attrs | user_id: another_user.id}
      )
      {:ok, stock} = fixture(:stock)

      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => another_portfolio.id,
          "stock_id" => stock.id
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      errors = response[:errors]
      assert errors != nil
      assert [%{message: "portfolio not found"}] = errors
    end
  end

  describe "删除投资组合项" do

    setup [:with_delete_portfolio_item_mutation, :with_user, :with_portfolio]

    test "删除一个投资组合项", tags do
      {:ok, stock} = fixture(:stock)

      {:ok, portfolio_item} = fixture(:portfolio_item,
                                      tags.user.id,
                                      tags.portfolio.id,
                                      stock.id)

      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => tags.portfolio.id,
          "stock_id" => stock.id
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      assert response[:errors] == nil
      deleted_item = response[:data]["deletePortfolioItem"]
      assert deleted_item["id"] == portfolio_item.id |> to_string()
    end

    test "stock_id 不存在", tags do
      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => tags.portfolio.id,
          "stock_id" => 1
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      errors = response[:errors]
      assert errors != nil
      assert [%{message: "portfolio_item not found"}] = errors
    end

    test "portfolio_id 不存在", tags do
      {:ok, stock} = fixture(:stock)

      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => 99999,
          "stock_id" => stock.id
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      errors = response[:errors]
      assert errors != nil
      assert [%{message: "portfolio_item not found"}] = errors
    end

    test "删除他人的投资组合项", tags do
      {:ok, stock} = fixture(:stock)
      {:ok, another_user} = fixture(:user, @another_user_attrs)
      {:ok, another_portfolio} = fixture(
        :portfolio,
        another_user,
        %{@another_portfolio_attrs | user_id: another_user.id}
      )
      {:ok, portfolio_item} =
        %{
          user_id: another_user.id,
          portfolio_id: another_portfolio.id,
          stock_id: stock.id
        }
        |> Stocks.create_portfolio_item()

      context = %{current_user: tags.user}
      portfolio_item_input = %{
        "input" => %{
          "portfolio_id" => another_portfolio.id,
          "stock_id" => stock.id
        }
      }

      tags =
        tags
        |> Map.put(:variables, portfolio_item_input)
        |> Map.put(:context, context)

      response = execute_document(tags)

      errors = response[:errors]
      assert errors != nil
      assert [%{message: "portfolio_item not found"}] = errors
    end
  end
end
