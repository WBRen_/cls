defmodule AimidasWeb.Graph.Queries.UsersTest do
  @moduledoc """
  graphql 用户查询测试
  """

  use AimidasWeb.GraphCase
  alias Aimidas.Accounts

  defp with_users_query(_tags) do
    query =
      """
      query test_query {
        users {
          id,
          nickname,
          avatar
        }
      }
      """

    {:ok, query: query}
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_users(_tags) do
    %{
      nickname: "test_user",
      avatar: "head_icon",
      password_digest: "password",
      password_salt: "salt"
    }
    |> Accounts.create_user()

    :ok
  end

  # describe "users query" do
  #   setup [:with_users_query, :with_users]

  #   test "returns all users", tags do
  #     response =
  #       tags
  #       |> execute_document()

  #     data = response[:data]
  #     users = data["users"]
  #     assert Enum.count(users) == 1
  #     assert get_in(users, [Access.at(0), "nickname"]) == "test_user"
  #   end
  # end
end
