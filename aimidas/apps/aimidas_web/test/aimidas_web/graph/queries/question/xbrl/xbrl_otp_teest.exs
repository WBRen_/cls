defmodule AimidasWeb.Graph.Queries.Questions.XbrlOTPTest do
  @moduledoc """
  问答中的XBRL OPQ查询测试用例
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock

  alias Aimidas.Ontology
  alias Aimidas.Accounts

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  def fixture(:company_xbrl_cmb) do

    details =
      __DIR__
      |> Path.dirname()
      |> Path.join(["../", "../", "../", "../", "fixtures/", "xbrl_600036.json"])
      |> Path.expand()
      # |> IO.inspect(label: "json path:  ")
      |> File.read!()
      # |> IO.inspect(label: "xbrl_600036.json => ")
      |> Poison.decode!()
      |> Map.get("details")
      # |> IO.inspect(label: "details:  ")

    %{
      company_code: "600036",
      abbrev_name: "招商银行",
      full_name: "招商银行有限公司",
      exchange_code: "SH",
      src_updated_at: Date.utc_today(),
      details: details
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()

    # %{
    #   company_code: "600036",
    #   abbrev_name: "招商银行",
    #   full_name: "招商银行有限公司",
    #   exchange_code: "SH",
    #   src_udpated_at: Date.utc_today(),
    #   details: %{
    #     "年报" => %{
    #       "latest_year" => "2017",
    #       "基本信息" => %{
    #         "2017" => %{
    #           "公司法定中文名称" => "招商银行股份有限公司",
    #           "公司法定代表人" => "李建红"
    #         },
    #         "2016" => %{}
    #       },
    #       "股本结构" => %{
    #         "2017" => %{

    #         },
    #         "2016" => %{

    #         }
    #       },
    #       "资产负债表" => %{
    #         "2017" => %{
    #           "股本" => "25,220,000,000"
    #         }
    #       },
    #       "利润表" => %{
    #         "2017" => %{
    #           "营业收入" => "1,605,304,558.92"
    #         }
    #       }
    #     },
    #     "一季报" => %{

    #     },
    #     "半年报" => %{

    #     },
    #     "三季报" => %{

    #     }
    #   }
    # }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    # |> Ontology.create_company_xbrl()
  end

  def fixture(:company_xbrl_pufa) do
    %{
      company_code: "600000",
      abbrev_name: "浦发银行",
      full_name: "上海浦东发展银行股份有限公司",
      exchange_code: "SH",
      src_updated_at: Date.utc_today(),
      details: %{
        "年报" => %{
          "latest_year" => "2017",
          "基本信息" => %{
            "2017" => %{
              "公司法定中文名称" => "上海浦东发展银行股份有限公司",
              "公司法定代表人" => "高国富"
            },
            "2016" => %{}
          },
          "股本结构" => %{
            "2017" => %{

            },
            "2016" => %{

            }
          },
          "资产负债" => %{
            "2017" => %{
              "股本" => "29,352,000,000"
            }
          },
          "利润" => %{
            "2017" => %{
              "营业收入" => "1,605,304,558.92"
            }
          }
        },
        "一季报" => %{

        },
        "半年报" => %{

        },
        "三季报" => %{

        }
      }
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()
  end

  describe "简单的XBRL OTP(查询财务信息)" do

    defp with_data(_tags) do
      fixture(:company_xbrl_cmb)
      fixture(:company_xbrl_pufa)

      :ok
    end

    setup [:with_query, :with_data, :with_user]

    @doc """
    MIA-US0012-AC001 公司参数
    Given 在QnA页面
    And 数据中有"600036" "2017" "股本" 为 "25220000000"
    When 点击输入框
    And 招商银行2017年的股本
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017年的股本是25220000000。"
    """
    test "询问 OTP财务信息 - 招商银行2017年的股本", tags do
      response = use_cassette "questions/xbrls/query_nlp_cmb_otp_gb_2017" do
        execute_query(tags, "招商银行2017年的股本")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017年的股本是 252.2亿元"
    end

    @doc """
    MIA-US0012-AC002 公司参数
    Given 在QnA页面
    And 数据中有"600000" "2017" "股本" 为 "29,352,000,000"
    When 点击输入框
    And 一个问题：“招商银行2017年的净利润”
    #默认 T=[（最新年份）]
    Then 得到回答 "浦发银行2017年的股本是201亿。"
    """
    test "询问 OTP财务信息 - 招商银行2017年的净利润", tags do
      response = use_cassette "questions/xbrls/query_nlp_cmb_otp_net_profit_2017" do
        execute_query(tags, "招商银行2017年的净利润")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017年的净利润是 706.38亿元"
    end

    @doc """
    MIA-US012-AC003 公司参数
    Given 在迈达斯的QnA页面
    And 数据库有 "600036" "2016" "净利润" 为 "62,380,000,000"
    And 点击输入框
    And 一个问题：“招商银行2016年的净利润”
    #默认 Q=["quantity"]
    Then 得到回答 "招商银行2016年的股本是62,380,000,000。"
    """
    test "询问 OTP财务信息 - 招商银行2016年的净利润", tags do
      response = use_cassette "questions/xbrls/query_nlp_cmb_otp_net_profit_2016" do
        execute_query(tags, "招商银行2016年的净利润")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2016年的净利润是 623.8亿元"
    end

    @doc """
    MIA-US0012-AC004 公司参数
    Given 在迈达斯的QnA页面
    and Mysql数据库aimidas_xbrl_ss里"600036" "2017" 无 参数"额度"
    And 点击输入框
    And 一个问题：“招商银行2017年的额度”
    #默认 Q=["quantity"]
    Then 得到回答 "招商银行2017年暂无此数据"
    """
    test "询问 OTP财务信息 - 招商银行2017年的额度", tags do
      response = use_cassette "questions/xbrls/query_nlp_otp_non_match" do
        execute_query(tags, "招商银行2017年的额度")
      end

      # response
      # |> IO.inspect(label: "招商银行2017年的额度 => ", pretty: true)

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "暂时无法回答该问题"
    end

    @doc """
    MIA-US012-AC005 公司参数
    Given 在迈达斯的QnA页面
    And 数据库里"600036" 无 "2010" 数据
    And 点击输入框
    And 一个问题：“招商银行2010年的净利润”
    #默认 Q=["quantity"]
    Then 得到回答 "招商银行2010年暂无此数据"
    """
    test "询问 OTP财务信息 - 招商银行2010年的净利润", tags do
      response = use_cassette "questions/xbrls/query_nlp_otp_no_2010" do
        execute_query(tags, "招商银行2010年的净利润")
      end

      # response
      # |> IO.inspect(label: "招商银行2010年的净利润 => ", pretty: true)

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2010年暂无此数据"
    end
  end

end
