defmodule AimidasWeb.Graph.Queries.QuestionOTest do
  @moduledoc """
  问答中的O查询测试用例
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock

  alias Aimidas.Ontology
  alias Aimidas.Accounts

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  def fixture(:company_cmb) do
    %{
      abbrev_name: "招商银行",
      company_code: "600036",
      details: %{
        "基本信息" => %{
          "简介" => "招商银行1987年成立于中国改革开放的最前沿——深圳蛇口,是中国境内第一家完全由企业法人持...",
          "组织形式" => "国有企业"
        }
      },
      full_name: "招商银行股份有限公司",
      season: 4,
      year: 2017
    }
    |> Ontology.create_company_info
  end

  def fixture(:company_pufa) do
    %{
      company_code: "600000",
      abbrev_name: "浦发银行",
      full_name: "上海浦东发展银行股份有限公司",
      year: 2017,
      season: 4,
      details: %{
        "基本信息" => %{
          "简介" => "上海浦东发展银行股份有限公司(以下简称:浦发银行)是1992年8月28日经中国人民银行批准设立、1993年1月9日开业、1999年在上海证券交易所挂牌上市(股票交易代码:600000)的全国性股份制商业银行,总行设在上海。目前,注册资本金196.53亿元。良好的业绩、诚信的声誉,使浦发银行成为中国证券市场中备受关注和尊敬的上市公司。",
          "组织形式" => "国有企业"
        }
      }
    }
    |> Ontology.create_company_info
  end

  describe "简单的O(查询简介)" do
    defp with_companies(_tags) do
      fixture(:company_cmb)
      fixture(:company_pufa)

      :ok
    end

    setup [:with_query, :with_companies, :with_user]

    @doc """
    MIA-US0009-AC001 公司介绍
    Given 在迈达斯的QnA页面
    And company_infos里 "招商银行" 的 "公司基本信息"
        里 "简介" 是："招商银行1987年成立于中国改革开放的最...."
    And 点击输入框
    And 输入一个问题：“招商银行”
    And 点击输入框右侧的按钮
    Then 得到回答 "招商银行1987年成立于中国改革开放的最....
    """
    test "询问 简单的O - 招商银行", tags do
      response = use_cassette "query_nlp_o_cmb" do
        execute_query(tags, "招商银行")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/招商银行1987年成立于中国改革开放的最/
    end

    @doc """
    MIA-US0009-AC002 公司介绍
    Given 在QnA页面
    And company_infos 里 "浦发银行" 的 "公司基本信息" 里
      "简介" 是："上海浦东发展银行股份有限公司(以下简称:浦发银行)是...."
    And 点击输入框
    And 输入一个问题：“浦发银行”
    And 点击输入框右侧的按钮
    Then 得到回答 "上海浦东发展银行股份有限公司(以下简称:浦发银行)是..."
    """
    test "询问 简单的O - 浦发银行", tags do
      response = use_cassette "query_nlp_o_pufa" do
        execute_query(tags, "浦发银行")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/上海浦东发展银行股份有限公司/
    end

    @doc """
    MIA-US0009-AC003 公司介绍
    Given 在迈达斯的QnA页面
        and MongoDB数据库CompanyData里无 "迈达斯"
    And 点击输入框
    And 输入一个问题：“迈达斯”
    And 点击输入框右侧的按钮
    Then 得到回答 "暂无此公司介绍。"
    """
    test "询问 简单的O - 迈达斯", tags do
      response = use_cassette "query_nlp_o_nonexist" do
        execute_query(tags, "迈达斯")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      # assert question["answer"]["content"] =~ ~R/not found/
      assert question["answer"]["content"] =~ ~R/暂时无法回答该问题/
    end
  end
end
