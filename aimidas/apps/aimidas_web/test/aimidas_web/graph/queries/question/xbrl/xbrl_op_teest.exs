defmodule AimidasWeb.Graph.Queries.Questions.XbrlOPTest do
  @moduledoc """
  问答中的XBRL OPQ查询测试用例
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock

  alias Aimidas.Ontology
  alias Aimidas.Accounts

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  def fixture(:company_xbrl_cmb) do

    details =
      __DIR__
      |> Path.dirname()
      |> Path.join(["../", "../", "../", "../", "fixtures/", "xbrl_600036.json"])
      |> Path.expand()
      # |> IO.inspect(label: "json path:  ")
      |> File.read!()
      # |> IO.inspect(label: "xbrl_600036.json => ")
      |> Poison.decode!()
      |> Map.get("details")
      # |> IO.inspect(label: "details:  ")

    %{
      company_code: "600036",
      abbrev_name: "招商银行",
      full_name: "招商银行有限公司",
      exchange_code: "SH",
      src_updated_at: Date.utc_today(),
      details: details
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()
  end

  def fixture(:company_xbrl_pufa) do
    %{
      company_code: "600000",
      abbrev_name: "浦发银行",
      full_name: "上海浦东发展银行股份有限公司",
      exchange_code: "SH",
      src_updated_at: Date.utc_today(),
      details: %{
        "年报" => %{
          "latest_year" => "2017",
          "基本信息" => %{
            "2017" => %{
              "公司法定中文名称" => "上海浦东发展银行股份有限公司",
              "公司法定代表人" => "高国富"
            },
            "2016" => %{}
          },
          "股本结构" => %{
            "2017" => %{

            },
            "2016" => %{

            }
          },
          "资产负债" => %{
            "2017" => %{
              "股本" => 29_352_000_000
            }
          },
          "利润" => %{
            "2017" => %{
              "营业收入" => 1_605_304_558.92
            }
          }
        },
        "一季报" => %{

        },
        "半年报" => %{

        },
        "三季报" => %{

        }
      }
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()
  end

  describe "简单的XBRL OPQ(查询财务信息)" do

    defp with_data(_tags) do
      fixture(:company_xbrl_cmb)
      fixture(:company_xbrl_pufa)

      :ok
    end

    setup [:with_query, :with_data, :with_user]

    @doc """
    MIA-US0011-AC001 公司参数
    Given 在QnA页面
    And 数据中有"600036" "2017" "股本" 为 "25220000000"
    When 点击输入框
    And 一个问题：“招商银行的股本是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017年的股本是25220000000。"
    """
    test "询问 OPQ财务信息 - 招商银行的股本是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_cmb_opq_gb" do
        execute_query(tags, "招商银行的股本是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017年的股本是 252.2亿元"
    end

    @doc """
    MIA-US0011-AC002 公司参数
    Given 在QnA页面
    And 数据中有"600000" "2017" "股本" 为 "29,352,000,000"
    When 点击输入框
    And 一个问题：“浦发银行的股本是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "浦发银行2017年的股本是201亿。"
    """
    test "询问 OPQ财务信息 - 浦发银行的股本是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_pf_opq_gb" do
        execute_query(tags, "浦发银行的股本是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "浦发银行2017年的股本是 293.52亿元"
    end

    @doc """
    MIA-US0011-AC003 公司参数
    Given 在迈达斯的QnA页面
        and Q标准词表无 “随便原因”
    And 点击输入框
    And 一个问题：“招商银行的股本的随便原因”
    Then 得到回答 "暂时无法回答此类问题。"
    """
    test "询问 OPQ财务信息 - 招商银行的股本的随便原因", tags do
      response = use_cassette "questions/xbrls/query_nlp_opq_non_match" do
        execute_query(tags, "招商银行的股本的随便原因")
      end

      # response
      # |> IO.inspect(label: "招商银行的股本的随便原因 => ", pretty: true)

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil
      #赖建发修改了Q的逻辑，Q无法识别情况下，直接返回OP
      assert answer["content"] =~ "请问您问的是哪一年？"
    end

    @doc """
    MIA-US011-AC004 查询 "招商银行的营业收入"
    Given 在QnA页面
    And xbrl_ss里有"600036" "2017" "营业收入" 为 "1,605,304,558.92"
    And 点击输入框
    And 一个问题：“招商银行的营业收入是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017年的营业收入是 1,605,304,558.92 (元)。"
    """
    test "询问 OPQ财务信息 - 招商银行的营业收入是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_opq_cmb_income" do
        execute_query(tags, "招商银行的营业收入是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017年的营业收入是 2208.97亿元"
    end

    @doc """
    MIA-US011-AC005 查询 "招商银行的2017营业收入是多少"
    Given 在QnA页面
    And xbrl_ss里有"600036" "2017" "营业收入" 为 "1,605,304,558.92"
    And 点击输入框
    And 一个问题：“招商银行的营业收入是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017年的营业收入是 1,605,304,558.92 (元)。"
    """
    test "询问 OTPQ财务信息 - 招商银行的2017营业收入是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_cmb_income" do
        execute_query(tags, "招商银行的2017年营业收入是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017年的营业收入是 2208.97亿元"
    end

    @doc """
    MIA-US011-AC006 查询 "招行的营业收入"
    Given 在QnA页面
    And xbrl_ss里有"600036" "2017" "营业收入" 为 "1,605,304,558.92"
    And 点击输入框
    And 一个问题：“招商银行的营业收入是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017年的营业收入是 1,605,304,558.92 (元)。"
    """
    test "询问 OTPQ财务信息 - 招行的营业收入", tags do
      response = use_cassette "questions/xbrls/query_nlp_op_implied_q_cmb_income" do
        execute_query(tags, "招行的营业收入")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017年的营业收入是 2208.97亿元"
    end

    test "询问 OP基本信息 - 招商银行的公司注册地址", tags do
      response = use_cassette "questions/xbrls/query_nlp_op_implied_q_cmb_address" do
        execute_query(tags, "招商银行的公司注册地址")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017年的公司注册地址是 中国广东省深圳市福田区深南大道7088号"
    end

  end

end
