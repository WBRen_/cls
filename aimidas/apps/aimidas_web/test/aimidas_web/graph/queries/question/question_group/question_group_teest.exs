defmodule AimidasWeb.Graph.Queries.Questions.QuestionGroupTest do
  @moduledoc """
    QuestionGroupTest
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock

  alias Aimidas.Ontology
  alias Aimidas.Accounts
  alias Aimidas.Qa

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp with_query_list(_tags) do
    query =
      """
      query unit_test_question_list($question_list_input: QuestionGroupListInput!) {
        questionGroupList(input: $question_list_input){
          name
          updatedAt
          items{
            id
            query
            answer
            otpq
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  defp execute_query_list(tags, timestamp, size) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_list_input" => %{
        "timestamp" => timestamp,
        "size" => size
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  defp get_user_id(tags) do
    current_user = Map.get(tags, :user)
    # IO.inspect(current_user, pretty: true)
    current_user.id
  end

  def fixture(:company_xbrl_cmb) do

    details =
      __DIR__
      |> Path.dirname()
      |> Path.join(["../", "../", "../", "../", "fixtures/", "xbrl_600036.json"])
      |> Path.expand()
      # |> IO.inspect(label: "json path:  ")
      |> File.read!()
      # |> IO.inspect(label: "xbrl_600036.json => ")
      |> Poison.decode!()
      |> Map.get("details")
      # |> IO.inspect(label: "details:  ")

    %{
      company_code: "600036",
      abbrev_name: "招商银行",
      full_name: "招商银行有限公司",
      exchange_code: "SH",
      src_updated_at: Date.utc_today(),
      details: details
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()

  end

  def fixture(:company_xbrl_pufa) do
    %{
      company_code: "600000",
      abbrev_name: "浦发银行",
      full_name: "上海浦东发展银行股份有限公司",
      exchange_code: "SH",
      src_updated_at: Date.utc_today(),
      details: %{
        "年报" => %{
          "latest_year" => "2017",
          "基本信息" => %{
            "2017" => %{
              "公司法定中文名称" => "上海浦东发展银行股份有限公司",
              "公司法定代表人" => "高国富"
            },
            "2016" => %{}
          },
          "股本结构" => %{
            "2017" => %{

            },
            "2016" => %{

            }
          },
          "资产负债" => %{
            "2017" => %{
              "股本" => 29_352_000_000
            }
          },
          "利润" => %{
            "2017" => %{
              "营业收入" => 1_605_304_558.92
            }
          }
        },
        "一季报" => %{

        },
        "半年报" => %{

        },
        "三季报" => %{

        }
      }
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()
  end

  describe "连续询问，查询话题列表" do

    defp with_data(_tags) do
      fixture(:company_xbrl_cmb)
      fixture(:company_xbrl_pufa)

      :ok
    end

    setup [:with_query, :with_query_list, :with_data, :with_user]

    @doc """
    连续问问题，然后查找话题列表
    """
    test "连续询问，查询话题列表", tags do
      # response = use_cassette "questions/xbrls/query_chain" do
        execute_query(tags, "招商银行的股本是多少")
        execute_query(tags, "招商银行的净利润是多少")
        execute_query(tags, "招商银行的营业总额是多少")
        execute_query(tags, "招商银行的营业收入多少")
        execute_query(tags, "浦发银行的股本是多少")
        execute_query(tags, "浦发银行的净利润是多少")
        execute_query(tags, "浦发银行的营业总额是多少")
        execute_query(tags, "浦发银行的营业收入多少")
        execute_query(tags, "招商银行的股本是多少")
        execute_query(tags, "净利润是多少")
        execute_query(tags, "营业总额")
        execute_query(tags, "营业总收入")
        # 查询话题列表

        current_timestamp = DateTime.utc_now
        |> DateTime.to_unix
        # from database
        # {:ok, time} = DateTime.from_unix(current_timestamp)
        # {:ok, list} =
        #   tags
        #   |> get_user_id()
        #   |> Qa.find_question_groups(time, 10)
        # IO.inspect(list, label: "List of QuestionGroup", pretty: true)

        # from graphql
        # IO.inspect get_user_id(tags)
        response = execute_query_list(tags, current_timestamp, 10)
        # IO.inspect(response, label: "List of QuestionGroup", pretty: true)
        # assert 1 == 0
      # end
    end
  end

end
