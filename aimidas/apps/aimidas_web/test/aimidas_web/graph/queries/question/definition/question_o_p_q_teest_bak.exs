defmodule AimidasWeb.Graph.Queries.QuestionOPQTest do
  @moduledoc """
  问答中的OPQ查询测试用例
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock

  alias Aimidas.Ontology
  alias Aimidas.Accounts

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: tags.user}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  describe "简单的OPQ(查询财务信息)" do

    setup [:with_query, :with_user]

    @doc """
    MIA-US0011-AC001 公司参数
    Given 在迈达斯的QnA页面
        and 数据中有"600036" "2017" "股本" 为 "25220000000"
    And 点击输入框
    And 一个问题：“招商银行的股本是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017年的股本是25220000000。"
    """
    test "询问 OPQ财务信息 - 招商银行的股本是多少", tags do
      response = use_cassette "query_nlp_o_p_q=sds" do
        execute_query(tags, "招商银行的股本是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/股本是25220000000/
    end

    @doc """
    MIA-US0011-AC002 公司参数
    Given 在迈达斯的QnA页面
        and Mysql数据库aimidas_xbrl_ss里有"600036" "2017" "股本" 为 "25220000000"
    And 点击输入框
    And 一个问题：“招商银行的股本是什么”
    #默认 T=[（最新年份）],由于有O+P组合，Q=["quantity"]
    Then 得到回答 "招商银行2017年的股本是25220000000。"
    """
    test "询问 OPQ财务信息 - 招商银行的股本是什么", tags do
      response = use_cassette "query_nlp_o_p_q=ssm" do
        execute_query(tags, "招商银行的股本是什么")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/股本是25220000000/
    end

    @doc """
    MIA-US0011-AC003 公司参数
    Given 在迈达斯的QnA页面
        and Q标准词表无 “随便原因”
    And 点击输入框
    And 一个问题：“招商银行的股本的随便原因”
    Then 得到回答 "暂时无法回答此类问题。"
    """
    test "询问 OPQ财务信息 - 招商银行的股本的随便原因", tags do
      response = use_cassette "query_nlp_o_p_q=sbyy" do
        execute_query(tags, "招商银行的股本的随便原因")
      end

      errors = response[:errors]
      assert errors != nil

      question = response[:data]["question"]
      assert question == nil
    end

  end
end
