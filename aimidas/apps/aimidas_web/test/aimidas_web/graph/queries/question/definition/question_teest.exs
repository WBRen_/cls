defmodule AimidasWeb.Graph.Queries.QuestionTeset do
  @moduledoc """
  提问
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock
  alias Aimidas.Ontology
  alias Aimidas.Accounts

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  describe "简单的P or P+Q, Q是什么" do

    @total_income "营业总收入"
    @total_cost "营业总成本"

    @total_income_def """
    营业收入是指企业在
    从事销售商品，提供劳务和让渡资产使用权等日常经营业务过程中所形成的
    经济利益的总流入。分为主营业务收入和其他业务收入。
    计算 营业收入=主营业务收入+其他业务收入 或
    营业收入=产品销售量（或服务量）*产品单价（或服务单价）
    主副产品（或不同等级产品）的销售收入应全部计入营业收入；所提供的
    不同类型服务收入也应计入营业收入。
    """

    @total_cost_def """
    营业总成本=营业成本+营业税金及附加+销售费用+管理费用+财务费用+资产减值损失
    """

    setup [:with_query, :with_terms, :with_user]

    defp with_terms(_tags) do
      Ontology.create_term(%{
        name: @total_income,
        description: @total_income_def
      })
      Ontology.create_term(%{
        name: @total_cost,
        description: @total_cost_def
      })

      :ok
    end

    @doc """
    MIA-US0008-AC001 参数定义
    Given 在迈达斯的QnA页面
    And MongoDB数据库Definition里 “营业总收入”的定义为：
        #{@total_cost_def}
    And 点击输入框
    And 输入一个问题：“营业总收入”
    And 点击输入框右侧的按钮
    Then 得到回答 #{@total_cost_def}
    """
    test "询问 '营业总收入'", tags do
      response = use_cassette "query_p_total_income" do
        execute_query(tags, @total_income)
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/营业收入是指企业/
    end

    @doc """
    MIA-US008-AC002 参数定义
    Given 在迈达斯的QnA页面
    And MongoDB数据库Definition里
      “营业总成本”的定义为：
      "营业总成本=营业成本+营业税金及附加+销售费用+管理费用+财务费用+资产减值损失"
    And 点击输入框
    And 输入一个问题：“营业总成本”
    And 点击输入框右侧的按钮
    Then 得到回答
      "营业总成本=营业成本+营业税金及附加+销售费用+管理费用+财务费用+资产减值损失"
    """
    test "询问 '营业总成本'", tags do
      response = use_cassette "query_p_total_cost" do
        execute_query(tags, @total_cost)
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/营业总成本=营业成本+/
    end

    @doc """
    MIA-US0008-AC003 参数定义
    Given 在迈达斯的QnA页面
        and MongoDB数据库Definition里 没有"额度"的定义
    And 点击输入框
    And 输入一个问题：“额度”
    And 点击输入框右侧的按钮
    Then 得到回答 "暂无此参数定义。
    """
    test "询问 不存在的 '额度'", tags do
      response = use_cassette "query_p_no_anser" do
        execute_query(tags, "额度")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "暂时无法回答该问题"
    end

    @doc """
    MIA-US008-AC004 参数定义
    Given 在QnA页面
    And terms表里 “营业总收入”的定义为 #{@total_income_def}
    And 点击输入框
    And 输入一个问题：“营业总收入是什么”
    And 点击输入框右侧的按钮
    Then 得到回答 #{@total_income_def}
    """
    test "询问 '营业总收入是什么'", tags do
      response = use_cassette "query_pq_total_income" do
        execute_query(tags, @total_income <> "是什么")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/营业收入是指企业/
    end

    @doc """
    MIA-US008-AC005 参数定义
    Given 在QnA页面
    And 表terms里 “营业总成本”的定义为：#{@total_cost_def}
    And 点击输入框
    And 输入一个问题：“营业总成本是什么”
    And 点击输入框右侧的按钮
    Then 得到回答 #{@total_cost_def}
    """
    test "询问 '营业总成本是什么'", tags do
      response = use_cassette "query_pq_total_cost" do
        execute_query(tags, @total_cost <> "是什么")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/营业总成本=营业成本+/
    end

    @doc """
    MIA-US0008-AC006 参数定义
    Given 在迈达斯的QnA页面
        and MongoDB数据库Definition里 没有"额度"的定义
    And 点击输入框
    And 输入一个问题：“额度是什么”
    And 点击输入框右侧的按钮
    Then 得到回答 "暂无此参数定义。
    """
    test "询问 不存在的 '额度是什么'", tags do
      response = use_cassette "query_pq_no_anser" do
        execute_query(tags, "额度是什么")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "暂时无法回答该问题"
    end
  end
end
