defmodule AimidasWeb.Graph.Queries.Questions.XbrlOTPQTest do
  @moduledoc """
  问答中的XBRL OPQ查询测试用例
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock

  alias Aimidas.Ontology
  alias Aimidas.Accounts

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          data
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  def fixture(:company_xbrl_cmb) do
    details =
      __DIR__
      |> Path.dirname()
      |> Path.join(["../", "../", "../", "../", "fixtures/", "xbrl_600036.json"])
      |> Path.expand()
      # |> IO.inspect(label: "json path:  ")
      |> File.read!()
      # |> IO.inspect(label: "xbrl_600036.json => ")
      |> Poison.decode!()
      |> Map.get("details")
      # |> IO.inspect(label: "details:  ")

    %{
      company_code: "600036",
      abbrev_name: "招商银行",
      full_name: "招商银行有限公司",
      exchange_code: "SH",
      src_updated_at: Date.utc_today(),
      details: details
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()
  end

  def fixture(:company_xbrl_pufa) do
    %{
      company_code: "600000",
      abbrev_name: "浦发银行",
      full_name: "上海浦东发展银行股份有限公司",
      exchange_code: "SH",
      src_updated_at: Date.utc_today(),
      details: %{
        "年报" => %{
          "latest_year" => "2017",
          "基本信息" => %{
            "2017" => %{
              "公司法定中文名称" => "上海浦东发展银行股份有限公司",
              "公司法定代表人" => "高国富"
            },
            "2016" => %{}
          },
          "股本结构" => %{
            "2017" => %{

            },
            "2016" => %{

            }
          },
          "资产负债" => %{
            "2017" => %{
              "股本" => 29352000000
            }
          },
          "利润" => %{
            "2017" => %{
              "营业收入" => 1605304559
            }
          },
          "分红" => %{
            "2017" => %{
            "分红年度" => "2017年度",
            "分红方案" => "10派1元(含税)",
            "新增股份上市日" => "",
            "股权登记日" => "20180712",
            "除权基准日" => "20180713"
           }
          }
        },
        "一季报" => %{
          "latest_year" => "2017",
          "基本信息" => %{
            "2017" => %{
              "公司法定中文名称" => "上海浦东发展银行股份有限公司",
              "公司法定代表人" => "高国富"
            },
            "2016" => %{}
          },
          "股本结构" => %{
            "2017" => %{

            },
            "2016" => %{

            }
          },
          "资产负债" => %{
            "2017" => %{
              "股本" => 29352000000
            },
            "2016" => %{
              "股本" => 19352000000
            }
          },
          "利润" => %{
            "2017" => %{
              "营业收入" => 1605304559
            }
          }
        },
        "半年报" => %{

        },
        "三季报" => %{

        }
      }
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()
  end

  describe "简单的XBRL OTPQ(查询财务信息)" do

    defp with_data(_tags) do
      fixture(:company_xbrl_cmb)
      fixture(:company_xbrl_pufa)

      :ok
    end

    setup [:with_query, :with_data, :with_user]

    # @doc """
    # MIA-US0011-AC001 公司参数
    # Given 在QnA页面
    # And 数据中有"600036" "2017" "股本" 为 "25220000000"
    # When 点击输入框
    # And 一个问题：“招商银行的股本是多少”
    # #默认 T=[（最新年份）]
    # Then 得到回答 "招商银行2017年的股本是25220000000。"
    # """
    # test "询问 OPQ财务信息 - 招商银行的股本是多少", tags do
    #   response = use_cassette "questions/xbrls/query_nlp_cmb_opq_gb" do
    #     execute_query(tags, "招商银行的股本是多少")
    #   end

    #   errors = response[:errors]
    #   assert errors == nil

    #   data = response[:data]
    #   IO.inspect data, label: "response: ", pretty: true
    #   answer = data["question"]["answer"]
    #   assert answer != nil

    #   assert answer["content"] =~ "招商银行2017年的股本是 25,220,000,000 (元)"
    # end

    # @doc """
    # MIA-US0011-AC002 公司参数
    # Given 在QnA页面
    # And 数据中有"600000" "2017" "股本" 为 "29,352,000,000"
    # When 点击输入框
    # And 一个问题：“浦发银行的股本是多少”
    # #默认 T=[（最新年份）]
    # Then 得到回答 "浦发银行2017年的股本是201亿。"
    # """
    # test "询问 OPQ财务信息 - 浦发银行的股本是多少", tags do
    #   response = use_cassette "questions/xbrls/query_nlp_pf_opq_gb" do
    #     execute_query(tags, "浦发银行的股本是多少")
    #   end

    #   errors = response[:errors]
    #   assert errors == nil

    #   data = response[:data]
    #   IO.inspect data, label: "response: ", pretty: true
    #   answer = data["question"]["answer"]
    #   assert answer != nil

    #   assert answer["content"] =~ "浦发银行2017年的股本是 29,352,000,000 (元)"
    # end

    # @doc """
    # MIA-US0011-AC003 公司参数
    # Given 在迈达斯的QnA页面
    #     and Q标准词表无 “随便原因”
    # And 点击输入框
    # And 一个问题：“招商银行的股本的随便原因”
    # Then 得到回答 "暂时无法回答此类问题。"
    # """
    # test "询问 OPQ财务信息 - 招商银行的股本的随便原因", tags do
    #   response = use_cassette "questions/xbrls/query_nlp_opq_non_match" do
    #     execute_query(tags, "招商银行的股本的随便原因")
    #   end

    #   response
    #   |> IO.inspect(label: "招商银行的股本的随便原因 => ", pretty: true)

    #   errors = response[:errors]
    #   assert errors != nil
    #   assert "暂时无法回答此类问题。" ==
    #             errors
    #             |> Enum.at(0)
    #             |> get_in([:message])

    #   question = response[:data]["question"]
    #   assert question == nil
    # end

    # @doc """
    # MIA-US011-AC004 查询 "招商银行的营业收入"
    # Given 在QnA页面
    # And xbrl_ss里有"600036" "2017" "营业收入" 为 "1,605,304,558.92"
    # And 点击输入框
    # And 一个问题：“招商银行的营业收入是多少”
    # #默认 T=[（最新年份）]
    # Then 得到回答 "招商银行2017年的营业收入是 1,605,304,558.92 (元)。"
    # """
    # test "询问 OPQ财务信息 - 招商银行的营业收入是多少", tags do
    #   response = use_cassette "questions/xbrls/query_nlp_opq_cmb_income" do
    #     execute_query(tags, "招商银行的营业收入是多少")
    #   end

    #   errors = response[:errors]
    #   assert errors == nil

    #   data = response[:data]
    #   IO.inspect data, label: "response: ", pretty: true
    #   answer = data["question"]["answer"]
    #   assert answer != nil

    #   assert answer["content"] =~ "招商银行2017年的营业收入是 1,605,304,558.92 (元)"
    # end

    @doc """
    MIA-US012-AC005 查询 "招商银行的2017营业收入是多少"
    Given 在QnA页面
    And xbrl_ss里有"600036" "2017" "营业收入" 为 "1,605,304,558.92"
    And 点击输入框
    And 一个问题：“招商银行的营业收入是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017年的营业收入是 1,605,304,558.92 (元)。"
    """
    test "询问 OTPQ财务信息 - 招商银行的2017年营业收入是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_cmb_income" do
        execute_query(tags, "招商银行的2017年营业收入是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017年的营业收入是 2208.97亿元"
    end

    @doc """
    MIA-US012-AC005 查询 "招商银行的2017营业收入是多少"
    Given 在QnA页面
    And xbrl_ss里有"600036" "2017" "营业收入" 为 "1,605,304,558.92"
    And 点击输入框
    And 一个问题：“招商银行的营业收入是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017年的营业收入是 1,605,304,558.92 (元)。"
    """
    test "询问 OTPQ财务信息 - 招商银行的2016年营业收入是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_cmb_income_2016" do
        execute_query(tags, "招商银行的2016年营业收入是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2016年的营业收入是 2090.25亿元"
    end

    @doc """
    MIA-US014-AC001 查询 " 招商银行的2017营业收入同比是多少"
    Given 在QnA页面
    And xbrl_ss里有"600036" "2017" "营业收入"
    And xbrl_ss里有"600036" "2016" "营业收入"
    And 点击输入框
    And 一个问题：“招商银行的营业收入是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017,2016年的营业收入分别是 220,897,000,000 (元),209,025,000,000 (元) 同比增长5.68%"
    """
    test "询问 OTPQ财务信息 - 招商银行的2017营业收入同比是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_cmb_income_yty_2017" do
        execute_query(tags, " 招商银行的2017营业收入同比是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017,2016年年报的营业收入分别是 2208.97亿元,2090.25亿元, 同比增长5.68%"
    end

    @doc """
    MIA-US014-AC001 查询 " 招商银行的2017营业收入同比"
    Given 在QnA页面
    And xbrl_ss里有"600036" "2017" "营业收入"
    And xbrl_ss里有"600036" "2016" "营业收入"
    And 点击输入框
    And 一个问题：“招商银行的营业收入是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "招商银行2017,2016年的营业收入分别是 220,897,000,000 (元),209,025,000,000 (元) 同比增长5.68%"
    """
    test "询问 OTPQ财务信息 - 招商银行的营业收入同比", tags do
      response = use_cassette "questions/xbrls/query_nlp_opq_cmb_income_yty_2017" do
        execute_query(tags, " 招商银行的营业收入同比")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行2017,2016年年报的营业收入分别是 2208.97亿元,2090.25亿元, 同比增长5.68%"
    end

    @doc """
    MIA-US014-AC002 查询 "浦发银行的2017营业收入同比"
    Given 在QnA页面
    And xbrl_ss里有"600036" "2017" "营业收入"
    And 点击输入框
    And 一个问题：“浦发银行的2017营业收入同比”
    #默认 T=[（最新年份）]
    Then 得到回答 "浦发银行2017年的营业收入是1,605,304,558.92 (元)，浦发银行2016年的营业收入不存在，同比无法计算。"
    """
    test "询问 OTPQ财务信息 - 浦发银行的2017营业收入同比", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_pufa_income_yty_2017" do
        execute_query(tags, "浦发银行的2017营业收入同比")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "浦发银行2017年年报的营业收入是16.053亿元，浦发银行2016年年报的营业收入不存在，同比无法计算。"
    end

     @doc """
    MIA-US014-AC003 查询 "浦发银行的2017营业收入同比"
    Given 在QnA页面
    And 点击输入框
    And 一个问题：“浦发银行的2017营业收入同比”
    #默认 T=[（最新年份）]
    Then 得到回答 "浦发银行2016年的营业收入不存在，同比无法计算。"
    """
    test "询问 OTPQ财务信息 - 浦发银行的2016营业收入同比", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_pufa_income_yty_2016" do
        execute_query(tags, "浦发银行的2016营业收入同比")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "浦发银行2016年年报的营业收入不存在，同比无法计算。"
    end

         @doc """
    MIA-US014-AC003 查询 "浦发银行的2017营业收入同比"
    Given 在QnA页面
    And 点击输入框
    And 一个问题：“浦发银行的2017营业收入同比”
    #默认 T=[（最新年份）]
    Then 得到回答 "浦发银行2016年的营业收入不存在，同比无法计算。"
    """
    test "询问 OTPQ财务信息 - 浦发银行的2016年一季度营业收入", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_pufa_income_season1_2016" do
        execute_query(tags, "浦发银行的2016一季度营业收入")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "浦发银行2016年暂无此数据"
    end

    test "询问 OTPQ财务信息 - 浦发银行的2017年第一季度营业收入", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_pufa_income_season1_2017" do
        execute_query(tags, "浦发银行的2017年第一季度营业收入")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "浦发银行2017年第一季度的营业收入是 16.053亿元"
    end

    test "询问 OTPQ财务信息 - 浦发银行的2017第一季度营业收入同比", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_pufa_income_yty_season1_2017" do
        execute_query(tags, "浦发银行的2017第一季度营业收入同比")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "浦发银行2017年一季报的营业收入是16.053亿元，浦发银行2016年一季报的营业收入不存在，同比无法计算。"
    end

    test "询问 OTPQ财务信息 - 浦发银行的2017第一季度股本同比", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_pufa_gb_yty_season1_2017" do
        execute_query(tags, "浦发银行的2017第一季度股本同比")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      picData =
        data["question"]["data"]
        |> Poison.decode!()
        |> to_atom_map

      assert picData ==
        %{
          keys: ["x", "y"],
          title: "浦发银行股本同比表",
          type: "yty",
          units: ["Year", "亿元"],
          values:
          %{
            x: [2016, 2017],
            y: [193.52, 293.52]
            }
          }
    end

    test "询问 OTPQ财务信息 - 浦发银行的2016第一季度营业收入同比", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_pufa_income_yty_season1_2016" do
        execute_query(tags, "浦发银行的2016第一季度营业收入同比")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "浦发银行2016年一季报的营业收入不存在，同比无法计算。"
    end

    test "询问 OTPQ财务信息 - 招商银行2016十大股东", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_zs_gd__2016" do
        execute_query(tags, "招商银行2016十大股东")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行在2016年的年报中十大股东信息如下：\n1-股东名称:香港中央结算（代理人）有限公司，持股数量:4539126386.0股，持股比例:18.0%。\n2-股东名称:招商局轮船股份有限公司，持股数量:3289470337.0股，持股比例:13.04%。\n3-股东名称:安邦财产保险股份有限公司－传统产品，持股数量:2704596216.0股，持股比例:10.72%。\n4-股东名称:中国远洋运输（集团）总公司，持股数量:1574729111.0股，持股比例:6.24%。\n5-股东名称:深圳市晏清投资发展有限公司，持股数量:1258542349.0股，持股比例:4.99%。\n6-股东名称:深圳市招融投资控股有限公司，持股数量:1147377415.0股，持股比例:4.55%。\n7-股东名称:深圳市楚源投资发展有限公司，持股数量:944013171.0股，持股比例:3.74%。\n8-股东名称:中国证券金融股份有限公司，持股数量:819311178.0股，持股比例:3.25%。\n9-股东名称:中远海运（广州）有限公司，持股数量:696450214.0股，持股比例:2.76%。\n10-股东名称:中国交通建设股份有限公司，持股数量:450164945.0股，持股比例:1.78%。"
    end

    test "询问 OTPQ财务信息 - 招商银行2016十大流通股东", tags do
      response = use_cassette "questions/xbrls/query_nlp_otpq_zs_ltgd_2016" do
        execute_query(tags, "招商银行2016十大流通股东")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "招商银行在2016年的年报中十大股东信息如下：\n1-股东名称:香港中央结算（代理人）有限公司，持股数量:4539126386.0股，持股比例:18.0%。\n2-股东名称:招商局轮船股份有限公司，持股数量:3289470337.0股，持股比例:13.04%。\n3-股东名称:安邦财产保险股份有限公司－传统产品，持股数量:2704596216.0股，持股比例:10.72%。\n4-股东名称:中国远洋运输（集团）总公司，持股数量:1574729111.0股，持股比例:6.24%。\n5-股东名称:深圳市晏清投资发展有限公司，持股数量:1258542349.0股，持股比例:4.99%。\n6-股东名称:深圳市招融投资控股有限公司，持股数量:1147377415.0股，持股比例:4.55%。\n7-股东名称:深圳市楚源投资发展有限公司，持股数量:944013171.0股，持股比例:3.74%。\n8-股东名称:中国证券金融股份有限公司，持股数量:819311178.0股，持股比例:3.25%。\n9-股东名称:中远海运（广州）有限公司，持股数量:696450214.0股，持股比例:2.76%。\n10-股东名称:中国交通建设股份有限公司，持股数量:450164945.0股，持股比例:1.78%。"
    end

  test "询问 OTPQ财务信息 - 招商银行2017分红", tags do
    response = use_cassette "questions/xbrls/query_nlp_otpq_zs_fh_2017" do
      execute_query(tags, "招商银行2017分红")
    end

    errors = response[:errors]
    assert errors == nil

    data = response[:data]
    # IO.inspect data, label: "response: ", pretty: true
    answer = data["question"]["answer"]
    assert answer != nil

    assert answer["content"] =~ "2017年度，招商银行以10派1元(含税)进行分红，新增股份于上市。股权登记日为20180712，除权基准日为20180713。"
  end

  test "询问 OTPQ财务信息 - 招商银行今天的股价", tags do
    response = use_cassette "questions/xbrls/query_nlp_otp_zs_stock" do
      execute_query(tags, "招商银行今天的股价")
    end

    errors = response[:errors]
    assert errors == nil

    data = response[:data]
    # IO.inspect data, label: "response: ", pretty: true
    answer = data["question"]["answer"]
    assert answer != nil

    assert answer["content"] =~ "招商银行在昨日收盘于"
  end

  test "询问 OTPQ财务信息 - 招商银行今天的股价是多少", tags do
    response = use_cassette "questions/xbrls/query_nlp_otpq_zs_stock" do
      execute_query(tags, "招商银行今天的股价是多少")
    end

    errors = response[:errors]
    assert errors == nil

    data = response[:data]
    # IO.inspect data, label: "response: ", pretty: true
    answer = data["question"]["answer"]
    assert answer != nil

    assert answer["content"] =~ "招商银行在昨日收盘于"
  end

  test "询问 OTPQ财务信息 - 招商银行昨天的股价是多少", tags do
    response = use_cassette "questions/xbrls/query_nlp_otpq_zs_stock_yesterday" do
      execute_query(tags, "招商银行昨天的股价是多少")
    end

    errors = response[:errors]
    assert errors == nil

    data = response[:data]
    # IO.inspect data, label: "response: ", pretty: true
    answer = data["question"]["answer"]
    assert answer != nil

    assert answer["content"] =~ "招商银行在昨日收盘于"
  end

  test "询问 OTPQ财务信息 - 招商银行昨天的股价", tags do
    response = use_cassette "questions/xbrls/query_nlp_otp_zs_stock_yesterday" do
      execute_query(tags, "招商银行昨天的股价")
    end

    errors = response[:errors]
    assert errors == nil

    data = response[:data]
    # IO.inspect data, label: "response: ", pretty: true
    answer = data["question"]["answer"]
    assert answer != nil

    assert answer["content"] =~ "招商银行在昨日收盘于"
  end
end

  def to_atom_map(%{} = json_map) do
    json_map
    |> Map.new(fn {k, v} -> {String.to_atom(k), to_atom_map(v)} end)
  end
  def to_atom_map([]), do: []
  def to_atom_map([head|tail]) do
    [to_atom_map(head)] ++ to_atom_map(tail)
  end
  def to_atom_map(data), do: data
end
