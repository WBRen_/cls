defmodule AimidasWeb.Graph.Queries.QuestionOPTest do
  @moduledoc """
  问答中的O查询测试用例
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock

  alias Aimidas.Ontology
  alias Aimidas.Accounts

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  def fixture(:company_cmb) do
    %{
      abbrev_name: "招商银行",
      company_code: "600036",
      details: %{
        "基本信息": %{
          "中文简称" => "招商银行",
          "主营业务" => "吸收社会公众存款、发放短期和中长期贷款、结算、办理票据贴现、发行金融债券、代理发行、代理兑付及承销政府债券、买卖政府债券、同业拆借等。",
          "信息披露报纸名称" => "上海证券报,证券时报,中国证券报",
          "信息披露网址" => "www.sse.com.cn",
          "公司全称" => "招商银行股份有限公司",
          "公司电子邮箱" => "cmb@cmbchina.com",
          "公司电话" => "0755-83198888",
          "公司网址" => "www.cmbchina.com",
          "办公地址" => "广东省深圳市福田区深南大道7088号,香港夏悫道12号美国银行中心21楼",
          "员工人数" => "72,530",
          "地域" => "广东",
          "总经理" => "田惠宇",
          "法人代表" => "李建红",
          "注册资本" => "2,521,985万元",
          "简介" => "招商银行1987年成立于中国改革开放的最前沿——深圳蛇口,是中国境内第一家完全由企业法人持股的股份制商业银行,也是国家从体制外推动银行业改革的第一家试点银行,是一家拥有商业银行、金融租赁、基金管理、人寿保险、境外投行等金融牌照的银行集团。成立30年来,招商银行始终坚持“因您而变”的经营服务理念,品牌知名度日益提升。",
          "组织形式" => "国有企业",
          "英文名称" => "China Merchants Bank Co.,Ltd.",
          "董事会秘书" => "王良",
          "董事长" => "李建红",
          "董秘传真" => "0755-83195109",
          "董秘电话" => "0755-83198888",
          "董秘邮箱" => "cmb@cmbchina.com"
        }
      },
      full_name: "招商银行股份有限公司",
      season: 4,
      year: 2017
    }
    |> Ontology.create_company_info
  end

  def fixture(:company_pufa) do
    %{
      company_code: "600000",
      abbrev_name: "浦发银行",
      full_name: "上海浦东发展银行股份有限公司",
      year: 2017,
      season: 4,
      details: %{
        "公司基本信息" => %{
          "简介" => "上海浦东发展银行股份有限公司(以下简称:浦发银行)是1992年8月28日经中国人民银行批准设立、1993年1月9日开业、1999年在上海证券交易所挂牌上市(股票交易代码:600000)的全国性股份制商业银行,总行设在上海。目前,注册资本金196.53亿元。良好的业绩、诚信的声誉,使浦发银行成为中国证券市场中备受关注和尊敬的上市公司。",
          "组织形式" => "国有企业"
        }
      }
    }
    |> Ontology.create_company_info
  end

  describe "简单的OP(查询基本信息)" do
    defp with_companies(_tags) do
      fixture(:company_cmb)
      fixture(:company_pufa)

      :ok
    end

    setup [:with_query, :with_companies, :with_user]

    @doc """
    MIA-US0010-AC001 公司参数
    Given 在迈达斯的QnA页面
    And company_infos里， 招商银行的基本信息中，主营业务为
       "吸收社会公众存款、发放短期和中长期贷款、结算、办理票据贴现、发行金融债券、代理发行、代理兑付及承销政府债券、买卖政府债券、同业拆借等。"
    And 点击输入框
    And 一个问题：“招商银行的主营业务”
    Then 得到回答 "吸收社会公众存款、发放短期和中长期贷款、结算、办理票据贴现、发行金融债券、代理发行、代理兑付及承销政府债券、买卖政府债券、同业拆借等"
    """
    test "询问 简单的O - 招商银行的主营业务", tags do
      response = use_cassette "query_nlp_o_p_cmb_biz" do
        execute_query(tags, "招商银行的主营业务")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ ~R/吸收社会公众存款、发放短期/
    end

    @doc """
    MIA-US0010-AC002 公司参数
    Given 在迈达斯的QnA页面
    And company_infos里， 招商银行的基本信息中，无 "股权架构" 信息
    And 点击输入框
    And 一个问题：“招商银行的股权架构”
    Then 得到回答 "抱歉, 该问题暂无答案"
    """
    test "询问 简单的O - 招商银行的股权架构", tags do
      response = use_cassette "query_nlp_o_p_cmb_ss" do
        execute_query(tags, "招商银行的股权架构")
      end
      #NLP逻辑存在问题
      # context: "{\"type\":\"OP\",\"context\":0,\"code\":0,\"T\":[],\"Q\":[{\"value\":\"definition\",\"nlp_reason\":\"implied\"}],\"P\":[{\"value\":\"简介\",\"text\":\"简介\",\"nlp_reason\":\"implied\"}],\"O\":[{\"value\":\"招商银行\",\"text\":\"招商银行\",\"nlp_reason\":\"exact\",\"exchange_code\":\"SH\",\"code\":\"600036\"}]}",
      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ "请问您问的是哪一年？"
    end

    test "反问", tags do
      response = use_cassette "ask" do
        execute_query(tags, "浦发银行公司大股东是否有增持股票计划")
      end
      #NLP逻辑存在问题
      # context: "{\"type\":\"OP\",\"context\":0,\"code\":0,\"T\":[],\"Q\":[{\"value\":\"definition\",\"nlp_reason\":\"implied\"}],\"P\":[{\"value\":\"简介\",\"text\":\"简介\",\"nlp_reason\":\"implied\"}],\"O\":[{\"value\":\"招商银行\",\"text\":\"招商银行\",\"nlp_reason\":\"exact\",\"exchange_code\":\"SH\",\"code\":\"600036\"}]}",
      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      question = data["question"]
      assert question["answer"]["content"] =~ "请问您问的是哪一年？"
    end

  end
end
