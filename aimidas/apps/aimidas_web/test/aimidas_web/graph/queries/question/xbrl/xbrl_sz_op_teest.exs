defmodule AimidasWeb.Graph.Queries.Questions.XbrlSZOPTest do
  @moduledoc """
  问答中的XBRL OPQ查询测试用例
  """

  use AimidasWeb.GraphCase
  use ExVCR.Mock

  alias Aimidas.Ontology
  alias Aimidas.Accounts

  @user_attrs %{
    nickname: "test_user",
    avatar: "head_icon",
    password_digest: "password",
    password_salt: "salt"
  }

  def fixture(:user), do: fixture(:user, @user_attrs)

  def fixture(:user, attrs) do
    attrs
    |> Accounts.create_user()
  end

  #"""
  #创建测试用户数据
  #"""
  defp with_user(_tags) do
    {:ok, user} = fixture(:user)
    {:ok, user: user}
  end

  defp with_query(_tags) do
    query =
      """
      query unit_test_question($question_input: QuestionInput!) {
        question(input: $question_input) {
          id
          answer {
            id,
            content
          }
        }
      }
      """

    {:ok, query: query}
  end

  defp execute_query(tags, query) do
    context = %{current_user: Map.get(tags, :user)}
    input = %{
      "question_input" => %{
        "query" => query
      }
    }

    tags
    |> Map.put(:variables, input)
    |> Map.put(:context, context)
    |> execute_document()
  end

  def fixture(:company_xbrl_cmb) do

    details =
      __DIR__
      |> Path.dirname()
      |> Path.join(["../", "../", "../", "../", "fixtures/", "xbrl_000001.json"])
      |> Path.expand()
      # |> IO.inspect(label: "json path:  ")
      |> File.read!()
      # |> IO.inspect(label: "xbrl_000001.json => ")
      |> Poison.decode!()
      |> Map.get("details")
      # |> IO.inspect(label: "details:  ")

    %{
      company_code: "000001",
      abbrev_name: "平安银行",
      full_name: "平安银行股份有限公司",
      exchange_code: "SZ",
      src_updated_at: Date.utc_today(),
      details: details
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()
  end

  def fixture(:company_xbrl_pufa) do
    %{
      company_code: "000001",
      abbrev_name: "平安银行",
      full_name: "平安银行股份有限公司",
      exchange_code: "SZ",
      src_updated_at: Date.utc_today(),
      details: %{
        "年报" => %{
        "latest_year" => "2017",
        "基本信息" => %{
          "2013" => %{
            "其他（B、H）流通股" => nil,
            "净利润" => -42.34,
            "净利润率" => -81.34,
            "净资产收益率" => -6.78,
            "总股本" => 91433.36,
            "总资产报酬率" => -3.38,
            "扣除非经常性损益每股收益" => -0.05,
            "每10股现金分红" => nil,
            "每股净资产" => 0.68,
            "每股未分配利润" => -0.61,
            "每股经营现金流量" => -0.12,
            "每股资本公积金" => 0.29,
            "流通A股" => 91374.3,
            "管理费用" => 33.88,
            "股东权益" => 624.24,
            "营运资金" => 260.83,
            "财务费用" => 7.65,
            "销售费用" => 3.2,
            "长期负债" => 0.33
          },
          "2014" => %{
            "其他（B、H）流通股" => nil,
            "净利润" => 43.16,
            "净利润率" => 81.79,
            "净资产收益率" => 6.45,
            "总股本" => 91433.36,
            "总资产报酬率" => 3.38,
            "扣除非经常性损益每股收益" => -0.06,
            "每10股现金分红" => nil,
            "每股净资产" => 0.73,
            "每股未分配利润" => -0.56,
            "每股经营现金流量" => -0.01,
            "每股资本公积金" => 0.3,
            "流通A股" => 91374.3,
            "管理费用" => 39.01,
            "股东权益" => 669.72,
            "营运资金" => 244.58,
            "财务费用" => 20.26,
            "销售费用" => 3.77,
            "长期负债" => 50.28
          },
          "2015" => %{
            "其他（B、H）流通股" => nil,
            "净利润" => -54.03,
            "净利润率" => -65.66,
            "净资产收益率" => -4.81,
            "总股本" => 91433.36,
            "总资产报酬率" => -3.1,
            "扣除非经常性损益每股收益" => -0.06,
            "每10股现金分红" => nil,
            "每股净资产" => 1.09,
            "每股未分配利润" => -0.54,
            "每股经营现金流量" => 0.02,
            "每股资本公积金" => 0.6,
            "流通A股" => 91374.3,
            "管理费用" => 56.74,
            "股东权益" => 1213.2,
            "营运资金" => 775.0,
            "财务费用" => 11.29,
            "销售费用" => 5.83,
            "长期负债" => 49.65
          },
          "2016" => %{
            "其他（B、H）流通股" => nil,
            "净利润" => 116.96,
            "净利润率" => 22.55,
            "净资产收益率" => 8.47,
            "总股本" => 105853.68,
            "总资产报酬率" => 4.6,
            "扣除非经常性损益每股收益" => -0.01,
            "每10股现金分红" => nil,
            "每股净资产" => 1.21,
            "每股未分配利润" => -0.44,
            "每股经营现金流量" => -0.18,
            "每股资本公积金" => 0.6,
            "流通A股" => 95122.25,
            "管理费用" => 92.57,
            "股东权益" => 1355.33,
            "营运资金" => 939.79,
            "财务费用" => 22.07,
            "销售费用" => 14.44,
            "长期负债" => 9.76
          },
          "2017" => %{
            "其他（B、H）流通股" => nil,
            "净利润" => 22.94,
            "净利润率" => 2.88,
            "净资产收益率" => 1.16,
            "总股本" => 105853.68,
            "总资产报酬率" => 0.56,
            "扣除非经常性损益每股收益" => -0.06,
            "每10股现金分红" => nil,
            "每股净资产" => 1.25,
            "每股未分配利润" => -0.42,
            "每股经营现金流量" => -0.23,
            "每股资本公积金" => 0.65,
            "流通A股" => 95122.25,
            "管理费用" => 105.34,
            "股东权益" => 1404.37,
            "营运资金" => 1183.09,
            "财务费用" => 45.03,
            "销售费用" => 16.38,
            "长期负债" => 133.08
          }
        },
        "分红" => %{
          "1993" => %{
            "分红年度" => "1993-12-31",
            "分红方案" => "10送3.333股",
            "新增股份上市日" => "1994-09-28",
            "股权登记日" => "1994-09-22",
            "除权基准日" => "1994-09-23"
          },
          "1994" => %{
            "分红年度" => "1994-12-31",
            "分红方案" => "10送1股",
            "新增股份上市日" => "1995-07-28",
            "股权登记日" => "1995-07-25",
            "除权基准日" => "1995-07-26"
          },
          "1995" => %{
            "分红年度" => "1995-12-31",
            "分红方案" => "10送1.5股",
            "新增股份上市日" => "1996-08-15",
            "股权登记日" => "1996-08-12",
            "除权基准日" => "1996-08-13"
          },
          "1996" => %{
            "分红年度" => "1996-12-31",
            "分红方案" => "10转增3股",
            "新增股份上市日" => "1997-08-27",
            "股权登记日" => "1997-08-22",
            "除权基准日" => "1997-08-25"
          },
          "1997" => %{
            "分红年度" => "1997-12-31",
            "分红方案" => "10送1转增4股",
            "新增股份上市日" => "1998-08-25",
            "股权登记日" => "1998-08-20",
            "除权基准日" => "1998-08-21"
          },
          "1999" => %{
            "分红年度" => "1999-12-31",
            "分红方案" => "10送1股派0.25元（含税）",
            "新增股份上市日" => "2000-09-01",
            "股权登记日" => "2000-08-30",
            "除权基准日" => "2000-08-31"
          },
          "2000" => %{
            "分红年度" => "2000-12-31",
            "分红方案" => "10送0.43719股转增0.43719股派0.11367元（含税）",
            "新增股份上市日" => "2001-07-17",
            "股权登记日" => "2001-07-13",
            "除权基准日" => "2001-07-16"
          },
          "2006" => %{
            "分红年度" => "2006-05-15",
            "分红方案" => "10转增5.5股",
            "新增股份上市日" => "2006-07-31",
            "股权登记日" => "2006-07-28",
            "除权基准日" => ""
          }
        },
        "融资情况" => %{
          "1990" => %{
            "公告日期" => "",
            "再融资类型" => "",
            "发行价格(元/股)" => "-",
            "发行对象" => "",
            "实际募集资金(万元)" => "-",
            "总发行数量(万股)" => "-"
          },
          "1994" => %{
            "公告日期" => "1994-09-17",
            "再融资类型" => "配股",
            "发行价格(元/股)" => "3.00",
            "发行对象" => "",
            "实际募集资金(万元)" => "27,000.00",
            "总发行数量(万股)" => "9,000.00"
          },
          "2000" => %{
            "公告日期" => "2000-12-06",
            "再融资类型" => "配股",
            "发行价格(元/股)" => "4.95",
            "发行对象" => "",
            "实际募集资金(万元)" => "40,520.94",
            "总发行数量(万股)" => "8,186.05"
          },
          "2015" => %{
            "公告日期" => "2015-12-29",
            "再融资类型" => "定向增发",
            "发行价格(元/股)" => "3.74",
            "发行对象" => "募集配套资金对象为深圳市博睿意碳源科技有限公司和上海勤幸投资管理中心（有限合伙）。",
            "实际募集资金(万元)" => "13,238.19",
            "总发行数量(万股)" => "3,539.62"
            }
          },
          "十大股东" => %{
            "update_time" => "2018-03-31",
            "十大流通股东" => %{
              "1" => %{
                "持股数量" => "8,258,245,083",
                "持股比例" => "48.10%",
                "股东名称" => "中国平安保险(集团)股份有限公司-集团本级-自有资金",
                "股份性质" => "流通A股"
              },
              "10" => %{
                "持股数量" => "40,708,918",
                "持股比例" => "0.24%",
                "股东名称" => "泰达宏利基金-民生银行-泰达宏利价值成长定向增发193号资产管理计划",
                "股份性质" => "流通A股"
              },
              "2" => %{
                "持股数量" => "1,049,462,784",
                "持股比例" => "6.11%",
                "股东名称" => "中国平安人寿保险股份有限公司-自有资金",
                "股份性质" => "流通A股"
              },
              "3" => %{
                "持股数量" => "400,837,357",
                "持股比例" => "2.33%",
                "股东名称" => "香港中央结算有限公司",
                "股份性质" => "流通A股"
              },
              "4" => %{
                "持股数量" => "389,735,963",
                "持股比例" => "2.27%",
                "股东名称" => "中国平安人寿保险股份有限公司-传统-普通保险产品",
                "股份性质" => "流通A股"
              },
              "5" => %{
                "持股数量" => "365,827,148",
                "持股比例" => "2.13%",
                "股东名称" => "中国证券金融股份有限公司",
                "股份性质" => "流通A股"
              },
              "6" => %{
                "持股数量" => "216,213,000",
                "持股比例" => "1.26%",
                "股东名称" => "中央汇金资产管理有限责任公司",
                "股份性质" => "流通A股"
              },
              "7" => %{
                "持股数量" => "186,051,938",
                "持股比例" => "1.08%",
                "股东名称" => "深圳中电投资股份有限公司",
                "股份性质" => "流通A股"
              },
              "8" => %{
                "持股数量" => "91,543,295",
                "持股比例" => "0.53%",
                "股东名称" => "河南鸿宝集团有限公司",
                "股份性质" => "流通A股"
              },
              "9" => %{
                "持股数量" => "49,603,502",
                "持股比例" => "0.29%",
                "股东名称" => "新华人寿保险股份有限公司-分红-个人分红-018L-FH002深",
                "股份性质" => "流通A股"
              }
            },
            "十大股东" => %{
              "1" => %{
                "持股数量" => "8,510,493,066",
                "持股比例" => "49.56%",
                "股东名称" => "中国平安保险(集团)股份有限公司-集团本级-自有资金",
                "股份性质" => "流通A股,流通受限股份"
              },
              "10" => %{
                "持股数量" => "40,708,918",
                "持股比例" => "0.24%",
                "股东名称" => "泰达宏利基金-民生银行-泰达宏利价值成长定向增发193号资产管理计划",
                "股份性质" => "流通A股"
              },
              "2" => %{
                "持股数量" => "1,049,462,784",
                "持股比例" => "6.11%",
                "股东名称" => "中国平安人寿保险股份有限公司-自有资金",
                "股份性质" => "流通A股"
              },
              "3" => %{
                "持股数量" => "400,837,357",
                "持股比例" => "2.33%",
                "股东名称" => "香港中央结算有限公司",
                "股份性质" => "流通A股"
              },
              "4" => %{
                "持股数量" => "389,735,963",
                "持股比例" => "2.27%",
                "股东名称" => "中国平安人寿保险股份有限公司-传统-普通保险产品",
                "股份性质" => "流通A股"
              },
              "5" => %{
                "持股数量" => "365,827,148",
                "持股比例" => "2.13%",
                "股东名称" => "中国证券金融股份有限公司",
                "股份性质" => "流通A股"
              },
              "6" => %{
                "持股数量" => "216,213,000",
                "持股比例" => "1.26%",
                "股东名称" => "中央汇金资产管理有限责任公司",
                "股份性质" => "流通A股"
              },
              "7" => %{
                "持股数量" => "186,051,938",
                "持股比例" => "1.08%",
                "股东名称" => "深圳中电投资股份有限公司",
                "股份性质" => "流通A股"
              },
              "8" => %{
                "持股数量" => "91,543,295",
                "持股比例" => "0.53%",
                "股东名称" => "河南鸿宝集团有限公司",
                "股份性质" => "流通A股"
              },
              "9" => %{
                "持股数量" => "49,603,502",
                "持股比例" => "0.29%",
                "股东名称" => "新华人寿保险股份有限公司-分红-个人分红-018L-FH002深",
                "股份性质" => "流通A股"
              }
            }
          },
          "利润" => %{
            "2014" => %{
              "一季报" => "15.29",
              "三季报" => "64.58",
              "半年报" => "48.09",
              "年报" => "157.45"
            },
            "2015" => %{
              "一季报" => "6.50",
              "三季报" => "68.54",
              "半年报" => "48.46",
              "年报" => "181.19"
            },
            "2016" => %{
              "一季报" => "8.33",
              "三季报" => "82.62",
              "半年报" => "53.51",
              "年报" => "210.23"
            },
            "2017" => %{
              "一季报" => "6.95",
              "三季报" => "110.91",
              "半年报" => "73.03",
              "年报" => "280.52"
            },
            "2018" => %{
              "一季报" => "8.95",
              "三季报" => "-",
              "半年报" => "-",
              "年报" => "-"
            }
          },
          "诚信情况" => %{
            "2014" => %{
              "公告日期" => "2014-04-11",
              "处罚内容" => "公司关于深圳市南山区“南油工业区福华厂区”城市更新项目发生重大仲裁事项未及时履行信息披露义务。",
              "处罚原因" => "",
              "处罚对象" => "上市公司",
              "处罚类型" => "非行政处罚-出具监管工作函",
              "处罚部门" => "深圳证券交易所"
            },
            "2018" => %{
              "公告日期" => "2018-01-03",
              "处罚内容" => "",
              "处罚原因" => "",
              "处罚对象" => "",
              "处罚类型" => "非行政处罚-出具监管关注函",
              "处罚部门" => "深圳证券交易所"
            }
          },
          "营业收入" => %{
            "一季报" => %{
              "2014" => 0.121815,
              "2015" => 0.134853,
              "2016" => 0.434215,
              "2017" => 0.714498,
              "2018" => 1.521507
            },
            "三季报" => %{
              "2014" => 0.358452,
              "2015" => 0.457572,
              "2016" => 3.504969,
              "2017" => 3.142272
            },
            "半年报" => %{
              "2014" => 0.238565,
              "2015" => 0.362853,
              "2016" => 1.899526,
              "2017" => 2.035988
            },
            "年报" => %{
              "2014" => 0.52777,
              "2015" => 0.84126,
              "2016" => 4.818634,
              "2017" => 5.309222
            }
          },
          "公司规模" => %{
            "update_time" => "2018-03-31",
            "利润总额" => "15,951,538.55",
            "总股本" => "105,853.68",
            "总负债" => "1,459,490,853.97",
            "总资产" => "2,879,810,300.89"
          }
        }
      }
    }
    # |> IO.inspect(label: "xbrl => ", pretty: true)
    |> Ontology.create_company_xbrl()
  end

  describe "简单的XBRL OPQ(查询财务信息)" do

    defp with_data(_tags) do
      fixture(:company_xbrl_cmb)
      fixture(:company_xbrl_pufa)

      :ok
    end

    setup [:with_query, :with_data, :with_user]

    @doc """
    MIA-US0011-AC001 公司参数
    Given 在QnA页面
    And 数据中有"000001" "2017" "每股净资产" 为 "11.77"
    When 点击输入框
    And 一个问题：“平安银行的每股净资产是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2017年的每股净资产是11.77。"
    """
    test "询问 OPQ财务信息 - 平安银行的每股净资产是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_cmb_sz_opq_gb" do
        execute_query(tags, "平安银行的每股净资产是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行2017年的每股净资产是 1.25元"
    end

    # @doc """
    # MIA-US0011-AC002 公司参数
    # Given 在QnA页面
    # And 数据中有"000005" "2017" "每股净资产" 为 "1.25"
    # When 点击输入框
    # And 一个问题：“世纪星源的每股净资产是多少”
    # #默认 T=[（最新年份）]
    # Then 得到回答 "世纪星源2017年的每股净资产是201亿。"
    # """
    # test "询问 OPQ财务信息 - 世纪星源的每股净资产是多少", tags do
    #   response = use_cassette "questions/xbrls/query_nlp_pf_sz_opq_gb" do
    #     execute_query(tags, "世纪星源的每股净资产是多少")
    #   end

    #   errors = response[:errors]
    #   assert errors == nil

    #   data = response[:data]
    #   # IO.inspect data, label: "response: ", pretty: true
    #   answer = data["question"]["answer"]
    #   assert answer != nil

    #   assert answer["content"] =~ "世纪星源2017年的每股净资产是 1.25元"
    # end

    @doc """
    MIA-US0011-AC003 公司参数
    Given 在迈达斯的QnA页面
        and Q标准词表无 “随便原因”
    And 点击输入框
    And 一个问题：“平安银行的每股净资产的随便原因”
    Then 得到回答 "暂时无法回答此类问题。"
    """
    test "询问 OPQ财务信息 - 平安银行的每股净资产的随便原因", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_opq_non_match" do
        execute_query(tags, "平安银行的每股净资产的随便原因")
      end

      # response
      # |> IO.inspect(label: "平安银行的每股净资产的随便原因 => ", pretty: true)

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil
      #赖建发修改了Q的逻辑，Q无法识别情况下，直接返回OP
      assert answer["content"] =~ "请问您问的是哪一年？"
    end

    @doc """
    MIA-US011-AC004 查询 "平安银行的总股本"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2017" "总股本" 为 "17170411400"
    And 点击输入框
    And 一个问题：“平安银行的总股本是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2017年的总股本是 17170411400 (股)。"
    """
    test "询问 OPQ财务信息 - 平安银行的总股本是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_opq_cmb_income" do
        execute_query(tags, "平安银行的总股本是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行2017年的总股本是 10.585万股"
    end

    @doc """
    MIA-US011-AC005 查询 "平安银行的2017总股本是多少"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2017" "总股本" 为 "17170411400"
    And 点击输入框
    And 一个问题：“平安银行的总股本是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2017年的总股本是 17170411400 (股)。"
    """
    test "询问 OTPQ财务信息 - 平安银行的2017总股本是多少", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_otpq_cmb_income" do
        execute_query(tags, "平安银行的2017年总股本是多少")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行2017年的总股本是 10.585万股"
    end

    @doc """
    MIA-US011-AC006 查询 "平安银行的总股本"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2017" "总股本" 为 "17170411400"
    And 点击输入框
    And 一个问题：“平安银行的总股本是多少”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2017年的总股本是 17170411400 (股)。"
    """
    test "询问 OTPQ财务信息 - 平安银行的总股本", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_op_implied_q_cmb_income" do
        execute_query(tags, "平安银行的总股本")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行2017年的总股本是 10.585万股"
    end

    @doc """
    MIA-US011-AC006 查询 "平安银行2015年的融资情况"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2015" "融资情况"
    And 点击输入框
    And 一个问题：“平安银行2015年的融资情况”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2015年"
    """
    test "询问 OTPQ财务信息 - 平安银行2015年的融资情况", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_otp_implied_2015rz" do
        execute_query(tags, "平安银行2015年的融资情况")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "2015-12-29，平安银行以发行价格3.74(元/股)进行融资，再融资类型为定向增发，实际募集资金13,238.19(万元)，总发行数量3,539.62(万股)。募集配套资金对象为深圳市博睿意碳源科技有限公司和上海勤幸投资管理中心（有限合伙）。"
    end

        @doc """
    MIA-US011-AC006 查询 "平安银行2015年的融资情况"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2015" "融资情况"
    And 点击输入框
    And 一个问题：“平安银行2015年的融资情况”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2015年"
    """
    test "询问 OTPQ财务信息 - 平安银行2017年的融资情况", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_otp_implied_2017rz" do
        execute_query(tags, "平安银行2017年的融资情况")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行2017年未进行融资"
    end

            @doc """
    MIA-US011-AC006 查询 "平安银行2015年的融资情况"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2015" "融资情况"
    And 点击输入框
    And 一个问题：“平安银行2015年的融资情况”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2015年"
    """
    test "询问 OTPQ财务信息 - 平安银行2017年的分红", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_otp_implied_2017fh" do
        execute_query(tags, "平安银行2017年的分红")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行2017年未进行分红"
    end

                @doc """
    MIA-US011-AC006 查询 "平安银行2015年的融资情况"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2015" "融资情况"
    And 点击输入框
    And 一个问题：“平安银行2015年的融资情况”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2015年"
    """
    test "询问 OTPQ财务信息 - 平安银行2006年的分红", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_otp_implied_2006fh" do
        execute_query(tags, "平安银行2006年的分红")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "2006-05-15，平安银行以10转增5.5股进行分红，新增股份于2006-07-31上市。股权登记日为2006-07-28，除权基准日为。"
    end

    @doc """
    MIA-US011-AC006 查询 "平安银行2015年的融资情况"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2015" "融资情况"
    And 点击输入框
    And 一个问题：“平安银行2015年的融资情况”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2015年"
    """
    test "询问 OTPQ财务信息 - 平安银行的十大股东", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_otp_implied_sdgd" do
        execute_query(tags, "平安银行的十大股东")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "截止于2018-03-31，平安银行的十大股东信息如下：\n1-股东名称:中国平安保险(集团)股份有限公司-集团本级-自有资金，持股数量:8,510,493,066股，持股比例:49.56%，股份性质:流通A股,流通受限股份。\n2-股东名称:中国平安人寿保险股份有限公司-自有资金，持股数量:1,049,462,784股，持股比例:6.11%，股份性质:流通A股。\n3-股东名称:香港中央结算有限公司，持股数量:400,837,357股，持股比例:2.33%，股份性质:流通A股。\n4-股东名称:中国平安人寿保险股份有限公司-传统-普通保险产品，持股数量:389,735,963股，持股比例:2.27%，股份性质:流通A股。\n5-股东名称:中国证券金融股份有限公司，持股数量:365,827,148股，持股比例:2.13%，股份性质:流通A股。\n6-股东名称:中央汇金资产管理有限责任公司，持股数量:216,213,000股，持股比例:1.26%，股份性质:流通A股。\n7-股东名称:深圳中电投资股份有限公司，持股数量:186,051,938股，持股比例:1.08%，股份性质:流通A股。\n8-股东名称:河南鸿宝集团有限公司，持股数量:91,543,295股，持股比例:0.53%，股份性质:流通A股。\n9-股东名称:新华人寿保险股份有限公司-分红-个人分红-018L-FH002深，持股数量:49,603,502股，持股比例:0.29%，股份性质:流通A股。\n10-股东名称:泰达宏利基金-民生银行-泰达宏利价值成长定向增发193号资产管理计划，持股数量:40,708,918股，持股比例:0.24%，股份性质:流通A股。"
    end

    @doc """
    MIA-US011-AC006 查询 "平安银行2015年的融资情况"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2015" "融资情况"
    And 点击输入框
    And 一个问题：“平安银行2015年的融资情况”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2015年"
    """
    test "询问 OTPQ财务信息 - 平安银行的十大流通股东", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_otp_implied_sdltgd" do
        execute_query(tags, "平安银行的十大流通股东")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "截止于2018-03-31，平安银行的十大流通股东信息如下：\n1-股东名称:中国平安保险(集团)股份有限公司-集团本级-自有资金，持股数量:8,258,245,083股，持股比例:48.10%，股份性质:流通A股。\n2-股东名称:中国平安人寿保险股份有限公司-自有资金，持股数量:1,049,462,784股，持股比例:6.11%，股份性质:流通A股。\n3-股东名称:香港中央结算有限公司，持股数量:400,837,357股，持股比例:2.33%，股份性质:流通A股。\n4-股东名称:中国平安人寿保险股份有限公司-传统-普通保险产品，持股数量:389,735,963股，持股比例:2.27%，股份性质:流通A股。\n5-股东名称:中国证券金融股份有限公司，持股数量:365,827,148股，持股比例:2.13%，股份性质:流通A股。\n6-股东名称:中央汇金资产管理有限责任公司，持股数量:216,213,000股，持股比例:1.26%，股份性质:流通A股。\n7-股东名称:深圳中电投资股份有限公司，持股数量:186,051,938股，持股比例:1.08%，股份性质:流通A股。\n8-股东名称:河南鸿宝集团有限公司，持股数量:91,543,295股，持股比例:0.53%，股份性质:流通A股。\n9-股东名称:新华人寿保险股份有限公司-分红-个人分红-018L-FH002深，持股数量:49,603,502股，持股比例:0.29%，股份性质:流通A股。\n10-股东名称:泰达宏利基金-民生银行-泰达宏利价值成长定向增发193号资产管理计划，持股数量:40,708,918股，持股比例:0.24%，股份性质:流通A股。"
    end

    @doc """
    MIA-US011-AC006 查询 "平安银行2015年的融资情况"
    Given 在QnA页面
    And xbrl_ss里有"000001" "2015" "融资情况"
    And 点击输入框
    And 一个问题：“平安银行2015年的融资情况”
    #默认 T=[（最新年份）]
    Then 得到回答 "平安银行2015年"
    """
    test "询问 OTPQ财务信息 - 平安银行的股东", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_otp_implied_gd" do
        execute_query(tags, "平安银行的股东")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "截止于2018-03-31，平安银行的十大股东信息如下：\n1-股东名称:中国平安保险(集团)股份有限公司-集团本级-自有资金，持股数量:8,510,493,066股，持股比例:49.56%，股份性质:流通A股,流通受限股份。\n2-股东名称:中国平安人寿保险股份有限公司-自有资金，持股数量:1,049,462,784股，持股比例:6.11%，股份性质:流通A股。\n3-股东名称:香港中央结算有限公司，持股数量:400,837,357股，持股比例:2.33%，股份性质:流通A股。\n4-股东名称:中国平安人寿保险股份有限公司-传统-普通保险产品，持股数量:389,735,963股，持股比例:2.27%，股份性质:流通A股。\n5-股东名称:中国证券金融股份有限公司，持股数量:365,827,148股，持股比例:2.13%，股份性质:流通A股。\n6-股东名称:中央汇金资产管理有限责任公司，持股数量:216,213,000股，持股比例:1.26%，股份性质:流通A股。\n7-股东名称:深圳中电投资股份有限公司，持股数量:186,051,938股，持股比例:1.08%，股份性质:流通A股。\n8-股东名称:河南鸿宝集团有限公司，持股数量:91,543,295股，持股比例:0.53%，股份性质:流通A股。\n9-股东名称:新华人寿保险股份有限公司-分红-个人分红-018L-FH002深，持股数量:49,603,502股，持股比例:0.29%，股份性质:流通A股。\n10-股东名称:泰达宏利基金-民生银行-泰达宏利价值成长定向增发193号资产管理计划，持股数量:40,708,918股，持股比例:0.24%，股份性质:流通A股。"
    end

    # >>>>>>>> 测试有问题，特此备注
    # test "询问 OTPQ财务信息 - 平安银行的利润", tags do
    #   response = use_cassette "questions/xbrls/query_nlp_sz_profit" do
    #     execute_query(tags, "平安银行的利润")
    #   end

    #   errors = response[:errors]
    #   assert errors == nil

    #   data = response[:data]
    #   # IO.inspect data, label: "response: ", pretty: true
    #   answer = data["question"]["answer"]
    #   assert answer != nil

    #   assert answer["content"] =~ "平安银行在2017年年报中的归属于上市公司股东净利润为280.52亿元"
    # end

    test "询问 OTPQ财务信息 - 平安银行的净利润", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_profit" do
        execute_query(tags, "平安银行的净利润")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2017年年报中的归属于上市公司股东净利润为280.52亿元"
    end

    test "询问 OTPQ财务信息 - 平安银行2017的利润", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_profit_2017" do
        execute_query(tags, "平安银行2017的利润")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2017年年报中的归属于上市公司股东净利润为280.52亿元"
    end

    test "询问 OTPQ财务信息 - 平安银行2018第一季度的利润", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_profit_2018s1" do
        execute_query(tags, "平安银行2018第一季度的利润")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2018年一季报中的归属于上市公司股东净利润为8.95亿元"
    end

    test "询问 OTPQ财务信息 - 平安银行2018第二季度的利润", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_profit_2018s2" do
        execute_query(tags, "平安银行2018第二季度的利润")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2018年半年报中的利润暂时未收录"
    end

    test "询问 OTPQ财务信息 - 平安银行2018的处罚情况", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_integrity_2018" do
        execute_query(tags, "平安银行2018的处罚情况")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "在2018-01-03，平安银行的诚信处罚信息如下：\n处罚内容：\n处罚原因：\n处罚对象：\n处罚类型：非行政处罚-出具监管关注函\n处罚部门：深圳证券交易所"
    end

    test "询问 OTPQ财务信息 - 平安银行2017的诚信情况", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_integrity_2017" do
        execute_query(tags, "平安银行2017的诚信情况")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2017诚信情况无异常"
    end

    test "询问 OTPQ财务信息 - 平安银行的诚信情况", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_integrity" do
        execute_query(tags, "平安银行2017的诚信情况")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2017诚信情况无异常"
    end


    test "询问 OTPQ财务信息 - 平安银行的营业收入", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_income" do
        execute_query(tags, "平安银行的营业收入")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2017年年报中的营业收入为5.309222亿元"
    end

    test "询问 OTPQ财务信息 - 平安银行的收入", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_income_short" do
        execute_query(tags, "平安银行的收入")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2017年年报中的营业收入为5.309222亿元"
    end

    test "询问 OTPQ财务信息 - 平安银行在2018第一季度的营业收入", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_income_2018s1" do
        execute_query(tags, "平安银行在2018第一季度的营业收入")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2018年一季报中的营业收入为1.521507亿元"
    end

    test "询问 OTPQ财务信息 - 平安银行在2018第二季度的营业收入", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_income_2018s2" do
        execute_query(tags, "平安银行在2018第二季度的营业收入")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "平安银行在2018年半年报中的营业收入暂时未收录"
    end

    test "询问 OTPQ财务信息 - 平安银行的总负债", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_liabilities" do
        execute_query(tags, "平安银行的总负债")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "截止于2018-03-31，平安银行的总负债为：1,459,490,853.97(元)"
    end

    test "询问 OTPQ财务信息 - 平安银行的公司规模", tags do
      response = use_cassette "questions/xbrls/query_nlp_sz_scale" do
        execute_query(tags, "平安银行的公司规模")
      end

      errors = response[:errors]
      assert errors == nil

      data = response[:data]
      # IO.inspect data, label: "response: ", pretty: true
      answer = data["question"]["answer"]
      assert answer != nil

      assert answer["content"] =~ "截止于2018-03-31，平安银行的公司规模：\n总资产：2,879,810,300.89(元)\n总负债：1,459,490,853.97(元)\n总股本：105,853.68(万股)\n利润总额：15,951,538.55(元)"
    end
  end
end
