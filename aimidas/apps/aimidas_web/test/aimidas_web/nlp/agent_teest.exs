defmodule AimidasWeb.NLP.AgentTest do
  use ExUnit.Case, async: true
  use ExVCR.Mock
  alias AimidasWeb.NLP.Agent, as: NAgent

  describe "query P" do
    test "P" do
      result = {
        :ok,
        %{
          code: 0,
          type: "P",
          context_tag: 1,
          context: %{
            O: [],
            P: [
              %{
                nlp_reason: "exact",
                text: "营业总成本",
                value: "营业总成本"
              }
            ],
            Q: [
              %{
                nlp_reason: "implied",
                text: "",
                value: "definition",
                year: 0
              }
            ],
            T: [],
            type: "P"
          },
          related_question: ["招商银行营业成本", "浦发银行本期营业收入", "航发动力：公司与重庆天骄航空动力有限公司的关系是什么？"],
          question_md5: "95387e93026a69be1c441310d0a645bc"
        }
      }

      use_cassette "total_cost_post" do
        assert result == NAgent.analyze("营业总成本", "")
      end
    end
  end

  describe "query O" do
    test "O" do
      result = {
        :ok,
        %{
          code: 0,
          type: "O",
          context_tag: 0,
          context: %{
            O: [
                %{
                  code: "600036",
                  exchange_code: "SH",
                  nlp_reason: "exact",
                  text: "招商银行",
                  value: "招商银行"
                }
              ],
            P: [],
            Q: [%{nlp_reason: "implied", text: "", value: "definition", year: 0}],
            T: [],
            type: "O"
          },
          related_question: ["招商银行支付的其他与筹资活动有关的现金", "招商银行应收票据", "财政政策目标"],
          question_md5: "1fdd495da57cf6fac7ff86ed95bd6b5f"
        }
      }

      use_cassette "query_nlp_o_cmb" do
        assert result == NAgent.analyze("招商银行", "")
      end
    end
  end

end
