defmodule AimidasWeb.NLP.ParserTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock

  alias AimidasWeb.NLP.Parser, as: NParser

  setup_all do
    HTTPotion.start
  end

  describe "#parse" do
    test "‘营业总成本’" do
      result = use_cassette "parser/p_totol_cost" do
        NParser.parse("营业总成本", "")
      end
      # IO.puts("result: #{inspect(result, pretty: true)}")
      %{type: "P"} = result
    end
  end
end
