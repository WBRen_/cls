use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :aimidas_web, AimidasWeb.Endpoint,
  http: [port: 4001],
  server: false

config :aimidas_web, :nlp_url, "http://47.106.225.37:19090/analyze/"

config :aliyun, :oss_endpoint, "oss-cn-shenzhen.aliyuncs.com"
config :aliyun, :oss_access_key_id, "LTAI2LUVupzuMakN"
config :aliyun, :oss_access_key_secret, "QklKdAJcmYB7fYGSoQo3Z5JWiz7hUs"

config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: System.get_env("GITHUB_CLIENT_ID") || "ef8db83ba44013717fc1",
  client_secret: System.get_env("GITHUB_CLIENT_SECRET") || "f4a7834205108f342ed1bcc8d108591c36b56ccc"

config :ueberauth, Ueberauth.Strategy.Wechat.OAuth,
  client_id: System.get_env("WECHAT_APPID") || "wx65998e39016f4fa0",
  client_secret: System.get_env("WECHAT_SECRET") || "4923340552db56f51f889f239d69961e"

config :ueberauth, Ueberauth.Strategy.WechatMiniapp.OAuth,
  client_id: System.get_env("WECHAT_MINIAPP_APPID") || "wxd907a379de9ccf45",
  client_secret: System.get_env("WECHAT_MINIAPP_SECRET") || "5f469b08c9dde9eddb9e8e4ab65b6589"

config :exvcr, [
  vcr_cassette_library_dir: "test/fixtures/vcr_cassettes",
  custom_cassette_library_dir: "test/fixtures/custom_cassettes",
  filter_sensitive_data: [
    [pattern: "<PASSWORD>.+</PASSWORD>", placeholder: "PASSWORD_PLACEHOLDER"]
  ],
  filter_url_params: false,
  filter_request_headers: [],
  response_headers_blacklist: []
]
