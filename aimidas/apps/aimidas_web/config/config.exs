# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :aimidas_web,
  namespace: AimidasWeb,
  ecto_repos: [Aimidas.Repo]

# Configures the endpoint
config :aimidas_web, AimidasWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "TZsxaSZTPO3U5CsMZ4yM4i2SiNWXtx9vzib5RlkxHMq4H9ENu3QkWFXHbwaAIz72",
  render_errors: [view: AimidasWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AimidasWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :aimidas_web, :generators,
  context_app: :aimidas

config :ueberauth, Ueberauth,
  providers: [
    github: {Ueberauth.Strategy.Github, []},
    wechat: {Ueberauth.Strategy.Wechat, []},
    wechat_miniapp: {Ueberauth.Strategy.WechatMiniapp, []}
  ]

config :aimidas_web, AimidasWeb.Guardian,
  issuer: "Adimias",
  secret_key: "my secret"

# config :aimidas_web, AimidasWeb.Endpoint,
#   instrumenters: [Appsignal.Phoenix.Instrumenter]

# config :phoenix, :template_engines,
#   eex: Appsignal.Phoenix.Template.EExEngine,
#   exs: Appsignal.Phoenix.Template.ExsEngine

# config :aimidas_web, :nlp_url, "http://127.0.0.1:4000"
config :aimidas_web, :nlp_url, "http://47.104.140.197:8080/analyze/"

config :aliyun, :oss_endpoint, "oss-cn-shenzhen.aliyuncs.com"
config :aliyun, :oss_access_key_id, "LTAI2LUVupzuMakN"
config :aliyun, :oss_access_key_secret, "QklKdAJcmYB7fYGSoQo3Z5JWiz7hUs"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
