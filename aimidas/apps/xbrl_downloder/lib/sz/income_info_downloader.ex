defmodule XbrlDownloader.SZ.IncomeInfos do
  import Aimidas.Xbrl.SZ.BasicInfoFields, only: [fields: 0]
  alias XbrlDownloader.ConvertHelper

  alias XbrlDownloader.SZ.FieldsExtractor, as: Extractor

  @url "http://xbrl.cninfo.com.cn/do/chartAction/barchart?seccode="

  def craw(stock_code) do
    url = @url <> stock_code
    # IO.inspect("BasicInfo Download >> URL: #{url}")
    response = HTTPotion.get url
    %{status_code: 200, body: body} = response

    body
      |> Floki.find("series")
      |> extract_cols
      # |> IO.inspect(label: "Income Download >> RESPONSE:")
  end

  def extract_cols(table) do
    values =
      table
      |> Enum.map(fn series ->
        {"series", name, data} = series
        data |> extract_season
      end)
    keys = ["一季报", "半年报", "三季报", "年报"]
    Stream.zip(keys, values) |> Enum.into(%{})
  end

  def extract_season(data) do
    keys =
      data
      |> Enum.map(fn point ->
        {"point", line, _} = point
        [{_, key}, {_, _}] = line
        key
      end)
    values =
      data
      |> Enum.map(fn point ->
        {"point", line, _} = point
        [{_, _}, {_, text}] = line
        {value, _} =
          text
          |> Float.parse()
        value
      end)
      Stream.zip(keys, values)|> Enum.into(%{})
  end

  def extract_response(%{status_code: sc, body: body}, {stock_code, url}) do
    IO.puts(
      """
      [#{__MODULE__}.extract_response] failed to get basic info for #{stock_code}:
      url: #{url}
      status_code: #{sc}
      body: #{body}
      """
    )
  end

  defp handle_response(%{status_code: 200} = response) do
    response.body
      |> Poison.decode!()
  end

  defp handle_response(%{} = response) do
    %{
      "error" => response.body
    }
  end

end
