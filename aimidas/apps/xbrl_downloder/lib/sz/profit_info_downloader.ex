defmodule XbrlDownloader.SZ.ProfitInfos do
  import Aimidas.Xbrl.SZ.BasicInfoFields, only: [fields: 0]
  alias XbrlDownloader.ConvertHelper

  alias XbrlDownloader.SZ.FieldsExtractor, as: Extractor

  @url "http://xbrl.cninfo.com.cn/do/generalinfo/getcompanygeneralinfo?ticker="

  def craw(stock_code) do
    url = @url <> stock_code
    # IO.inspect("BasicInfo Download >> URL: #{url}")
    response = HTTPotion.get url
    response
    # |> IO.inspect(label: "BasicInfo Download >> RESPONSE:")
    |> extract_response({stock_code, url})
  end

  def extract_response(%{status_code: 200, body: body}, _) do
    current_year_data =
    body
    |> Floki.find("table.companyGenerationInfo_table7")
    |> extract_cols()
    # |> IO.inspect
    # last 2-5 years
    # body
    # |> Floki.find(".companyGenerationInfo_table6")
    # |> extract_cols()
    # |> List.insert_at(0, current_year_data |> List.first)
    # |> Map.new
  end
  def extract_response(%{status_code: sc, body: body}, {stock_code, url}) do
    IO.puts(
      """
      [#{__MODULE__}.extract_response] failed to get basic info for #{stock_code}:
      url: #{url}
      status_code: #{sc}
      body: #{body}
      """
    )
  end

  def extract_cols(table) do
    key = ["一季报", "半年报", "三季报", "年报"]
    data =
      table
      |> Enum.map(fn table ->
        {"table", _, data} = table
        data
        |> Enum.map(fn tr ->
          {"tr", _, [{"td", _, [value]}]} = tr
          value
        end)
      end)
    year =
      data
      |> Enum.map(fn item ->
        [year | _] = item
        year
      end)
    data =
      data
      |> Enum.map(fn item ->
        col_value =
          item
          |> List.delete_at(0)
        Stream.zip(key, col_value) |> Enum.into(%{})
      end)
    Stream.zip(year, data) |> Enum.into(%{})
    # |> Enum.map(fn values ->
    #   year = values |> List.first |> String.slice(0, 4)
    #   [_ | raw_values] = values
    #   data_fileds =
    #     fields()
    #     |> Extractor.field_names()

    #   {
    #     "#{year}",
    #     Extractor.extract_fields(raw_values, data_fileds)
    #   }
    # end)
  end

  defp handle_response(%{status_code: 200} = response) do
    response.body
      |> Poison.decode!()
  end

  defp handle_response(%{} = response) do
    %{
      "error" => response.body
    }
  end

end
