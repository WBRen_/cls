defmodule XbrlDownloader.SZ do
  @doc """
  下载深交所数据
  """

  alias XbrlDownloader.SZ.BasicInfos
  alias XbrlDownloader.SZ.Dividend
  alias XbrlDownloader.SZ.Finance
  alias XbrlDownloader.SZ.ShareholderInfos
  alias XbrlDownloader.SZ.ProfitInfos
  alias XbrlDownloader.SZ.Integrity
  alias XbrlDownloader.SZ.IncomeInfos
  alias XbrlDownloader.SZ.ScaleInfos

  @doc """
  更多可爬取信息可以填充在此
  """
  def download(stock_code) do
    IO.puts("[XbrlDownloader.SZ.download] stock_code => #{stock_code}")
    %{
      "基本信息" => BasicInfos.craw(stock_code),
      "分红" => Dividend.craw(stock_code),
      "融资情况" => Finance.craw(stock_code),
      "十大股东" => ShareholderInfos.craw(stock_code),
      "利润" => ProfitInfos.craw(stock_code),
      "诚信情况" => Integrity.craw(stock_code),
      "营业收入" => IncomeInfos.craw(stock_code),
      "公司规模" => ScaleInfos.craw(stock_code)
    }
  end
end
