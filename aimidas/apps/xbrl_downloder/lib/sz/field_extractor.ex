defmodule XbrlDownloader.SZ.FieldsExtractor do

  alias Aimidas.Xbrl.AllFields, as: XbrlFields
  alias Aimidas.Xbrl.InConverter, as: ConvertHelper

  def field_names(fields) do
    fields
    |> Enum.map(fn x -> x |> List.first end)
  end

  def extract_fields(src_values, fields) do
    src_values
    |> Enum.reduce({0, %{}, fields}, &process_value/2)
    |> Tuple.to_list
    |> Enum.at(1)
    
  end

  def process_value(value, {index, data, fields}) do
    field_name = 
      fields
      |> Enum.at(index)

    # IO.inspect(fields, label: "fields: ", pretty: true)

    field_info = 
      XbrlFields.field_infos()
      |> get_in(["SZ", field_name])

    src_value = value |> to_string() |> String.trim()
    field_value =
      ConvertHelper
      |> apply(field_info.in_converter, [src_value])

    new_data = 
      data
      |> Map.put(field_name, field_value)

    {
      index + 1,
      new_data,
      fields
    }
  end
end