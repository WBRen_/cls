defmodule XbrlCrawler.SZ do
  @moduledoc """
  Documentation for XbrlCrawler. For SZ
  爬取、解析、持久化于一体
  XbrlCrawler.craw_stock("000005")
  """

  alias XbrlDownloader.SZ
  alias Mix.Tasks.Aimidas.Import.CompanyXbrls

  @doc """
    爬取并解析、持久化
  """
  def craw_stock(stock_code) do
    %{
      "exchange_code" => "SZ",
      "company_code" => stock_code,
      # industry: "",
      # full_name: "",
      # abbrev_name: "",
      "src_updated_at" => today(),
      "details" => %{
        "年报" => fill_content(stock_code)
      }
    }
    |> CompanyXbrls.do_import()
  end

  @doc """
    爬取并插入 latest_year
  """
  defp fill_content(stock_code) do
    report = SZ.download(stock_code)
    latest_year = find_latest_year(report)
    report
    |> Map.put("latest_year", latest_year)
  end

  @doc """
    latest_year 为“基本信息”的最新时间
  """
  defp find_latest_year(report) do
    report["基本信息"]
    # |> IO.inspect
    |> Map.keys
    |> Enum.sort(&(&1 >= &2))
    |> Enum.at(0)
  end

  defp today() do
    DateTime.utc_now()
    |> Date.to_iso8601
    # "2018-01-01"
  end
end
