defmodule XbrlDownloader.SZ.Dividend do
  @doc """
  分红页面，需要翻页爬取
  """
  @url "http://xbrl.cninfo.com.cn/do/dividend/getdividendhistory"

  alias XbrlDownloader.SZ.FieldsExtractor, as: Extractor
  import Aimidas.Xbrl.SZ.DividendInfoFields, only: [fields: 0]

  def craw(stock_code) do
    start_page = 1
    extract_page(stock_code, start_page)
    |> Enum.into(%{})
  end

  def extract_page(stock_code, page) do
    response = HTTPotion.get @url <> "?ticker=#{stock_code}&page=#{page}"
    response
    # |> IO.inspect(label: "Dividend Download >> RESPONSE:")
    %{status_code: 200, body: body} = response

    list = body
    |> Floki.find("table.dividendHistorySubpage_table > tr")
    |> extract()

    total_page = body
    |> Floki.find("input#pageCount")
    |> Floki.attribute("value")
    |> List.first()
    |> String.to_integer()
    # 如果未到最后一页，则继续下一页抓取
    if page < total_page do
      list_next = extract_page(stock_code, page + 1)
      list ++ list_next
    else
      list
    end
  end

  def extract(table) do
    table
    |> Enum.map(fn tr ->
      {"tr", _, tds} = tr
      tds
      |> Enum.map(fn td ->
        {"td", _, [value]} = td
        value
      end)
    end)
    |> List.delete_at(0) # 表头信息
    |> Enum.map(fn item ->
      item |> to_map()
    end)
  end

  def to_map(cols) do
    key = cols |> List.first() |> String.slice(1..4) # 2001-12-31
    field_names =
      fields()
      |> Extractor.field_names()

    {
      key,
      Extractor.extract_fields(cols, field_names)
    }
  end
end
