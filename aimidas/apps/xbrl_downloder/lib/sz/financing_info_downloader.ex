defmodule XbrlDownloader.SZ.Finance do
  @doc """
  融资情况，需要翻页爬取时间信息，然后分别抓取对应时间下的具体数据
  XbrlDownloader.SZ.Finance.craw("000005")
  """
  @url_date "http://xbrl.cninfo.com.cn/do/financing/getfinancedate"
  @url_info "http://xbrl.cninfo.com.cn/do/financing/getfinance?type=1&index=0&dataCount=9"


  def craw(stock_code) do
    start_page = 1
    extract_page(stock_code, start_page)
    |> Enum.into(%{})
  end

  def extract_page(stock_code, page) do
    response =
      @url_date <> "?ticker=#{stock_code}&page=#{page}"
      # |> IO.inspect(label: "finance extract_page_url: ")
      |> HTTPotion.get
      # |> IO.inspect(label: "Finance extract_page >> RESPONSE:")

    %{status_code: 200, body: body} = response

    list =
      body
      |> Floki.find("table#notice > tr")
      # |> IO.inspect(label: "[#{__MODULE__}.extract_page] table#notice > tr => ")
      |> extract(stock_code)
      # |> IO.inspect

    total_page =
      body
      |> Floki.find("input#pageCount")
      |> Floki.attribute("value")
      |> List.first()
      |> String.to_integer()
      # |> IO.inspect(label: "[extract_page] total_page: ")
    # 如果未到最后一页，则继续下一页抓取
    if page < total_page do
      list_next = extract_page(stock_code, page + 1)
      case list_next do
        [_] -> list ++ list_next
        _ -> list
      end
      # list ++ list_next
    else
      list
    end
  end

  def extract(table, stock_code) do
    table
    |> Enum.map(fn tr ->
      {"tr", _, tds} = tr
      case tds |> List.first() do
        {"td", _, [value]} -> value
        _ -> ""
      end
    end)
    |> Enum.map(fn item ->
      details = item |> craw_info_page(stock_code) # 爬取具体的页面
      {item |> String.slice(0..3), details}
    end)
    |> Enum.into(%{})
  end

  def craw_info_page(date, stock_code) do
    # {:ok, %{body: body}} = HTTPoison.get @url_info <> "&ticker=#{stock_code}&date=#{date}"
    response =
      @url_info <> "&ticker=#{stock_code}&date=#{date}"
      # |> IO.inspect(label: "financial url: ")
      |> HTTPotion.get
      # |> IO.inspect(label: "Finance craw_info_page >> RESPONSE:")

    %{status_code: 200, body: body} = response

    body
    |> Floki.find("table.againfinance_table > tr")
    # |> IO.inspect
    |> Enum.map(fn tr ->
      {"tr", _, tds} = tr
      title = tds |> List.first() |> Floki.text
      value = tds |> List.delete_at(0) |> List.first() |> Floki.text |> convert_value()
      {title, value}
    end)
    |> Enum.into(%{})
  end

  def to_map(cols) do
    key = cols |> List.first() |> convert_value() # 2001-12-31
    names = ["分红方案", "股权登记日", "除权基准日", "新增股份上市日"]
    col_values = cols
                  |> List.delete_at(0)
                  |> Enum.map(fn x ->
                    convert_value(x)
                  end)
    values = Stream.zip(names, col_values)
              |> Enum.into(%{})
    {key, values}
  end

  def convert_value(str) do
    str |> String.trim() # 第一个和最后一个为空格
  end
end
