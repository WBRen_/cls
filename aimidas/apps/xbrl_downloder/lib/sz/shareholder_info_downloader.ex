defmodule XbrlDownloader.SZ.ShareholderInfos do

  @url "http://xbrl.cninfo.com.cn/do/shareholder/shareholders?ticker="

  def nil_form do
      {"table", [{"class", "shareholders_table"}],
       [
         {"tr", [],
          [
            {"td", [{"class", "shareholders_td2"}], ["截止日期"]},
            {"td", [{"class", "shareholders_td3"}], ["股东名称"]},
            {"td", [{"class", "shareholders_td4"}], ["持股数量(股)"]},
            {"td", [{"class", "shareholders_td5"}], ["持股比例"]},
            {"td", [{"class", "shareholders_td6"}], ["股份性质"]}
          ]}
       ]}
  end

  def craw(stock_code) do
    url = @url <> stock_code
    # IO.inspect("ShareholderInfo Download >> URL: #{url}")
    response = HTTPotion.get url
    response
    # |> IO.inspect(label: "ShareholderInfo Download >> RESPONSE:")
    %{status_code: 200, body: body} = response

    form =
      body
      |> Floki.find("table.shareholders_table")

    [form_1|_] = form
    [_|form_2] = form

    if form_1 != nil_form and form_2 != nil_form
     do
      data =
        form
        |> extract_cols()
        |> Enum.map(fn x -> x |> form_data end)
      time =
        form
        |> extract_time()

      data_1 =
        data
        |> List.first()
      data_2 =
        data
        |> List.delete_at(0)
        |> List.first()
      %{
        "update_time" => List.first(time),
        "十大股东" => data_1,
        "十大流通股东" => data_2,
      } else
        %{
        "update_time" => nil,
        "十大股东" => nil,
        "十大流通股东" => nil,
      }
    end
  end

  def extract_cols(table) do
    table
    |> Enum.map(fn table ->
      {"table", _, data} = table
      data
      # |> IO.inspect(label: ">>>>>>>>>>data: ", pretty: true)
      |> Enum.map(fn tr ->
        {"tr", _, line} = tr
        line
        # |> IO.inspect(label: ">>>>>>>>>>line: ", pretty: true)
        |> Enum.map(fn td ->
          {"td", _, [value]} = td
          value
          # |> IO.inspect(label: ">>>>>>>>>>value: ", pretty: true)
          end)
      end)
      |> fill_col
    end)
  end

  def extract_time(table) do
    table
    |> Enum.map(fn table ->
      {"table", _, data} = table
      data
      # |> IO.inspect(label: ">>>>>>>>>>data: ", pretty: true)
      |> Enum.map(fn tr ->
        {"tr", _, line} = tr
        line
        # |> IO.inspect(label: ">>>>>>>>>>line: ", pretty: true)
        |> Enum.map(fn td ->
          {"td", _, [value]} = td
          value
          # |> IO.inspect(label: ">>>>>>>>>>value: ", pretty: true)
          end)
      end)
      |> get_time
    end)
  end

  def fill_col(src_values) do
    first_values =
      src_values
      # |> IO.inspect(label: ">>>>>>>>>>src: ", pretty: true)
      |> List.delete_at(0) |> List.first |> List.delete_at(0)
    # |> IO.inspect(label: ">>>>>>>>>>first: ", pretty: true)
    values =
      src_values
      |> List.delete_at(0)
      |> List.replace_at(0, first_values)
      # |> IO.inspect(label: ">>>>>>>>>>values: ", pretty: true)
    values
    # |> IO.inspect(label: ">>>>>>>>>>values: ", pretty: true)
  end

  def get_time(src_values) do
    src_values
      |> List.delete_at(0) |> List.first |> List.first()
  end

  def form_data(data) do
    start_item = 1
    list = %{}
    fill_data(list, data, start_item)
  end

  def fill_data(list, data, item) do
    names = ["股东名称", "持股数量", "持股比例", "股份性质"]
    if item <= 10 do
      if data != [] do
        col_values =
          data
          |> List.first
        data =
          data
          |> List.delete_at(0)
        value =
          Stream.zip(names, col_values)
          |> Enum.into(%{})
        list =
          list
          |> Map.put_new("#{item}", value)
          # |> IO.inspect(label: ">>>>>>>>>>list: ", pretty: true)
          |> fill_data(data, item + 1)
      else
        list =
          list
          |> Map.put_new("#{item}", nil)
      end
    else
      list
    end
  end

  defp convert_value(v) when v == "-" do
    nil
  end

  # 其余数据全是 float
  defp convert_value(v) do
    v |> String.replace(",", "") |> String.replace(" ", "") |> to_float()
  end

  defp to_float(v) when v == "-" do
    nil
  end

  defp to_float(v) do
    v |> String.to_float()
  end

  # defp find_latest_year(report) do
  #   report
  #   |> Map.keys
  #   |> Enum.sort(&(&1 >= &2))
  #   |> Enum.at(0)
  # end


  defp handle_response(%{status_code: 200} = response) do
    response.body
      |> Poison.decode!()
  end

  defp handle_response(%{} = response) do
    %{
      "error" => response.body
    }
  end

end
