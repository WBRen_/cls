defmodule Mix.Tasks.Aimidas.Import.DownloadXbrls do
  use Mix.Task

  import Mix.Ecto

  @shortdoc "Download CompanyXbrls"
  def run(["sh", company_code] = params) do
    # params
    # |> IO.inspect(label: "params: ", pretty: true)

    ensure_started(Aimidas.Repo, [])

    HTTPotion.start()

    XbrlCrawler.craw_stock(company_code)
  end

  def run(["sz", company_code] = params) do
    # params
    # |> IO.inspect(label: "params: ", pretty: true)

    ensure_started(Aimidas.Repo, [])

    HTTPotion.start()

    XbrlCrawler.SZ.craw_stock(company_code)
  end
end
