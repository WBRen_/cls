defmodule Mix.Tasks.Aimidas.Import.SyncXbrls do
  use Mix.Task

  import Mix.Ecto
  import Ecto.Query

  alias Aimidas.Ontology.CompanyXbrl
  alias Aimidas.Repo

  @shortdoc "Synchronize CompanyXbrls"
  def run(["sh", before_date] = _params) do
    # params
    # |> IO.inspect(label: "params: ", pretty: true)

    ensure_started(Aimidas.Repo, [])

    HTTPotion.start()

    query = 
      from xbrl in CompanyXbrl,
      where: xbrl.src_updated_at < ^(Date.from_iso8601!(before_date)) and
              xbrl.exchange_code == "SH"
      

    query
    |> Repo.all()
    |> Enum.each(fn(company_xbrl) ->
      IO.puts("Synchronizing SH #{company_xbrl.company_code} ...")
      XbrlCrawler.craw_stock(company_xbrl.company_code)
    end)
  end

  def run(["sz", before_date] = _params) do
    # params
    # |> IO.inspect(label: "params: ", pretty: true)

    ensure_started(Aimidas.Repo, [])

    HTTPotion.start()

    query = 
      from xbrl in CompanyXbrl,
      where: xbrl.src_updated_at < ^(Date.from_iso8601!(before_date)) and
              xbrl.exchange_code == "SZ"

    query
    |> Repo.all()
    |> Enum.each(fn(company_xbrl) ->
      IO.puts("Synchronizing SZ #{company_xbrl.company_code} ...")
      XbrlCrawler.SZ.craw_stock(company_xbrl.company_code)
    end)
  end
end
