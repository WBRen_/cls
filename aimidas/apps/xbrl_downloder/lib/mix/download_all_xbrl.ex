defmodule Mix.Tasks.Aimidas.Import.DownloadAllXbrls do
  use Mix.Task

  import Mix.Ecto
  import Ecto.Query

  alias Aimidas.Ontology.CompanyXbrl
  alias Aimidas.Repo

  @shortdoc "Download CompanyXbrls"
  def run(["sh", company_codes_file] = _params) do
    # params
    # |> IO.inspect(label: "params: ", pretty: true)

    ensure_started(Aimidas.Repo, [])

    HTTPotion.start()

    company_codes_file
    |> load_company_codes()
    |> Enum.each(fn(company_code) ->
      IO.puts("Downloading #{company_code} ...")
      XbrlCrawler.craw_stock(String.trim(company_code))
    end)
  end

  def run(["sz", company_codes_file] = _params) do
    # params
    # |> IO.inspect(label: "params: ", pretty: true)

    ensure_started(Aimidas.Repo, [])

    HTTPotion.start()

    company_codes_file
    |> load_company_codes()
    |> Enum.each(fn(company_code) ->
      IO.puts("Downloading #{company_code} ...")
      XbrlCrawler.SZ.craw_stock(company_code)
    end)
  end

  defp load_company_codes(file) do
    file
    |> File.read!()
    |> Poison.decode!()
  end
end
