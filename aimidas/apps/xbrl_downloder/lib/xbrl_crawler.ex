defmodule XbrlCrawler do
  @moduledoc """
  Documentation for XbrlCrawler.
  爬取、解析、持久化于一体
  XbrlCrawler.craw_stock("600036")
  """

  alias XbrlDownloder
  alias Mix.Tasks.Aimidas.Import.CompanyXbrls

  @doc """
    爬取并解析、持久化
  """
  def craw_stock(stock_code) do
    %{
      "exchange_code" => "SH",
      "company_code" => stock_code,
      # industry: "",
      # full_name: "",
      # abbrev_name: "",
      "src_updated_at" => today(),
      "details" => %{
        "一季报" => fill_content(stock_code, 1),
        "半年报" => fill_content(stock_code, 2),
        "三季报" => fill_content(stock_code, 3),
        "年报"   => fill_content(stock_code, 4)
      }
    }
    |> CompanyXbrls.do_import()
  end

  @doc """
    爬取并插入 latest_year
  """
  defp fill_content(stock_code, quarter) when quarter <= 4 and quarter >= 1 do
    report = XbrlDownloader.craw_extract(stock_code, quarter)
    latest_year = find_latest_year(report)
    report
    |> Map.put("latest_year", latest_year)
  end

  @doc """
    latest_year 为“基本信息”的最新时间
  """
  defp find_latest_year(report) do
    report["基本信息"]
    |> Map.keys
    |> Enum.sort(&(&1 >= &2))
    |> Enum.at(0)
  end

  defp today() do
    DateTime.utc_now()
    |> Date.to_iso8601
    # "2018-01-01"
  end
end
