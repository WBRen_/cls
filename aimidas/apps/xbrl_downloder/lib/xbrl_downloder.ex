defmodule XbrlDownloader do
  @moduledoc """
  Documentation for XbrlDownloader.
  """

  alias XbrlDownloader.BalanceInfoExtractor
  alias XbrlDownloader.BasicInfoExtractor
  alias XbrlDownloader.CapitalInfoExtractor
  alias XbrlDownloader.CashInfoExtractor
  alias XbrlDownloader.ProfitInfoExtractor
  alias XbrlDownloader.SH.ShareholderInfoExtractor
  alias XbrlDownloader.SH.Dividend

  def url_balance_info(stock_code, period) do
    "http://listxbrl.sse.com.cn/companyInfo/showBalance.do?report_period_id=" <> period <> "&stock_id=" <> stock_code
  end
  def url_basic_info(stock_code, period) do
    "http://listxbrl.sse.com.cn/companyInfo/showmap.do?report_period_id=" <> period <> "&stock_id=" <> stock_code
  end
  def url_capital_info(stock_code, period) do
    "http://listxbrl.sse.com.cn/capital/showmap.do?report_period_id=" <> period <> "&stock_id=" <> stock_code
  end
  def url_cash_info(stock_code, period) do
    "http://listxbrl.sse.com.cn/cash/showmap.do?report_period_id=" <> period <> "&stock_id=" <> stock_code
  end
  def url_profit_info(stock_code, period) do
    "http://listxbrl.sse.com.cn/profit/showmap.do?report_period_id=" <> period <> "&stock_id=" <> stock_code
  end
  def url_shareholder_info(stock_code, period) do
    "http://listxbrl.sse.com.cn/companyInfo/showTopTenMap.do?report_period_id=" <> period <> "&stock_id=" <> stock_code
  end

  @doc """
    quarter: 1  一季度
             2  二季度（半年报）
             3  三季度
             4  四季度（年报）
  """

  @doc """
    爬取并解析，输出 map
  """
  def craw_extract(stock_code, quarter) when quarter in [1, 3] do
    craw_extract_default(stock_code, quarter)
  end
  def craw_extract(stock_code, quarter) when quarter in [2, 4] do
    capital = craw_quarter(stock_code, quarter, :url_capital_info)
              |> CapitalInfoExtractor.process()

    shareholder = craw_quarter(stock_code, quarter, :url_shareholder_info)
              |> ShareholderInfoExtractor.process()

    devidend = Dividend.craw(stock_code)

    stock_code
    |> craw_extract_default(quarter)
    |> Map.put("股本结构", capital)
    |> Map.put("十大股东", shareholder)
    |> Map.put("分红", devidend)
  end

  defp craw_extract_default(stock_code, quarter) do
    %{
      "基本信息" => stock_code
                  |> craw_quarter(quarter, :url_basic_info)
                  |> BasicInfoExtractor.process(),
      "资产负债" => craw_quarter(stock_code, quarter, :url_balance_info)
                    |> BalanceInfoExtractor.process(),
      "利润"    => craw_quarter(stock_code, quarter, :url_profit_info)
                    |> ProfitInfoExtractor.process(),
      "现金流量" => craw_quarter(stock_code, quarter, :url_cash_info)
                    |> CashInfoExtractor.process()
    }
  end

  @doc """
    爬取操作，输出 json，不做解析及持久化处理
  """
  def craw(stock_code, quarter) when quarter in [1, 3] do
    craw_default(stock_code, quarter)
  end

  def craw(stock_code, quarter) when quarter in [2, 4] do
    capital = craw_quarter(stock_code, quarter, :url_capital_info)

    stock_code
    |> craw_default(quarter)
    |> Map.put("股本结构", capital)
  end

  defp craw_default(stock_code, quarter) do
    %{
      "基本信息" => craw_quarter(stock_code, quarter, :url_basic_info),
      "资产负债" => craw_quarter(stock_code, quarter, :url_balance_info),
      "利润" => craw_quarter(stock_code, quarter, :url_profit_info),
      "现金流量" => craw_quarter(stock_code, quarter, :url_cash_info)
    }
  end

  @doc """
    1、2、3、4 季报专用，其中 2、4 为 半年报、年报
  """
  defp craw_quarter(stock_code, quarter, info_url) do
    # period = case quarter do
    #    1 -> "4000"
    #    2 -> "1000"
    #    3 -> "4400"
    #    4 -> "5000"
    # end
    period = ["4000", "1000", "4400", "5000"]
                  |> Enum.at(quarter - 1)

    apply(XbrlDownloader, info_url, [stock_code, period])
    # |> IO.inspect(label: "info_url: ")
    |> HTTPotion.get
    |> handle_response
  end

  defp handle_response(%{status_code: 200} = response) do
    response.body
      |> Poison.decode!()
  end

  defp handle_response(%{} = response) do
    %{
      "error" => response.body
    }
  end
end
