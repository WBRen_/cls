defmodule XbrlDownloader.ProfitInfoExtractor do
  import Aimidas.Xbrl.SH.ProfitInfoFields, only: [fields: 0]
  alias XbrlDownloader.SH.FieldsExtractor

  def process(json) do
    json
    |> FieldsExtractor.process(fields())
  end
end