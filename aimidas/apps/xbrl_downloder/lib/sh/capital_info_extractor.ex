defmodule XbrlDownloader.CapitalInfoExtractor do
  alias XbrlDownloader.SH.FieldsExtractor
  import Aimidas.Xbrl.SH.CapitalInfoFields, only: [fields: 0]

  def process(json) do
    json
    |> FieldsExtractor.process(fields())
  end
end