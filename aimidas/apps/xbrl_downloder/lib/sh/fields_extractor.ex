defmodule XbrlDownloader.SH.FieldsExtractor do
  # alias XbrlDownloader.ConvertHelper

  alias Aimidas.Xbrl.SH.XbrlFieldInfos

  alias Aimidas.Xbrl.InConverter, as: ConvertHelper

  def field_names(fields) do
    fields
    |> Enum.map(fn x -> x |> List.first end)
  end

  def process(json, fields, handlers \\%{}) do
    new_handlers = %{
      row_handler: Map.get(handlers, :row_handler, &reduce_row/2),
      columns_handler: Map.get(handlers, :columns_handler, &process_columns/6)
    }

    process_headers(json)
    |> process_data(json, fields, new_handlers)
  end

  defp process_headers(%{"error" => _} = _json) do
    nil
  end
  defp process_headers(%{"columns" => _} = json) do
    json["columns"]
    |> Enum.at(0)
    |> Enum.reduce(%{}, fn(col, acc) ->
        acc
        |> Map.put(col["title"], %{})
      end)
  end

  defp process_data(nil, _json, _fields, _) do
    %{}
  end

  defp process_data(data, json, fields, handlers) do
    json["rows"]
    |> Enum.reduce({0, data, json, fields, handlers.columns_handler}, handlers.row_handler)
    |> Tuple.to_list()
    |> Enum.at(1)
  end

  defp reduce_row(row, {index, acc_data, json, fields, columns_handler} = params) do
    # params
    # |> IO.inspect(label: "[reduct_row] params: ", pretty: true)

    [field_name, _] =
      fields
      |> Enum.at(index)

    field_converter = XbrlFieldInfos.field_info(field_name).in_converter

    %{
      index: index,
      acc_data: acc_data,
      row: row,
      field_name: field_name,
      field_converter: field_converter,
      json: json
    }
    # |> IO.inspect(label: "reduce_row: ", pretty: true)

    {
      index + 1,
      columns_handler.(acc_data, index, row, field_name, field_converter, json),
      json,
      fields,
      columns_handler
    }
  end

  defp process_columns(data, _index, row, field_name, converter, json) do
    json["columns"]
    |> Enum.at(0)
    |> Enum.reduce(data, fn(column, acc_data) ->
        %{"field" => value_key, "title" => year} = column
        # IO.puts("#{field_name} #{year} - #{value_key}")
        src_value = row[value_key] |> to_string() |> String.trim()
        value =
          apply(ConvertHelper, converter, [ src_value ])
        put_in(acc_data, [year, field_name], value)
      end)
  end
end 
