defmodule XbrlDownloader.CashInfoExtractor do
  import Aimidas.Xbrl.SH.CashInfoFields, only: [fields: 0]
  alias XbrlDownloader.SH.FieldsExtractor, as: Extractor

  def process(json) do
    json
    |> Extractor.process(fields())
  end  
end