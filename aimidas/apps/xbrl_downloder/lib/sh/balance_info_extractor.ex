defmodule XbrlDownloader.BalanceInfoExtractor do
  @moduledoc """
  资产负载表提取器
  """
  
  alias XbrlDownloader.SH.FieldsExtractor
  import Aimidas.Xbrl.SH.BalanceInfoFields, only: [fields: 0]

  def process(json) do
    json
    |> FieldsExtractor.process(fields())
  end
end
