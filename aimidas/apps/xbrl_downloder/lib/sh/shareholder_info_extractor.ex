defmodule XbrlDownloader.SH.ShareholderInfoExtractor do
  import Aimidas.Xbrl.SH.ShareholderInfoFields, only: [fields: 0]
  alias XbrlDownloader.SH.FieldsExtractor

  def process(json) do
    json
    |> FieldsExtractor.process(fields())
    # |> IO.inspect(label: "[ShareholderInfoExtractor.proecess] result -")
    |> fix_rank_order()
    # |> IO.inspect(label: "[ShareholderInfoExtractor.proecess] fixed -")

  end

  defp fix_rank_order(data) do
    data
    |> Enum.into(%{}, fn {year, holders} -> 
      {
        year,
        holders
        |> Enum.into(%{}, fn {key, values} ->
          {
            key,
            Map.put(values, "股东排名", String.to_integer(key))
          }
        end)
      }
    end)
  end
end
