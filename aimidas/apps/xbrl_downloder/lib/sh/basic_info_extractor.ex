defmodule XbrlDownloader.BasicInfoExtractor do
  alias XbrlDownloader.SH.FieldsExtractor

  import Aimidas.Xbrl.SH.BasicInfoFields, only: [fields: 0]

  def process(json) do
    json
    |> FieldsExtractor.process(fields())
  end
end
