defmodule XbrlDownloaderTest do
  use ExUnit.Case, async: true

  use XbrlDownloader.ConnCase

  doctest XbrlDownloader

  @xbrl_year %{
    "基本信息" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value2" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value3" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value4" => "<div style='text-align: left;'>招商银行股份有限公司</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>李建红</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>李建红</div>",
          "value2" => "<div style='text-align: left;'>李建红</div>",
          "value3" => "<div style='text-align: left;'>李建红</div>",
          "value4" => "<div style='text-align: left;'>傅育宁</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
          "value2" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
          "value3" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
          "value4" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>518040</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>518040</div>",
          "value2" => "<div style='text-align: left;'>518040</div>",
          "value3" => "<div style='text-align: left;'>518040</div>",
          "value4" => "<div style='text-align: left;'>518040</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>www.cmbchina.com</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>www.cmbchina.com</div>",
          "value2" => "<div style='text-align: left;'>www.cmbchina.com</div>",
          "value3" => "<div style='text-align: left;'>www.cmbchina.com</div>",
          "value4" => "<div style='text-align: left;'>www.cmbchina.com</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>王良</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>王良</div>",
          "value2" => "<div style='text-align: left;'>许世清</div>",
          "value3" => "<div style='text-align: left;'>许世清</div>",
          "value4" => "-"
        },
        %{
          "value0" => "<div style='text-align: left;'>86755-83198888</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>86755-83198888</div>",
          "value2" => "<div style='text-align: left;'>86755-83198888</div>",
          "value3" => "<div style='text-align: left;'>86755-83198888</div>",
          "value4" => "-"
        },
        %{
          "value0" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
          "value2" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
          "value3" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
          "value4" => "-"
        },
        %{
          "value0" => "227,732",
          "group" => "p",
          "value1" => "214,596",
          "value2" => "266,723",
          "value3" => "442,825",
          "value4" => "522,003"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "8.4",
          "group" => "p",
          "value1" => "7.4",
          "value2" => "6.2",
          "value3" => "0",
          "value4" => "6.2"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "220,897,000,000",
          "group" => "p",
          "value1" => "209,025,000,000",
          "value2" => "201,471,000,000",
          "value3" => "165,863,000,000",
          "value4" => "132,604,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "90,680,000,000",
          "group" => "p",
          "value1" => "78,963,000,000",
          "value2" => "75,079,000,000",
          "value3" => "73,431,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "70,150,000,000",
          "group" => "p",
          "value1" => "62,081,000,000",
          "value2" => "57,696,000,000",
          "value3" => "55,911,000,000",
          "value4" => "51,743,000,000"
        },
        %{
          "value0" => "69,769,000,000",
          "group" => "p",
          "value1" => "61,142,000,000",
          "value2" => "57,045,000,000",
          "value3" => "55,391,000,000",
          "value4" => "51,342,000,000"
        },
        %{
          "value0" => "-5,660,000,000",
          "group" => "p",
          "value1" => "-120,615,000,000",
          "value2" => "400,420,000,000",
          "value3" => "272,173,000,000",
          "value4" => "119,153,000,000"
        },
        %{
          "value0" => "6,297,638,000,000",
          "group" => "p",
          "value1" => "5,942,311,000,000",
          "value2" => "5,474,978,000,000",
          "value3" => "4,731,829,000,000",
          "value4" => "4,016,399,000,000"
        },
        %{
          "value0" => "480,210,000,000",
          "group" => "p",
          "value1" => "402,350,000,000",
          "value2" => "360,806,000,000",
          "value3" => "314,404,000,000",
          "value4" => "265,465,000,000"
        },
        %{
          "value0" => "2.78",
          "group" => "p",
          "value1" => "2.46",
          "value2" => "2.29",
          "value3" => "2.22",
          "value4" => "2.3"
        },
        %{
          "value0" => "2.78",
          "group" => "p",
          "value1" => "2.46",
          "value2" => "2.29",
          "value3" => "2.22",
          "value4" => "2.3"
        },
        %{
          "value0" => "2.77",
          "group" => "p",
          "value1" => "2.42",
          "value2" => "2.26",
          "value3" => "2.2",
          "value4" => "2.28"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "16.54",
          "group" => "p",
          "value1" => "16.27",
          "value2" => "17.09",
          "value3" => "19.28",
          "value4" => "23.12"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "16.45",
          "group" => "p",
          "value1" => "16.02",
          "value2" => "16.9",
          "value3" => "19.1",
          "value4" => "22.94"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        }
      ]
    },
    "股本结构" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "20,628,944,429"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "4,590,901,172"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "25,219,845,601"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "25,219,845,601"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => 0.0
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => 81.8
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => 0.0
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => 18.2
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => 0.0
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => 100.0
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => 100.0
        }
      ]
    },
    "资产负债" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "154,628,000,000",
          "group" => "p",
          "value1" => "200,251,000,000",
          "value2" => "185,693,000,000",
          "value3" => "124,085,000,000",
          "value4" => "148,047,000,000"
        },
        %{
          "value0" => "55,415,000,000",
          "group" => "p",
          "value1" => "43,333,000,000",
          "value2" => "0",
          "value3" => "40,190,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "28,726,000,000",
          "group" => "p",
          "value1" => "26,251,000,000",
          "value2" => "24,934,000,000",
          "value3" => "23,560,000,000",
          "value4" => "17,699,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "455,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "913,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "252,550,000,000",
          "group" => "p",
          "value1" => "278,699,000,000",
          "value2" => "343,924,000,000",
          "value3" => "344,980,000,000",
          "value4" => "318,905,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "3,414,612,000,000",
          "group" => "p",
          "value1" => "3,151,649,000,000",
          "value2" => "2,739,444,000,000",
          "value3" => "2,448,754,000,000",
          "value4" => "2,148,330,000,000"
        },
        %{
          "value0" => "383,101,000,000",
          "group" => "p",
          "value1" => "389,138,000,000",
          "value2" => "299,559,000,000",
          "value3" => "278,526,000,000",
          "value4" => "289,265,000,000"
        },
        %{
          "value0" => "558,218,000,000",
          "group" => "p",
          "value1" => "477,064,000,000",
          "value2" => "353,137,000,000",
          "value3" => "259,434,000,000",
          "value4" => "208,927,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "5,079,000,000",
          "group" => "p",
          "value1" => "3,712,000,000",
          "value2" => "2,786,000,000",
          "value3" => "1,484,000,000",
          "value4" => "1,424,000,000"
        },
        %{
          "value0" => "1,612,000,000",
          "group" => "p",
          "value1" => "1,701,000,000",
          "value2" => "1,708,000,000",
          "value3" => "1,684,000,000",
          "value4" => "1,701,000,000"
        },
        %{
          "value0" => "49,181,000,000",
          "group" => "p",
          "value1" => "43,068,000,000",
          "value2" => "30,813,000,000",
          "value3" => "26,504,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "7,255,000,000",
          "group" => "p",
          "value1" => "3,914,000,000",
          "value2" => "3,595,000,000",
          "value3" => "3,292,000,000",
          "value4" => "2,996,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "9,954,000,000",
          "group" => "p",
          "value1" => "9,954,000,000",
          "value2" => "9,954,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "50,120,000,000",
          "group" => "p",
          "value1" => "31,010,000,000",
          "value2" => "16,020,000,000",
          "value3" => "10,291,000,000",
          "value4" => "8,064,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "6,297,638,000,000",
          "group" => "p",
          "value1" => "5,942,311,000,000",
          "value2" => "5,474,978,000,000",
          "value3" => "4,731,829,000,000",
          "value4" => "4,016,399,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "414,838,000,000",
          "group" => "p",
          "value1" => "330,108,000,000",
          "value2" => "62,600,000,000",
          "value3" => "20,000,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "272,734,000,000",
          "group" => "p",
          "value1" => "248,876,000,000",
          "value2" => "178,771,000,000",
          "value3" => "94,603,000,000",
          "value4" => "125,132,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "1,007,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "125,620,000,000",
          "group" => "p",
          "value1" => "162,942,000,000",
          "value2" => "185,652,000,000",
          "value3" => "66,988,000,000",
          "value4" => "153,164,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "8,020,000,000",
          "group" => "p",
          "value1" => "7,048,000,000",
          "value2" => "6,486,000,000",
          "value3" => "6,068,000,000",
          "value4" => "5,119,000,000"
        },
        %{
          "value0" => "26,701,000,000",
          "group" => "p",
          "value1" => "19,523,000,000",
          "value2" => "12,820,000,000",
          "value3" => "11,656,000,000",
          "value4" => "8,722,000,000"
        },
        %{
          "value0" => "36,501,000,000",
          "group" => "p",
          "value1" => "36,246,000,000",
          "value2" => "39,073,000,000",
          "value3" => "45,349,000,000",
          "value4" => "30,988,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "7,001,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "42,207,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "116,000,000",
          "value4" => "21,218,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "296,477,000,000",
          "group" => "p",
          "value1" => "275,082,000,000",
          "value2" => "251,507,000,000",
          "value3" => "106,155,000,000",
          "value4" => "68,936,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "1,070,000,000",
          "group" => "p",
          "value1" => "897,000,000",
          "value2" => "867,000,000",
          "value3" => "771,000,000",
          "value4" => "770,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "5,814,246,000,000",
          "group" => "p",
          "value1" => "5,538,949,000,000",
          "value2" => "5,113,220,000,000",
          "value3" => "4,416,769,000,000",
          "value4" => "3,750,443,000,000"
        },
        %{
          "value0" => "25,220,000,000",
          "group" => "p",
          "value1" => "25,220,000,000",
          "value2" => "25,220,000,000",
          "value3" => "25,220,000,000",
          "value4" => "25,220,000,000"
        },
        %{
          "value0" => "67,523,000,000",
          "group" => "p",
          "value1" => "67,523,000,000",
          "value2" => "67,523,000,000",
          "value3" => "67,523,000,000",
          "value4" => "61,976,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "46,159,000,000",
          "group" => "p",
          "value1" => "39,708,000,000",
          "value2" => "34,009,000,000",
          "value3" => "28,690,000,000",
          "value4" => "23,502,000,000"
        },
        %{
          "value0" => "70,921,000,000",
          "group" => "p",
          "value1" => "67,838,000,000",
          "value2" => "64,679,000,000",
          "value3" => "53,979,000,000",
          "value4" => "46,347,000,000"
        },
        %{
          "value0" => "241,063,000,000",
          "group" => "p",
          "value1" => "199,110,000,000",
          "value2" => "163,289,000,000",
          "value3" => "138,562,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "-1,736,000,000"
        },
        %{
          "value0" => "480,210,000,000",
          "group" => "p",
          "value1" => "402,350,000,000",
          "value2" => "360,806,000,000",
          "value3" => "314,404,000,000",
          "value4" => "265,465,000,000"
        },
        %{
          "value0" => "3,182,000,000",
          "group" => "p",
          "value1" => "1,012,000,000",
          "value2" => "952,000,000",
          "value3" => "656,000,000",
          "value4" => "491,000,000"
        },
        %{
          "value0" => "483,392,000,000",
          "group" => "p",
          "value1" => "403,362,000,000",
          "value2" => "361,758,000,000",
          "value3" => "315,060,000,000",
          "value4" => "265,956,000,000"
        },
        %{
          "value0" => "6,297,638,000,000",
          "group" => "p",
          "value1" => "5,942,311,000,000",
          "value2" => "5,474,978,000,000",
          "value3" => "4,731,829,000,000",
          "value4" => "4,016,399,000,000"
        }
      ]
    },
    "利润" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "220,897,000,000",
          "group" => "p",
          "value1" => "209,025,000,000",
          "value2" => "201,471,000,000",
          "value3" => "165,863,000,000",
          "value4" => "132,604,000,000"
        },
        %{
          "value0" => "242,005,000,000",
          "group" => "p",
          "value1" => "215,481,000,000",
          "value2" => "234,722,000,000",
          "value3" => "222,834,000,000",
          "value4" => "173,495,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "69,908,000,000",
          "group" => "p",
          "value1" => "66,003,000,000",
          "value2" => "57,798,000,000",
          "value3" => "48,543,000,000",
          "value4" => "31,365,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "130,357,000,000",
          "group" => "p",
          "value1" => "131,307,000,000",
          "value2" => "127,223,000,000",
          "value3" => "93,094,000,000",
          "value4" => "64,693,000,000"
        },
        %{
          "value0" => "97,153,000,000",
          "group" => "p",
          "value1" => "80,886,000,000",
          "value2" => "97,993,000,000",
          "value3" => "110,834,000,000",
          "value4" => "74,582,000,000"
        },
        %{
          "value0" => "5,890,000,000",
          "group" => "p",
          "value1" => "5,138,000,000",
          "value2" => "4,379,000,000",
          "value3" => "3,847,000,000",
          "value4" => "2,181,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "2,152,000,000",
          "group" => "p",
          "value1" => "6,362,000,000",
          "value2" => "11,929,000,000",
          "value3" => "10,425,000,000",
          "value4" => "8,579,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "59,926,000,000",
          "group" => "p",
          "value1" => "66,159,000,000",
          "value2" => "59,266,000,000",
          "value3" => "31,681,000,000",
          "value4" => "10,218,000,000"
        },
        %{
          "value0" => "375,000,000",
          "group" => "p",
          "value1" => "-2,511,000,000",
          "value2" => "1,316,000,000",
          "value3" => "308,000,000",
          "value4" => "-575,000,000"
        },
        %{
          "value0" => "6,205,000,000",
          "group" => "p",
          "value1" => "11,953,000,000",
          "value2" => "7,127,000,000",
          "value3" => "5,762,000,000",
          "value4" => "3,615,000,000"
        },
        %{
          "value0" => "998,000,000",
          "group" => "p",
          "value1" => "321,000,000",
          "value2" => "136,000,000",
          "value3" => "158,000,000",
          "value4" => "88,000,000"
        },
        %{
          "value0" => "1,934,000,000",
          "group" => "p",
          "value1" => "2,857,000,000",
          "value2" => "2,398,000,000",
          "value3" => "2,467,000,000",
          "value4" => "891,000,000"
        },
        %{
          "value0" => "90,540,000,000",
          "group" => "p",
          "value1" => "77,718,000,000",
          "value2" => "74,248,000,000",
          "value3" => "72,769,000,000",
          "value4" => "67,911,000,000"
        },
        %{
          "value0" => "343,000,000",
          "group" => "p",
          "value1" => "1,386,000,000",
          "value2" => "970,000,000",
          "value3" => "810,000,000",
          "value4" => "631,000,000"
        },
        %{
          "value0" => "203,000,000",
          "group" => "p",
          "value1" => "141,000,000",
          "value2" => "139,000,000",
          "value3" => "148,000,000",
          "value4" => "117,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "90,680,000,000",
          "group" => "p",
          "value1" => "78,963,000,000",
          "value2" => "75,079,000,000",
          "value3" => "73,431,000,000",
          "value4" => "68,425,000,000"
        },
        %{
          "value0" => "20,042,000,000",
          "group" => "p",
          "value1" => "16,583,000,000",
          "value2" => "17,061,000,000",
          "value3" => "17,382,000,000",
          "value4" => "16,683,000,000"
        },
        %{
          "value0" => "70,638,000,000",
          "group" => "p",
          "value1" => "62,380,000,000",
          "value2" => "58,018,000,000",
          "value3" => "56,049,000,000",
          "value4" => "51,742,000,000"
        },
        %{
          "value0" => "70,150,000,000",
          "group" => "p",
          "value1" => "62,081,000,000",
          "value2" => "57,696,000,000",
          "value3" => "55,911,000,000",
          "value4" => "51,743,000,000"
        },
        %{
          "value0" => "488,000,000",
          "group" => "p",
          "value1" => "299,000,000",
          "value2" => "322,000,000",
          "value3" => "138,000,000",
          "value4" => "-1,000,000"
        },
        %{
          "value0" => "2.78",
          "group" => "p",
          "value1" => "2.46",
          "value2" => "2.29",
          "value3" => "2.22",
          "value4" => "2.3"
        },
        %{
          "value0" => "2.78",
          "group" => "p",
          "value1" => "2.46",
          "value2" => "2.29",
          "value3" => "2.22",
          "value4" => "2.3"
        }
      ]
    },
    "现金流量" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "262,296,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "84,730,000,000",
          "group" => "p",
          "value1" => "230,351,000,000",
          "value2" => "281,373,000,000",
          "value3" => "712,428,000,000",
          "value4" => "242,832,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "267,508,000,000",
          "value2" => "42,600,000,000",
          "value3" => "20,000,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "255,490,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "257,953,000,000",
          "group" => "p",
          "value1" => "234,993,000,000",
          "value2" => "244,743,000,000",
          "value3" => "230,977,000,000",
          "value4" => "181,159,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "47,395,000,000",
          "value2" => "202,832,000,000",
          "value3" => "24,714,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "41,941,000,000",
          "group" => "p",
          "value1" => "18,229,000,000",
          "value2" => "27,140,000,000",
          "value3" => "8,501,000,000",
          "value4" => "29,472,000,000"
        },
        %{
          "value0" => "677,517,000,000",
          "group" => "p",
          "value1" => "798,476,000,000",
          "value2" => "962,603,000,000",
          "value3" => "996,815,000,000",
          "value4" => "722,035,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "322,105,000,000",
          "group" => "p",
          "value1" => "470,444,000,000",
          "value2" => "347,286,000,000",
          "value3" => "331,091,000,000",
          "value4" => "294,773,000,000"
        },
        %{
          "value0" => "25,205,000,000",
          "group" => "p",
          "value1" => "62,488,000,000",
          "value2" => "0",
          "value3" => "59,267,000,000",
          "value4" => "34,993,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "89,759,000,000",
          "group" => "p",
          "value1" => "78,941,000,000",
          "value2" => "101,447,000,000",
          "value3" => "97,185,000,000",
          "value4" => "66,201,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "34,540,000,000",
          "group" => "p",
          "value1" => "32,287,000,000",
          "value2" => "26,638,000,000",
          "value3" => "29,330,000,000",
          "value4" => "24,216,000,000"
        },
        %{
          "value0" => "44,617,000,000",
          "group" => "p",
          "value1" => "41,626,000,000",
          "value2" => "34,765,000,000",
          "value3" => "31,452,000,000",
          "value4" => "24,673,000,000"
        },
        %{
          "value0" => "36,998,000,000",
          "group" => "p",
          "value1" => "47,774,000,000",
          "value2" => "52,047,000,000",
          "value3" => "59,612,000,000",
          "value4" => "23,650,000,000"
        },
        %{
          "value0" => "683,177,000,000",
          "group" => "p",
          "value1" => "919,091,000,000",
          "value2" => "562,183,000,000",
          "value3" => "724,642,000,000",
          "value4" => "602,882,000,000"
        },
        %{
          "value0" => "-5,660,000,000",
          "group" => "p",
          "value1" => "-120,615,000,000",
          "value2" => "400,420,000,000",
          "value3" => "272,173,000,000",
          "value4" => "119,153,000,000"
        },
        %{
          "value0" => "803,283,000,000",
          "group" => "p",
          "value1" => "765,069,000,000",
          "value2" => "451,491,000,000",
          "value3" => "579,100,000,000",
          "value4" => "552,287,000,000"
        },
        %{
          "value0" => "52,205,000,000",
          "group" => "p",
          "value1" => "60,509,000,000",
          "value2" => "51,407,000,000",
          "value3" => "39,675,000,000",
          "value4" => "21,849,000,000"
        },
        %{
          "value0" => "191,000,000",
          "group" => "p",
          "value1" => "561,000,000",
          "value2" => "167,000,000",
          "value3" => "1,297,000,000",
          "value4" => "405,000,000"
        },
        %{
          "value0" => "67,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "2,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "5,000,000",
          "value2" => "0",
          "value3" => "620,074,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "855,746,000,000",
          "group" => "p",
          "value1" => "826,144,000,000",
          "value2" => "503,067,000,000",
          "value3" => "620,074,000,000",
          "value4" => "574,543,000,000"
        },
        %{
          "value0" => "16,336,000,000",
          "group" => "p",
          "value1" => "17,504,000,000",
          "value2" => "9,079,000,000",
          "value3" => "8,125,000,000",
          "value4" => "8,211,000,000"
        },
        %{
          "value0" => "923,275,000,000",
          "group" => "p",
          "value1" => "794,146,000,000",
          "value2" => "865,591,000,000",
          "value3" => "787,928,000,000",
          "value4" => "798,001,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "606,000,000",
          "group" => "p",
          "value1" => "774,000,000",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "-796,053,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "940,217,000,000",
          "group" => "p",
          "value1" => "812,424,000,000",
          "value2" => "874,670,000,000",
          "value3" => "796,053,000,000",
          "value4" => "806,212,000,000"
        },
        %{
          "value0" => "-84,471,000,000",
          "group" => "p",
          "value1" => "13,720,000,000",
          "value2" => "-371,603,000,000",
          "value3" => "-175,979,000,000",
          "value4" => "-231,669,000,000"
        },
        %{
          "value0" => "495,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "83,000,000",
          "value3" => "84,000,000",
          "value4" => "81,000,000"
        },
        %{
          "value0" => "495,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "83,000,000",
          "value3" => "84,000,000",
          "value4" => "81,000,000"
        },
        %{
          "value0" => "19,086,000,000",
          "group" => "p",
          "value1" => "14,740,000,000",
          "value2" => "23,105,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "52,449,000,000",
          "group" => "p",
          "value1" => "12,432,000,000",
          "value2" => "200,000,000",
          "value3" => "20,471,000,000",
          "value4" => "4,000,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "74,087,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "667,060,000,000",
          "group" => "p",
          "value1" => "217,972,000,000",
          "value2" => "317,301,000,000",
          "value3" => "74,087,000,000",
          "value4" => "69,827,000,000"
        },
        %{
          "value0" => "30,186,000,000",
          "group" => "p",
          "value1" => "5,227,000,000",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "33,175,000,000",
          "group" => "p",
          "value1" => "29,976,000,000",
          "value2" => "16,925,000,000",
          "value3" => "0",
          "value4" => "13,593,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "-52,208,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "644,397,000,000",
          "group" => "p",
          "value1" => "221,968,000,000",
          "value2" => "192,416,000,000",
          "value3" => "52,208,000,000",
          "value4" => "58,815,000,000"
        },
        %{
          "value0" => "22,663,000,000",
          "group" => "p",
          "value1" => "-3,996,000,000",
          "value2" => "124,885,000,000",
          "value3" => "21,879,000,000",
          "value4" => "11,012,000,000"
        },
        %{
          "value0" => "-4,219,000,000",
          "group" => "p",
          "value1" => "7,160,000,000",
          "value2" => "10,670,000,000",
          "value3" => "3,449,000,000",
          "value4" => "-1,402,000,000"
        },
        %{
          "value0" => "-71,687,000,000",
          "group" => "p",
          "value1" => "-103,731,000,000",
          "value2" => "164,372,000,000",
          "value3" => "121,522,000,000",
          "value4" => "-102,906,000,000"
        },
        %{
          "value0" => "460,425,000,000",
          "group" => "p",
          "value1" => "532,112,000,000",
          "value2" => "635,843,000,000",
          "value3" => "471,471,000,000",
          "value4" => "349,949,000,000"
        }
      ]
    }
  }
  @xbrl_season1 %{
    "基本信息" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2018"
          },
          %{
            "field" => "value1",
            "title" => "2017"
          },
          %{
            "field" => "value2",
            "title" => "2016"
          },
          %{
            "field" => "value3",
            "title" => "2015"
          },
          %{
            "field" => "value4",
            "title" => "2014"
          }
        ]
      ],
      "tip" => "数据取自：招商银行一季报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value2" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "<div style='text-align: left;'>李建红</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>李建红</div>",
          "value2" => "<div style='text-align: left;'>李建红</div>",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "254,334",
          "group" => "p",
          "value1" => "205,206",
          "value2" => "262,669",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "61,296,000,000",
          "group" => "p",
          "value1" => "57,075,000,000",
          "value2" => "58,252,000,000",
          "value3" => "50,747,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "29,286,000,000",
          "group" => "p",
          "value1" => "25,502,000,000",
          "value2" => "23,796,000,000",
          "value3" => "22,696,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "22,674,000,000",
          "group" => "p",
          "value1" => "19,977,000,000",
          "value2" => "18,350,000,000",
          "value3" => "17,220,000,000",
          "value4" => "14,945,000,000"
        },
        %{
          "value0" => "22,546,000,000",
          "group" => "p",
          "value1" => "19,836,000,000",
          "value2" => "18,272,000,000",
          "value3" => "17,148,000,000",
          "value4" => "14,863,000,000"
        },
        %{
          "value0" => "-53,651,000,000",
          "group" => "p",
          "value1" => "-214,350,000,000",
          "value2" => "-287,039,000,000",
          "value3" => "28,774,000,000",
          "value4" => "56,649,000,000"
        },
        %{
          "value0" => "6,252,238,000,000",
          "group" => "p",
          "value1" => "6,000,674,000,000",
          "value2" => "5,432,042,000,000",
          "value3" => "4,908,944,000,000",
          "value4" => "4,404,549,000,000"
        },
        %{
          "value0" => "496,146,000,000",
          "group" => "p",
          "value1" => "420,634,000,000",
          "value2" => "379,140,000,000",
          "value3" => "331,315,000,000",
          "value4" => "283,335,000,000"
        },
        %{
          "value0" => "0.9",
          "group" => "p",
          "value1" => "0.79",
          "value2" => "0.73",
          "value3" => "0.68",
          "value4" => "0.59"
        },
        %{
          "value0" => "0.9",
          "group" => "p",
          "value1" => "0.79",
          "value2" => "0.73",
          "value3" => "0.68",
          "value4" => "0.59"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "19.97",
          "group" => "p",
          "value1" => "19.42",
          "value2" => "19.84",
          "value3" => "21.33",
          "value4" => "21.79"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        }
      ]
    },
    "资产负债" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2018"
          },
          %{
            "field" => "value1",
            "title" => "2017"
          },
          %{
            "field" => "value2",
            "title" => "2016"
          },
          %{
            "field" => "value3",
            "title" => "2015"
          },
          %{
            "field" => "value4",
            "title" => "2014"
          }
        ]
      ],
      "tip" => "数据取自：招商银行一季报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "150,750,000,000",
          "group" => "p",
          "value1" => "176,104,000,000",
          "value2" => "152,117,000,000",
          "value3" => "174,666,000,000",
          "value4" => "139,534,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "31,252,000,000",
          "group" => "p",
          "value1" => "28,524,000,000",
          "value2" => "26,994,000,000",
          "value3" => "26,268,000,000",
          "value4" => "21,429,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "203,646,000,000",
          "group" => "p",
          "value1" => "114,398,000,000",
          "value2" => "186,510,000,000",
          "value3" => "267,908,000,000",
          "value4" => "394,225,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "3,553,672,000,000",
          "group" => "p",
          "value1" => "3,308,271,000,000",
          "value2" => "2,835,496,000,000",
          "value3" => "2,549,815,000,000",
          "value4" => "2,295,925,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "395,667,000,000",
          "value2" => "319,865,000,000",
          "value3" => "298,190,000,000",
          "value4" => "290,853,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "486,687,000,000",
          "value2" => "389,766,000,000",
          "value3" => "266,610,000,000",
          "value4" => "215,969,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "5,307,000,000",
          "group" => "p",
          "value1" => "3,915,000,000",
          "value2" => "2,793,000,000",
          "value3" => "2,542,000,000",
          "value4" => "1,483,000,000"
        },
        %{
          "value0" => "1,496,000,000",
          "group" => "p",
          "value1" => "1,655,000,000",
          "value2" => "1,737,000,000",
          "value3" => "1,646,000,000",
          "value4" => "1,705,000,000"
        },
        %{
          "value0" => "48,315,000,000",
          "group" => "p",
          "value1" => "44,239,000,000",
          "value2" => "30,835,000,000",
          "value3" => "28,495,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "10,171,000,000",
          "group" => "p",
          "value1" => "4,024,000,000",
          "value2" => "3,608,000,000",
          "value3" => "3,289,000,000",
          "value4" => "3,191,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "9,954,000,000",
          "group" => "p",
          "value1" => "9,954,000,000",
          "value2" => "9,954,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "53,558,000,000",
          "group" => "p",
          "value1" => "35,145,000,000",
          "value2" => "20,169,000,000",
          "value3" => "10,423,000,000",
          "value4" => "7,329,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "6,252,238,000,000",
          "group" => "p",
          "value1" => "6,000,674,000,000",
          "value2" => "5,432,042,000,000",
          "value3" => "4,908,944,000,000",
          "value4" => "4,404,549,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "367,471,000,000",
          "group" => "p",
          "value1" => "274,978,000,000",
          "value2" => "101,600,000,000",
          "value3" => "32,500,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "172,672,000,000",
          "group" => "p",
          "value1" => "280,124,000,000",
          "value2" => "110,669,000,000",
          "value3" => "96,370,000,000",
          "value4" => "163,842,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "210,732,000,000",
          "group" => "p",
          "value1" => "203,742,000,000",
          "value2" => "117,371,000,000",
          "value3" => "114,509,000,000",
          "value4" => "43,027,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "6,913,000,000",
          "group" => "p",
          "value1" => "5,377,000,000",
          "value2" => "6,135,000,000",
          "value3" => "6,514,000,000",
          "value4" => "6,984,000,000"
        },
        %{
          "value0" => "29,480,000,000",
          "group" => "p",
          "value1" => "27,870,000,000",
          "value2" => "21,314,000,000",
          "value3" => "15,327,000,000",
          "value4" => "10,213,000,000"
        },
        %{
          "value0" => "36,574,000,000",
          "group" => "p",
          "value1" => "34,500,000,000",
          "value2" => "37,818,000,000",
          "value3" => "41,890,000,000",
          "value4" => "33,746,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "279,462,000,000",
          "group" => "p",
          "value1" => "355,577,000,000",
          "value2" => "353,156,000,000",
          "value3" => "161,569,000,000",
          "value4" => "82,216,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "1,038,000,000",
          "group" => "p",
          "value1" => "910,000,000",
          "value2" => "822,000,000",
          "value3" => "760,000,000",
          "value4" => "784,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "5,752,755,000,000",
          "group" => "p",
          "value1" => "5,578,850,000,000",
          "value2" => "5,051,991,000,000",
          "value3" => "4,576,829,000,000",
          "value4" => "4,120,707,000,000"
        },
        %{
          "value0" => "25,220,000,000",
          "group" => "p",
          "value1" => "25,220,000,000",
          "value2" => "25,220,000,000",
          "value3" => "25,220,000,000",
          "value4" => "25,220,000,000"
        },
        %{
          "value0" => "67,523,000,000",
          "group" => "p",
          "value1" => "67,523,000,000",
          "value2" => "67,523,000,000",
          "value3" => "67,523,000,000",
          "value4" => "64,461,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "46,159,000,000",
          "group" => "p",
          "value1" => "39,708,000,000",
          "value2" => "34,009,000,000",
          "value3" => "28,690,000,000",
          "value4" => "23,502,000,000"
        },
        %{
          "value0" => "70,751,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "64,666,000,000",
          "value3" => "54,043,000,000",
          "value4" => "46,445,000,000"
        },
        %{
          "value0" => "254,999,000,000",
          "group" => "p",
          "value1" => "218,968,000,000",
          "value2" => "181,652,000,000",
          "value3" => "155,718,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "-1,496,000,000"
        },
        %{
          "value0" => "496,146,000,000",
          "group" => "p",
          "value1" => "420,634,000,000",
          "value2" => "379,140,000,000",
          "value3" => "331,315,000,000",
          "value4" => "283,335,000,000"
        },
        %{
          "value0" => "3,337,000,000",
          "group" => "p",
          "value1" => "1,190,000,000",
          "value2" => "911,000,000",
          "value3" => "800,000,000",
          "value4" => "507,000,000"
        },
        %{
          "value0" => "499,483,000,000",
          "group" => "p",
          "value1" => "421,824,000,000",
          "value2" => "380,051,000,000",
          "value3" => "332,115,000,000",
          "value4" => "283,842,000,000"
        },
        %{
          "value0" => "6,252,238,000,000",
          "group" => "p",
          "value1" => "6,000,674,000,000",
          "value2" => "5,432,042,000,000",
          "value3" => "4,908,944,000,000",
          "value4" => "4,404,549,000,000"
        }
      ]
    },
    "利润" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2018"
          },
          %{
            "field" => "value1",
            "title" => "2017"
          },
          %{
            "field" => "value2",
            "title" => "2016"
          },
          %{
            "field" => "value3",
            "title" => "2015"
          },
          %{
            "field" => "value4",
            "title" => "2014"
          }
        ]
      ],
      "tip" => "数据取自：招商银行一季报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "61,296,000,000",
          "group" => "p",
          "value1" => "57,075,000,000",
          "value2" => "58,252,000,000",
          "value3" => "50,747,000,000",
          "value4" => "40,871,000,000"
        },
        %{
          "value0" => "64,589,000,000",
          "group" => "p",
          "value1" => "57,060,000,000",
          "value2" => "55,881,000,000",
          "value3" => "59,719,000,000",
          "value4" => "50,491,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "20,363,000,000",
          "group" => "p",
          "value1" => "20,001,000,000",
          "value2" => "20,866,000,000",
          "value3" => "16,654,000,000",
          "value4" => "12,404,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "32,024,000,000",
          "group" => "p",
          "value1" => "31,751,000,000",
          "value2" => "34,557,000,000",
          "value3" => "28,146,000,000",
          "value4" => "21,218,000,000"
        },
        %{
          "value0" => "26,650,000,000",
          "group" => "p",
          "value1" => "22,146,000,000",
          "value2" => "21,575,000,000",
          "value3" => "26,330,000,000",
          "value4" => "24,308,000,000"
        },
        %{
          "value0" => "1,442,000,000",
          "group" => "p",
          "value1" => "1,361,000,000",
          "value2" => "1,042,000,000",
          "value3" => "1,021,000,000",
          "value4" => "793,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "441,000,000",
          "group" => "p",
          "value1" => "505,000,000",
          "value2" => "3,347,000,000",
          "value3" => "3,075,000,000",
          "value4" => "2,556,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "14,759,000,000",
          "group" => "p",
          "value1" => "17,315,000,000",
          "value2" => "18,516,000,000",
          "value3" => "12,744,000,000",
          "value4" => "7,428,000,000"
        },
        %{
          "value0" => "1,015,000,000",
          "group" => "p",
          "value1" => "-15,000,000",
          "value2" => "-1,292,000,000",
          "value3" => "-245,000,000",
          "value4" => "-158,000,000"
        },
        %{
          "value0" => "2,764,000,000",
          "group" => "p",
          "value1" => "1,881,000,000",
          "value2" => "4,598,000,000",
          "value3" => "1,712,000,000",
          "value4" => "1,900,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "-386,000,000",
          "group" => "p",
          "value1" => "1,073,000,000",
          "value2" => "493,000,000",
          "value3" => "112,000,000",
          "value4" => "1,180,000,000"
        },
        %{
          "value0" => "29,272,000,000",
          "group" => "p",
          "value1" => "25,324,000,000",
          "value2" => "23,695,000,000",
          "value3" => "22,601,000,000",
          "value4" => "19,653,000,000"
        },
        %{
          "value0" => "32,000,000",
          "group" => "p",
          "value1" => "190,000,000",
          "value2" => "115,000,000",
          "value3" => "112,000,000",
          "value4" => "126,000,000"
        },
        %{
          "value0" => "18,000,000",
          "group" => "p",
          "value1" => "12,000,000",
          "value2" => "14,000,000",
          "value3" => "17,000,000",
          "value4" => "18,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "29,286,000,000",
          "group" => "p",
          "value1" => "25,502,000,000",
          "value2" => "23,796,000,000",
          "value3" => "22,696,000,000",
          "value4" => "19,761,000,000"
        },
        %{
          "value0" => "6,578,000,000",
          "group" => "p",
          "value1" => "5,410,000,000",
          "value2" => "5,377,000,000",
          "value3" => "5,393,000,000",
          "value4" => "4,794,000,000"
        },
        %{
          "value0" => "22,708,000,000",
          "group" => "p",
          "value1" => "20,092,000,000",
          "value2" => "18,419,000,000",
          "value3" => "17,303,000,000",
          "value4" => "14,967,000,000"
        },
        %{
          "value0" => "22,674,000,000",
          "group" => "p",
          "value1" => "19,977,000,000",
          "value2" => "18,350,000,000",
          "value3" => "17,220,000,000",
          "value4" => "14,945,000,000"
        },
        %{
          "value0" => "34,000,000",
          "group" => "p",
          "value1" => "115,000,000",
          "value2" => "69,000,000",
          "value3" => "83,000,000",
          "value4" => "22,000,000"
        },
        %{
          "value0" => "0.9",
          "group" => "p",
          "value1" => "0.79",
          "value2" => "0.73",
          "value3" => "0.68",
          "value4" => "0.59"
        },
        %{
          "value0" => "0.9",
          "group" => "p",
          "value1" => "0.79",
          "value2" => "0.73",
          "value3" => "0.68",
          "value4" => "0.59"
        }
      ]
    },
    "现金流量" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2018"
          },
          %{
            "field" => "value1",
            "title" => "2017"
          },
          %{
            "field" => "value2",
            "title" => "2016"
          },
          %{
            "field" => "value3",
            "title" => "2015"
          },
          %{
            "field" => "value4",
            "title" => "2014"
          }
        ]
      ],
      "tip" => "数据取自：招商银行一季报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "39,000,000,000",
          "value3" => "12,500,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "80,242,000,000",
          "group" => "p",
          "value1" => "65,965,000,000",
          "value2" => "72,967,000,000",
          "value3" => "73,403,000,000",
          "value4" => "56,145,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "86,489,000,000",
          "group" => "p",
          "value1" => "25,380,000,000",
          "value2" => "3,207,000,000",
          "value3" => "4,646,000,000",
          "value4" => "1,255,000,000"
        },
        %{
          "value0" => "229,117,000,000",
          "group" => "p",
          "value1" => "310,228,000,000",
          "value2" => "149,378,000,000",
          "value3" => "201,044,000,000",
          "value4" => "479,952,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "151,548,000,000",
          "group" => "p",
          "value1" => "175,236,000,000",
          "value2" => "114,414,000,000",
          "value3" => "108,817,000,000",
          "value4" => "162,088,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "25,660,000,000",
          "group" => "p",
          "value1" => "30,162,000,000",
          "value2" => "22,574,000,000",
          "value3" => "30,330,000,000",
          "value4" => "20,988,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "11,208,000,000",
          "group" => "p",
          "value1" => "9,901,000,000",
          "value2" => "7,950,000,000",
          "value3" => "7,281,000,000",
          "value4" => "5,540,000,000"
        },
        %{
          "value0" => "8,025,000,000",
          "group" => "p",
          "value1" => "5,406,000,000",
          "value2" => "4,729,000,000",
          "value3" => "6,349,000,000",
          "value4" => "5,938,000,000"
        },
        %{
          "value0" => "7,856,000,000",
          "group" => "p",
          "value1" => "8,319,000,000",
          "value2" => "5,355,000,000",
          "value3" => "4,019,000,000",
          "value4" => "4,235,000,000"
        },
        %{
          "value0" => "282,768,000,000",
          "group" => "p",
          "value1" => "524,578,000,000",
          "value2" => "436,417,000,000",
          "value3" => "172,270,000,000",
          "value4" => "423,303,000,000"
        },
        %{
          "value0" => "-53,651,000,000",
          "group" => "p",
          "value1" => "-214,350,000,000",
          "value2" => "-287,039,000,000",
          "value3" => "28,774,000,000",
          "value4" => "56,649,000,000"
        },
        %{
          "value0" => "160,985,000,000",
          "group" => "p",
          "value1" => "144,029,000,000",
          "value2" => "168,485,000,000",
          "value3" => "458,665,000,000",
          "value4" => "345,279,000,000"
        },
        %{
          "value0" => "12,369,000,000",
          "group" => "p",
          "value1" => "13,662,000,000",
          "value2" => "8,336,000,000",
          "value3" => "10,485,000,000",
          "value4" => "7,959,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "16,000,000",
          "value3" => "8,000,000",
          "value4" => "2,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "173,575,000,000",
          "group" => "p",
          "value1" => "157,693,000,000",
          "value2" => "176,842,000,000",
          "value3" => "469,158,000,000",
          "value4" => "353,240,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "1,820,000,000",
          "value2" => "212,000,000",
          "value3" => "871,000,000",
          "value4" => "375,000,000"
        },
        %{
          "value0" => "170,642,000,000",
          "group" => "p",
          "value1" => "251,173,000,000",
          "value2" => "207,445,000,000",
          "value3" => "639,058,000,000",
          "value4" => "458,636,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "175,846,000,000",
          "group" => "p",
          "value1" => "252,993,000,000",
          "value2" => "207,657,000,000",
          "value3" => "640,929,000,000",
          "value4" => "459,011,000,000"
        },
        %{
          "value0" => "-2,271,000,000",
          "group" => "p",
          "value1" => "-95,300,000,000",
          "value2" => "-30,815,000,000",
          "value3" => "-171,771,000,000",
          "value4" => "-105,771,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "13,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "3,966,000,000",
          "group" => "p",
          "value1" => "8,350,000,000",
          "value2" => "3,800,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "115,995,000,000",
          "group" => "p",
          "value1" => "196,190,000,000",
          "value2" => "206,172,000,000",
          "value3" => "69,300,000,000",
          "value4" => "21,307,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "134,624,000,000",
          "group" => "p",
          "value1" => "117,042,000,000",
          "value2" => "104,268,000,000",
          "value3" => "12,094,000,000",
          "value4" => "8,841,000,000"
        },
        %{
          "value0" => "-18,629,000,000",
          "group" => "p",
          "value1" => "79,148,000,000",
          "value2" => "101,904,000,000",
          "value3" => "57,206,000,000",
          "value4" => "12,466,000,000"
        },
        %{
          "value0" => "2,107,000,000",
          "group" => "p",
          "value1" => "-421,000,000",
          "value2" => "-431,000,000",
          "value3" => "344,000,000",
          "value4" => "394,000,000"
        },
        %{
          "value0" => "-72,444,000,000",
          "group" => "p",
          "value1" => "-230,923,000,000",
          "value2" => "-216,381,000,000",
          "value3" => "-85,447,000,000",
          "value4" => "-36,262,000,000"
        },
        %{
          "value0" => "387,981,000,000",
          "group" => "p",
          "value1" => "301,189,000,000",
          "value2" => "419,462,000,000",
          "value3" => "386,024,000,000",
          "value4" => "313,687,000,000"
        }
      ]
    }
  }
  @xbrl_season2 %{
    "基本信息" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行半年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value2" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value3" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value4" => "<div style='text-align: left;'>招商银行股份有限公司</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>李建红</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>李建红</div>",
          "value2" => "<div style='text-align: left;'>李建红</div>",
          "value3" => "<div style='text-align: left;'>李建红</div>",
          "value4" => "<div style='text-align: left;'>傅育宁</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
          "value2" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
          "value3" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
          "value4" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>518040</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>518040</div>",
          "value2" => "<div style='text-align: left;'>518040</div>",
          "value3" => "<div style='text-align: left;'>518040</div>",
          "value4" => "<div style='text-align: left;'>518040</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>www.cmbchina.com</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>www.cmbchina.com</div>",
          "value2" => "<div style='text-align: left;'>www.cmbchina.com</div>",
          "value3" => "<div style='text-align: left;'>www.cmbchina.com</div>",
          "value4" => "<div style='text-align: left;'>www.cmbchina.com</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>王良</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>王良</div>",
          "value2" => "<div style='text-align: left;'>许世清</div>",
          "value3" => "-",
          "value4" => "<div style='text-align: left;'>许世清（任职资格尚待核准）</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>86755-83198888</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>86755-83198888</div>",
          "value2" => "<div style='text-align: left;'>86755-83198888</div>",
          "value3" => "-",
          "value4" => "<div style='text-align: left;'>0755-8319 8888</div>"
        },
        %{
          "value0" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
          "value2" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
          "value3" => "-",
          "value4" => "<div style='text-align: left;'>cmb@cmbchina.com</div>"
        },
        %{
          "value0" => "207,524",
          "group" => "p",
          "value1" => "233,862",
          "value2" => "426,896",
          "value3" => "511,206",
          "value4" => "512,984"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "112,666,000,000",
          "group" => "p",
          "value1" => "112,919,000,000",
          "value2" => "104,135,000,000",
          "value3" => "84,260,000,000",
          "value4" => "64,057,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "49,942,000,000",
          "group" => "p",
          "value1" => "45,495,000,000",
          "value2" => "43,384,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "39,259,000,000",
          "group" => "p",
          "value1" => "35,231,000,000",
          "value2" => "32,976,000,000",
          "value3" => "30,519,000,000",
          "value4" => "26,271,000,000"
        },
        %{
          "value0" => "38,967,000,000",
          "group" => "p",
          "value1" => "34,862,000,000",
          "value2" => "32,788,000,000",
          "value3" => "30,269,000,000",
          "value4" => "26,154,000,000"
        },
        %{
          "value0" => "-55,876,000,000",
          "group" => "p",
          "value1" => "-300,326,000,000",
          "value2" => "321,275,000,000",
          "value3" => "408,780,000,000",
          "value4" => "-49,637,000,000"
        },
        %{
          "value0" => "6,199,690,000,000",
          "group" => "p",
          "value1" => "5,537,298,000,000",
          "value2" => "5,221,221,000,000",
          "value3" => "5,033,122,000,000",
          "value4" => "3,810,629,000,000"
        },
        %{
          "value0" => "419,377,000,000",
          "group" => "p",
          "value1" => "378,177,000,000",
          "value2" => "332,075,000,000",
          "value3" => "285,936,000,000",
          "value4" => "212,343,000,000"
        },
        %{
          "value0" => "1.56",
          "group" => "p",
          "value1" => "1.4",
          "value2" => "1.31",
          "value3" => "1.21",
          "value4" => "1.22"
        },
        %{
          "value0" => "1.56",
          "group" => "p",
          "value1" => "1.4",
          "value2" => "1.31",
          "value3" => "1.21",
          "value4" => "1.22"
        },
        %{
          "value0" => "1.55",
          "group" => "p",
          "value1" => "1.38",
          "value2" => "1.3",
          "value3" => "1.2",
          "value4" => "1.21"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "19.11",
          "group" => "p",
          "value1" => "19.07",
          "value2" => "20.4",
          "value3" => "21.49",
          "value4" => "25.46"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "18.97",
          "group" => "p",
          "value1" => "18.87",
          "value2" => "20.29",
          "value3" => "21.35",
          "value4" => "25.35"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        }
      ]
    },
    "股本结构" => %{
      "columns" => [
        [
          %{"field" => "value0", "title" => "2017"},
          %{"field" => "value1", "title" => "2016"},
          %{"field" => "value2", "title" => "2015"},
          %{"field" => "value3", "title" => "2014"},
          %{"field" => "value4", "title" => "2013"}
        ]
      ],
      "rows" => [
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{
          "group" => "p",
          "value0" => "0",
          "value1" => "0",
          "value2" => "0",
          "value3" => "20,628,944,429",
          "value4" => "17,666,130,885"
        },
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{
          "group" => "p",
          "value0" => "0",
          "value1" => "0",
          "value2" => "0",
          "value3" => "4,590,901,172",
          "value4" => "3,910,478,000"
        },
        %{"group" => "p", "value0" => "0", "value1" => "0", "value2" => "0", "value3" => "0", "value4" => "0"},
        %{
          "group" => "p",
          "value0" => "0",
          "value1" => "0",
          "value2" => "0",
          "value3" => "25,219,845,601",
          "value4" => "21,576,608,885"
        },
        %{
          "group" => "p",
          "value0" => "0",
          "value1" => "0",
          "value2" => "0",
          "value3" => "25,219,845,601",
          "value4" => "21,576,608,885"
        },
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => "-", "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => 81.8, "value4" => 81.88},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => 0.0, "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => 18.2, "value4" => 18.12},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => 0.0, "value4" => "-"},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => 100.0, "value4" => 100.0},
        %{"group" => "p", "value0" => "-", "value1" => "-", "value2" => "-", "value3" => 100.0, "value4" => 100.0}
      ],
      "stock_id" => "600036",
      "tip" => "数据取自：招商银行半年报&nbsp;XBRL报告",
      "total" => 5
    },
    "资产负债" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行半年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "166,841,000,000",
          "group" => "p",
          "value1" => "211,229,000,000",
          "value2" => "120,575,000,000",
          "value3" => "183,414,000,000",
          "value4" => "96,947,000,000"
        },
        %{
          "value0" => "42,732,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "44,530,000,000",
          "value3" => "0",
          "value4" => "15,428,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "28,173,000,000",
          "group" => "p",
          "value1" => "26,328,000,000",
          "value2" => "25,692,000,000",
          "value3" => "23,226,000,000",
          "value4" => "16,686,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "217,190,000,000",
          "group" => "p",
          "value1" => "155,045,000,000",
          "value2" => "411,368,000,000",
          "value3" => "668,832,000,000",
          "value4" => "426,088,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "3,404,094,000,000",
          "group" => "p",
          "value1" => "2,922,036,000,000",
          "value2" => "2,565,277,000,000",
          "value3" => "2,362,544,000,000",
          "value4" => "2,052,599,000,000"
        },
        %{
          "value0" => "404,182,000,000",
          "group" => "p",
          "value1" => "339,927,000,000",
          "value2" => "272,809,000,000",
          "value3" => "267,608,000,000",
          "value4" => "304,309,000,000"
        },
        %{
          "value0" => "516,094,000,000",
          "group" => "p",
          "value1" => "435,829,000,000",
          "value2" => "299,230,000,000",
          "value3" => "237,705,000,000",
          "value4" => "196,841,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "4,016,000,000",
          "group" => "p",
          "value1" => "2,894,000,000",
          "value2" => "2,650,000,000",
          "value3" => "915,000,000",
          "value4" => "1,104,000,000"
        },
        %{
          "value0" => "1,583,000,000",
          "group" => "p",
          "value1" => "1,686,000,000",
          "value2" => "1,715,000,000",
          "value3" => "1,690,000,000",
          "value4" => "1,610,000,000"
        },
        %{
          "value0" => "46,056,000,000",
          "group" => "p",
          "value1" => "31,015,000,000",
          "value2" => "28,665,000,000",
          "value3" => "0",
          "value4" => "19,703,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "4,079,000,000",
          "group" => "p",
          "value1" => "3,551,000,000",
          "value2" => "3,235,000,000",
          "value3" => "3,224,000,000",
          "value4" => "2,897,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "9,954,000,000",
          "group" => "p",
          "value1" => "9,954,000,000",
          "value2" => "9,954,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "40,556,000,000",
          "group" => "p",
          "value1" => "24,504,000,000",
          "value2" => "16,282,000,000",
          "value3" => "8,216,000,000",
          "value4" => "5,062,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "6,199,690,000,000",
          "group" => "p",
          "value1" => "5,537,298,000,000",
          "value2" => "5,221,221,000,000",
          "value3" => "5,033,122,000,000",
          "value4" => "3,810,629,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "351,542,000,000",
          "group" => "p",
          "value1" => "70,500,000,000",
          "value2" => "25,000,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "212,457,000,000",
          "group" => "p",
          "value1" => "155,671,000,000",
          "value2" => "93,796,000,000",
          "value3" => "145,165,000,000",
          "value4" => "117,790,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "142,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "158,357,000,000",
          "group" => "p",
          "value1" => "294,234,000,000",
          "value2" => "122,190,000,000",
          "value3" => "126,690,000,000",
          "value4" => "132,570,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "6,821,000,000",
          "group" => "p",
          "value1" => "8,720,000,000",
          "value2" => "9,417,000,000",
          "value3" => "8,425,000,000",
          "value4" => "4,140,000,000"
        },
        %{
          "value0" => "17,825,000,000",
          "group" => "p",
          "value1" => "15,037,000,000",
          "value2" => "14,909,000,000",
          "value3" => "10,110,000,000",
          "value4" => "7,377,000,000"
        },
        %{
          "value0" => "34,274,000,000",
          "group" => "p",
          "value1" => "36,416,000,000",
          "value2" => "38,696,000,000",
          "value3" => "40,903,000,000",
          "value4" => "27,051,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "346,902,000,000",
          "group" => "p",
          "value1" => "284,882,000,000",
          "value2" => "162,156,000,000",
          "value3" => "99,981,000,000",
          "value4" => "88,784,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "906,000,000",
          "group" => "p",
          "value1" => "854,000,000",
          "value2" => "750,000,000",
          "value3" => "775,000,000",
          "value4" => "787,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "5,777,866,000,000",
          "group" => "p",
          "value1" => "5,158,110,000,000",
          "value2" => "4,888,303,000,000",
          "value3" => "4,746,758,000,000",
          "value4" => "3,598,131,000,000"
        },
        %{
          "value0" => "25,220,000,000",
          "group" => "p",
          "value1" => "25,220,000,000",
          "value2" => "25,220,000,000",
          "value3" => "25,220,000,000",
          "value4" => "21,577,000,000"
        },
        %{
          "value0" => "67,523,000,000",
          "group" => "p",
          "value1" => "67,523,000,000",
          "value2" => "67,523,000,000",
          "value3" => "66,664,000,000",
          "value4" => "37,181,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "39,708,000,000",
          "group" => "p",
          "value1" => "34,009,000,000",
          "value2" => "28,690,000,000",
          "value3" => "23,502,000,000",
          "value4" => "18,618,000,000"
        },
        %{
          "value0" => "68,081,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "53,981,000,000",
          "value3" => "46,422,000,000",
          "value4" => "39,361,000,000"
        },
        %{
          "value0" => "219,463,000,000",
          "group" => "p",
          "value1" => "181,191,000,000",
          "value2" => "154,615,000,000",
          "value3" => "0",
          "value4" => "97,431,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "-1,251,000,000",
          "value4" => "-1,527,000,000"
        },
        %{
          "value0" => "419,377,000,000",
          "group" => "p",
          "value1" => "378,177,000,000",
          "value2" => "332,075,000,000",
          "value3" => "285,936,000,000",
          "value4" => "212,343,000,000"
        },
        %{
          "value0" => "1,277,000,000",
          "group" => "p",
          "value1" => "1,011,000,000",
          "value2" => "843,000,000",
          "value3" => "428,000,000",
          "value4" => "155,000,000"
        },
        %{
          "value0" => "421,824,000,000",
          "group" => "p",
          "value1" => "379,188,000,000",
          "value2" => "332,918,000,000",
          "value3" => "286,364,000,000",
          "value4" => "212,498,000,000"
        },
        %{
          "value0" => "6,199,690,000,000",
          "group" => "p",
          "value1" => "5,537,298,000,000",
          "value2" => "5,221,221,000,000",
          "value3" => "5,033,122,000,000",
          "value4" => "3,810,629,000,000"
        }
      ]
    },
    "利润" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行半年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "64,057,000,000"
        },
        %{
          "value0" => "112,666,000,000",
          "group" => "p",
          "value1" => "112,919,000,000",
          "value2" => "104,135,000,000",
          "value3" => "84,260,000,000",
          "value4" => "64,057,000,000"
        },
        %{
          "value0" => "116,393,000,000",
          "group" => "p",
          "value1" => "108,857,000,000",
          "value2" => "118,359,000,000",
          "value3" => "106,277,000,000",
          "value4" => "80,383,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "37,526,000,000",
          "group" => "p",
          "value1" => "39,991,000,000",
          "value2" => "33,240,000,000",
          "value3" => "25,335,000,000",
          "value4" => "15,083,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "29,364,000,000"
        },
        %{
          "value0" => "63,090,000,000",
          "group" => "p",
          "value1" => "67,899,000,000",
          "value2" => "60,997,000,000",
          "value3" => "44,245,000,000",
          "value4" => "29,364,000,000"
        },
        %{
          "value0" => "45,497,000,000",
          "group" => "p",
          "value1" => "41,380,000,000",
          "value2" => "52,255,000,000",
          "value3" => "52,419,000,000",
          "value4" => "32,942,000,000"
        },
        %{
          "value0" => "2,776,000,000",
          "group" => "p",
          "value1" => "2,212,000,000",
          "value2" => "2,143,000,000",
          "value3" => "1,633,000,000",
          "value4" => "919,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "1,073,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "6,266,000,000",
          "value3" => "5,201,000,000",
          "value4" => "4,116,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "32,648,000,000",
          "group" => "p",
          "value1" => "36,170,000,000",
          "value2" => "29,171,000,000",
          "value3" => "16,320,000,000",
          "value4" => "4,959,000,000"
        },
        %{
          "value0" => "-208,000,000",
          "group" => "p",
          "value1" => "-2,255,000,000",
          "value2" => "197,000,000",
          "value3" => "42,000,000",
          "value4" => "-429,000,000"
        },
        %{
          "value0" => "4,409,000,000",
          "group" => "p",
          "value1" => "8,283,000,000",
          "value2" => "5,985,000,000",
          "value3" => "4,222,000,000",
          "value4" => "1,757,000,000"
        },
        %{
          "value0" => "402,000,000",
          "group" => "p",
          "value1" => "152,000,000",
          "value2" => "134,000,000",
          "value3" => "93,000,000",
          "value4" => "26,000,000"
        },
        %{
          "value0" => "1,605,000,000",
          "group" => "p",
          "value1" => "939,000,000",
          "value2" => "324,000,000",
          "value3" => "2,130,000,000",
          "value4" => "714,000,000"
        },
        %{
          "value0" => "49,576,000,000",
          "group" => "p",
          "value1" => "45,020,000,000",
          "value2" => "43,138,000,000",
          "value3" => "40,015,000,000",
          "value4" => "34,693,000,000"
        },
        %{
          "value0" => "400,000,000",
          "group" => "p",
          "value1" => "502,000,000",
          "value2" => "285,000,000",
          "value3" => "289,000,000",
          "value4" => "178,000,000"
        },
        %{
          "value0" => "34,000,000",
          "group" => "p",
          "value1" => "27,000,000",
          "value2" => "39,000,000",
          "value3" => "39,000,000",
          "value4" => "23,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "49,942,000,000",
          "group" => "p",
          "value1" => "45,495,000,000",
          "value2" => "43,384,000,000",
          "value3" => "40,265,000,000",
          "value4" => "34,848,000,000"
        },
        %{
          "value0" => "10,476,000,000",
          "group" => "p",
          "value1" => "10,163,000,000",
          "value2" => "10,215,000,000",
          "value3" => "9,746,000,000",
          "value4" => "8,582,000,000"
        },
        %{
          "value0" => "39,466,000,000",
          "group" => "p",
          "value1" => "35,332,000,000",
          "value2" => "33,169,000,000",
          "value3" => "30,519,000,000",
          "value4" => "26,266,000,000"
        },
        %{
          "value0" => "39,259,000,000",
          "group" => "p",
          "value1" => "35,231,000,000",
          "value2" => "32,976,000,000",
          "value3" => "30,519,000,000",
          "value4" => "26,271,000,000"
        },
        %{
          "value0" => "207,000,000",
          "group" => "p",
          "value1" => "101,000,000",
          "value2" => "0",
          "value3" => "60,000,000",
          "value4" => "-5,000,000"
        },
        %{
          "value0" => "1.56",
          "group" => "p",
          "value1" => "1.4",
          "value2" => "1.31",
          "value3" => "1.21",
          "value4" => "1.22"
        },
        %{
          "value0" => "1.56",
          "group" => "p",
          "value1" => "1.4",
          "value2" => "1.31",
          "value3" => "1.21",
          "value4" => "1.22"
        }
      ]
    },
    "现金流量" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行半年报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "340,205,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "320,702,000,000",
          "value3" => "0",
          "value4" => "264,940,000,000"
        },
        %{
          "value0" => "21,434,000,000",
          "group" => "p",
          "value1" => "7,900,000,000",
          "value2" => "5,000,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "126,719,000,000",
          "group" => "p",
          "value1" => "147,454,000,000",
          "value2" => "149,467,000,000",
          "value3" => "112,232,000,000",
          "value4" => "87,066,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "54,395,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "12,749,000,000",
          "group" => "p",
          "value1" => "12,733,000,000",
          "value2" => "10,152,000,000",
          "value3" => "11,082,000,000",
          "value4" => "3,268,000,000"
        },
        %{
          "value0" => "553,165,000,000",
          "group" => "p",
          "value1" => "378,613,000,000",
          "value2" => "608,484,000,000",
          "value3" => "1,065,331,000,000",
          "value4" => "450,301,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "285,010,000,000",
          "group" => "p",
          "value1" => "217,369,000,000",
          "value2" => "145,103,000,000",
          "value3" => "230,371,000,000",
          "value4" => "194,033,000,000"
        },
        %{
          "value0" => "18,610,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "6,175,000,000",
          "value3" => "75,316,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "44,033,000,000",
          "group" => "p",
          "value1" => "46,249,000,000",
          "value2" => "61,051,000,000",
          "value3" => "43,801,000,000",
          "value4" => "21,331,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "17,961,000,000",
          "group" => "p",
          "value1" => "14,163,000,000",
          "value2" => "12,563,000,000",
          "value3" => "11,069,000,000",
          "value4" => "12,414,000,000"
        },
        %{
          "value0" => "28,457,000,000",
          "group" => "p",
          "value1" => "23,433,000,000",
          "value2" => "19,721,000,000",
          "value3" => "15,530,000,000",
          "value4" => "12,000,000,000"
        },
        %{
          "value0" => "18,261,000,000",
          "group" => "p",
          "value1" => "44,987,000,000",
          "value2" => "42,596,000,000",
          "value3" => "71,040,000,000",
          "value4" => "6,648,000,000"
        },
        %{
          "value0" => "609,041,000,000",
          "group" => "p",
          "value1" => "678,939,000,000",
          "value2" => "287,209,000,000",
          "value3" => "656,551,000,000",
          "value4" => "499,938,000,000"
        },
        %{
          "value0" => "-55,876,000,000",
          "group" => "p",
          "value1" => "-300,326,000,000",
          "value2" => "321,275,000,000",
          "value3" => "408,780,000,000",
          "value4" => "-49,637,000,000"
        },
        %{
          "value0" => "221,529,000,000",
          "group" => "p",
          "value1" => "475,545,000,000",
          "value2" => "352,374,000,000",
          "value3" => "226,025,000,000",
          "value4" => "372,657,000,000"
        },
        %{
          "value0" => "33,842,000,000",
          "group" => "p",
          "value1" => "26,904,000,000",
          "value2" => "28,548,000,000",
          "value3" => "18,322,000,000",
          "value4" => "7,630,000,000"
        },
        %{
          "value0" => "18,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "62,000,000",
          "value3" => "13,000,000",
          "value4" => "20,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "255,389,000,000",
          "group" => "p",
          "value1" => "502,872,000,000",
          "value2" => "380,984,000,000",
          "value3" => "244,360,000,000",
          "value4" => "380,307,000,000"
        },
        %{
          "value0" => "6,024,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "4,222,000,000",
          "value3" => "2,688,000,000",
          "value4" => "2,174,000,000"
        },
        %{
          "value0" => "331,834,000,000",
          "group" => "p",
          "value1" => "445,242,000,000",
          "value2" => "664,638,000,000",
          "value3" => "348,906,000,000",
          "value4" => "452,880,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "337,858,000,000",
          "group" => "p",
          "value1" => "447,754,000,000",
          "value2" => "668,860,000,000",
          "value3" => "351,594,000,000",
          "value4" => "455,054,000,000"
        },
        %{
          "value0" => "-82,469,000,000",
          "group" => "p",
          "value1" => "55,118,000,000",
          "value2" => "-287,876,000,000",
          "value3" => "-107,234,000,000",
          "value4" => "-74,747,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "38,000,000",
          "value3" => "31,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "38,000,000",
          "value3" => "31,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "32,318,000,000",
          "group" => "p",
          "value1" => "7,910,000,000",
          "value2" => "3,055,000,000",
          "value3" => "15,397,000,000",
          "value4" => "2,000,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "208,463,000,000",
          "group" => "p",
          "value1" => "286,799,000,000",
          "value2" => "97,154,000,000",
          "value3" => "59,109,000,000",
          "value4" => "21,607,000,000"
        },
        %{
          "value0" => "138,253,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "41,768,000,000",
          "value3" => "26,334,000,000",
          "value4" => "6,619,000,000"
        },
        %{
          "value0" => "15,265,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "1,870,000,000",
          "value3" => "0",
          "value4" => "11,396,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "134,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "160,744,000,000",
          "group" => "p",
          "value1" => "258,741,000,000",
          "value2" => "43,653,000,000",
          "value3" => "27,376,000,000",
          "value4" => "19,028,000,000"
        },
        %{
          "value0" => "47,719,000,000",
          "group" => "p",
          "value1" => "28,058,000,000",
          "value2" => "53,501,000,000",
          "value3" => "31,733,000,000",
          "value4" => "2,579,000,000"
        },
        %{
          "value0" => "-1,340,000,000",
          "group" => "p",
          "value1" => "-761,000,000",
          "value2" => "-2,674,000,000",
          "value3" => "14,428,000,000",
          "value4" => "-886,000,000"
        },
        %{
          "value0" => "-91,966,000,000",
          "group" => "p",
          "value1" => "-217,911,000,000",
          "value2" => "84,226,000,000",
          "value3" => "347,707,000,000",
          "value4" => "-122,691,000,000"
        },
        %{
          "value0" => "440,146,000,000",
          "group" => "p",
          "value1" => "417,932,000,000",
          "value2" => "555,697,000,000",
          "value3" => "697,656,000,000",
          "value4" => "330,164,000,000"
        }
      ]
    }
  }
  @xbrl_season3 %{
    "基本信息" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行三季报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value2" => "-",
          "value3" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
          "value4" => "-"
        },
        %{
          "value0" => "<div style='text-align: left;'>李建红</div>",
          "group" => "p",
          "value1" => "<div style='text-align: left;'>李建红</div>",
          "value2" => "-",
          "value3" => "<div style='text-align: left;'>李建红</div>",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "-",
          "group" => "p",
          "value1" => "-",
          "value2" => "-",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "218,770",
          "group" => "p",
          "value1" => "215,532",
          "value2" => "279,910",
          "value3" => "-",
          "value4" => "-"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "165,703,000,000",
          "group" => "p",
          "value1" => "160,291,000,000",
          "value2" => "156,224,000,000",
          "value3" => "125,027,000,000",
          "value4" => "97,531,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "74,577,000,000",
          "group" => "p",
          "value1" => "67,438,000,000",
          "value2" => "63,990,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "58,805,000,000",
          "group" => "p",
          "value1" => "52,142,000,000",
          "value2" => "48,500,000,000",
          "value3" => "45,804,000,000",
          "value4" => "39,498,000,000"
        },
        %{
          "value0" => "58,283,000,000",
          "group" => "p",
          "value1" => "51,427,000,000",
          "value2" => "48,193,000,000",
          "value3" => "45,425,000,000",
          "value4" => "39,254,000,000"
        },
        %{
          "value0" => "-192,675,000,000",
          "group" => "p",
          "value1" => "-359,376,000,000",
          "value2" => "176,060,000,000",
          "value3" => "151,170,000,000",
          "value4" => "6,474,000,000"
        },
        %{
          "value0" => "6,169,239,000,000",
          "group" => "p",
          "value1" => "5,563,990,000,000",
          "value2" => "5,222,292,000,000",
          "value3" => "4,722,648,000,000",
          "value4" => "3,885,372,000,000"
        },
        %{
          "value0" => "438,035,000,000",
          "group" => "p",
          "value1" => "396,411,000,000",
          "value2" => "348,962,000,000",
          "value3" => "301,915,000,000",
          "value4" => "257,127,000,000"
        },
        %{
          "value0" => "2.33",
          "group" => "p",
          "value1" => "2.07",
          "value2" => "1.92",
          "value3" => "1.82",
          "value4" => "1.83"
        },
        %{
          "value0" => "2.33",
          "group" => "p",
          "value1" => "2.07",
          "value2" => "1.92",
          "value3" => "1.82",
          "value4" => "1.83"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "18.66",
          "group" => "p",
          "value1" => "0",
          "value2" => "19.5",
          "value3" => "21.33",
          "value4" => "24.59"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        }
      ]
    },
    "资产负债" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行三季报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "149,314,000,000",
          "group" => "p",
          "value1" => "188,797,000,000",
          "value2" => "164,413,000,000",
          "value3" => "188,210,000,000",
          "value4" => "103,895,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "40,880,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "27,670,000,000",
          "group" => "p",
          "value1" => "25,109,000,000",
          "value2" => "25,327,000,000",
          "value3" => "23,892,000,000",
          "value4" => "16,668,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "169,067,000,000",
          "group" => "p",
          "value1" => "128,924,000,000",
          "value2" => "256,766,000,000",
          "value3" => "371,451,000,000",
          "value4" => "308,901,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "3,491,964,000,000",
          "group" => "p",
          "value1" => "3,062,607,000,000",
          "value2" => "2,633,672,000,000",
          "value3" => "2,384,121,000,000",
          "value4" => "2,125,156,000,000"
        },
        %{
          "value0" => "395,487,000,000",
          "group" => "p",
          "value1" => "344,407,000,000",
          "value2" => "292,918,000,000",
          "value3" => "288,167,000,000",
          "value4" => "311,023,000,000"
        },
        %{
          "value0" => "541,930,000,000",
          "group" => "p",
          "value1" => "466,028,000,000",
          "value2" => "343,053,000,000",
          "value3" => "252,213,000,000",
          "value4" => "203,664,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "4,254,000,000",
          "group" => "p",
          "value1" => "3,703,000,000",
          "value2" => "2,663,000,000",
          "value3" => "1,427,000,000",
          "value4" => "1,120,000,000"
        },
        %{
          "value0" => "1,699,000,000",
          "group" => "p",
          "value1" => "1,626,000,000",
          "value2" => "1,714,000,000",
          "value3" => "1,674,000,000",
          "value4" => "1,584,000,000"
        },
        %{
          "value0" => "47,997,000,000",
          "group" => "p",
          "value1" => "35,914,000,000",
          "value2" => "29,979,000,000",
          "value3" => "0",
          "value4" => "21,877,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "4,181,000,000",
          "group" => "p",
          "value1" => "3,608,000,000",
          "value2" => "3,348,000,000",
          "value3" => "3,178,000,000",
          "value4" => "2,924,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "9,954,000,000",
          "group" => "p",
          "value1" => "9,954,000,000",
          "value2" => "9,954,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "44,125,000,000",
          "group" => "p",
          "value1" => "26,618,000,000",
          "value2" => "19,535,000,000",
          "value3" => "9,874,000,000",
          "value4" => "6,078,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "6,169,239,000,000",
          "group" => "p",
          "value1" => "5,563,990,000,000",
          "value2" => "5,222,292,000,000",
          "value3" => "4,722,648,000,000",
          "value4" => "3,885,372,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "330,822,000,000",
          "group" => "p",
          "value1" => "94,000,000,000",
          "value2" => "31,000,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "280,770,000,000",
          "group" => "p",
          "value1" => "231,908,000,000",
          "value2" => "156,259,000,000",
          "value3" => "125,874,000,000",
          "value4" => "116,323,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "163,597,000,000",
          "group" => "p",
          "value1" => "321,829,000,000",
          "value2" => "169,525,000,000",
          "value3" => "48,179,000,000",
          "value4" => "76,945,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "10,867,000,000",
          "group" => "p",
          "value1" => "10,662,000,000",
          "value2" => "11,855,000,000",
          "value3" => "10,250,000,000",
          "value4" => "4,755,000,000"
        },
        %{
          "value0" => "22,575,000,000",
          "group" => "p",
          "value1" => "19,571,000,000",
          "value2" => "19,378,000,000",
          "value3" => "12,376,000,000",
          "value4" => "8,188,000,000"
        },
        %{
          "value0" => "35,815,000,000",
          "group" => "p",
          "value1" => "35,750,000,000",
          "value2" => "39,507,000,000",
          "value3" => "44,766,000,000",
          "value4" => "30,481,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "384,780,000,000",
          "group" => "p",
          "value1" => "306,636,000,000",
          "value2" => "203,916,000,000",
          "value3" => "101,047,000,000",
          "value4" => "67,470,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "890,000,000",
          "group" => "p",
          "value1" => "853,000,000",
          "value2" => "762,000,000",
          "value3" => "759,000,000",
          "value4" => "779,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "5,728,599,000,000",
          "group" => "p",
          "value1" => "5,166,556,000,000",
          "value2" => "4,872,327,000,000",
          "value3" => "4,420,250,000,000",
          "value4" => "3,628,098,000,000"
        },
        %{
          "value0" => "25,220,000,000",
          "group" => "p",
          "value1" => "25,220,000,000",
          "value2" => "25,220,000,000",
          "value3" => "25,220,000,000",
          "value4" => "25,220,000,000"
        },
        %{
          "value0" => "67,523,000,000",
          "group" => "p",
          "value1" => "67,523,000,000",
          "value2" => "67,523,000,000",
          "value3" => "67,322,000,000",
          "value4" => "65,390,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "39,708,000,000",
          "group" => "p",
          "value1" => "34,009,000,000",
          "value2" => "28,690,000,000",
          "value3" => "23,502,000,000",
          "value4" => "18,618,000,000"
        },
        %{
          "value0" => "68,039,000,000",
          "group" => "p",
          "value1" => "64,689,000,000",
          "value2" => "53,938,000,000",
          "value3" => "46,473,000,000",
          "value4" => "39,395,000,000"
        },
        %{
          "value0" => "239,051,000,000",
          "group" => "p",
          "value1" => "198,019,000,000",
          "value2" => "170,206,000,000",
          "value3" => "0",
          "value4" => "110,624,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "-1,516,000,000",
          "value4" => "-1,565,000,000"
        },
        %{
          "value0" => "438,035,000,000",
          "group" => "p",
          "value1" => "396,411,000,000",
          "value2" => "348,962,000,000",
          "value3" => "301,915,000,000",
          "value4" => "257,127,000,000"
        },
        %{
          "value0" => "2,605,000,000",
          "group" => "p",
          "value1" => "1,023,000,000",
          "value2" => "1,003,000,000",
          "value3" => "483,000,000",
          "value4" => "147,000,000"
        },
        %{
          "value0" => "440,640,000,000",
          "group" => "p",
          "value1" => "397,434,000,000",
          "value2" => "349,965,000,000",
          "value3" => "302,398,000,000",
          "value4" => "257,274,000,000"
        },
        %{
          "value0" => "6,169,239,000,000",
          "group" => "p",
          "value1" => "5,563,990,000,000",
          "value2" => "5,222,292,000,000",
          "value3" => "4,722,648,000,000",
          "value4" => "3,885,372,000,000"
        }
      ]
    },
    "利润" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行三季报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "97,531,000,000"
        },
        %{
          "value0" => "165,703,000,000",
          "group" => "p",
          "value1" => "160,291,000,000",
          "value2" => "156,224,000,000",
          "value3" => "125,027,000,000",
          "value4" => "97,531,000,000"
        },
        %{
          "value0" => "178,008,000,000",
          "group" => "p",
          "value1" => "161,930,000,000",
          "value2" => "176,897,000,000",
          "value3" => "165,345,000,000",
          "value4" => "125,679,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "53,599,000,000",
          "group" => "p",
          "value1" => "52,640,000,000",
          "value2" => "48,131,000,000",
          "value3" => "36,800,000,000",
          "value4" => "23,211,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "45,441,000,000"
        },
        %{
          "value0" => "91,784,000,000",
          "group" => "p",
          "value1" => "93,774,000,000",
          "value2" => "92,637,000,000",
          "value3" => "65,153,000,000",
          "value4" => "45,441,000,000"
        },
        %{
          "value0" => "70,623,000,000",
          "group" => "p",
          "value1" => "61,208,000,000",
          "value2" => "75,738,000,000",
          "value3" => "83,078,000,000",
          "value4" => "53,107,000,000"
        },
        %{
          "value0" => "4,265,000,000",
          "group" => "p",
          "value1" => "3,404,000,000",
          "value2" => "3,154,000,000",
          "value3" => "2,554,000,000",
          "value4" => "1,505,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "1,547,000,000",
          "group" => "p",
          "value1" => "0",
          "value2" => "9,268,000,000",
          "value3" => "7,783,000,000",
          "value4" => "6,416,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "43,838,000,000",
          "group" => "p",
          "value1" => "47,640,000,000",
          "value2" => "43,952,000,000",
          "value3" => "21,998,000,000",
          "value4" => "8,162,000,000"
        },
        %{
          "value0" => "-45,000,000",
          "group" => "p",
          "value1" => "-2,231,000,000",
          "value2" => "939,000,000",
          "value3" => "261,000,000",
          "value4" => "-240,000,000"
        },
        %{
          "value0" => "5,368,000,000",
          "group" => "p",
          "value1" => "9,755,000,000",
          "value2" => "6,947,000,000",
          "value3" => "5,564,000,000",
          "value4" => "2,560,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "1,697,000,000",
          "group" => "p",
          "value1" => "1,687,000,000",
          "value2" => "1,665,000,000",
          "value3" => "2,220,000,000",
          "value4" => "486,000,000"
        },
        %{
          "value0" => "73,919,000,000",
          "group" => "p",
          "value1" => "66,517,000,000",
          "value2" => "63,587,000,000",
          "value3" => "59,874,000,000",
          "value4" => "52,090,000,000"
        },
        %{
          "value0" => "740,000,000",
          "group" => "p",
          "value1" => "981,000,000",
          "value2" => "483,000,000",
          "value3" => "587,000,000",
          "value4" => "405,000,000"
        },
        %{
          "value0" => "82,000,000",
          "group" => "p",
          "value1" => "60,000,000",
          "value2" => "80,000,000",
          "value3" => "89,000,000",
          "value4" => "81,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "74,577,000,000",
          "group" => "p",
          "value1" => "67,438,000,000",
          "value2" => "63,990,000,000",
          "value3" => "60,372,000,000",
          "value4" => "52,414,000,000"
        },
        %{
          "value0" => "15,413,000,000",
          "group" => "p",
          "value1" => "15,094,000,000",
          "value2" => "15,204,000,000",
          "value3" => "14,453,000,000",
          "value4" => "12,923,000,000"
        },
        %{
          "value0" => "59,164,000,000",
          "group" => "p",
          "value1" => "52,344,000,000",
          "value2" => "48,786,000,000",
          "value3" => "45,919,000,000",
          "value4" => "39,491,000,000"
        },
        %{
          "value0" => "58,805,000,000",
          "group" => "p",
          "value1" => "52,142,000,000",
          "value2" => "48,500,000,000",
          "value3" => "45,804,000,000",
          "value4" => "39,498,000,000"
        },
        %{
          "value0" => "359,000,000",
          "group" => "p",
          "value1" => "202,000,000",
          "value2" => "286,000,000",
          "value3" => "115,000,000",
          "value4" => "-7,000,000"
        },
        %{
          "value0" => "2.33",
          "group" => "p",
          "value1" => "2.07",
          "value2" => "1.92",
          "value3" => "1.82",
          "value4" => "1.83"
        },
        %{
          "value0" => "2.33",
          "group" => "p",
          "value1" => "2.07",
          "value2" => "1.92",
          "value3" => "1.82",
          "value4" => "1.83"
        }
      ]
    },
    "现金流量" => %{
      "total" => 5,
      "stock_id" => "600036",
      "columns" => [
        [
          %{
            "field" => "value0",
            "title" => "2017"
          },
          %{
            "field" => "value1",
            "title" => "2016"
          },
          %{
            "field" => "value2",
            "title" => "2015"
          },
          %{
            "field" => "value3",
            "title" => "2014"
          },
          %{
            "field" => "value4",
            "title" => "2013"
          }
        ]
      ],
      "tip" => "数据取自：招商银行三季报&nbsp;XBRL报告",
      "rows" => [
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "714,000,000",
          "group" => "p",
          "value1" => "31,400,000,000",
          "value2" => "11,000,000,000",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "213,381,000,000",
          "group" => "p",
          "value1" => "201,631,000,000",
          "value2" => "218,240,000,000",
          "value3" => "190,959,000,000",
          "value4" => "141,055,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "45,079,000,000",
          "group" => "p",
          "value1" => "23,007,000,000",
          "value2" => "13,645,000,000",
          "value3" => "11,002,000,000",
          "value4" => "9,562,000,000"
        },
        %{
          "value0" => "510,191,000,000",
          "group" => "p",
          "value1" => "491,582,000,000",
          "value2" => "605,826,000,000",
          "value3" => "919,152,000,000",
          "value4" => "614,326,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "383,518,000,000",
          "group" => "p",
          "value1" => "367,966,000,000",
          "value2" => "205,531,000,000",
          "value3" => "257,535,000,000",
          "value4" => "268,743,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "67,449,000,000",
          "group" => "p",
          "value1" => "58,833,000,000",
          "value2" => "82,246,000,000",
          "value3" => "68,824,000,000",
          "value4" => "43,379,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "23,174,000,000",
          "group" => "p",
          "value1" => "19,397,000,000",
          "value2" => "17,878,000,000",
          "value3" => "16,296,000,000",
          "value4" => "17,968,000,000"
        },
        %{
          "value0" => "36,981,000,000",
          "group" => "p",
          "value1" => "30,923,000,000",
          "value2" => "27,780,000,000",
          "value3" => "18,969,000,000",
          "value4" => "15,474,000,000"
        },
        %{
          "value0" => "42,638,000,000",
          "group" => "p",
          "value1" => "58,292,000,000",
          "value2" => "27,860,000,000",
          "value3" => "88,326,000,000",
          "value4" => "4,124,000,000"
        },
        %{
          "value0" => "702,866,000,000",
          "group" => "p",
          "value1" => "850,958,000,000",
          "value2" => "429,766,000,000",
          "value3" => "767,982,000,000",
          "value4" => "607,852,000,000"
        },
        %{
          "value0" => "-192,675,000,000",
          "group" => "p",
          "value1" => "-359,376,000,000",
          "value2" => "176,060,000,000",
          "value3" => "151,170,000,000",
          "value4" => "6,474,000,000"
        },
        %{
          "value0" => "482,205,000,000",
          "group" => "p",
          "value1" => "505,602,000,000",
          "value2" => "573,869,000,000",
          "value3" => "350,803,000,000",
          "value4" => "601,804,000,000"
        },
        %{
          "value0" => "48,093,000,000",
          "group" => "p",
          "value1" => "52,003,000,000",
          "value2" => "43,663,000,000",
          "value3" => "29,022,000,000",
          "value4" => "12,472,000,000"
        },
        %{
          "value0" => "96,000,000",
          "group" => "p",
          "value1" => "422,000,000",
          "value2" => "59,000,000",
          "value3" => "20,000,000",
          "value4" => "31,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "530,396,000,000",
          "group" => "p",
          "value1" => "558,032,000,000",
          "value2" => "617,593,000,000",
          "value3" => "379,847,000,000",
          "value4" => "614,309,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "6,669,000,000",
          "value2" => "2,316,000,000",
          "value3" => "2,970,000,000",
          "value4" => "2,973,000,000"
        },
        %{
          "value0" => "499,943,000,000",
          "group" => "p",
          "value1" => "474,736,000,000",
          "value2" => "908,880,000,000",
          "value3" => "546,108,000,000",
          "value4" => "787,543,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "508,400,000,000",
          "group" => "p",
          "value1" => "482,080,000,000",
          "value2" => "912,196,000,000",
          "value3" => "549,078,000,000",
          "value4" => "790,516,000,000"
        },
        %{
          "value0" => "21,996,000,000",
          "group" => "p",
          "value1" => "75,952,000,000",
          "value2" => "-294,603,000,000",
          "value3" => "-169,231,000,000",
          "value4" => "-176,207,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "51,996,000,000",
          "group" => "p",
          "value1" => "7,910,000,000",
          "value2" => "3,138,000,000",
          "value3" => "15,397,000,000",
          "value4" => "4,000,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "546,887,000,000",
          "group" => "p",
          "value1" => "463,501,000,000",
          "value2" => "145,658,000,000",
          "value3" => "62,266,000,000",
          "value4" => "61,679,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "404,233,000,000",
          "value2" => "46,936,000,000",
          "value3" => "29,803,000,000",
          "value4" => "33,366,000,000"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "16,897,000,000",
          "value3" => "1,481,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "132,000,000",
          "value4" => "0"
        },
        %{
          "value0" => "0",
          "group" => "p",
          "value1" => "0",
          "value2" => "0",
          "value3" => "0",
          "value4" => "0"
        },
        %{
          "value0" => "457,424,000,000",
          "group" => "p",
          "value1" => "433,940,000,000",
          "value2" => "66,429,000,000",
          "value3" => "47,052,000,000",
          "value4" => "49,707,000,000"
        },
        %{
          "value0" => "89,463,000,000",
          "group" => "p",
          "value1" => "29,561,000,000",
          "value2" => "79,229,000,000",
          "value3" => "15,214,000,000",
          "value4" => "11,972,000,000"
        },
        %{
          "value0" => "1,720,000,000",
          "group" => "p",
          "value1" => "2,619,000,000",
          "value2" => "-3,075,000,000",
          "value3" => "8,494,000,000",
          "value4" => "-994,000,000"
        },
        %{
          "value0" => "-79,496,000,000",
          "group" => "p",
          "value1" => "-251,244,000,000",
          "value2" => "-42,389,000,000",
          "value3" => "5,647,000,000",
          "value4" => "-158,755,000,000"
        },
        %{
          "value0" => "452,616,000,000",
          "group" => "p",
          "value1" => "384,599,000,000",
          "value2" => "429,082,000,000",
          "value3" => "355,596,000,000",
          "value4" => "294,100,000,000"
        }
      ]
    }
  }

  # describe "give a stock code and get a right json map" do
  #   test "xbrl info" do
  #     result =
  #       "600036"
  #       |> XbrlDownloader.craw(4)
  #     #   |> IO.inspect(label: "XBRL Download Info => ")

  #     assert @xbrl_year == result
  #   end

  #   test "xbrl season1 info" do
  #     result =
  #       "600036"
  #       |> XbrlDownloader.craw(1)
  #     # |> IO.inspect(label: "XBRL Download Info => ")

  #     assert @xbrl_season1 == result
  #   end

  #   test "xbrl season2 info" do
  #     result =
  #       "600036"
  #       |> XbrlDownloader.craw(2)
  #     # |> IO.inspect(label: "XBRL Download Info => ")

  #     assert @xbrl_season2 == result
  #   end

  #   test "xbrl season3 info" do
  #     result =
  #       "600036"
  #       |> XbrlDownloader.craw(3)
  #     # |> IO.inspect(label: "XBRL Download Info => ")

  #     assert @xbrl_season3 == result
  #   end

  # end
end
