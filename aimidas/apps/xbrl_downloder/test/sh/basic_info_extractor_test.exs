defmodule XbrlDownloader.BasicInfoExtractorTest do
  use ExUnit.Case, async: true

  alias XbrlDownloader.BasicInfoExtractor

  @json %{
    "columns" => [
      [
        %{
          "field" => "value0",
          "title" => "2017"
        },
        %{
          "field" => "value1",
          "title" => "2016"
        }
      ]
    ],
    "rows" => [
      %{
        "group" => "p",
        "value0" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
        "value1" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
        "value2" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
        "value3" => "<div style='text-align: left;'>招商银行股份有限公司</div>",
        "value4" => "<div style='text-align: left;'>招商银行股份有限公司</div>"
      },
      %{
        "group" => "p",
        "value0" => "<div style='text-align: left;'>李建红</div>",
        "value1" => "<div style='text-align: left;'>李建红</div>",
        "value2" => "<div style='text-align: left;'>李建红</div>",
        "value3" => "<div style='text-align: left;'>李建红</div>",
        "value4" => "<div style='text-align: left;'>傅育宁</div>"
      },
      %{
        "group" => "p",
        "value0" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
        "value1" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
        "value2" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
        "value3" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>",
        "value4" => "<div style='text-align: left;'>中国广东省深圳市福田区深南大道7088号</div>"
      },
      %{
        "group" => "p",
        "value0" => "<div style='text-align: left;'>518040</div>",
        "value1" => "<div style='text-align: left;'>518040</div>",
        "value2" => "<div style='text-align: left;'>518040</div>",
        "value3" => "<div style='text-align: left;'>518040</div>",
        "value4" => "<div style='text-align: left;'>518040</div>"
      },
      %{
        "group" => "p",
        "value0" => "<div style='text-align: left;'>www.cmbchina.com</div>",
        "value1" => "<div style='text-align: left;'>www.cmbchina.com</div>",
        "value2" => "<div style='text-align: left;'>www.cmbchina.com</div>",
        "value3" => "<div style='text-align: left;'>www.cmbchina.com</div>",
        "value4" => "<div style='text-align: left;'>www.cmbchina.com</div>"
      },
      %{
        "group" => "p",
        "value0" => "<div style='text-align: left;'>王良</div>",
        "value1" => "<div style='text-align: left;'>王良</div>",
        "value2" => "<div style='text-align: left;'>许世清</div>",
        "value3" => "<div style='text-align: left;'>许世清</div>",
        "value4" => "-"
      },
      %{
        "group" => "p",
        "value0" => "<div style='text-align: left;'>86755-83198888</div>",
        "value1" => "<div style='text-align: left;'>86755-83198888</div>",
        "value2" => "<div style='text-align: left;'>86755-83198888</div>",
        "value3" => "<div style='text-align: left;'>86755-83198888</div>",
        "value4" => "-"
      },
      %{
        "group" => "p",
        "value0" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
        "value1" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
        "value2" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
        "value3" => "<div style='text-align: left;'>cmb@cmbchina.com</div>",
        "value4" => "-"
      },
      %{
        "group" => "p",
        "value0" => "227,732",
        "value1" => "214,596",
        "value2" => "266,723",
        "value3" => "442,825",
        "value4" => "522,003"
      },
      %{
        "group" => "p",
        "value0" => "0",
        "value1" => "0",
        "value2" => "0",
        "value3" => "0",
        "value4" => "0"
      },
      %{
        "group" => "p",
        "value0" => "8.4",
        "value1" => "7.4",
        "value2" => "6.2",
        "value3" => "0",
        "value4" => "6.2"
      },
      %{
        "group" => "p",
        "value0" => "0",
        "value1" => "0",
        "value2" => "0",
        "value3" => "0",
        "value4" => "0"
      },
      %{
        "group" => "p",
        "value0" => "220,897,000,000",
        "value1" => "209,025,000,000",
        "value2" => "201,471,000,000",
        "value3" => "165,863,000,000",
        "value4" => "132,604,000,000"
      },
      %{
        "group" => "p",
        "value0" => "0",
        "value1" => "0",
        "value2" => "0",
        "value3" => "0",
        "value4" => "0"
      },
      %{
        "group" => "p",
        "value0" => "90,680,000,000",
        "value1" => "78,963,000,000",
        "value2" => "75,079,000,000",
        "value3" => "73,431,000,000",
        "value4" => "0"
      },
      %{
        "group" => "p",
        "value0" => "70,150,000,000",
        "value1" => "62,081,000,000",
        "value2" => "57,696,000,000",
        "value3" => "55,911,000,000",
        "value4" => "51,743,000,000"
      },
      %{
        "group" => "p",
        "value0" => "69,769,000,000",
        "value1" => "61,142,000,000",
        "value2" => "57,045,000,000",
        "value3" => "55,391,000,000",
        "value4" => "51,342,000,000"
      },
      %{
        "group" => "p",
        "value0" => "-5,660,000,000",
        "value1" => "-120,615,000,000",
        "value2" => "400,420,000,000",
        "value3" => "272,173,000,000",
        "value4" => "119,153,000,000"
      },
      %{
        "group" => "p",
        "value0" => "6,297,638,000,000",
        "value1" => "5,942,311,000,000",
        "value2" => "5,474,978,000,000",
        "value3" => "4,731,829,000,000",
        "value4" => "4,016,399,000,000"
      },
      %{
        "group" => "p",
        "value0" => "480,210,000,000",
        "value1" => "402,350,000,000",
        "value2" => "360,806,000,000",
        "value3" => "314,404,000,000",
        "value4" => "265,465,000,000"
      },
      %{
        "group" => "p",
        "value0" => "2.78",
        "value1" => "2.46",
        "value2" => "2.29",
        "value3" => "2.22",
        "value4" => "2.3"
      },
      %{
        "group" => "p",
        "value0" => "2.78",
        "value1" => "2.46",
        "value2" => "2.29",
        "value3" => "2.22",
        "value4" => "2.3"
      },
      %{
        "group" => "p",
        "value0" => "2.77",
        "value1" => "2.42",
        "value2" => "2.26",
        "value3" => "2.2",
        "value4" => "2.28"
      },
      %{
        "group" => "p",
        "value0" => "0",
        "value1" => "0",
        "value2" => "0",
        "value3" => "0",
        "value4" => "0"
      },
      %{
        "group" => "p",
        "value0" => "16.54",
        "value1" => "16.27",
        "value2" => "17.09",
        "value3" => "19.28",
        "value4" => "23.12"
      },
      %{
        "group" => "p",
        "value0" => "0",
        "value1" => "0",
        "value2" => "0",
        "value3" => "0",
        "value4" => "0"
      },
      %{
        "group" => "p",
        "value0" => "16.45",
        "value1" => "16.02",
        "value2" => "16.9",
        "value3" => "19.1",
        "value4" => "22.94"
      },
      %{
        "group" => "p",
        "value0" => "0",
        "value1" => "0",
        "value2" => "0",
        "value3" => "0",
        "value4" => "0"
      },
      %{
        "group" => "p",
        "value0" => "0",
        "value1" => "0",
        "value2" => "0",
        "value3" => "0",
        "value4" => "0"
      }
    ]
  }

  describe "extract basic info" do
    test "success" do
      result =
        @json
        |> BasicInfoExtractor.process()
        # |> IO.inspect(label: "BasicInfoExtractor => ")

      assert \
        %{
          "2017" => %{
            "公司办公地址邮政编码" => "518040",
            "公司国际互联网网址" => "www.cmbchina.com",
            "公司法定中文名称" => "招商银行股份有限公司",
            "公司法定代表人" => "李建红",
            "公司注册地址" => "中国广东省深圳市福田区深南大道7088号",
            "公司董事会秘书姓名" => "王良",
            "公司董事会秘书电子信箱" => "cmb@cmbchina.com",
            "公司董事会秘书电话" => "86755-83198888",
            "报告期末股东总数" => 227_732,
            "每10股送红股数" => 0,
            "每10股派息数" => 8.4,
            "每10股转增数" => 0,
            "本期营业收入" => 220_897_000_000,
            "本期营业利润" => 0,
            "利润总额" => 90_680_000_000,
            "归属于上市公司股东的净利润" => 70_150_000_000,
            "归属于上市公司股东的扣除非经常性损益的净利润" => 69_769_000_000,
            "经营活动产生的现金流量净额" => -5_660_000_000,
            "总资产" => 6_297_638_000_000,
            "所有者权益" => 480_210_000_000,
            "基本每股收益" => 2.78,
            "稀释每股收益" => 2.78,
            "扣除非经常性损益后的基本每股收益" => 2.77,
            "全面摊薄净资产收益率" => 0.0,
            "加权平均净资产收益率" => 16.54,
            "扣除非经常性损益后全面摊薄净资产收益率" => 0.0,
            "扣除非经常性损益后的加权平均净资产收益率" => 16.45,
            "每股经营活动产生的现金流量净额" => 0.0,
            "归属于上市公司股东的每股净资产" => 0.0,
          },
          "2016" => %{
            "公司办公地址邮政编码" => "518040",
            "公司国际互联网网址" => "www.cmbchina.com",
            "公司法定中文名称" => "招商银行股份有限公司",
            "公司法定代表人" => "李建红",
            "公司注册地址" => "中国广东省深圳市福田区深南大道7088号",
            "公司董事会秘书姓名" => "王良",
            "公司董事会秘书电子信箱" => "cmb@cmbchina.com",
            "公司董事会秘书电话" => "86755-83198888",
            "报告期末股东总数" => 214_596,
            "每10股送红股数" => 0,
            "每10股派息数" => 7.4,
            "每10股转增数" => 0,
            "本期营业收入" => 209_025_000_000,
            "本期营业利润" => 0,
            "利润总额" => 78_963_000_000,
            "归属于上市公司股东的净利润" => 62_081_000_000,
            "归属于上市公司股东的扣除非经常性损益的净利润" => 61_142_000_000,
            "经营活动产生的现金流量净额" => -120_615_000_000,
            "总资产" => 5_942_311_000_000,
            "所有者权益" => 402_350_000_000,
            "基本每股收益" => 2.46,
            "稀释每股收益" => 2.46,
            "扣除非经常性损益后的基本每股收益" => 2.42,
            "全面摊薄净资产收益率" => 0.0,
            "加权平均净资产收益率" => 16.27,
            "扣除非经常性损益后全面摊薄净资产收益率" => 0.0,
            "扣除非经常性损益后的加权平均净资产收益率" => 16.02,
            "每股经营活动产生的现金流量净额" => 0.0,
            "归属于上市公司股东的每股净资产" => 0.0,
          },
        } == result
    end
  end
end
