defmodule XbrlDownloder.MixProject do
  use Mix.Project

  def project do
    [
      app: :xbrl_downloder,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      elixirc_paths: elixirc_paths(Mix.env),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [
        :logger,
        :httpotion
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      # {:sibling_app_in_umbrella, in_umbrella: true},
      {:aimidas, in_umbrella: true},
      {:poison, "~> 3.1"},
      {:httpotion, "~> 3.1.0"},
      {:html_sanitize_ex, "~> 1.3.0-rc3"},
      {:rollbax, ">= 0.0.0"},
      {:floki, "~> 0.20.0"}
    ]
  end
end
