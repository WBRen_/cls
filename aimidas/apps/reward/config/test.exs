use Mix.Config

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :reward, Reward.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_REWARD_USER") || "postgres",
  password: System.get_env("POSTGRES_REWARD_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_REWARD_DB") || "aimidas_reward_test",
  hostname: System.get_env("POSTGRES_REWARD_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox