use Mix.Config

# Configure your database
config :reward, Reward.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_REWARD_DB_HOST") || "aim-postgresql",
  username: System.get_env("AIMIDAS_REWARD_DB_USER") || "postgres",
  password: System.get_env("AIMIDAS_REWARD_DB_PASSWORD") || "postgres",
  database: System.get_env("AIMIDAS_REWARD_DB_NAME") || "aimidas_reward_citest",
  pool_size: 10,
  pool_timeout: 20_000,
  timeout: 20_000
