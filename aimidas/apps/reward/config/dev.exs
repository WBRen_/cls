use Mix.Config

# Watch static and templates for browser reloading.
config :reward, RewardWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{lib/reward_web/views/.*(ex)$},
      ~r{lib/reward_web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :reward, Reward.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_REWARD_DB_HOST") || "localhost",
  username: System.get_env("AIMIDAS_REWARD_DB_USER") || "postgres",
  password: System.get_env("AIMIDAS_REWARD_DB_PASSWORD") || "postgres",
  database: System.get_env("AIMIDAS_REWARD_DB_NAME") || "aimidas_reward_dev",
  port: System.get_env("AIMIDAS_REWARD_DB_PORT") || 5432,
  pool_size: 10,
  pool_timeout: 60_000,
  timeout: 60_000