use Mix.Config

# Do not print debug messages in production
config :logger, level: :info

# Configure your database
config :reward, Reward.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_REWARD_DB_HOST") || "localhost",
  username: System.get_env("AIMIDAS_REWARD_DB_USER") || "postgres",
  password: System.get_env("AIMIDAS_REWARD_DB_PASSWORD") || "postgres",
  database: System.get_env("AIMIDAS_REWARD_DB_NAME") || "aimidas_reward",
  port: System.get_env("AIMIDAS_REWARD_DB_PORT") || 5432,
  pool_size: 15

