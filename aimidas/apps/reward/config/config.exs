# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :reward,
  namespace: Reward,
  ecto_repos: [Reward.Repo]

# Configures the endpoint
config :reward, RewardWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XSkMZBRfA+PPQt6d0OwQcSAx7Z5J3QR9q7cjSeUsP4FyGZyAYPbCI+Acfds9pXPQ",
  render_errors: [view: RewardWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Reward.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]
  
# http
config :reward, RewardWeb.Endpoint,
  http: [port: 14050],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../assets", __DIR__)]]
# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
