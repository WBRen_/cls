defmodule Reward.Repo.Migrations.CreateAnswers do
  use Ecto.Migration

  def change do
    create table(:answers) do
      add :question_id, :integer
      add :type, :integer
      add :content, :string
      add :like, :integer
      add :dislike, :integer

      timestamps()
    end

  end
end
