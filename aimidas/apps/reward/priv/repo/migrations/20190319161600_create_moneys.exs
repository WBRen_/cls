defmodule Reward.Repo.Migrations.CreateMoneys do
  use Ecto.Migration

  def change do
    create table(:moneys) do
      add :status, :integer
      add :question_id, :integer
      add :user_id, :integer
      add :price, :float
      add :balance, :float
      add :start_time, :date
      add :valid_period, :date
      add :end_time, :date

      timestamps()
    end

  end
end
