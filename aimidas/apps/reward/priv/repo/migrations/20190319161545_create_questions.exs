defmodule Reward.Repo.Migrations.CreateQuestions do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :question, :string
      add :user_id, :integer
      add :out_time, :boolean, default: false, null: false
      add :context, :string
      add :release_time, :date

      timestamps()
    end

  end
end
