defmodule Reward.Repo.Migrations.CreateDispatchRecords do
  use Ecto.Migration

  def change do
    create table(:dispatch_records) do
      add :reward_id, :integer
      add :to_user_id, :integer
      add :answer_id, :integer
      add :price, :float

      timestamps()
    end

  end
end
