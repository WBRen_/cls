defmodule Reward.Repo.Migrations.CreateAnswerDiscussions do
  use Ecto.Migration

  def change do
    create table(:answer_discussions) do
      add :answer_id, :integer
      add :user_id, :integer
      add :content, :string
      add :like, :integer
      add :unlike, :integer

      timestamps()
    end

  end
end
