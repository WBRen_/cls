defmodule RewardWeb.Router do
  use RewardWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug RewardWeb.Context # 会验证小程序登陆状态，Bearer
  end

  # Other scopes may use custom stacks.
  scope "/app/api/reward", RewardWeb do
    pipe_through :api
    # get "/xxx", XXXController, :method_name
    
    resources "/questions", QuestionController, only: [:index, :show, :create, :update, :delete]
    
    resources "/questions/:question_id/answers", AnswerController, only: [:index, :show, :create, :update, :delete]
    # or:
    # resources "/answers", AnswerController, only: [:index, :show, :create, :update, :delete]
    
    resources "/questions/:question_id/answers/:answer_id/discussions", AnswerDiscussionController, only: [:index, :show, :create, :update, :delete]
    # or:
    # resources "/answer_discussions", AnswerDiscussionController, only: [:index, :show, :create, :update, :delete]
    
    resources "/reward_money", MoneyController, only: [:index, :show, :create, :update, :delete]
    
    resources "/dispatch_records", DispatchRecordController, only: [:index, :show, :create, :update, :delete]
  end
end
