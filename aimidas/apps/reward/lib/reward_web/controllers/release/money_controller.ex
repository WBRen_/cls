defmodule RewardWeb.MoneyController do
  use RewardWeb, :controller

  alias Reward.RewardRp
  alias Reward.RewardRp.Money

  action_fallback RewardWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    moneys = RewardRp.page_moneys(page)
    render(conn, "page.json", page: moneys)
  end

  def create(conn, %{"money" => money_params}) do
    with {:ok, %Money{} = money} <- RewardRp.create_money(money_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", money_path(conn, :show, money))
      |> render("show.json", money: money)
    end
  end

  def show(conn, %{"id" => id}) do
    money = RewardRp.get_money!(id)
    render(conn, "show.json", money: money)
  end

  def update(conn, %{"id" => id, "money" => money_params}) do
    money = RewardRp.get_money!(id)

    with {:ok, %Money{} = money} <- RewardRp.update_money(money, money_params) do
      render(conn, "show.json", money: money)
    end
  end

  def delete(conn, %{"id" => id}) do
    money = RewardRp.get_money!(id)
    with {:ok, %Money{}} <- RewardRp.delete_money(money) do
      send_resp(conn, :no_content, "")
    end
  end
end
