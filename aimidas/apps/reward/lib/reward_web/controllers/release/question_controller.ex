defmodule RewardWeb.QuestionController do
  use RewardWeb, :controller

  alias Reward.RewardRp
  alias Reward.RewardRp.Question

  action_fallback RewardWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    questions = RewardRp.page_questions(page)
    render(conn, "page.json", page: questions)
  end

  def create(conn, %{"question" => question_params}) do
    with {:ok, %Question{} = question} <- RewardRp.create_question(question_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", question_path(conn, :show, question))
      |> render("show.json", question: question)
    end
  end

  def show(conn, %{"id" => id}) do
    question = RewardRp.get_question!(id)
    render(conn, "show.json", question: question)
  end

  def update(conn, %{"id" => id, "question" => question_params}) do
    question = RewardRp.get_question!(id)

    with {:ok, %Question{} = question} <- RewardRp.update_question(question, question_params) do
      render(conn, "show.json", question: question)
    end
  end

  def delete(conn, %{"id" => id}) do
    question = RewardRp.get_question!(id)
    with {:ok, %Question{}} <- RewardRp.delete_question(question) do
      send_resp(conn, :no_content, "")
    end
  end
end
