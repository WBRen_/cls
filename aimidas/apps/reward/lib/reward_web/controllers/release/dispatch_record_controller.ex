defmodule RewardWeb.DispatchRecordController do
  use RewardWeb, :controller

  alias Reward.RewardRp
  alias Reward.RewardRp.DispatchRecord

  action_fallback RewardWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    dispatch_records = RewardRp.page_dispatch_records(page)
    render(conn, "page.json", page: dispatch_records)
  end

  def create(conn, %{"dispatch_record" => dispatch_record_params}) do
    with {:ok, %DispatchRecord{} = dispatch_record} <- RewardRp.create_dispatch_record(dispatch_record_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", dispatch_record_path(conn, :show, dispatch_record))
      |> render("show.json", dispatch_record: dispatch_record)
    end
  end

  def show(conn, %{"id" => id}) do
    dispatch_record = RewardRp.get_dispatch_record!(id)
    render(conn, "show.json", dispatch_record: dispatch_record)
  end

  def update(conn, %{"id" => id, "dispatch_record" => dispatch_record_params}) do
    dispatch_record = RewardRp.get_dispatch_record!(id)

    with {:ok, %DispatchRecord{} = dispatch_record} <- RewardRp.update_dispatch_record(dispatch_record, dispatch_record_params) do
      render(conn, "show.json", dispatch_record: dispatch_record)
    end
  end

  def delete(conn, %{"id" => id}) do
    dispatch_record = RewardRp.get_dispatch_record!(id)
    with {:ok, %DispatchRecord{}} <- RewardRp.delete_dispatch_record(dispatch_record) do
      send_resp(conn, :no_content, "")
    end
  end
end
