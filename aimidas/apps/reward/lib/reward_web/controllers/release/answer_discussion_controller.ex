defmodule RewardWeb.AnswerDiscussionController do
  use RewardWeb, :controller

  alias Reward.RewardRp
  alias Reward.RewardRp.AnswerDiscussion

  action_fallback RewardWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    answer_discussions = RewardRp.page_answer_discussions(page)
    render(conn, "page.json", page: answer_discussions)
  end

  def create(conn, %{"answer_discussion" => answer_discussion_params}) do
    with {:ok, %AnswerDiscussion{} = answer_discussion} <- RewardRp.create_answer_discussion(answer_discussion_params) do
      conn
      |> put_status(:created)
      # |> put_resp_header("location", answer_discussion_path(conn, :show, answer_discussion))
      |> render("show.json", answer_discussion: answer_discussion)
    end
  end

  def show(conn, %{"id" => id}) do
    answer_discussion = RewardRp.get_answer_discussion!(id)
    render(conn, "show.json", answer_discussion: answer_discussion)
  end

  def update(conn, %{"id" => id, "answer_discussion" => answer_discussion_params}) do
    answer_discussion = RewardRp.get_answer_discussion!(id)

    with {:ok, %AnswerDiscussion{} = answer_discussion} <- RewardRp.update_answer_discussion(answer_discussion, answer_discussion_params) do
      render(conn, "show.json", answer_discussion: answer_discussion)
    end
  end

  def delete(conn, %{"id" => id}) do
    answer_discussion = RewardRp.get_answer_discussion!(id)
    with {:ok, %AnswerDiscussion{}} <- RewardRp.delete_answer_discussion(answer_discussion) do
      send_resp(conn, :no_content, "")
    end
  end
end
