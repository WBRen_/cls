defmodule RewardWeb.AnswerController do
  use RewardWeb, :controller

  alias Reward.RewardRp
  alias Reward.RewardRp.Answer

  action_fallback RewardWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    answers = RewardRp.page_answers(page)
    render(conn, "page.json", page: answers)
  end

  def create(conn, %{"answer" => answer_params}) do
    with {:ok, %Answer{} = answer} <- RewardRp.create_answer(answer_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", answer_path(conn, :show, answer))
      |> render("show.json", answer: answer)
    end
  end

  def show(conn, %{"id" => id}) do
    answer = RewardRp.get_answer!(id)
    render(conn, "show.json", answer: answer)
  end

  def update(conn, %{"id" => id, "answer" => answer_params}) do
    answer = RewardRp.get_answer!(id)

    with {:ok, %Answer{} = answer} <- RewardRp.update_answer(answer, answer_params) do
      render(conn, "show.json", answer: answer)
    end
  end

  def delete(conn, %{"id" => id}) do
    answer = RewardRp.get_answer!(id)
    with {:ok, %Answer{}} <- RewardRp.delete_answer(answer) do
      send_resp(conn, :no_content, "")
    end
  end
end
