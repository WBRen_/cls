defmodule RewardWeb.AnswerDiscussionView do
  use RewardWeb, :view
  alias RewardWeb.AnswerDiscussionView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, AnswerDiscussionView, "answer_discussion.json")
    }
  end

  def render("index.json", %{answer_discussions: answer_discussions}) do
    %{data: render_many(answer_discussions, AnswerDiscussionView, "answer_discussion.json")}
  end

  def render("show.json", %{answer_discussion: answer_discussion}) do
    %{data: render_one(answer_discussion, AnswerDiscussionView, "answer_discussion.json")}
  end

  def render("answer_discussion.json", %{answer_discussion: answer_discussion}) do
    %{
      id: answer_discussion.id,
      answer_id: answer_discussion.answer_id,
      user_id: answer_discussion.user_id,
      content: answer_discussion.content,
      like: answer_discussion.like,
      unlike: answer_discussion.unlike,
      updated_at: answer_discussion.updated_at,
      inserted_at: answer_discussion.inserted_at
    }
  end
end
