defmodule RewardWeb.MoneyView do
  use RewardWeb, :view
  alias RewardWeb.MoneyView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, MoneyView, "money.json")
    }
  end

  def render("index.json", %{moneys: moneys}) do
    %{data: render_many(moneys, MoneyView, "money.json")}
  end

  def render("show.json", %{money: money}) do
    %{data: render_one(money, MoneyView, "money.json")}
  end

  def render("money.json", %{money: money}) do
    %{
      id: money.id,
      status: money.status,
      question_id: money.question_id,
      user_id: money.user_id,
      price: money.price,
      balance: money.balance,
      start_time: money.start_time,
      valid_period: money.valid_period,
      end_time: money.end_time,
      updated_at: money.updated_at,
      inserted_at: money.inserted_at
    }
  end
end
