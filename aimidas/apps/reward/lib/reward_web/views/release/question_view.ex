defmodule RewardWeb.QuestionView do
  use RewardWeb, :view
  alias RewardWeb.QuestionView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, QuestionView, "question.json")
    }
  end

  def render("index.json", %{questions: questions}) do
    %{data: render_many(questions, QuestionView, "question.json")}
  end

  def render("show.json", %{question: question}) do
    %{data: render_one(question, QuestionView, "question.json")}
  end

  def render("question.json", %{question: question}) do
    %{
      id: question.id,
      question: question.question,
      user_id: question.user_id,
      out_time: question.out_time,
      context: question.context,
      release_time: question.release_time,
      updated_at: question.updated_at,
      inserted_at: question.inserted_at
    }
  end
end
