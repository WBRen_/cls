defmodule RewardWeb.AnswerView do
  use RewardWeb, :view
  alias RewardWeb.AnswerView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, AnswerView, "answer.json")
    }
  end

  def render("index.json", %{answers: answers}) do
    %{data: render_many(answers, AnswerView, "answer.json")}
  end

  def render("show.json", %{answer: answer}) do
    %{data: render_one(answer, AnswerView, "answer.json")}
  end

  def render("answer.json", %{answer: answer}) do
    %{
      id: answer.id,
      question_id: answer.question_id,
      type: answer.type,
      content: answer.content,
      like: answer.like,
      dislike: answer.dislike,
      updated_at: answer.updated_at,
      inserted_at: answer.inserted_at
    }
  end
end
