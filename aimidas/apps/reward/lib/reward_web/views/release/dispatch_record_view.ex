defmodule RewardWeb.DispatchRecordView do
  use RewardWeb, :view
  alias RewardWeb.DispatchRecordView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, DispatchRecordView, "dispatch_record.json")
    }
  end

  def render("index.json", %{dispatch_records: dispatch_records}) do
    %{data: render_many(dispatch_records, DispatchRecordView, "dispatch_record.json")}
  end

  def render("show.json", %{dispatch_record: dispatch_record}) do
    %{data: render_one(dispatch_record, DispatchRecordView, "dispatch_record.json")}
  end

  def render("dispatch_record.json", %{dispatch_record: dispatch_record}) do
    %{
      id: dispatch_record.id,
      reward_id: dispatch_record.reward_id,
      to_user_id: dispatch_record.to_user_id,
      answer_id: dispatch_record.answer_id,
      price: dispatch_record.price,
      updated_at: dispatch_record.updated_at,
      inserted_at: dispatch_record.inserted_at
    }
  end
end
