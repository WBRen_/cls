defmodule RewardWeb.Context do
  @moduledoc """
  RewardWeb Context
  用于提供当前登录用户信息 :current_user
  """

  @behaviour Plug

  import Plug.Conn

  alias AimidasWeb.Guardian, as: AGuardian
  alias Aimidas.Accounts

  def init(opts), do: opts

  def call(conn, _) do
    if user = build_context(conn) do
      conn |> put_session(:user, user)
    else
      conn |> resp(401, "authorization fail") |> halt()
    end
    # Absinthe.Plug.put_options(conn, context: context)
  end

  @doc """
  Return the current user context based on the authorization header
  """
  def build_context(conn) do
    current_user = with {:ok, token} <- get_token(conn) do
      get_current_user(token)
    end

    # case current_user do
    #   nil ->
    #     nil
    #   _ ->
    #     current_user
    # end
  end

  defp get_token(conn) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> token] -> {:ok, token}
      _ -> nil
    end
  end

  defp get_current_user("user_" <> user_id) do
    Accounts.get_user(user_id)
  end

  defp get_current_user(token) do
    case AGuardian.decode_and_verify(token) do
      {:ok, claims} ->
        Accounts.get_user(claims["sub"])
      _ ->
        nil
    end
  end
end
