defmodule Reward.RewardRp do
  @moduledoc """
  The RewardRp context.
  """

  import Ecto.Query, warn: false
  alias Reward.Repo

  alias Reward.RewardRp.Question


  @doc """
  Returns the page of questions.

  ## Examples

      iex> page_questions(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%Question{}, ...]
      }

  """
  def page_questions(page) do
    Question
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of questions.

  ## Examples

      iex> list_questions()
      [%Question{}, ...]

  """
  def list_questions do
    Repo.all(Question)
  end

  @doc """
  Gets a single question.

  Raises `Ecto.NoResultsError` if the Question does not exist.

  ## Examples

      iex> get_question!(123)
      %Question{}

      iex> get_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_question!(id), do: Repo.get!(Question, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_question(id) do
    Question
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a question.

  ## Examples

      iex> create_question(%{field: value})
      {:ok, %Question{}}

      iex> create_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_question(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %Question{}
    |> Question.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a question.

  ## Examples

      iex> update_question(question, %{field: new_value})
      {:ok, %Question{}}

      iex> update_question(question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_question(%Question{} = question, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    question
    |> Question.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a Question.

  ## Examples

      iex> delete_question(question)
      {:ok, %Question{}}

      iex> delete_question(question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_question(%Question{} = question) do
    Repo.delete(question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking question changes.

  ## Examples

      iex> change_question(question)
      %Ecto.Changeset{source: %Question{}}

  """
  def change_question(%Question{} = question) do
    Question.changeset(question, %{})
  end

  alias Reward.RewardRp.Answer


  @doc """
  Returns the page of answers.

  ## Examples

      iex> page_answers(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%Answer{}, ...]
      }

  """
  def page_answers(page) do
    Answer
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of answers.

  ## Examples

      iex> list_answers()
      [%Answer{}, ...]

  """
  def list_answers do
    Repo.all(Answer)
  end

  @doc """
  Gets a single answer.

  Raises `Ecto.NoResultsError` if the Answer does not exist.

  ## Examples

      iex> get_answer!(123)
      %Answer{}

      iex> get_answer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_answer!(id), do: Repo.get!(Answer, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_answer(id) do
    Answer
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a answer.

  ## Examples

      iex> create_answer(%{field: value})
      {:ok, %Answer{}}

      iex> create_answer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_answer(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %Answer{}
    |> Answer.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a answer.

  ## Examples

      iex> update_answer(answer, %{field: new_value})
      {:ok, %Answer{}}

      iex> update_answer(answer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_answer(%Answer{} = answer, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    answer
    |> Answer.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a Answer.

  ## Examples

      iex> delete_answer(answer)
      {:ok, %Answer{}}

      iex> delete_answer(answer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_answer(%Answer{} = answer) do
    Repo.delete(answer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking answer changes.

  ## Examples

      iex> change_answer(answer)
      %Ecto.Changeset{source: %Answer{}}

  """
  def change_answer(%Answer{} = answer) do
    Answer.changeset(answer, %{})
  end

  alias Reward.RewardRp.Money


  @doc """
  Returns the page of moneys.

  ## Examples

      iex> page_moneys(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%Money{}, ...]
      }

  """
  def page_moneys(page) do
    Money
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of moneys.

  ## Examples

      iex> list_moneys()
      [%Money{}, ...]

  """
  def list_moneys do
    Repo.all(Money)
  end

  @doc """
  Gets a single money.

  Raises `Ecto.NoResultsError` if the Money does not exist.

  ## Examples

      iex> get_money!(123)
      %Money{}

      iex> get_money!(456)
      ** (Ecto.NoResultsError)

  """
  def get_money!(id), do: Repo.get!(Money, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_money(id) do
    Money
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a money.

  ## Examples

      iex> create_money(%{field: value})
      {:ok, %Money{}}

      iex> create_money(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_money(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %Money{}
    |> Money.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a money.

  ## Examples

      iex> update_money(money, %{field: new_value})
      {:ok, %Money{}}

      iex> update_money(money, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_money(%Money{} = money, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    money
    |> Money.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a Money.

  ## Examples

      iex> delete_money(money)
      {:ok, %Money{}}

      iex> delete_money(money)
      {:error, %Ecto.Changeset{}}

  """
  def delete_money(%Money{} = money) do
    Repo.delete(money)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking money changes.

  ## Examples

      iex> change_money(money)
      %Ecto.Changeset{source: %Money{}}

  """
  def change_money(%Money{} = money) do
    Money.changeset(money, %{})
  end

  alias Reward.RewardRp.DispatchRecord


  @doc """
  Returns the page of dispatch_records.

  ## Examples

      iex> page_dispatch_records(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%DispatchRecord{}, ...]
      }

  """
  def page_dispatch_records(page) do
    DispatchRecord
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of dispatch_records.

  ## Examples

      iex> list_dispatch_records()
      [%DispatchRecord{}, ...]

  """
  def list_dispatch_records do
    Repo.all(DispatchRecord)
  end

  @doc """
  Gets a single dispatch_record.

  Raises `Ecto.NoResultsError` if the Dispatch record does not exist.

  ## Examples

      iex> get_dispatch_record!(123)
      %DispatchRecord{}

      iex> get_dispatch_record!(456)
      ** (Ecto.NoResultsError)

  """
  def get_dispatch_record!(id), do: Repo.get!(DispatchRecord, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_dispatch_record(id) do
    DispatchRecord
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a dispatch_record.

  ## Examples

      iex> create_dispatch_record(%{field: value})
      {:ok, %DispatchRecord{}}

      iex> create_dispatch_record(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_dispatch_record(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %DispatchRecord{}
    |> DispatchRecord.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a dispatch_record.

  ## Examples

      iex> update_dispatch_record(dispatch_record, %{field: new_value})
      {:ok, %DispatchRecord{}}

      iex> update_dispatch_record(dispatch_record, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_dispatch_record(%DispatchRecord{} = dispatch_record, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    dispatch_record
    |> DispatchRecord.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a DispatchRecord.

  ## Examples

      iex> delete_dispatch_record(dispatch_record)
      {:ok, %DispatchRecord{}}

      iex> delete_dispatch_record(dispatch_record)
      {:error, %Ecto.Changeset{}}

  """
  def delete_dispatch_record(%DispatchRecord{} = dispatch_record) do
    Repo.delete(dispatch_record)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking dispatch_record changes.

  ## Examples

      iex> change_dispatch_record(dispatch_record)
      %Ecto.Changeset{source: %DispatchRecord{}}

  """
  def change_dispatch_record(%DispatchRecord{} = dispatch_record) do
    DispatchRecord.changeset(dispatch_record, %{})
  end

  alias Reward.RewardRp.AnswerDiscussion


  @doc """
  Returns the page of answer_discussions.

  ## Examples

      iex> page_answer_discussions(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%AnswerDiscussion{}, ...]
      }

  """
  def page_answer_discussions(page) do
    AnswerDiscussion
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of answer_discussions.

  ## Examples

      iex> list_answer_discussions()
      [%AnswerDiscussion{}, ...]

  """
  def list_answer_discussions do
    Repo.all(AnswerDiscussion)
  end

  @doc """
  Gets a single answer_discussion.

  Raises `Ecto.NoResultsError` if the Answer discussion does not exist.

  ## Examples

      iex> get_answer_discussion!(123)
      %AnswerDiscussion{}

      iex> get_answer_discussion!(456)
      ** (Ecto.NoResultsError)

  """
  def get_answer_discussion!(id), do: Repo.get!(AnswerDiscussion, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_answer_discussion(id) do
    AnswerDiscussion
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a answer_discussion.

  ## Examples

      iex> create_answer_discussion(%{field: value})
      {:ok, %AnswerDiscussion{}}

      iex> create_answer_discussion(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_answer_discussion(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %AnswerDiscussion{}
    |> AnswerDiscussion.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a answer_discussion.

  ## Examples

      iex> update_answer_discussion(answer_discussion, %{field: new_value})
      {:ok, %AnswerDiscussion{}}

      iex> update_answer_discussion(answer_discussion, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_answer_discussion(%AnswerDiscussion{} = answer_discussion, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    answer_discussion
    |> AnswerDiscussion.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a AnswerDiscussion.

  ## Examples

      iex> delete_answer_discussion(answer_discussion)
      {:ok, %AnswerDiscussion{}}

      iex> delete_answer_discussion(answer_discussion)
      {:error, %Ecto.Changeset{}}

  """
  def delete_answer_discussion(%AnswerDiscussion{} = answer_discussion) do
    Repo.delete(answer_discussion)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking answer_discussion changes.

  ## Examples

      iex> change_answer_discussion(answer_discussion)
      %Ecto.Changeset{source: %AnswerDiscussion{}}

  """
  def change_answer_discussion(%AnswerDiscussion{} = answer_discussion) do
    AnswerDiscussion.changeset(answer_discussion, %{})
  end
end
