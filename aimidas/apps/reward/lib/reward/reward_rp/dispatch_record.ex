defmodule Reward.RewardRp.DispatchRecord do
  use Ecto.Schema
  import Ecto.Changeset


  schema "dispatch_records" do
    field :answer_id, :integer
    field :price, :float
    field :reward_id, :integer
    field :to_user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(dispatch_record, attrs) do
    dispatch_record
    |> cast(attrs, [:reward_id, :to_user_id, :answer_id, :price, :inserted_at, :updated_at])
    |> validate_required([:reward_id, :to_user_id, :answer_id, :price])
  end
end
