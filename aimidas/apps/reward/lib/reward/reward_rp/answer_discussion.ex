defmodule Reward.RewardRp.AnswerDiscussion do
  use Ecto.Schema
  import Ecto.Changeset


  schema "answer_discussions" do
    field :answer_id, :integer
    field :content, :string
    field :like, :integer
    field :unlike, :integer
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(answer_discussion, attrs) do
    answer_discussion
    |> cast(attrs, [:answer_id, :user_id, :content, :like, :unlike, :inserted_at, :updated_at])
    |> validate_required([:answer_id, :user_id, :content, :like, :unlike])
  end
end
