defmodule Reward.RewardRp.Money do
  use Ecto.Schema
  import Ecto.Changeset


  schema "moneys" do
    field :balance, :float
    field :end_time, :date
    field :price, :float
    field :question_id, :integer
    field :start_time, :date
    field :status, :integer
    field :user_id, :integer
    field :valid_period, :date

    timestamps()
  end

  @doc false
  def changeset(money, attrs) do
    money
    |> cast(attrs, [:status, :question_id, :user_id, :price, :balance, :start_time, :valid_period, :end_time, :inserted_at, :updated_at])
    |> validate_required([:status, :question_id, :user_id, :price, :balance, :start_time, :valid_period, :end_time])
  end
end
