defmodule Reward.RewardRp.Answer do
  use Ecto.Schema
  import Ecto.Changeset


  schema "answers" do
    field :content, :string
    field :dislike, :integer
    field :like, :integer
    field :question_id, :integer
    field :type, :integer

    timestamps()
  end

  @doc false
  def changeset(answer, attrs) do
    answer
    |> cast(attrs, [:question_id, :type, :content, :like, :dislike, :inserted_at, :updated_at])
    |> validate_required([:question_id, :type, :content, :like, :dislike])
  end
end
