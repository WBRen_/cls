defmodule Reward.RewardRp.Question do
  use Ecto.Schema
  import Ecto.Changeset


  schema "questions" do
    field :context, :string
    field :out_time, :boolean, default: false
    field :question, :string
    field :release_time, :date
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:question, :user_id, :out_time, :context, :release_time, :inserted_at, :updated_at])
    |> validate_required([:question, :user_id, :out_time, :context, :release_time])
  end
end
