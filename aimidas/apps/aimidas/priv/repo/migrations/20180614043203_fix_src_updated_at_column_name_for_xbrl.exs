defmodule Aimidas.Repo.Migrations.FixSrcUpdatedAtColumnNameForXbrl do
  use Ecto.Migration

  def up do
    rename table(:company_xbrls), :src_udpated_at, to: :src_updated_at
  end

  def down do
    rename table(:company_xbrls), :src_updated_at, to: :src_udpated_at
  end
end
