defmodule :"Elixir.Aimidas.Repo.Migrations.Add-exchangeTypeCode-to-stocks" do
  use Ecto.Migration

  def up do
    alter table(:stocks) do
      add :exchange_type_code, :string
    end

    drop index(:stocks, [:code])
    create index(:stocks, [:code, :exchange_type_code], unique: true)
  end

  def down do
    drop index(:stocks, [:code, :exchange_type_code])

    alter table(:stocks) do
      remove :exchange_type_code
    end

    create index(:stocks, [:code], unique: true)
  end
end
