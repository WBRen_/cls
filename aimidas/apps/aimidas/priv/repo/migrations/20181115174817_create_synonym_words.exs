defmodule Aimidas.Repo.Migrations.CreateSynonymWords do
  use Ecto.Migration

  def change do
    create table(:syn_words) do
      add :word, :string
      add :synonym_words, :string

      timestamps()
    end

  end
end
