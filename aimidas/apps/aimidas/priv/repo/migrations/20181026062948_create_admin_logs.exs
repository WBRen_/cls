defmodule Aimidas.Repo.Migrations.CreateAdminLogs do
  use Ecto.Migration

  def change do
    create table(:admin_logs) do
      add :type, :string
      add :params, :text
      add :status, :string
      add :user_id, references(:admin_users, on_delete: :nothing) # delete_all

      timestamps()
    end
  end
end
