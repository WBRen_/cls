defmodule Aimidas.Repo.Migrations.AlertQnaQuestions do
  use Ecto.Migration

  def change do
    alter table(:pro_qna_questions) do
      modify :answer, :text
    end
  end
end
