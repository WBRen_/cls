defmodule Aimidas.Repo.Migrations.AlertDmQnaQuestions do
  use Ecto.Migration

  def change do
    alter table(:dm_qna_questions) do
      modify :answer, :text
    end
  end
end
