defmodule Aimidas.Repo.Migrations.AddIndexToCompanyInfos do
  use Ecto.Migration

  def up do
    create index(:company_infos, [:company_code, :year], unique: true)
  end

  def down do
    drop index(:company_infos, [:company_code, :year])
  end
end
