defmodule Aimidas.Repo.Migrations.AlterPdfpages do
  use Ecto.Migration

  def change do
    alter table(:pdf_pages) do
      # older:
      # add :content, :text
      # add :start_page, :integer
      # add :end_page, :integer
      # add :image_url, :string
      # add :company_code, :string
      # add :year, :integer
      # newer:
      remove :quarter
      remove :is_abstract
      add :src_updated_at, :integer
      add :type_code, :string
      add :detail_code, :string

    end
    # drop index(:pdf_pages, [:company_code, :year, :quarter, :is_abstract, :start_page, :end_page], unique: true)
    create index(:pdf_pages, [:company_code, :year, :src_updated_at, :type_code, :detail_code, :start_page, :end_page], unique: true)

  end
end
