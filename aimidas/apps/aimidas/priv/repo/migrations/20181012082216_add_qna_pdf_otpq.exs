defmodule Aimidas.Repo.Migrations.AddQnaPdfOtpq do
  use Ecto.Migration

  def change do
    alter table(:pdf_qna_questions) do
      add :otpq, :map
      remove :query
    end
  end
end
