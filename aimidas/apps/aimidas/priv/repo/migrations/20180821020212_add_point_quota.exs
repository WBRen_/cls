defmodule Aimidas.Repo.Migrations.AddPointQuota do
  use Ecto.Migration

  def change do
    alter table(:points) do
      # 用户每日问题限额
      add :quota, :integer, default: 3
    end
  end
end
