defmodule Aimidas.Repo.Migrations.CreateQaTable do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :query, :string
      add :answer, :string
      add :answer_level, :integer
      add :last_otpq, :map
      add :otpq, :map
      add :question_group_id, :integer
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create table(:question_groups) do
      add :name, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:question_groups, [:user_id, :name], unique: true)

  end
end
