defmodule Aimidas.Repo.Migrations.CreatTradeTableAndUpdate do
  use Ecto.Migration

  def change do
      create table(:portfolio_item_trades) do
        add :portfolio_item_id, references(:portfolio_items, on_delete: :nothing)
        add :type, :string
        add :quantity, :integer
        add :price, :decimal

        timestamps()
      end

      alter table(:portfolio_items) do
        add :total_quantity, :integer
        add :average_price, :decimal
        add :total_price, :decimal
      end

      alter table(:portfolios) do
        add :total_price, :decimal
      end
  end
end
