defmodule Aimidas.Repo.Migrations.CreateTongjiAnswer do
  use Ecto.Migration

  def change do
    create table(:user_answers) do
      add :answer, :string
      add :stars, :integer, default: 0
      add :contras, :integer, default: 0

      add :question_id, :integer
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
  end
end
