defmodule Aimidas.Repo.Migrations.CreateDmQna do
  use Ecto.Migration

  def change do
    create table(:dm_qna_questions) do
      add :md5, :string
      add :query, :string
      add :answer, :string

      timestamps()
    end

    create index(:dm_qna_questions, [:md5])
  end
end
