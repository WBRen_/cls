defmodule Aimidas.Repo.Migrations.CreatePortfolioItems do
  use Ecto.Migration

  def change do
    create table(:portfolio_items) do
      add :user_id, references(:users, on_delete: :nothing)
      add :portfolio_id, references(:portfolios, on_delete: :nothing)
      add :stock_id, references(:stocks, on_delete: :nothing)
      # add :user_id, :integer
      # add :portfolio_id, :integer
      # add :stock_id, :integer

      timestamps()
    end

    create index(:portfolio_items, [:user_id])
    create index(:portfolio_items, [:portfolio_id, :stock_id], unique: true)
  end
end
