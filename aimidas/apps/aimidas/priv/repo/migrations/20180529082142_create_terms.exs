defmodule Aimidas.Repo.Migrations.CreateTerms do
  use Ecto.Migration

  def change do
    create table(:terms) do
      add :name, :string
      add :description, :text

      timestamps()
    end

  end
end
