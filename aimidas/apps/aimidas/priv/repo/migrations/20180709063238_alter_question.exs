defmodule Aimidas.Repo.Migrations.AlterQuestion do
  use Ecto.Migration

  def up do
    alter table(:questions) do
      remove :last_otpq
    end
  end

  def down do
  end
end
