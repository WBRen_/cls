defmodule Aimidas.Repo.Migrations.CreatePdfPages do
  use Ecto.Migration

  def change do
    create table(:pdf_pages) do
      add :content, :text
      add :start_page, :integer
      add :end_page, :integer
      add :image_url, :string
      add :company_code, :string
      add :year, :integer
      add :quarter, :integer
      add :is_abstract, :integer

      timestamps()
    end

    create index(:pdf_pages, [:company_code, :year, :quarter, :is_abstract, :start_page, :end_page], unique: true)
  end
end
