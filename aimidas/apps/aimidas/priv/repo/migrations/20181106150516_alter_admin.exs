defmodule Aimidas.Repo.Migrations.AlterAdmin do
  use Ecto.Migration

  def change do
    alter table(:admin_logs) do
      remove :user_id
      add :user_id, references(:admin_users, on_delete: :delete_all)
    end
  end
end
