defmodule Aimidas.Repo.Migrations.AddSourceAndUpdatedAtToCompanyInfos do
  use Ecto.Migration

  def up do
    alter table(:company_infos) do
      add :source, :string
      add :info_updated_at, :date
    end
  end

  def down do
    alter table(:company_infos) do
      remove :source
      remove :info_updated_at
    end
  end
end
