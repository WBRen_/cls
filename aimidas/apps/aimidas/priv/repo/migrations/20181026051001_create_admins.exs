defmodule Aimidas.Repo.Migrations.CreateAdmins do
  use Ecto.Migration

  def change do
    create table(:admin_users) do
      add :name, :string
      add :password, :string
      add :level, :integer
      add :auths, {:array, :string}

      timestamps()
    end
  end
end
