defmodule Aimidas.Repo.Migrations.AlterDmQna do
  use Ecto.Migration

  def change do
    alter table(:dm_qna_questions) do
      modify :query, :text
    end
  end
end
