defmodule Aimidas.Repo.Migrations.AddQuestionsData do
  use Ecto.Migration

  def change do
    alter table(:questions) do
      # 图表数据
      add :data, :map
    end
  end
end
