defmodule Aimidas.Repo.Migrations.AlterUserAnswerText do
  use Ecto.Migration

  def change do
    alter table(:user_answers) do
      modify :answer, :text
    end
  end
end
