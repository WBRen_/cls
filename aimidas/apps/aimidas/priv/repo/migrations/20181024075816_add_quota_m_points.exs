defmodule Aimidas.Repo.Migrations.AddQuotaMPoints do
  use Ecto.Migration

  def change do
    alter table(:points) do
      add :current_quota_use, :integer, default: 0
    end
    create index(:points, [:user_id, :current_quota_use, :point, :quota], unique: true)

  end
end
