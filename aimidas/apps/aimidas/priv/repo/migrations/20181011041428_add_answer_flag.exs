defmodule Aimidas.Repo.Migrations.AddAnswerFlag do
  use Ecto.Migration

  def change do
    alter table(:user_answers) do
      add :status, :integer, default: 0
    end
  end
end
