defmodule Aimidas.Repo.Migrations.CreateTongjiQuestion do
  use Ecto.Migration

  def change do
    create table(:normalized_questions) do
      add :md5, :string
      add :query, :string
      add :otpq, :map
      add :ask_count, :integer, default: 0

      timestamps()
    end
    create index(:normalized_questions, [:md5], unique: true)

  end
end
