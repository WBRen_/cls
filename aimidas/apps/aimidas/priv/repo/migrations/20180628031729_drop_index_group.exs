defmodule Aimidas.Repo.Migrations.DropIndexGroup do
  use Ecto.Migration

  def up do
    drop index(:question_groups, [:user_id, :name], unique: true)
  end

  def down do
    create index(:question_groups, [:user_id, :name], unique: true)
  end
end
