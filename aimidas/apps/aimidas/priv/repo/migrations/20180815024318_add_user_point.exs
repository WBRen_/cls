defmodule Aimidas.Repo.Migrations.AddUserPoint do
  use Ecto.Migration

  def change do
    create table(:points) do
      add :point, :integer, default: 0
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
  end
end
