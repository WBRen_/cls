defmodule Aimidas.Repo.Migrations.CreatePointConfig do
  use Ecto.Migration

  def change do
    create table(:configs) do
      add :key_name, :string
      add :value, :string
    end
  end
end
