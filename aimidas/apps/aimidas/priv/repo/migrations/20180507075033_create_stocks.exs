defmodule Aimidas.Repo.Migrations.CreateStocks do
  use Ecto.Migration

  def change do
    create table(:stocks) do
      add :code, :string
      add :name, :string
      add :last_px, :decimal
      add :last_px_at, :utc_datetime
      add :px_change_rate, :decimal

      timestamps()
    end

    create index(:stocks, [:code], unique: true)
  end
end
