defmodule Aimidas.Repo.Migrations.CreateHotQuestions do
  use Ecto.Migration

  def change do
    create table(:hot_questions) do
      add :content, :string
      add :level, :integer

      timestamps()
    end

  end
end
