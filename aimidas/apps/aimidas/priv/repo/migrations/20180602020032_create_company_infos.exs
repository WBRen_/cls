defmodule Aimidas.Repo.Migrations.CreateCompanyInfos do
  use Ecto.Migration

  def change do
    create table(:company_infos) do
      add :company_code, :string
      add :year, :integer
      add :season, :integer
      add :abbrev_name, :string
      add :full_name, :string
      add :details, :map

      timestamps()
    end

  end
end
