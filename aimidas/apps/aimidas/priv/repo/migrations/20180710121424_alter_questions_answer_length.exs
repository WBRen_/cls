defmodule Aimidas.Repo.Migrations.AlterQuestionsAnswerLength do
  use Ecto.Migration


  def up do
    alter table(:questions) do
      modify :answer, :text
    end
  end

  def down do
  end
end
