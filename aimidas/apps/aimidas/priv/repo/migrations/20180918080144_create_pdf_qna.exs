defmodule Aimidas.Repo.Migrations.CreatePdfQna do
  use Ecto.Migration

  def change do
    create table(:pdf_qna_questions) do
      add :md5, :string
      add :query, :string
      add :answer, :text

      timestamps()
    end

    create index(:pdf_qna_questions, [:md5])
  end
end
