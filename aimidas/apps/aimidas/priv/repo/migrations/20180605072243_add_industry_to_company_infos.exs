defmodule Aimidas.Repo.Migrations.AddIndustryToCompanyInfos do
  use Ecto.Migration

  def up do
    alter table(:company_infos) do
      add :industry, :string
    end
  end

  def down do
    alter table(:company_infos) do
      remove :industry
    end
  end
end
