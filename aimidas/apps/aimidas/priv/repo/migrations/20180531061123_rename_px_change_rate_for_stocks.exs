defmodule Aimidas.Repo.Migrations.RenamePxChangeRateForStocks do
  use Ecto.Migration

  def up do
    rename table(:stocks), :px_change_rate, to: :px_exchange_rate
  end

  def down do
    rename table(:stocks), :px_exchange_rate, to: :px_change_rate
  end
end
