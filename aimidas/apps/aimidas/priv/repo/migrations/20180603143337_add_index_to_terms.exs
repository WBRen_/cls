defmodule Aimidas.Repo.Migrations.AddIndexToTerms do
  use Ecto.Migration

  def up do
    create index(:terms, [:name], unique: true)
  end

  def down do
    drop index(:terms, [:name])
  end
end
