defmodule Aimidas.Repo.Migrations.CreateQuestion do
  use Ecto.Migration

  def change do
    create table(:question) do
      add :question, :string
      add :otpq, :map
      add :answer_level, :integer
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:question, [:user_id])
  end
end
