defmodule Aimidas.Repo.Migrations.CreateAdvices do
  use Ecto.Migration

  def change do
    create table(:advices) do
      add :content, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

  end
end
