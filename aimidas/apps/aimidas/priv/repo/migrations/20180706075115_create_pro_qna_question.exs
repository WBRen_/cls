defmodule Aimidas.Repo.Migrations.CreateProQnaQuestion do
  use Ecto.Migration

  def change do
    create table(:pro_qna_questions) do
      add :md5, :string
      add :query, :string
      add :answer, :string

      timestamps()
    end

    create index(:pro_qna_questions, [:md5])
  end
end
