defmodule Aimidas.Repo.Migrations.CreateAuthentications do
  use Ecto.Migration

  def change do
    create table(:authentications) do
      add :uid, :string
      add :provider, :string
      add :name, :string
      add :nickname, :string
      add :email, :string
      add :telephone, :string
      add :image, :string
      add :user_id, :integer
      add :token, :text
      add :refresh_token, :text
      add :token_secret, :text
      add :union_id, :string

      timestamps()
    end

  end
end
