defmodule Aimidas.Repo.Migrations.AddUserQuestions do
  use Ecto.Migration

  def change do
    alter table(:normalized_questions) do
      add :can_answer, :boolean, default: true
    end
  end
end
