defmodule Aimidas.Repo.Migrations.AddPointRecordTable do
  use Ecto.Migration

  def up do
    create table(:point_records) do
      add :type, :string
      add :point, :integer, default: 0
      add :quota, :integer, default: 0
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
  end

  def down do
    drop table(:point_records)
  end
end
