# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
#
# repositories directly:
#     Aimidas.Repo.insert!(%Aimidas.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

{:ok, user} = Aimidas.Accounts.create_user(%{nickname: "jjj", avatar: "image", password_digest: "aaa", password_salt: "sss"})

{:ok, portfolio} = Aimidas.Stocks.create_portfolio(%{user_id: user.id, name: "平安恒大"})
