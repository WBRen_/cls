defmodule Mix.Tasks.Aimidas.Import.Quota do
  use Mix.Task
  import Mix.Ecto
  alias Aimidas.Points

  @shortdoc "Update quota"
  def run(quota) do
    ensure_started(Aimidas.Repo, [])

    Aimidas.Accounts.list_users
    |> Enum.map(fn item ->
      item.id |> do_update(quota)
    end)
  end

  def do_update(user_id, quota) do
    Points.set_quota(user_id, quota)
    Points.create_point_record(%{user_id: user_id, type: "UpdateQuota", quota: quota, point: 0})
  end
end

