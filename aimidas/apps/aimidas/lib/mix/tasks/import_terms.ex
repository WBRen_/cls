defmodule Mix.Tasks.Aimidas.Import.Terms do
  use Mix.Task
  import Mix.Ecto
  alias Aimidas.Ontology

  @shortdoc "Import Terms"
  def run(source_file) do
    ensure_started(Aimidas.Repo, [])

    source_file
    |> File.read!()
    |> Poison.decode!()
    |> do_import()
  end

  def do_import(terms) when is_list(terms) do
    terms
    |> Enum.each(&import_term/1)
  end

  def import_term(term_raw) do
    term_raw["name"]
    |> Ontology.find_term_by_name
    |> get_term()
    |> import_term(term_raw)
  end

  def import_term(nil, %{"name" => name, "description" => description}) do
    %{
      name: name,
      description: description
    }
    |> Ontology.create_term()
  end

  def import_term(term, %{"description" => description}) do
    term
    |> Ontology.update_term(%{description: description})
  end

  defp get_term({:error, _}), do: nil
  defp get_term({:ok, term}), do: term
end
