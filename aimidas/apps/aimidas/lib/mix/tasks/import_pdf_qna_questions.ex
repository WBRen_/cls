defmodule Mix.Tasks.Aimidas.Import.PdfQnaQuestions do
  use Mix.Task
  import Mix.Ecto
  alias Aimidas.Qa

  @shortdoc "Import PdfQnaQuestions"
  def run(source_file) do
    ensure_started(Aimidas.Repo, [])

    source_file
    |> File.read!()
    |> Poison.decode!()
    |> do_import()
  end

  def do_import(questions) when is_list(questions) do
    questions
    |> Enum.each(&import_pdf_qna_question/1)
  end

  # def import_questions(question) do
  #   question.md5
  #   |> Qa.find_pdf_qna_question
  #   |> get_question()
  #   |> import_questions(question)
  # end

  # def import_questions(nil, question) do
  #   question
  #   |> Qa.create_pdf_qna_question()
  # end

  def import_pdf_qna_question(question_raw) do
    question_raw["md5"]
    |> Qa.find_pdf_qna_question
    |> get_question()
    |> import_pdf_qna_question(question_raw)
  end

  def import_pdf_qna_question(nil, %{"md5" => md5, "question" => question, "answer" => answer}) do
    %{
      md5: md5,
      query: question,
      answer: answer
    }
    |> Qa.create_pdf_qna_question()
  end

  def import_pdf_qna_question(question, %{"answer" => answer}) do
    question
    |> Qa.update_pdf_qna_question(%{answer: answer})
  end

  def delete_pdf_qna_questions(md5s) do
    md5s
    |> Qa.delete_pdf_qna_questions()
  end

  defp get_question({:error, _}), do: nil
  defp get_question({:ok, question}), do: question
end
