defmodule Mix.Tasks.Aimidas.Import.CompanyXbrls do
  use Mix.Task

  import Mix.Ecto

  alias Aimidas.Ontology

  @shortdoc "Import CompanyXbrls"
  def run(src_path) do
    ensure_started(Aimidas.Repo, [])

    src_path
    |> Path.join(["*.json"])
    |> Path.wildcard()
    |> Enum.each(&import_xbrl_file/1)
  end

  def import_xbrl_file(xbrl_file) do
    xbrl_file
    |> File.read!()
    |> Poison.decode!()
    |> do_import()
    |> IO.inspect(label: "import #{xbrl_file}: ", pretty: true)
  end

  def do_import(xbrl_raw) do
    xbrl_raw["company_code"]
    |> Ontology.find_company_xbrl_by_code()
    |> get_xbrl()
    |> import_xbrl(xbrl_raw)
  end

  def import_xbrl(nil, xbrl_raw) do
    src_updated_at = xbrl_raw["src_updated_at"] |> Ecto.Date.cast!
    IO.puts("importing #{xbrl_raw["company_code"]}, src_updated_at: #{src_updated_at}")

    %{
      company_code: xbrl_raw["company_code"],
      abbrev_name: xbrl_raw["abbrev_name"],
      full_name: xbrl_raw["full_name"],
      industry: xbrl_raw["industry"],
      exchange_code: xbrl_raw["exchange_code"],
      src_updated_at: src_updated_at,
      details: xbrl_raw["details"]
    }
    |> Ontology.create_company_xbrl()
  end

  def import_xbrl(xbrl, xbrl_raw) do
    xbrl_raw["src_updated_at"]
    |> Ecto.Date.cast!()
    |> Ecto.Date.compare(xbrl.src_updated_at |> Ecto.Date.cast!)
    |> update_xbrl(xbrl, xbrl_raw)
  end

  def update_xbrl(:gt, xbrl, xbrl_raw) do
    IO.puts("Company Xbrl [#{xbrl.company_code}] changed, update it")
    attrs = %{
      src_updated_at: xbrl_raw["src_updated_at"] |> Ecto.Date.cast!(),
      exchange_code: xbrl_raw["exchange_code"],
      details: xbrl.details |> Map.merge(xbrl_raw["details"])
    }

    xbrl
    |> Ontology.update_company_xbrl(attrs)
  end
  
  def update_xbrl(_, xbrl, _) do
    IO.puts("Nothing changed. Skip xbrl #{xbrl.company_code}")
  end

  defp get_xbrl({:error, _}), do: nil
  defp get_xbrl({:ok, xbrl}), do: xbrl
end