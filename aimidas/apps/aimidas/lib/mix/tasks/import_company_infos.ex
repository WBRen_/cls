defmodule Mix.Tasks.Aimidas.Import.CompanyInfos do
  use Mix.Task

  import Mix.Ecto

  alias Aimidas.Ontology

  @shortdoc "Import CompanyInfos"
  def run(source_file) do

    ensure_started(Aimidas.Repo, [])

    source_file
    |> File.read!()
    |> Poison.decode!()
    |> do_import()
  end

  def do_import(companies) when is_list(companies) do
    companies
    |> Enum.each(&import_company/1)
  end

  def import_company(company_raw) do
    company_raw["company"]
    |> Ontology.find_company_info_by_code()
    |> get_company()
    |> import_company(company_raw)
  end

  def import_company(nil, company_raw) do
    updated_at = company_raw["updated_at"] |> Ecto.Date.cast!
    IO.puts("importing #{company_raw["company"]}, updated_at: #{updated_at}")

    %{
      company_code: company_raw["company"],
      year: company_raw["year"] |> String.to_integer(),
      abbrev_name: company_raw["基本信息"]["中文简称"],
      full_name: company_raw["基本信息"]["公司全称"],
      industry: company_raw["行业"],
      info_updated_at: updated_at,
      details: %{
        "基本信息" => company_raw["基本信息"]
      }
    }
    |> Ontology.create_company_info()
  end

  def import_company(company, company_raw) do
    company_raw["updated_at"]
    |> Ecto.Date.cast!()
    |> Ecto.Date.compare(company.info_updated_at |> Ecto.Date.cast!)
    |> update_company(company, company_raw)
  end

  def update_company(:gt, company, company_raw) do
    IO.puts("Company [#{company.company_code}] changed, update it")
    attrs = %{
      year: company_raw["year"] |> String.to_integer(),
      abbrev_name: company_raw["基本信息"]["中文简称"],
      full_name: company_raw["基本信息"]["公司全称"],
      industry: company_raw["行业"],
      info_updated_at: company_raw["updated_at"] |> Ecto.Date.cast!(),
      details: %{
        "基本信息" => company_raw["基本信息"]
      }
    }

    company
    |> Ontology.update_company_info(attrs)
  end
  def update_company(_, company, _) do
    IO.puts("Nothing changed. Skip company #{company.company_code}")
  end

  def get_company({:ok, company}), do: company
  def get_company({:error, _}), do: nil
end
