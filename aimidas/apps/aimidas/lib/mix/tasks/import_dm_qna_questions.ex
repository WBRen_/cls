defmodule Mix.Tasks.Aimidas.Import.DmQnaQuestions do
  use Mix.Task
  import Mix.Ecto
  alias Aimidas.Qa

  @shortdoc "Import DmQnaQuestions"
  def run(source_file) do
    ensure_started(Aimidas.Repo, [])

    source_file
    |> File.read!()
    |> Poison.decode!()
    |> do_import()
  end

  def do_import(questions) when is_list(questions) do
    questions
    |> Enum.each(&import_dm_qna_question/1)
  end

  def import_dm_qna_question(question_raw) do
    question_raw["md5"]
    |> Qa.find_dm_qna_question
    |> get_question()
    |> import_dm_qna_question(question_raw)
  end

  def import_dm_qna_question(nil, %{"md5" => md5, "question" => question, "answer" => answer}) do
    %{
      md5: md5,
      query: question,
      answer: answer
    }
    |> Qa.create_dm_qna_question()
  end

  def import_dm_qna_question(question, %{"answer" => answer}) do
    question
    |> Qa.update_dm_qna_question(%{answer: answer})
  end

  def delete_dm_qna_questions(md5s) do
    md5s
    |> Qa.delete_dm_qna_questions()
  end

  defp get_question({:error, _}), do: nil
  defp get_question({:ok, question}), do: question
end
