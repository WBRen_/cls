defmodule Aimidas.ConfigDao do
  @moduledoc """
  The Configs context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Config.Config

  def fetch(key) do
    case Config
        |> where(key_name: ^key)
        |> Repo.all
        |> List.first do
    nil ->
      attr = %{
        key_name: key,
        value: "0",
      }
      {:ok, config} = attr |> create_config()
      # |> IO.inspect(label: ">>>> 新建:\n")
      config
    config ->
      config
    end
  end

  @doc """
  Creates a Config.

  ## Examples

      iex> create_config(%{field: value})
      {:ok, %Config{}}

      iex> create_config(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_config(attrs \\ %{}) do
    %Config{}
    |> Config.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a Config.

  ## Examples

      iex> delete_config(Config)
      {:ok, %Config{}}

      iex> delete_config(Config)
      {:error, %Ecto.Changeset{}}

  """
  def delete_config(%Config{} = config) do
    Repo.delete(config)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking Config changes.

  ## Examples

      iex> change_config(config)
      %Ecto.Changeset{source: %Config{}}

  """
  def change_config(%Config{} = config) do
    Config.changeset(config, %{})
  end


  @doc """
  Updates a Config.

  ## Examples

      iex> update_config(Config, %{field: new_value})
      {:ok, %Config{}}

      iex> update_config(Config, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_config(%Config{} = config, attrs) do
    config
    |> Config.changeset(attrs)
    |> Repo.update()
  end

end
