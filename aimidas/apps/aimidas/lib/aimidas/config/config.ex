defmodule Aimidas.Config.Config do
  use Ecto.Schema
  import Ecto.Changeset

  schema "configs" do
    field :key_name, :string
    field :value, :string
  end

  @doc false
  def changeset(config, attrs) do
    config
    |> cast(attrs, [:key_name, :value])
    |> validate_required([:key_name, :value])
  end
end
