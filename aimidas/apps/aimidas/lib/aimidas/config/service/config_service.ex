defmodule Aimidas.Configer do
  @moduledoc """
  获取配置
  point = Aimidas.Configer.fetch("AddAnswer")
  point == 10
  """


  alias Aimidas.Config.Config
  alias Aimidas.ConfigDao

  def fetch("AddAnswer") do fetch_from_db("AddAnswer") end
  def fetch("AskQuestion") do fetch_from_db("AskQuestion") end
  def fetch("Like") do fetch_from_db("Like") end
  def fetch("DisLike") do fetch_from_db("DisLike") end
  def fetch("PassAnswer") do fetch_from_db("PassAnswer") end
  def fetch("Share") do fetch_from_db("Share") end
  def fetch(_) do {:error, "无此配置"} end

  defp fetch_from_db(key) do
    config = ConfigDao.fetch(key)
    config.value |> String.to_integer
  end

  def set(key, value) do
    config = ConfigDao.fetch(key)
    params = %{
      key_name: key,
      value: value
    }
    config |> ConfigDao.update_config(params)
  end
end