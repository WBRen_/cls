defmodule Aimidas.Points do
  @moduledoc """
  The Points context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Points.Point
  alias Aimidas.Points.PointRecord

  def find_point(user_id) do
    case Point
        |> where(user_id: ^user_id)
        |> Repo.all
        |> List.first do
    nil ->
      # 初始化一条记录给用户
      attr = %{
        user_id: user_id,
        point: 0,
        quota: 20,
        urrent_quota_use: 0
      }
      attr |> create_point()
      # |> IO.inspect(label: ">>>> 新建:\n")
      # {:ok, attr}

    point ->
      {{year_, month_, day_}, _} = DateTime.utc_now |> NaiveDateTime.to_erl # today
      {{year, month, day}, _} = point.updated_at |> NaiveDateTime.to_erl
      if {year_, month_, day_} == {year, month, day} do
        # 今日已经更新过，不做改变
        {:ok, point}
      else
        # 清空 current_quota_use，重置为 0
        point |> update_point(%{current_quota_use: 0})
      end
    end
  end

  @doc """
  Creates a Point.

  ## Examples

      iex> create_point(%{field: value})
      {:ok, %Point{}}

      iex> create_point(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_point(attrs \\ %{}) do
    %Point{}
    |> Point.changeset(attrs)
    |> Repo.insert()
  end

  def add_point(user_id, point) do
    Task.async(fn -> update_point_count(user_id, point) end)
  end

  def add_quota(user_id, quota) do
    Task.async(fn -> update_quota_count(user_id, quota) end)
  end

  def set_quota(user_id, quota) do
    Task.async(fn -> set_quota_count(user_id, quota) end)
  end

  def add_quota_use_today(user_id, quota_use) do
    Task.async(fn -> set_today_quota_use_count(user_id, quota_use) end)
  end

  def update_point_count(user_id, point) do
    {:ok, point_record} = find_point(user_id)
    point_record
    # |> IO.inspect(label: ">>>> Point:\n")
    attrs = %{
      # user_id: point_record.user_id,
      point: point_record.point + point
    }
    point_record
    |> update_point(attrs)
  end

  def update_quota_count(user_id, quota) do
    {:ok, point_record} = find_point(user_id)
    point_record
    # |> IO.inspect(label: ">>>> Point:\n")
    attrs = %{
      # user_id: point_record.user_id,
      quota: point_record.quota + quota
    }
    point_record
    |> update_point(attrs)
  end

  def set_quota_count(user_id, quota) do
    {:ok, point_record} = find_point(user_id)
    point_record
    # |> IO.inspect(label: ">>>> Point:\n")
    attrs = %{
      # user_id: point_record.user_id,
      quota: quota
    }
    point_record
    |> update_point(attrs)
  end

  # 今日使用额度，如果是新的一天，则会自动在 find_point/1 设为 0 再操作
  def set_today_quota_use_count(user_id, quota_use) do
    {:ok, point_record} = find_point(user_id)
    attrs = %{
      current_quota_use: point_record.current_quota_use + quota_use
    }
    point_record |> update_point(attrs)
  end

  @doc """
  Deletes a Point.

  ## Examples

      iex> delete_point(Point)
      {:ok, %Point{}}

      iex> delete_point(Point)
      {:error, %Ecto.Changeset{}}

  """
  def delete_point(%Point{} = point) do
    Repo.delete(point)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking Point changes.

  ## Examples

      iex> change_point(point)
      %Ecto.Changeset{source: %Point{}}

  """
  def change_point(%Point{} = point) do
    Point.changeset(point, %{})
  end


  @doc """
  Returns the list of Points.

  ## Examples

      iex> list_Points()
      [%Point{}, ...]

  """
  def list_Points do
    Repo.all(Point)
  end



  @doc """
  Gets a single Point.

  Raises `Ecto.NoResultsError` if the Point does not exist.

  ## Examples

      iex> get_point!(123)
      %Point{}

      iex> get_point!(456)
      ** (Ecto.NoResultsError)

  """
  def get_point!(id), do: Repo.get!(Point, id)

  def get_point(id) do
    case Point
         |> where(id: ^id)
         |> Repo.all
         |> List.first do
      nil ->
        {:error, "Point not found"}
      Point ->
        {:ok, Point}
    end
  end


  @doc """
  Updates a Point.

  ## Examples

      iex> update_point(Point, %{field: new_value})
      {:ok, %Point{}}

      iex> update_point(Point, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_point(%Point{} = point, attrs) do
    point
    |> Point.changeset(attrs)
    |> Repo.update()
  end

  # def update_point(%Point{} = point, attrs) do
  #   point
  #   |> Point.changeset(attrs)
  #   |> Repo.update()
  # end

  def create_point_record(attrs \\ %{}) do
    %PointRecord{}
    |> PointRecord.changeset(attrs)
    |> Repo.insert()
  end

  def list_PointRecords do
    Repo.all(PointRecord)
  end

end
