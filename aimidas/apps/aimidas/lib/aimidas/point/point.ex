defmodule Aimidas.Points.Point do
  @moduledoc """
  用户模型
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Accounts.User

  schema "points" do
    field :point, :integer, default: 0
    field :quota, :integer, default: 20
    field :current_quota_use, :integer, default: 0

    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(point, attrs) do
    point
    |> cast(attrs, [:point, :quota, :current_quota_use, :user_id])
    |> validate_required([:point])
  end
end
