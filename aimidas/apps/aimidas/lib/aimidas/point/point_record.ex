defmodule Aimidas.Points.PointRecord do
  @moduledoc """
  用户模型
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Accounts.User

  schema "point_records" do
    field :type, :string # 用户操作积分变更，如： AddAnswer添加答案,AskQuestion提问,Like点赞，DisLike踩，PassAnswer答案通过
    field :point, :integer, default: 0
    field :quota, :integer, default: 0

    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(point, attrs) do
    point
    |> cast(attrs, [:point, :quota, :user_id, :type])
    |> validate_required([])
  end
end
