defmodule Aimidas.Statistics do
  @moduledoc """
  The Statistics context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Statistics.NormalizedQuestion
  alias Aimidas.Statistics.UserAnswer

  def find_question!(id), do: Repo.get!(NormalizedQuestion, id)
  def find_hot_question!() do
    list = ["first page", "首页", "第一页", "first", "next page", "下一页", "下", "下页", "往下", "previous page", "上一页", "上页", "上", "往上", "引导机制", "next", "out", "Out", "公告", "资金流向", "业绩预告", "最新公告", "新股申购", "并购重组", "升跌异动", "最高跌幅", "最高涨幅", "最高换手", "最低换手", "最近一月增持", "最近一月减持", "最新增减持", "股票回购", "停复牌", "分红送股", "股权质押", "风险警示", "质押比例", "业绩预增", "业绩预减"]
    record =
      Repo.all(
      from q in NormalizedQuestion,
              where:  q.ask_count >= 10
              and q.query not in ^list
              and q.can_answer == true,
              order_by: fragment("RANDOM()"),
              limit:  3
    )
    if length(record) == 3 do
      record
    else
      limit = 3 - length(record)
      another_record =
        Repo.all(
          from q in NormalizedQuestion,
                  where:  q.ask_count >= 5
                  and q.ask_count < 10
                  and q.query not in ^list
                  and q.can_answer == true,
                  order_by: fragment("RANDOM()"),
                  limit:  ^limit
        )
      record ++ another_record
    end
  end
  def find_question_by_id(id) do
    NormalizedQuestion
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  def find_question(md5) do
    case NormalizedQuestion
        |> where(md5: ^md5)
        |> Repo.all
        |> List.first do
    nil ->
      {:error, "no such question"}
    question ->
      {:ok, question}
    end
  end

  @doc """
  查询一个问题，如果该问题不存在，则创建一个
  """
  def find_question(md5, query, otpq) do
    case NormalizedQuestion
        |> where(md5: ^md5)
        |> Repo.all
        |> List.first do
    nil ->
      # 初始化一条记录给用户
      attr = %{
        md5: md5,
        query: query,
        otpq: otpq
      }
      |> create_question()
    question ->
      {:ok, question}
    end
  end

  @doc """
  至多返回 3 个问题
  Aimidas.Statistics.fetch_3hot_questions()
  """
  def fetch_3hot_questions() do
    query =
      from q in NormalizedQuestion,
      distinct: true,
      where: q.can_answer == true,
      # left_join: u in UserAnswer, on: (u.question_id == q.id and u.status == 101),
      select: {q.query, q.ask_count},
      order_by: [desc: q.ask_count],
      limit: 100
    query
    |> Repo.all
    |> Enum.take_random(3)
    |> Enum.map(fn record ->
      {query, count} = record
      query
    end)
  end


  @doc """
  Creates a NormalizedQuestion.

  ## Examples

      iex> create_question(%{field: value})
      {:ok, %NormalizedQuestion{}}

      iex> create_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_question(attrs \\ %{}) do
    %NormalizedQuestion{}
    |> NormalizedQuestion.changeset(attrs)
    |> Repo.insert()
  end

  def add_count(id) do
    record = find_question!(id)
             #|> IO.inspect(label: ">>>> Statistics.NormalizedQuestion:\n")
    attrs = %{
      ask_count: record.ask_count + 1
    }
    record
    |> NormalizedQuestion.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a NormalizedQuestion.

  ## Examples

      iex> delete_question(NormalizedQuestion)
      {:ok, %NormalizedQuestion{}}

      iex> delete_question(NormalizedQuestion)
      {:error, %Ecto.Changeset{}}

  """
  def delete_question(%NormalizedQuestion{} = question) do
    Repo.delete(question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking NormalizedQuestion changes.

  ## Examples

      iex> change_question(question)
      %Ecto.Changeset{source: %NormalizedQuestion{}}

  """
  def change_question(%NormalizedQuestion{} = question) do
    NormalizedQuestion.changeset(question, %{})
  end

  @doc """
  Updates a NormalizedQuestion.

  ## Examples

      iex> update_question(NormalizedQuestion, %{field: new_value})
      {:ok, %NormalizedQuestion{}}

      iex> update_question(NormalizedQuestion, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_question(%NormalizedQuestion{} = question, attrs) do
    question
    |> NormalizedQuestion.changeset(attrs)
    |> Repo.update()
  end

  #### answer ####

  def find_answer!(id) do
    UserAnswer
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end

  def find_answer(id) do
    case find_answer!(id) do
      nil ->
        {:error, "no such answer"}
      answer ->
        {:ok, answer}
    end
  end

  def find_answer_by_question_id!(question_id) do
    UserAnswer
    |> where(question_id: ^question_id)
    |> Repo.all
    |> List.first
  end

  def find_answer_list_by_question_id!(question_id) do
    UserAnswer
    |> where(question_id: ^question_id)
    |> Repo.all
  end

  @doc """
    Aimidas.Statistics.find_answers_by_question_id!(1)
    when count > 3: 2 个点赞最高，1 个随机
    when count = 3: all
    when count < 3: all

    updated 2018/11/02 count 修改为 2
  """
  def find_answers_by_question_id!(question_id) do
    prerecords =
      Repo.all(
        from q in UserAnswer,
                where:  q.question_id == ^question_id
                  and   q.status >= 99 and q.status != 101,
                order_by: [asc: :contras, desc: :stars],
                limit:  2
      )
    # UserAnswer
    # |> where([u], u.question_id == ^question_id and u.status >= 99 and u.status != 101)
    # |> order_by([asc: :contras, desc: :stars])
    # |> limit(2)
    # |> preload([:user, :question])
    # # |> preload([:authentications])
    # # |> preload([:point])
    # # |> preload([:point_records])
    # # |> preload([:user_answers])
    # |> Repo.all
    if length(prerecords) <= 2 do
      prerecords
    else
      # 多查询一条随机的，且不为第 1 条数据
      {_, ids} = prerecords |> Enum.map(fn record -> record.id end) |> List.pop_at(1)
      another_record =
        Repo.all(
        from q in UserAnswer,
                where:  q.id not in ^ids and q.question_id == ^question_id and q.status >= 99 and q.status != 101,
                order_by: fragment("RANDOM()"),
                limit:  1
      )
      # UserAnswer
      # |> where([u], u.id not in ^ids and u.status >= 99 and u.status != 101)
      # |> order_by(fragment("RANDOM()"))
      # |> preload([:user, :question])
      # |> limit(1)
      # |> Repo.all
      {_, records} = prerecords |> List.pop_at(1)
      records ++ another_record
    end
    # case result do
    #   nil ->
    #     {:error, "answers not found"}
    #   [] ->
    #     {:error, "answers is empty"}
    #   _ ->
    #     {:ok, result}
    # end
  end

  def find_user_answer(question_id) do
    answer =
      Repo.all(
        from q in UserAnswer,
                where:  q.question_id == ^question_id
                  and   q.status >= 99 and q.status != 101,
                order_by: [asc: :contras, desc: :stars],
                limit:  1
      )
    case answer do
      nil ->
        {:error, "answer not found"}
      [] ->
        {:error, "answer is empty"}
      _ ->
        {:ok, List.first(answer)}
    end
  end

  def create_answer(attrs \\ %{}) do
    %UserAnswer{}
    |> UserAnswer.changeset(attrs)
    |> Repo.insert()
  end

  def append_standard_answer(statistics_question_id, answer) do
    case statistics_answer = find_answer_by_question_id!(statistics_question_id) do
      nil ->
        %{
          question_id: statistics_question_id,
          answer: answer,
          status: 101 # 标准回答的 status 设为 101，需要查询的时候请注意加上 != 101 的条件，避免被大于 99 的条件 select 出来
        }
        |> create_answer()
      user_answer -> # {:ok, "Standard Answer exist, do not append again."}
        {:ok, statistics_answer}
    end
  end

  def add_answer_star_count(id) do
    case find_answer!(id) do
      nil ->
        {:error, "无此回答可被点赞"}
      record ->
        attrs = %{
          stars: record.stars + 1
        }
        record
        |> UserAnswer.changeset(attrs)
        |> Repo.update()
    end
  end

  def add_answer_contra_count(id) do
    case find_answer!(id) do
      nil ->
        {:error, "无此回答可被反对"}
      record ->
        attrs = %{
          contras: record.contras + 1
        }
        record
        |> UserAnswer.changeset(attrs)
        |> Repo.update()
    end
  end

  def delete_answer(%UserAnswer{} = answer) do
    Repo.delete(answer)
  end

  def change_answer(%UserAnswer{} = answer) do
    UserAnswer.changeset(answer, %{})
  end

  def update_answer(%UserAnswer{} = answer, attrs) do
    answer
    |> UserAnswer.changeset(attrs)
    |> Repo.update()
  end
end
