defmodule Aimidas.Statistics.UserAnswer do
  use Ecto.Schema
  import Ecto.Changeset
  @moduledoc """
  用户反馈的问题答案
  """

  alias Aimidas.Repo
  alias Aimidas.Accounts.User
  alias Aimidas.Statistics.NormalizedQuestion

  schema "user_answers" do
    field :answer, :string
    field :stars, :integer, default: 0 #点赞次数
    field :contras, :integer, default: 0 #反对次数
    field :status, :integer, default: 0 #审核状态

    belongs_to :question, NormalizedQuestion
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(answer, attrs) do
    answer
    |> cast(attrs, [:user_id, :question_id, :answer, :stars, :contras, :status])
    |> validate_required([:question_id, :answer])
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Statistics.UserAnswer do
    def equals(left, right) do
      left.id == right.id and
      left.user_id == right.user_id
    end
  end
end
