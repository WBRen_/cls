defmodule Aimidas.Statistics.NormalizedQuestion do
  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Statistics.UserAnswer

  schema "normalized_questions" do
    field :md5, :string
    field :query, :string
    field :ask_count, :integer, default: 0 #询问次数
    field :otpq, :map

    field :can_answer, :boolean, default: true

    has_many :answer, UserAnswer

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:md5, :query, :otpq, :ask_count, :can_answer])
    # |> validate_required([:md5, :query, :otpq, :ask_count])
    |> validate_required([:query, :otpq, :ask_count]) #为了满足反问机制
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Statistics.NormalizedQuestion do
    def equals(left, right) do
      left.id == right.id
    end
  end
end
