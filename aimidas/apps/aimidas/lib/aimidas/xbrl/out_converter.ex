defmodule Aimidas.Xbrl.OutConverter do
  
  def to_smart_float(nil) do
    {"-", ""}
  end
  def to_smart_float(value) when value > 100_000_000.00 do
    {Float.round(value / 1_00_000_000, 3), "亿"}
  end
  def to_smart_float(value) when value > 10_000_000.00 do
    {Float.round(value / 10_000_000, 3), "千万"}
  end
  def to_smart_float(value) when value > 1_000_000.00 do
    {Float.round(value / 1_000_000, 3), "百万"}
  end
  def to_smart_float(value) when value > 10_000.00 do
    {Float.round(value / 10_000.00, 3), "万"}
  end
  def to_smart_float(value) when value > -10_000 do
    {value, ""}
  end
  def to_smart_float(value) when value > -1_000_000 do
    {Float.round(value / 10_000, 3), "万"}
  end
  def to_smart_float(value) when value > -10_000_000 do
    {Float.round(value / 1_000_000.00, 3), "百万"}
  end
  def to_smart_float(value) when value > -100_000_000.0 do
    {Float.round(value / 10_000_000.00, 3), "千万"}
  end
  def to_smart_float(value) do
    {Float.round(value / 100_000_000.00, 3), "亿"}
  end

  def to_percentage(nil) do
    {"-", ""}
  end
  def to_percentage(value) do
    {value, "%"}
  end

  def to_text(nil) do
    {"", ""}
  end
  def to_text(value) do
    {
      value |> to_string(),
      ""
    }
  end
end