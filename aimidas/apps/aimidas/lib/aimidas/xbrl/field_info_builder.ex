defmodule Aimidas.Xbrl.FieldInfoBuilder do
  @unit_none ""
  @unit_yuan "元"

  def currency_field_info(field_name, group) do
    smart_float_field_info(field_name, group, @unit_yuan)
  end

  def millions_currency_field_info(field_name, group) do
    smart_float_field_info(field_name, group, @unit_yuan, :from_float_1000k)
  end

  def smart_float_field_info(field_name, group, unit \\@unit_yuan, from_converter \\:from_float) do
    build_field_info(
      field_name,
      group,
      unit,
      from_converter,
      :to_smart_float
    )
  end

  def smart_float_10k_field_info(field_name, group, unit \\@unit_yuan) do
    smart_float_field_info(field_name, group, unit, :from_float_10k)
  end

  def percentage_field_info(field_name, group, unit \\@unit_none) do
    build_field_info(
      field_name,
      group,
      unit,
      :from_float,
      :to_percentage
    )
  end

  def text_field_info(field_name, group) do
    build_field_info(
      field_name,
      group,
      @unit_none,
      :from_text,
      :to_text
    )
  end

  def integer_field_info(field_name, group, unit \\@unit_none) do
    build_field_info(
      field_name,
      group,
      unit,
      :from_integer,
      :to_smart_float
    )
  end

  def shareholder_field_info(field_name, group) do
    build_field_info(
      field_name,
      group,
      "",
      :from_shareholder,
      :to_text
    )
  end

  def build_field_info(field_name, field_group, unit, input_converter, output_converter) do
    %{
      field_name: field_name,
      group: field_group,
      unit: unit,
      in_converter: input_converter,
      out_converter: output_converter
    }
  end
end