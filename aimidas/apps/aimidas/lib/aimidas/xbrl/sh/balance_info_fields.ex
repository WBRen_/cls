defmodule Aimidas.Xbrl.SH.BalanceInfoFields do

  import Aimidas.Xbrl.FieldInfoBuilder

  @group "资产负债"

  def fields do
    [
      # [字段名称, 输入转换, 输出转换]
      ["货币资金", :convert_float],
      ["结算备付金", :convert_float],
      ["拆出资金", :convert_float],
      ["交易性金融资产", :convert_float],
      ["应收票据", :convert_float],
      ["应收帐款", :convert_float],
      ["预付帐款", :convert_float],
      ["应收保费", :convert_float],
      ["应收分保账款", :convert_float],
      ["应收分保合同准备金", :convert_float],
      ["应收利息", :convert_float],
      ["应收股利", :convert_float],
      ["其他应收款", :convert_float],
      ["买入返售金融资产", :convert_float],
      ["存货", :convert_float],
      ["一年内到期的非流动资产", :convert_float],
      ["其他流动资产", :convert_float],
      ["流动资产合计", :convert_float],
      ["发放贷款和垫款", :convert_float],
      ["可供出售金融资产", :convert_float],
      ["持有至到期投资", :convert_float],
      ["长期应收款", :convert_float],
      ["长期股权投资", :convert_float],
      ["投资性房地产", :convert_float],
      ["固定资产净额", :convert_float],
      ["在建工程", :convert_float],
      ["工程物资", :convert_float],
      ["固定资产清理", :convert_float],
      ["生产性生物资产", :convert_float],
      ["油气资产", :convert_float],
      ["无形资产", :convert_float],
      ["开发支出", :convert_float],
      ["商誉", :convert_float],
      ["长期待摊费用", :convert_float],
      ["递延税款借项合计", :convert_float],
      ["其他长期资产", :convert_float],
      ["非流动资产合计", :convert_float],
      ["资产总计", :convert_float],
      ["短期借款", :convert_float],
      ["向中央银行借款", :convert_float],
      ["吸收存款及同业存放", :convert_float],
      ["拆入资金", :convert_float],
      ["交易性金融负债", :convert_float],
      ["应付票据", :convert_float],
      ["应付帐款", :convert_float],
      ["预收帐款", :convert_float],
      ["卖出回购金融资产款", :convert_float],
      ["应付手续费及佣金", :convert_float],
      ["应付职工薪酬", :convert_float],
      ["应交税金", :convert_float],
      ["应付利息", :convert_float],
      ["应付股利", :convert_float],
      ["其他应付款", :convert_float],
      ["应付分保账款", :convert_float],
      ["保险合同准备金", :convert_float],
      ["代理买卖证券款", :convert_float],
      ["代理承销证券款", :convert_float],
      ["一年内到期的长期负债", :convert_float],
      ["其他流动负债", :convert_float],
      ["流动负债合计", :convert_float],
      ["长期借款", :convert_float],
      ["应付债券", :convert_float],
      ["长期应付款", :convert_float],
      ["专项应付款", :convert_float],
      ["预计负债", :convert_float],
      ["递延税款贷项合计", :convert_float],
      ["其他长期负债", :convert_float],
      ["长期负债合计", :convert_float],
      ["负债合计", :convert_float],
      ["股本", :convert_float],
      ["资本公积", :convert_float],
      ["库存股", :convert_float],
      ["盈余公积", :convert_float],
      ["一般风险准备", :convert_float],
      ["未分配利润", :convert_float],
      ["外币报表折算差额", :convert_float],
      ["归属于母公司所有者权益合计", :convert_float],
      ["少数股东权益", :convert_float],
      ["股东权益合计", :convert_float],
      ["负债和股东权益合计", :convert_float]
    ]
  end

  # def field_info(field_name) do
  #   field_infos_map()
  #   |> Map.get(field_name)
  # end

  def field_infos do
    %{
      "货币资金" => currency_field_info("货币资金", @group),
      "结算备付金" => currency_field_info("结算备付金", @group),
      "拆出资金" => currency_field_info("拆出资金", @group),
      "交易性金融资产" => currency_field_info("交易性金融资产", @group),
      "应收票据" => currency_field_info("应收票据", @group),
      "应收帐款" => currency_field_info("应收帐款", @group),
      "预付帐款" => currency_field_info("预付帐款", @group),
      "应收保费" => currency_field_info("应收保费", @group),
      "应收分保账款" => currency_field_info("应收分保账款", @group),
      "应收分保合同准备金" => currency_field_info("应收分保合同准备金", @group),
      "应收利息" => currency_field_info("应收利息", @group),
      "应收股利" => currency_field_info("应收股利", @group),
      "其他应收款" => currency_field_info("其他应收款", @group),
      "买入返售金融资产" => currency_field_info("买入返售金融资产", @group),
      "存货" => currency_field_info("存货", @group),
      "一年内到期的非流动资产" => currency_field_info("一年内到期的非流动资产", @group),
      "其他流动资产" => currency_field_info("其他流动资产", @group),
      "流动资产合计" => currency_field_info("流动资产合计", @group),
      "发放贷款和垫款" => currency_field_info("发放贷款和垫款", @group),
      "可供出售金融资产" => currency_field_info("可供出售金融资产", @group),
      "持有至到期投资" => currency_field_info("持有至到期投资", @group),
      "长期应收款" => currency_field_info("长期应收款", @group),
      "长期股权投资" => currency_field_info("长期股权投资", @group),
      "投资性房地产" => currency_field_info("投资性房地产", @group),
      "固定资产净额" => currency_field_info("固定资产净额", @group),
      "在建工程" => currency_field_info("在建工程", @group),
      "工程物资" => currency_field_info("工程物资", @group),
      "固定资产清理" => currency_field_info("固定资产清理", @group),
      "生产性生物资产" => currency_field_info("生产性生物资产", @group),
      "油气资产" => currency_field_info("油气资产", @group),
      "无形资产" => currency_field_info("无形资产", @group),
      "开发支出" => currency_field_info("开发支出", @group),
      "商誉" => currency_field_info("商誉", @group),
      "长期待摊费用" => currency_field_info("长期待摊费用", @group),
      "递延税款借项合计" => currency_field_info("递延税款借项合计", @group),
      "其他长期资产" => currency_field_info("其他长期资产", @group),
      "非流动资产合计" => currency_field_info("非流动资产合计", @group),
      "资产总计" => currency_field_info("资产总计", @group),
      "短期借款" => currency_field_info("短期借款", @group),
      "向中央银行借款" => currency_field_info("向中央银行借款", @group),
      "吸收存款及同业存放" => currency_field_info("吸收存款及同业存放", @group),
      "拆入资金" => currency_field_info("拆入资金", @group),
      "交易性金融负债" => currency_field_info("交易性金融负债", @group),
      "应付票据" => currency_field_info("应付票据", @group),
      "应付帐款" => currency_field_info("应付帐款", @group),
      "预收帐款" => currency_field_info("预收帐款", @group),
      "卖出回购金融资产款" => currency_field_info("卖出回购金融资产款", @group),
      "应付手续费及佣金" => currency_field_info("应付手续费及佣金", @group),
      "应付职工薪酬" => currency_field_info("应付职工薪酬", @group),
      "应交税金" => currency_field_info("应交税金", @group),
      "应付利息" => currency_field_info("应付利息", @group),
      "应付股利" => currency_field_info("应付股利", @group),
      "其他应付款" => currency_field_info("其他应付款", @group),
      "应付分保账款" => currency_field_info("应付分保账款", @group),
      "保险合同准备金" => currency_field_info("保险合同准备金", @group),
      "代理买卖证券款" => currency_field_info("代理买卖证券款", @group),
      "代理承销证券款" => currency_field_info("代理承销证券款", @group),
      "一年内到期的长期负债" => currency_field_info("一年内到期的长期负债", @group),
      "其他流动负债" => currency_field_info("其他流动负债", @group),
      "流动负债合计" => currency_field_info("流动负债合计", @group),
      "长期借款" => currency_field_info("长期借款", @group),
      "应付债券" => currency_field_info("应付债券", @group),
      "长期应付款" => currency_field_info("长期应付款", @group),
      "专项应付款" => currency_field_info("专项应付款", @group),
      "预计负债" => currency_field_info("预计负债", @group),
      "递延税款贷项合计" => currency_field_info("递延税款贷项合计", @group),
      "其他长期负债" => currency_field_info("其他长期负债", @group),
      "长期负债合计" => currency_field_info("长期负债合计", @group),
      "负债合计" => currency_field_info("负债合计", @group),
      "股本" => currency_field_info("股本", @group),
      "资本公积" => currency_field_info("资本公积", @group),
      "库存股" => currency_field_info("库存股", @group),
      "盈余公积" => currency_field_info("盈余公积", @group),
      "一般风险准备" => currency_field_info("一般风险准备", @group),
      "未分配利润" => currency_field_info("未分配利润", @group),
      "外币报表折算差额" => currency_field_info("外币报表折算差额", @group),
      "归属于母公司所有者权益合计" => currency_field_info("归属于母公司所有者权益合计", @group),
      "少数股东权益" => currency_field_info("少数股东权益", @group),
      "股东权益合计" => currency_field_info("股东权益合计", @group),
      "负债和股东权益合计" => currency_field_info("负债和股东权益合计", @group),
    }
  end
end
