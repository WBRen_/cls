defmodule Aimidas.Xbrl.SH.XbrlFieldInfos do
  alias Aimidas.Xbrl.SH.{
    BasicInfoFields,
    BalanceInfoFields,
    CapitalInfoFields,
    CashInfoFields,
    ProfitInfoFields,
    ShareholderInfoFields
  }

  def field_info(field_name) do
    field_infos()
    |> Map.get(field_name)
  end

  def field_infos do
    %{}
    |> Map.merge(BasicInfoFields.field_infos)
    |> Map.merge(BalanceInfoFields.field_infos)
    |> Map.merge(CapitalInfoFields.field_infos)
    |> Map.merge(CashInfoFields.field_infos)
    |> Map.merge(ProfitInfoFields.field_infos)
    |> Map.merge(ShareholderInfoFields.field_infos)
  end
end