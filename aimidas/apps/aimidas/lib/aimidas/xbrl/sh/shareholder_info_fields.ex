defmodule Aimidas.Xbrl.SH.ShareholderInfoFields do
  import Aimidas.Xbrl.FieldInfoBuilder

  @group "十大股东"

  @shareholder_fields [
    ["1", :from_shareholder],
    ["2", :convert_html],
    ["3", :convert_html],
    ["4", :convert_html],
    ["5", :convert_html],
    ["6", :convert_html],
    ["7", :convert_html],
    ["8", :convert_html],
    ["9", :convert_html],
    ["10", :convert_html],
  ]

  def fields do
    @shareholder_fields
  end

  def field_infos do
    %{
      "1" => shareholder_field_info("1", @group),
      "2" => shareholder_field_info("2", @group),
      "3" => shareholder_field_info("3", @group),
      "4" => shareholder_field_info("4", @group),
      "5" => shareholder_field_info("5", @group),
      "6" => shareholder_field_info("5", @group),
      "7" => shareholder_field_info("5", @group),
      "8" => shareholder_field_info("5", @group),
      "9" => shareholder_field_info("5", @group),
      "10" => shareholder_field_info("5", @group),
    }
  end

end