defmodule Aimidas.Xbrl.SH.CapitalInfoFields do

  import Aimidas.Xbrl.FieldInfoBuilder

  @group "股本结构"
  @stock_unit "股"

  @capital_fields [
    # [字段名称, 输入转换, 输出转换]
    ["国家持有的有限售条件流通股份数", :convert_float],
    ["国有法人持有的有限售条件流通股份数", :convert_float],
    ["其他有限售条件的内资流通股份数", :convert_float],
    ["境内法人持有的有限售条件流通股份数", :convert_float],
    ["境内自然人持有的有限售条件流通股份数", :convert_float],
    ["有限售条件的外资流通股份数", :convert_float],
    ["境外法人持有的有限售条件流通股份数", :convert_float],
    ["境外自然人持有的有限售条件流通股份数", :convert_float],
    ["其他有限售股流通股数", :convert_float],
    ["有限售条件流通股数", :convert_float],
    ["无限售条件人民币普通股数", :convert_float],
    ["无限售条件境内上市的外资股数", :convert_float],
    ["无限售条件境外上市的外资股数", :convert_integer],
    ["其他无限售条件已上市流通股份数", :convert_float],
    ["无限售条件流通股份合计", :convert_float],
    ["股份总数", :convert_float],
    ["国家持有的有限售条件流通股份占总股本比例", :convert_float],
    ["国有法人持有的有限售条件流通股份占总股本比例", :convert_float],
    ["其他有限售条件的内资流通股份占总股本比例", :convert_float],
    ["境内法人持有的有限售条件流通股份占总股本比例", :convert_float],
    ["境内自然人持有的有限售条件流通股份占总股本比例", :convert_float],
    ["有限售条件的外资流通股份占总股本比例", :convert_float],
    ["境外法人持有的有限售条件流通股份占总股本比例", :convert_float],
    ["境外自然人持有的有限售条件流通股份占总股本比例", :convert_float],
    ["其他有限售股流通股份占总股本比例", :convert_float],
    ["有限售条件流通股占总股本比例", :convert_float],
    ["无限售条件人民币普通股占总股本比例", :convert_float],
    ["无限售条件境内上市的外资股占总股本比例", :convert_float],
    ["无限售条件境外上市的外资股占总股本比例", :convert_float],
    ["其他无限售条件已上市流通股份占总股本比例", :convert_float],
    ["无限售条件流通股占总股本比例", :convert_float],
    ["股份总数占总股本比例", :convert_float]
  ]

  def fields do
    @capital_fields
  end

  def field_infos do
    %{
      "国家持有的有限售条件流通股份数" => smart_float_field_info("国家持有的有限售条件流通股份数", @group, @stock_unit),
      "国有法人持有的有限售条件流通股份数" => smart_float_field_info("国有法人持有的有限售条件流通股份数", @group, @stock_unit),
      "其他有限售条件的内资流通股份数" => smart_float_field_info("其他有限售条件的内资流通股份数", @group, @stock_unit),
      "境内法人持有的有限售条件流通股份数" => smart_float_field_info("境内法人持有的有限售条件流通股份数", @group, @stock_unit),
      "境内自然人持有的有限售条件流通股份数" => smart_float_field_info("境内自然人持有的有限售条件流通股份数", @group, @stock_unit),
      "有限售条件的外资流通股份数" => smart_float_field_info("有限售条件的外资流通股份数", @group, @stock_unit),
      "境外法人持有的有限售条件流通股份数" => smart_float_field_info("境外法人持有的有限售条件流通股份数", @group, @stock_unit),
      "境外自然人持有的有限售条件流通股份数" => smart_float_field_info("境外自然人持有的有限售条件流通股份数", @group, @stock_unit),
      "其他有限售股流通股数" => smart_float_field_info("其他有限售股流通股数", @group, @stock_unit),
      "有限售条件流通股数" => smart_float_field_info("有限售条件流通股数", @group, @stock_unit),
      "无限售条件人民币普通股数" => smart_float_field_info("无限售条件人民币普通股数", @group, @stock_unit),
      "无限售条件境内上市的外资股数" => smart_float_field_info("无限售条件境内上市的外资股数", @group, @stock_unit),
      "无限售条件境外上市的外资股数" => smart_float_field_info("无限售条件境外上市的外资股数", @group, @stock_unit),
      "其他无限售条件已上市流通股份数" => smart_float_field_info("其他无限售条件已上市流通股份数", @group, @stock_unit),
      "无限售条件流通股份合计" => smart_float_field_info("无限售条件流通股份合计", @group, @stock_unit),
      "股份总数" => smart_float_field_info("股份总数", @group, @stock_unit),
      "国家持有的有限售条件流通股份占总股本比例" => percentage_field_info("国家持有的有限售条件流通股份占总股本比例", @group),
      "国有法人持有的有限售条件流通股份占总股本比例" => percentage_field_info("国有法人持有的有限售条件流通股份占总股本比例", @group),
      "其他有限售条件的内资流通股份占总股本比例" => percentage_field_info("其他有限售条件的内资流通股份占总股本比例", @group),
      "境内法人持有的有限售条件流通股份占总股本比例" => percentage_field_info("境内法人持有的有限售条件流通股份占总股本比例", @group),
      "境内自然人持有的有限售条件流通股份占总股本比例" => percentage_field_info("境内自然人持有的有限售条件流通股份占总股本比例", @group),
      "有限售条件的外资流通股份占总股本比例" => percentage_field_info("有限售条件的外资流通股份占总股本比例", @group),
      "境外法人持有的有限售条件流通股份占总股本比例" => percentage_field_info("境外法人持有的有限售条件流通股份占总股本比例", @group),
      "境外自然人持有的有限售条件流通股份占总股本比例" => percentage_field_info("境外自然人持有的有限售条件流通股份占总股本比例", @group),
      "其他有限售股流通股份占总股本比例" => percentage_field_info("其他有限售股流通股份占总股本比例", @group),
      "有限售条件流通股占总股本比例" => percentage_field_info("有限售条件流通股占总股本比例", @group),
      "无限售条件人民币普通股占总股本比例" => percentage_field_info("无限售条件人民币普通股占总股本比例", @group),
      "无限售条件境内上市的外资股占总股本比例" => percentage_field_info("无限售条件境内上市的外资股占总股本比例", @group),
      "无限售条件境外上市的外资股占总股本比例" => percentage_field_info("无限售条件境外上市的外资股占总股本比例", @group),
      "其他无限售条件已上市流通股份占总股本比例" => percentage_field_info("其他无限售条件已上市流通股份占总股本比例", @group),
      "无限售条件流通股占总股本比例" => percentage_field_info("无限售条件流通股占总股本比例", @group),
      "股份总数占总股本比例" => percentage_field_info("股份总数占总股本比例", @group)

    }
  end
end
