defmodule Aimidas.Xbrl.SH.ProfitInfoFields do
  import Aimidas.Xbrl.FieldInfoBuilderMacro

  @group "利润"

  @profit_fields [
    # [字段名称, 输入转换, 输出转换]
    ["营业总收入", :convert_float],
    ["营业收入", :convert_float],
    ["金融资产利息收入", :convert_float],
    ["已赚保费", :convert_float],
    ["手续费及佣金收入", :convert_float],
    ["营业总成本", :convert_float],
    ["营业成本", :convert_float],
    ["金融资产利息支出", :convert_float],
    ["手续费及佣金支出", :convert_float],
    ["退保金", :convert_float],
    ["赔付支出净额", :convert_float],
    ["提取保险合同准备金净额", :convert_float],
    ["保单红利支出", :convert_float],
    ["分保费用", :convert_float],
    ["营业税金及附加", :convert_float],
    ["销售费用", :convert_float],
    ["管理费用", :convert_float],
    ["财务费用", :convert_float],
    ["资产减值损失", :convert_float],
    ["公允价值变动收益", :convert_float],
    ["投资收益", :convert_float],
    ["对联营企业和合营企业的投资收益", :convert_float],
    ["汇兑收益", :convert_float],
    ["营业利润", :convert_float],
    ["营业外收入", :convert_float],
    ["营业外支出", :convert_float],
    ["非流动资产处置净损失", :convert_float],
    ["利润总额", :convert_float],
    ["所得税", :convert_float],
    ["净利润", :convert_float],
    ["归属于母公司所有者的净利润", :convert_float],
    ["少数股东损益", :convert_float],
    ["基本每股收益", :convert_float],
    ["稀释每股收益", :convert_float],
  ]

  def fields do
    @profit_fields
  end

  def field_infos do
    %{
      "营业总收入" => currency_field_info("营业总收入", @group),
      "营业收入" => currency_field_info("营业收入", @group),
      "金融资产利息收入" => currency_field_info("金融资产利息收入", @group),
      "已赚保费" => currency_field_info("已赚保费", @group),
      "手续费及佣金收入" => currency_field_info("手续费及佣金收入", @group),
      "营业总成本" => currency_field_info("营业总成本", @group),
      "营业成本" => currency_field_info("营业成本", @group),
      "金融资产利息支出" => currency_field_info("金融资产利息支出", @group),
      "手续费及佣金支出" => currency_field_info("手续费及佣金支出", @group),
      "退保金" => currency_field_info("退保金", @group),
      "赔付支出净额" => currency_field_info("赔付支出净额", @group),
      "提取保险合同准备金净额" => currency_field_info("提取保险合同准备金净额", @group),
      "保单红利支出" => currency_field_info("保单红利支出", @group),
      "分保费用" => currency_field_info("分保费用", @group),
      "营业税金及附加" => currency_field_info("营业税金及附加", @group),
      "销售费用" => currency_field_info("销售费用", @group),
      "管理费用" => currency_field_info("管理费用", @group),
      "财务费用" => currency_field_info("财务费用", @group),
      "资产减值损失" => currency_field_info("资产减值损失", @group),
      "公允价值变动收益" => currency_field_info("公允价值变动收益", @group),
      "投资收益" => currency_field_info("投资收益", @group),
      "对联营企业和合营企业的投资收益" => currency_field_info("对联营企业和合营企业的投资收益", @group),
      "汇兑收益" => currency_field_info("汇兑收益", @group),
      "营业利润" => currency_field_info("营业利润", @group),
      "营业外收入" => currency_field_info("营业外收入", @group),
      "营业外支出" => currency_field_info("营业外支出", @group),
      "非流动资产处置净损失" => currency_field_info("非流动资产处置净损失", @group),
      "利润总额" => currency_field_info("利润总额", @group),
      "所得税" => currency_field_info("所得税", @group),
      "净利润" => currency_field_info("净利润", @group),
      "归属于母公司所有者的净利润" => currency_field_info("归属于母公司所有者的净利润", @group),
      "少数股东损益" => currency_field_info("少数股东损益", @group),
      "基本每股收益" => currency_field_info("基本每股收益", @group),
      "稀释每股收益" => currency_field_info("稀释每股收益", @group),

    }
  end

end
