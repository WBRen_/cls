defmodule Aimidas.Xbrl.SH.BasicInfoFields do
  import Aimidas.Xbrl.FieldInfoBuilder

  @group "基本信息"

  def fields do
    [
      # [字段名称, 输入转换, 输出转换]
      ["公司法定中文名称", :convert_html],
      ["公司法定代表人", :convert_html],
      ["公司注册地址", :convert_html],
      ["公司办公地址邮政编码", :convert_html],
      ["公司国际互联网网址", :convert_html],
      ["公司董事会秘书姓名", :convert_html],
      ["公司董事会秘书电话", :convert_html],
      ["公司董事会秘书电子信箱", :convert_html],
      ["报告期末股东总数", :convert_integer],
      ["每10股送红股数", :convert_float],
      ["每10股派息数", :convert_float],
      ["每10股转增数", :convert_float],
      ["本期营业收入", :convert_float],
      ["本期营业利润", :convert_float],
      ["利润总额", :convert_float],
      ["归属于上市公司股东的净利润", :convert_float],
      ["归属于上市公司股东的扣除非经常性损益的净利润", :convert_float],
      ["经营活动产生的现金流量净额", :convert_float],
      ["总资产", :convert_float],
      ["所有者权益", :convert_float],
      ["基本每股收益", :convert_float],
      ["稀释每股收益", :convert_float],
      ["扣除非经常性损益后的基本每股收益", :convert_float],
      ["全面摊薄净资产收益率", :convert_float],
      ["加权平均净资产收益率", :convert_float],
      ["扣除非经常性损益后全面摊薄净资产收益率", :convert_float],
      ["扣除非经常性损益后的加权平均净资产收益率", :convert_float],
      ["每股经营活动产生的现金流量净额", :convert_float],
      ["归属于上市公司股东的每股净资产", :convert_float],
    ]
  end

  def field_infos do
    %{
      "公司法定中文名称" => text_field_info("公司法定中文名称", @group),
      "公司法定代表人" => text_field_info("公司法定代表人", @group),
      "公司注册地址" => text_field_info("公司注册地址", @group),
      "公司办公地址邮政编码" => text_field_info("公司办公地址邮政编码", @group),
      "公司国际互联网网址" => text_field_info("公司国际互联网网址", @group),
      "公司董事会秘书姓名" => text_field_info("公司董事会秘书姓名", @group),
      "公司董事会秘书电话" => text_field_info("公司董事会秘书电话", @group),
      "公司董事会秘书电子信箱" => text_field_info("公司董事会秘书电子信箱", @group),
      "报告期末股东总数" => currency_field_info("报告期末股东总数", @group),
      "每10股送红股数" => currency_field_info("每10股送红股数", @group),
      "每10股派息数" => currency_field_info("每10股派息数", @group),
      "每10股转增数" => currency_field_info("每10股转增数", @group),
      "本期营业收入" => currency_field_info("本期营业收入", @group),
      "本期营业利润" => currency_field_info("本期营业利润", @group),
      "利润总额" => currency_field_info("利润总额", @group),
      "归属于上市公司股东的净利润" => currency_field_info("归属于上市公司股东的净利润", @group),
      "归属于上市公司股东的扣除非经常性损益的净利润" => currency_field_info("归属于上市公司股东的扣除非经常性损益的净利润", @group),
      "经营活动产生的现金流量净额" => currency_field_info("经营活动产生的现金流量净额", @group),
      "总资产" => currency_field_info("总资产", @group),
      "所有者权益" => currency_field_info("所有者权益", @group),
      "基本每股收益" => currency_field_info("基本每股收益", @group),
      "稀释每股收益" => currency_field_info("稀释每股收益", @group),
      "扣除非经常性损益后的基本每股收益" => currency_field_info("扣除非经常性损益后的基本每股收益", @group),
      "全面摊薄净资产收益率" => currency_field_info("全面摊薄净资产收益率", @group),
      "加权平均净资产收益率" => currency_field_info("加权平均净资产收益率", @group),
      "扣除非经常性损益后全面摊薄净资产收益率" => currency_field_info("扣除非经常性损益后全面摊薄净资产收益率", @group),
      "扣除非经常性损益后的加权平均净资产收益率" => currency_field_info("扣除非经常性损益后的加权平均净资产收益率", @group),
      "每股经营活动产生的现金流量净额" => currency_field_info("每股经营活动产生的现金流量净额", @group),
      "归属于上市公司股东的每股净资产" => currency_field_info("归属于上市公司股东的每股净资产", @group),
    }
  end

end
