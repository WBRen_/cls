defmodule Aimidas.Xbrl.AllFields do

  alias Aimidas.Xbrl.SH.XbrlFieldInfos, as: SHFieldInfos
  alias Aimidas.Xbrl.SZ.XbrlFieldInfos, as: SZFieldInfos

  def field_infos do
    %{
      "SH" => build_sh_fields(),
      "SZ" => build_sz_fields(),
    }
  end

  defp build_sh_fields do
    %{
      "0" => %{group: "年报"},
      "1" => %{group: "一季报"},
      "2" => %{group: "半年报"},
      "3" => %{group: "三季报"}
    }
    |> Map.merge(SHFieldInfos.field_infos())
  end

  defp build_sz_fields do
    %{
      "0" => %{group: "年报"},
      "1" => %{group: "一季报"},
      "2" => %{group: "半年报"},
      "3" => %{group: "三季报"}
    }
    |> Map.merge(SZFieldInfos.field_infos())
  end
end
