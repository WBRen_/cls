defmodule Aimidas.Xbrl.InConverter do

  def from_none(value), do: value  

  def from_text(nil), do: ""
  def from_text(""), do: ""
  def from_text(" "), do: ""
  def from_text(text) do
    text
    |> HtmlSanitizeEx.strip_tags()
    |> String.trim()
  end

  def from_integer(nil), do: nil
  def from_integer(""), do: nil
  def from_integer(" "), do: nil
  def from_integer("-"), do: nil
  def from_integer("null"), do: nil
  def from_integer(value) do
    value
    |> to_string()
    |> String.trim()
    |> String.replace(",", "")
    |> String.to_integer()
  end

  def from_float(nil), do: nil
  def from_float(""), do: nil
  def from_float("-"), do: nil
  def from_float(" "), do: nil
  def from_float("null"), do: nil
  def from_float(value) do
    {f_value, _} =
      value
      |> to_string()
      |> String.trim()
      |> String.replace(",", "")
      # |> IO.inspect(label: "from_float value: ")
      |> Float.parse
    
    f_value
  end

  def from_float_10k(value) do
    value
    |> from_float()
    |> multiply(10_000)
  end

  def from_float_1000k(value) do
    value
    |> from_float()
    |> multiply(1_000_000)
  end

  def from_percentage(nil), do: nil
  def from_percentage(""), do: nil
  def from_percentage(value), do: from_float(value)

  def from_shareholder(nil), do: %{}
  def from_shareholder(value) do
    src_value = 
      value
      |> to_string()
      |> String.trim()
      |> String.split("</br>")
      
    {holder, hold_count, hold_rate} = fetch_shareholder(src_value)

    %{
      "股东名称" => holder,
      "持股数量" => from_float(hold_count),
      "持股比例" => from_percentage(hold_rate),
      "股份性质" => "",
      "股东排名" => "1",
    }
    # |> IO.inspect(label: "[from_shareholder] ")
  end

  defp fetch_shareholder(nil), do: {"", "", ""}
  defp fetch_shareholder([""]), do: {"", "", ""}
  defp fetch_shareholder(src) do
    [
      "股东名称：" <> holder,
      "持股数量：" <> hold_count,
      "持股比例：" <> hold_rate
    ] = src
    {holder, hold_count, hold_rate}
  end

  defp multiply(nil, _), do: nil
  defp multiply(value, times), do: value * times

end
