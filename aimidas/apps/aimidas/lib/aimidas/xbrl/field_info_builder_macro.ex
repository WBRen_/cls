defmodule Aimidas.Xbrl.FieldInfoBuilderMacro do
  @unit_none ""
  @unit_yuan "元"

  defmacro build_field_info(field_name, field_group, unit, input_converter, output_converter) do
    quote do
      %{
        field_name: unquote(field_name),
        group: unquote(field_group),
        unit: unquote(unit),
        in_converter: unquote(input_converter),
        out_converter: unquote(output_converter)
      }
    end
  end

  defmacro smart_float_field_info(field_name, group, unit \\@unit_yuan) do
    quote do
      build_field_info(
        unquote(field_name),
        unquote(group),
        unquote(unit),
        :from_float,
        :to_smart_float
      )
    end
  end

  defmacro currency_field_info(field_name, group) do
    quote do
      smart_float_field_info(
        unquote(field_name),
        unquote(group),
        unquote(@unit_yuan)
      )
    end
  end

  defmacro field_infos_builder(fields) do
    quote do
      %{
        
      }
    end
  end

  def percentage_field_info(field_name, group, unit \\@unit_yuan) do
    build_field_info(
      field_name,
      group,
      unit,
      :from_float,
      :to_percentage
    )
  end

  def text_field_info(field_name, group) do
    build_field_info(
      field_name,
      group,
      @unit_none,
      :from_float,
      :to_auto_currency
    )
  end

  def integer_field_info(field_name, group, unit \\@unit_none) do
    build_field_info(
      field_name,
      group,
      unit,
      :from_float,
      :to_auto_currency
    )
  end

  def build_field_info_(field_name, field_group, unit, input_converter, output_converter) do
    %{
      field_name: field_name,
      group: field_group,
      unit: unit,
      in_converter: input_converter,
      out_converter: output_converter
    }
  end
end