defmodule Aimidas.Xbrl.SZ.XbrlFieldInfos do
  alias Aimidas.Xbrl.SZ.{
    BasicInfoFields,
    DividendInfoFields,
    # BalanceInfoFields,
    # CapitalInfoFields,
    # CashInfoFields,
    # ProfitInfoFields
  }

  def field_info(field_name) do
    field_infos()
    |> Map.get(field_name)
  end

  def field_infos do
    %{}
    |> Map.merge(BasicInfoFields.field_infos)
    |> Map.merge(DividendInfoFields.field_infos)
    # |> Map.merge(CapitalInfoFields.field_infos)
    # |> Map.merge(CashInfoFields.field_infos)
    # |> Map.merge(ProfitInfoFields.field_infos)
  end
end