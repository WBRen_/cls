defmodule Aimidas.Xbrl.SZ.DividendInfoFields do
  import Aimidas.Xbrl.FieldInfoBuilder

  @group "分红"

  def fields do
    [
      # [字段名称, 输入转换, 输出转换]
      ["分红年度", :convert_float, "元"],
      ["分红方案", :convert_float, "元"],
      ["股权登记日", :convert_float, "元"],
      ["除权基准日", :convert_float, "元"],
      ["新增股份上市日", :convert_float, "元"],
    ]
  end

  def field_infos do
    %{
      "分红年度" => text_field_info("分红年度", @group),
      "分红方案" => text_field_info("分红方案", @group),
      "股权登记日" => text_field_info("股权登记日", @group),
      "除权基准日" => text_field_info("除权基准日", @group),
      "新增股份上市日" => text_field_info("新增股份上市日", @group),
    }
  end
end
