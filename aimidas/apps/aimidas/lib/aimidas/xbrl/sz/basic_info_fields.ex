defmodule Aimidas.Xbrl.SZ.BasicInfoFields do
  import Aimidas.Xbrl.FieldInfoBuilder

  @group "基本信息"
  @stock_unit "股"

  def fields do
    [
      # [字段名称, 输入转换, 输出转换]
      ["扣除非经常性损益每股收益", :convert_float, "元"],
      ["每股经营现金流量", :convert_float, "元"],
      ["每股净资产", :convert_float, "元"],
      ["每股资本公积金", :convert_float, "元"],
      ["每股未分配利润", :convert_float, "元"],
      ["每10股现金分红", :convert_float, "元"],
      ["总股本", :convert_float, "万股"],
      ["流通A股", :convert_float, "万股"],
      ["其他（B、H）流通股", :convert_float, "万股"],
      ["销售费用", :convert_float, "百万元"],
      ["管理费用", :convert_float, "百万元"],
      ["财务费用", :convert_float, "百万元"],
      ["净利润", :convert_float, "百万元"],
      ["净利润率", :convert_float, "%"],
      ["营运资金", :convert_float, "百万元"],
      ["长期负债", :convert_float, "百万元"],
      ["股东权益", :convert_float, "百万元"],
      ["总资产报酬率", :convert_float, "%"],
      ["净资产收益率", :convert_float, "%"]
    ]
  end

  def field_infos do
    %{
      "扣除非经常性损益每股收益" => currency_field_info("扣除非经常性损益每股收益", @group),
      "每股经营现金流量" => currency_field_info("每股经营现金流量", @group),
      "每股净资产" => currency_field_info("每股净资产", @group),
      "每股资本公积金" => currency_field_info("每股资本公积金", @group),
      "每股未分配利润" => currency_field_info("每股未分配利润", @group),
      "每10股现金分红" => currency_field_info("每10股现金分红", @group),
      "总股本" => smart_float_10k_field_info("总股本", @group, @stock_unit),
      "流通A股" => smart_float_10k_field_info("流通A股", @group, @stock_unit),
      "其他（B、H）流通股" => smart_float_10k_field_info("其他（B、H）流通股", @group, @stock_unit),
      "销售费用" => millions_currency_field_info("销售费用", @group),
      "管理费用" => millions_currency_field_info("管理费用", @group),
      "财务费用" => millions_currency_field_info("财务费用", @group),
      "净利润" => millions_currency_field_info("净利润", @group),
      "净利润率" => percentage_field_info("净利润", @group),
      "营运资金" => millions_currency_field_info("营运资金", @group),
      "长期负债" => millions_currency_field_info("长期负债", @group),
      "股东权益" => millions_currency_field_info("股东权益", @group),
      "总资产报酬率" => percentage_field_info("总资产报酬率", @group),
      "净资产收益率" => percentage_field_info("净资产收益率", @group),
    }
  end
end
