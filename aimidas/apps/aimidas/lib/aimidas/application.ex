defmodule Aimidas.Application do
  @moduledoc """
  The Aimidas Application Service.

  The aimidas system business domain lives in this application.

  Exposes API to clients such as the `AimidasWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link([
      supervisor(Aimidas.Repo, []),
    ], strategy: :one_for_one, name: Aimidas.Supervisor)
  end
end
