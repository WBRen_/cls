defprotocol Aimidas.ModelComparable do
  @moduledoc """
  模型比对
  """

  @fallback_to_any true
  def equals(left, right)
  def list_equals(left, right)
end

defimpl Aimidas.ModelComparable, for: Any do
  def equals(nil, nil), do: true
  def equals(nil, _), do: false
  def equals(_, nil), do: false

  def list_equals(nil, nil), do: true
  def list_equals([], []), do: true
  def list_equals(nil, _), do: false
  def list_equals(_, nil), do: false
  def list_equals([], [_ | _]), do: false
  def list_equals([_ | _], []), do: false
  def list_equals([left_head | left_tail], [right_head | right_tail]) do
    Aimidas.ModelComparable.equals(left_head, right_head) &&
      list_equals(left_tail, right_tail)
  end
end
