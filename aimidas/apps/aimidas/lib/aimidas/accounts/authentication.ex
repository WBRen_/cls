defmodule Aimidas.Accounts.Authentication do
  @moduledoc """
  OAuth登录授权模型，用于记录第三方授权信息
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Accounts.User

  @derive {Poison.Encoder,
           only: [:id, :email, :image, :name, :nickname, :provider,
                  :refresh_token, :telephone, :uid, :union_id, :user_id,
                  :inserted_at, :updated_at]}
  schema "authentications" do
    field :email, :string
    field :image, :string
    field :name, :string
    field :nickname, :string
    field :provider, :string
    field :refresh_token, :string
    field :telephone, :string
    field :token, :string
    field :token_secret, :string
    field :uid, :string
    field :union_id, :string
    # field :user_id, :integer

    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(authentication, attrs) do
    authentication
    |> cast(attrs, [:uid, :provider, :name, :nickname, :email, :telephone,
                    :image, :user_id, :token, :refresh_token, :token_secret,
                    :union_id])
    |> validate_required([:uid, :provider, :name, :nickname, :image, :token])
  end
end
