defmodule Aimidas.Accounts.User do
  @moduledoc """
  用户模型
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Accounts.Authentication
  alias Aimidas.Stocks.Portfolio
  alias Aimidas.Points.Point
  alias Aimidas.Points.PointRecord
  alias Aimidas.Statistics.UserAnswer

  @derive {Poison.Encoder,
           only: [:id, :avatar, :nickname, :telephone, :inserted_at,
                  :updated_at]}
  schema "users" do
    field :avatar, :string
    field :nickname, :string
    field :password_digest, :string
    field :password_salt, :string
    field :telephone, :string

    has_many :authentications, Authentication
    has_many :portfolios, Portfolio
    has_many :point_records, PointRecord
    has_many :user_answers, UserAnswer
    has_one :point, Point

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:nickname, :telephone, :password_digest, :password_salt, :avatar])
    # |> validate_required([:nickname, :password_digest, :password_salt, :avatar])
  end
end
