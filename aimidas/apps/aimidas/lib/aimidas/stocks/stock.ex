defmodule Aimidas.Stocks.Stock do
  @moduledoc """
  股票信息
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "stocks" do
    field :code, :string
    field :exchange_type_code, :string # 新增证券交易所代码，如： SS.ESA上证,SZ.ESA深证,XBHS.HY沪深板块
    field :last_px, :decimal
    field :last_px_at, :utc_datetime
    field :name, :string
    field :px_exchange_rate, :decimal

    timestamps()
  end

  @doc false
  def changeset(stock, attrs) do
    stock
    |> cast(attrs, [:code, :exchange_type_code, :name, :last_px, :last_px_at, :px_exchange_rate])
    |> validate_required([:code, :name])
  end
end
