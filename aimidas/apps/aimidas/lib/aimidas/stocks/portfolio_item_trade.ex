defmodule Aimidas.Stocks.PortfolioItemTrade do
  @moduledoc """
  用户模型
  """
  use Ecto.Schema
  import Ecto.Changeset

  Aimidas.Stocks.PortfolioItem

  schema "portfolio_item_trades" do
    field :type, :string
    field :quantity, :integer
    field :price, :decimal

    belongs_to :portfolio_item, PortfolioItem

    timestamps()
  end

  @doc false
  def changeset(point, attrs) do
    point
    |> cast(attrs, [:price, :quantity, :portfolio_item_id, :type])
    |> validate_required([])
  end
end
