defmodule Aimidas.Stocks do
  @moduledoc """
  The Stocks context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Stocks.Portfolio

  alias Aimidas.Stocks.PortfolioItemTrade

  @doc """
  Returns the list of portfolios.

  ## Examples

      iex> list_portfolios()
      [%Portfolio{}, ...]

  """
  def list_portfolios do
    Repo.all(Portfolio)
  end

  @doc """
  Gets a single portfolio.

  Raises `Ecto.NoResultsError` if the Portfolio does not exist.

  ## Examples

      iex> get_portfolio!(123)
      %Portfolio{}

      iex> get_portfolio!(456)
      ** (Ecto.NoResultsError)

  """
  def get_portfolio!(id), do: Repo.get!(Portfolio, id)

  def get_user_portfolio(user_id, portfolio_id) do
    case Portfolio
          |> where(id: ^portfolio_id, user_id: ^user_id)
          |> Repo.all
          |> List.first do
      nil ->
        {:error, "portfolio not found"}

      portfolio ->
        {:ok, portfolio}
    end
  end

  def get_user_portfolio_price(portfolio_id) do
    result=
       Portfolio
          |> where(id: ^portfolio_id)
          |> Repo.all
          |> List.first
    if result.total_price == nil do
      attr = %{
        total_price: Decimal.new("0")
      }
      result =
        result
        |> update_portfolio(attr)
    else
      {:ok, result}
    end
  end

  @doc """
  Creates a portfolio.

  ## Examples

      iex> create_portfolio(%{field: value})
      {:ok, %Portfolio{}}

      iex> create_portfolio(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_portfolio(attrs \\ %{}) do
    %Portfolio{}
    |> Portfolio.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a portfolio.

  ## Examples

      iex> update_portfolio(portfolio, %{field: new_value})
      {:ok, %Portfolio{}}

      iex> update_portfolio(portfolio, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_portfolio(%Portfolio{} = portfolio, attrs) do
    portfolio
    |> Portfolio.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Portfolio.

  ## Examples

      iex> delete_portfolio(portfolio)
      {:ok, %Portfolio{}}

      iex> delete_portfolio(portfolio)
      {:error, %Ecto.Changeset{}}

  """
  def delete_portfolio(%Portfolio{} = portfolio) do
    Repo.delete(portfolio)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking portfolio changes.

  ## Examples

      iex> change_portfolio(portfolio)
      %Ecto.Changeset{source: %Portfolio{}}

  """
  def change_portfolio(%Portfolio{} = portfolio) do
    Portfolio.changeset(portfolio, %{})
  end

  alias Aimidas.Stocks.Stock

  @doc """
  Returns the list of stocks.

  ## Examples

      iex> list_stocks()
      [%Stock{}, ...]

  """
  def list_stocks do
    Repo.all(Stock)
  end

  @doc """
  Returns the list of stocks by search_word.

  ## Examples

      iex> list_stocks_by_search_word()
      [%Stock{}, ...]

  """
  def list_stocks_by_search_word(word) do
    query = from stock in Stock,
                 where: like(stock.code, ^"%#{word}%"),
                 limit: 10
    case Repo.all(query) do
      [] ->
        query2 = from stock in Stock,
                where: like(stock.name, ^"%#{word}%"),
                limit: 10
        case Repo.all(query2) do
          nil ->
            {:error, "stock not found"}
          stocks ->
            {:ok, stocks}
        end
      stocks ->
        {:ok, stocks}
    end
  end

  def get_stock_by_search_word(word) do
    query = from stock in Stock,
                 where: like(stock.name, ^"%#{word}%"),
                 limit: 10
    case Repo.all(query)
         |> List.first do
      nil ->
        {:error, "stock not found"}
      stock ->
        {:ok, stock}
    end
  end

  @doc """
  Gets a single stock.

  Raises `Ecto.NoResultsError` if the Stock does not exist.

  ## Examples

      iex> get_stock!(123)
      %Stock{}

      iex> get_stock!(456)
      ** (Ecto.NoResultsError)

  """
  def get_stock!(id), do: Repo.get!(Stock, id)

  def get_stock(id) do
    case Stock
         |> where(id: ^id)
         |> Repo.all
         |> List.first do
      nil ->
        {:error, "stock not found"}
      stock ->
        {:ok, stock}
    end
  end

  def get_stock_by_code(code) do
    case Stock
         |> where(code: ^code)
         |> Repo.all
         |> List.first do
      nil ->
        {:error, "stock not found"}
      stock ->
        {:ok, stock}
    end
  end

  @doc """
  Creates a stock.

  ## Examples

      iex> create_stock(%{field: value})
      {:ok, %Stock{}}

      iex> create_stock(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stock(attrs \\ %{}) do
    %Stock{}
    |> Stock.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stock.

  ## Examples

      iex> update_stock(stock, %{field: new_value})
      {:ok, %Stock{}}

      iex> update_stock(stock, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stock(%Stock{} = stock, attrs) do
    stock
    |> Stock.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Stock.

  ## Examples

      iex> delete_stock(stock)
      {:ok, %Stock{}}

      iex> delete_stock(stock)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stock(%Stock{} = stock) do
    Repo.delete(stock)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking stock changes.

  ## Examples

      iex> change_stock(stock)
      %Ecto.Changeset{source: %Stock{}}

  """
  def change_stock(%Stock{} = stock) do
    Stock.changeset(stock, %{})
  end

  alias Aimidas.Stocks.PortfolioItem

  @doc """
  Returns the list of portfolio_items.

  ## Examples

      iex> list_portfolio_items()
      [%PortfolioItem{}, ...]

  """
  def list_portfolio_items do
    Repo.all(PortfolioItem)
  end
  def list_portfolio_items(preloads) do
    list_portfolio_items |> Repo.preload(preloads)
  end

  @doc """
  Gets a single portfolio_item.

  Raises `Ecto.NoResultsError` if the Portfolio item does not exist.

  ## Examples

      iex> get_portfolio_item!(123)
      %PortfolioItem{}

      iex> get_portfolio_item!(456)
      ** (Ecto.NoResultsError)

  """
  def get_portfolio_item!(id), do: Repo.get!(PortfolioItem, id)

  def get_user_portfolio_item(user_id, portfolio_id, stock_id) do
    case PortfolioItem
         |> where(portfolio_id: ^portfolio_id,
                  user_id: ^user_id,
                  stock_id: ^stock_id)
         |> Repo.all
         |> List.first do
      nil ->
        {:error, "portfolio_item not found"}

      portfolio_item ->
        {:ok, portfolio_item}
    end
  end

  def get_portfolio_items(portfolio_id) do
    case PortfolioItem
         |> where(portfolio_id: ^portfolio_id)
         |> Repo.all do
      [] ->
        {:error, "portfolio_item not found"}
      portfolio_items ->
        {:ok, portfolio_items}
    end
  end

  @doc """
  Creates a portfolio_item.

  ## Examples

      iex> create_portfolio_item(%{field: value})
      {:ok, %PortfolioItem{}}

      iex> create_portfolio_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_portfolio_item(attrs \\ %{}) do
    %PortfolioItem{}
    |> PortfolioItem.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a portfolio_item.

  ## Examples

      iex> update_portfolio_item(portfolio_item, %{field: new_value})
      {:ok, %PortfolioItem{}}

      iex> update_portfolio_item(portfolio_item, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_portfolio_item(%PortfolioItem{} = portfolio_item, attrs) do
    portfolio_item
    |> PortfolioItem.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PortfolioItem.

  ## Examples

      iex> delete_portfolio_item(portfolio_item)
      {:ok, %PortfolioItem{}}

      iex> delete_portfolio_item(portfolio_item)
      {:error, %Ecto.Changeset{}}

  """
  def delete_portfolio_item(%PortfolioItem{} = portfolio_item) do
    Repo.delete(portfolio_item)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking portfolio_item changes.

  ## Examples

      iex> change_portfolio_item(portfolio_item)
      %Ecto.Changeset{source: %PortfolioItem{}}

  """
  def change_portfolio_item(%PortfolioItem{} = portfolio_item) do
    PortfolioItem.changeset(portfolio_item, %{})
  end

  def get_portfolio_item_trades(portfolio_item_id) do
    case PortfolioItemTrade
         |> where(portfolio_item_id: ^portfolio_item_id)
         |> Repo.all do
      [] ->
        {:error, "not found"}
      portfolio_item_trades ->
        {:ok, portfolio_item_trades}
    end
  end

  def get_portfolio_item_trade(id) do
    case PortfolioItemTrade
          |> where(id: ^id)
          |> Repo.all
          |> List.first do
            nil ->
              {:error, "not found"}

            portfolio_item_trade ->
              {:ok, portfolio_item_trade}
    end
  end

  def get_portfolio_item!(id), do: Repo.get!(PortfolioItem, id)

  def create_portfolio_item_trade(attrs \\ %{}) do
    %PortfolioItemTrade{}
    |> PortfolioItemTrade.changeset(attrs)
    |> Repo.insert()
  end

  def delete_portfolio_item_trade(%PortfolioItemTrade{} = portfolio_item_trade) do
    Repo.delete(portfolio_item_trade)
  end

  def update_portfolio_item_trade(%PortfolioItemTrade{} = portfolio_item_trade, attrs) do
    portfolio_item_trade
    |> PortfolioItemTrade.changeset(attrs)
    |> Repo.update()
  end
end
