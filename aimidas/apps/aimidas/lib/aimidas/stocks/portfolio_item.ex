defmodule Aimidas.Stocks.PortfolioItem do
  @moduledoc """
  投资组合明细
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Accounts.User
  alias Aimidas.Stocks.{Portfolio, Stock, PortfolioItemTrade}

  schema "portfolio_items" do
    belongs_to :user, User
    belongs_to :portfolio, Portfolio
    belongs_to :stock, Stock

    field :total_quantity, :integer, default: 0
    field :average_price, :decimal, default: Decimal.new("0")
    field :total_price, :decimal, default: Decimal.new("0")

    has_many :portfolio_item_trades, PortfolioItemTrade, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(portfolio_item, attrs) do
    portfolio_item
    |> cast(attrs, [:user_id, :portfolio_id, :stock_id, :total_quantity, :average_price, :total_price])
    |> validate_required([:user_id, :portfolio_id, :stock_id])
    |> unique_constraint(:stock, name: :portfolio_items_portfolio_id_stock_id_index)
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Stocks.PortfolioItem do
    def equals(left, right) do
      left.id == right.id and
      left.user_id == right.user_id and
      left.portfolio_id == right.portfolio_id and
      left.stock_id == right.stock_id
    end
  end
end
