defmodule Aimidas.Stocks.Portfolio do
  @moduledoc """
  投资组合
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Repo
  alias Aimidas.Accounts.User
  alias Aimidas.Stocks.PortfolioItem

  schema "portfolios" do
    field :name, :string
    field :total_price, :decimal

    has_many :portfolio_items, PortfolioItem, on_delete: :delete_all
    belongs_to :user, User, on_replace: :mark_as_invalid

    timestamps()
  end

  @doc false
  def changeset(portfolio, attrs) do
    portfolio
    |> cast(attrs, [:user_id, :name, :total_price])
    |> validate_required([:user_id, :name])
    |> unsafe_validate_unique([:name, :user_id], Repo)
    |> unique_constraint(:name, name: "portfolios_user_id_name_index")
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Stocks.Portfolio do
    def equals(left, right) do
      left.id == right.id and
      left.user_id == right.user_id and
      left.name == right.name
    end
  end
end
