defmodule Aimidas.Management do
  @moduledoc """
  The Management context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Management.HotQuestion

  @doc """
  Returns the list of hot_questions.

  ## Examples

      iex> list_hot_questions()
      [%HotQuestion{}, ...]

  """
  def list_hot_questions do
    Repo.all(HotQuestion)
  end

  @doc """
  Gets a single hot_question.

  Raises `Ecto.NoResultsError` if the Hot question does not exist.

  ## Examples

      iex> get_hot_question!(123)
      %HotQuestion{}

      iex> get_hot_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_hot_question!(id), do: Repo.get!(HotQuestion, id)

  def find_hot_question!() do
    query1 =
      from q in HotQuestion,
      where: q.level == 1,
      order_by: fragment("RANDOM()"),
      # left_join: u in UserAnswer, on: (u.question_id == q.id and u.status == 101),
      limit: 3
    query2 =
      from q in HotQuestion,
      where: q.level == 2,
      order_by: fragment("RANDOM()"),
      # left_join: u in UserAnswer, on: (u.question_id == q.id and u.status == 101),
      limit: 3
    query3 =
      from q in HotQuestion,
      where: q.level == 3,
      order_by: fragment("RANDOM()"),
      # left_join: u in UserAnswer, on: (u.question_id == q.id and u.status == 101),
      limit: 3
    question1 =
      query1
      |> Repo.all
      |> List.first
    question2 =
      query2
      |> Repo.all
      |> List.first
    question3 =
      query3
      |> Repo.all
      |> List.first
    list = []
    if question1 != nil do
      list = [question1 | list]
    end
    if question2 != nil do
      list = [question2 | list]
    end
    if question3 != nil do
      list = [question3 | list]
    end
    list
  end

  @doc """
  Creates a hot_question.

  ## Examples

      iex> create_hot_question(%{field: value})
      {:ok, %HotQuestion{}}

      iex> create_hot_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_hot_question(attrs \\ %{}) do
    %HotQuestion{}
    |> HotQuestion.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a hot_question.

  ## Examples

      iex> update_hot_question(hot_question, %{field: new_value})
      {:ok, %HotQuestion{}}

      iex> update_hot_question(hot_question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_hot_question(%HotQuestion{} = hot_question, attrs) do
    hot_question
    |> HotQuestion.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a HotQuestion.

  ## Examples

      iex> delete_hot_question(hot_question)
      {:ok, %HotQuestion{}}

      iex> delete_hot_question(hot_question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_hot_question(%HotQuestion{} = hot_question) do
    Repo.delete(hot_question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking hot_question changes.

  ## Examples

      iex> change_hot_question(hot_question)
      %Ecto.Changeset{source: %HotQuestion{}}

  """
  def change_hot_question(%HotQuestion{} = hot_question) do
    HotQuestion.changeset(hot_question, %{})
  end
end
