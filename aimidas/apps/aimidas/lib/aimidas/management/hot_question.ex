defmodule Aimidas.Management.HotQuestion do
  use Ecto.Schema
  import Ecto.Changeset


  schema "hot_questions" do
    field :content, :string
    field :level, :integer

    timestamps()
  end

  @doc false
  def changeset(hot_question, attrs) do
    hot_question
    |> cast(attrs, [:content, :level])
    |> validate_required([:content, :level])
  end
end
