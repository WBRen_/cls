defmodule Aimidas.Feedback.Advice do
  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Accounts.User

  schema "advices" do
    field :content, :string
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(advice, attrs) do
    advice
    |> cast(attrs, [:content, :user_id])
    |> validate_required([:content, :user_id])
  end
end
