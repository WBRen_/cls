defmodule Aimidas.Qa.PdfQnaQuestion do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pdf_qna_questions" do
    field :md5, :string
    # field :query, :string
    field :answer, :string
    field :otpq, :map

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:md5, :answer, :otpq])
    # |> validate_required([:md5, :answer, :otpq])
    |> validate_required([:md5, :answer])
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Qa.PdfQnaQuestion do
    def equals(left, right) do
      left.id == right.id and
      left.md5 == right.md5
    end
  end
end
