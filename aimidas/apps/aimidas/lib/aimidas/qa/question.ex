defmodule Aimidas.Qa.Question do
  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Accounts.User
  alias Aimidas.Qa.QuestionGroup

  schema "questions" do
    field :query, :string
    field :answer, :string
    field :answer_level, :integer
    # field :last_otpq, :map
    field :otpq, :map
    field :data, :map

    belongs_to :user, User
    belongs_to :question_group, QuestionGroup

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:user_id, :question_group_id, :answer, :answer_level, :otpq, :query, :data])
    |> validate_required([:answer, :answer_level, :otpq, :query])
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Qa.Question do
    def equals(left, right) do
      left.id == right.id and
      left.user_id == right.user_id and
      left.question_group_id == right.question_group_id
    end
  end
end
