defmodule Aimidas.Qna.PdfPage do
  use Ecto.Schema
  import Ecto.Changeset


  schema "pdf_pages" do
    field :company_code, :string
    field :content, :string
    field :end_page, :integer
    field :image_url, :string
    # field :is_abstract, :integer
    # field :quarter, :integer
    field :start_page, :integer
    field :year, :integer
    field :src_updated_at, :integer
    field :type_code, :string
    field :detail_code, :string

    timestamps()
  end

  @doc false
  def changeset(pdf_page, attrs) do
    pdf_page
    |> cast(attrs, [:content, :start_page, :end_page, :image_url, :company_code, :year, :type_code, :detail_code, :src_updated_at])
    |> validate_required([:content, :start_page, :end_page, :image_url, :company_code, :year, :type_code, :detail_code, :src_updated_at])
  end
end
