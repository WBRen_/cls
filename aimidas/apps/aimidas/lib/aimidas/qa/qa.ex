defmodule Aimidas.Qa do
  @moduledoc """
  The Qa context.
  """

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Qa.QuestionGroup
  alias Aimidas.Qa.ProQnaQuestion
  alias Aimidas.Qa.DmQnaQuestion
  alias Aimidas.Qa.PdfQnaQuestion
  alias Aimidas.Qna.PdfPage

  @doc """
  Returns the list of QuestionGroups.

  ## Examples
      iex> list_question_groups()
      [%QuestionGroup{}, ...]

  """
  def list_question_groups do
    Repo.all(QuestionGroup)
  end

  @doc """
  Gets a single QuestionGroup.

  Raises `Ecto.NoResultsError` if the QuestionGroup does not exist.

  ## Examples

      iex> get_question_group!(123)
      %QuestionGroup{}

      iex> get_question_group!(456)
      ** (Ecto.NoResultsError)

  """
  def get_question_group!(id), do: Repo.get!(QuestionGroup, id)

  def get_question_group_by_uesr(user_id) do
    case QuestionGroup
          |> where(user_id: ^user_id)
          |> order_by(desc: :id)
          |> Repo.all()
          |> List.first() do
      nil ->
        {:error, "QuestionGroup not found"}

      question_group ->
        {:ok, question_group}
    end
  end

  @doc """
  Creates a question_group.

  ## Examples

      iex> create_question_group(%{field: value})
      {:ok, %QuestionGroup{}}

      iex> create_question_group(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_question_group(attrs \\ %{}) do
    %QuestionGroup{}
    |> QuestionGroup.changeset(attrs)
    # |> IO.inspect(label: "插入内容", pretty: true)
    |> Repo.insert()
  end

  @doc """
  Updates a QuestionGroup.

  ## Examples

      iex> update_question_group(QuestionGroup, %{field: new_value})
      {:ok, %QuestionGroup{}}

      iex> update_question_group(QuestionGroup, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_question_group(%QuestionGroup{} = question_group, attrs) do
    question_group
    |> QuestionGroup.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a question_group.

  ## Examples

      iex> delete_question_group(QuestionGroup)
      {:ok, %QuestionGroup{}}

      iex> delete_question_group(QuestionGroup)
      {:error, %Ecto.Changeset{}}

  """
  def delete_question_group(%QuestionGroup{} = question_group) do
    Repo.delete(question_group)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking QuestionGroup changes.

  ## Examples

      iex> change_question_group(QuestionGroup)
      %Ecto.Changeset{source: %QuestionGroup{}}

  """
  def change_question_group(%QuestionGroup{} = question_group) do
    QuestionGroup.changeset(question_group, %{})
  end


  alias Aimidas.Qa.Question

  @doc """
  Returns the list of question.

  ## Examples

      iex> list_question()
      [%Question{}, ...]

  """
  def list_questions do
    Repo.all(Question)
  end
  def list_questions(preloads) do
    list_questions |> Repo.preload(preloads)
  end

  @doc """
  Gets a single question.

  Raises `Ecto.NoResultsError` if the Question does not exist.

  ## Examples

      iex> get_question!(123)
      %Question{}

      iex> get_question!(456)
      ** (Ecto.NoResultsError)

  """
  def get_question!(id), do: Repo.get!(Question, id)

  def get_user_questions(user_id) do
    Question
    |> where(user_id: ^user_id)
    |> Repo.all
  end

  def get_user_last_question(user_id) do
    question =
      Question
      |> where(user_id: ^user_id)
      |> order_by(desc: :id)
      |> limit(1)
      |> Repo.all()
      |> List.first()

    case question do
      nil -> {:error, "question not found"}
      _ -> {:ok, question}
    end
  end

  def get_questions_by_user_group(user_id, question_group_id) do
    case Question
        |> where(user_id: ^user_id,
                 question_group_id: ^question_group_id)
        |> Repo.all do
      nil ->
        {:error, "question not found"}

      question ->
        {:ok, question}
    end
  end

  def find_question_groups(user_id, timestamp, limit) do
    result =
      Repo.all(from q in QuestionGroup,
                where:  q.user_id == ^user_id
                  and   q.inserted_at < ^timestamp,
                order_by: [desc: q.inserted_at],
                limit:  ^limit)

    case result do
      nil ->
        {:error, "question groups not found"}
      [] ->
        {:error, "question groups is empty"}
      _ ->
        {:ok, result}
    end

  end


  @doc """
  Creates a question.

  ## Examples

      iex> create_question(%{field: value})
      {:ok, %Question{}}

      iex> create_question(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_question(attrs \\ %{}) do
    %Question{}
    |> Question.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a question.

  ## Examples

      iex> update_question(question, %{field: new_value})
      {:ok, %Question{}}

      iex> update_question(question, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_question(%Question{} = question, attrs) do
    question
    |> Question.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Question.

  ## Examples

      iex> delete_question(question)
      {:ok, %Question{}}

      iex> delete_question(question)
      {:error, %Ecto.Changeset{}}

  """
  def delete_question(%Question{} = question) do
    Repo.delete(question)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking question changes.

  ## Examples

      iex> change_question(question)
      %Ecto.Changeset{source: %Question{}}

  """
  def change_question(%Question{} = question) do
    Question.changeset(question, %{})
  end


  """
  专家问答
  """
  def find_pro_qna_question(md5) do
    question =
      ProQnaQuestion
      |> where(md5: ^md5)
      |> Repo.all()
      |> List.first()

    case question do
      nil -> {:error, "pro_qna_question not found"}
      _ -> {:ok, question}
    end
  end

  def create_pro_qna_question(attrs \\ %{}) do
    %ProQnaQuestion{}
    |> ProQnaQuestion.changeset(attrs)
    |> Repo.insert()
  end

  def update_pro_qna_question(%ProQnaQuestion{} = pro_qna_question, attrs) do
    pro_qna_question
    |> ProQnaQuestion.changeset(attrs)
    |> Repo.update()
  end

  def change_pro_qna_question(%ProQnaQuestion{} = pro_qna_question) do
    ProQnaQuestion.changeset(pro_qna_question, %{})
  end

  def delete_pro_qna_questions(md5s) do
    ProQnaQuestion
      |> where([q], q.md5 in ^md5s)
      |> Repo.delete_all()
  end

  """
  董秘问答
  """
  def find_dm_qna_question(md5) do
    question =
      DmQnaQuestion
      |> where(md5: ^md5)
      |> Repo.all()
      |> List.first()

    case question do
      nil -> {:error, "dm_qna_question not found"}
      _ -> {:ok, question}
    end
  end

  def create_dm_qna_question(attrs \\ %{}) do
    %DmQnaQuestion{}
    |> DmQnaQuestion.changeset(attrs)
    |> Repo.insert()
  end

  def update_dm_qna_question(%DmQnaQuestion{} = dm_qna_question, attrs) do
    dm_qna_question
    |> DmQnaQuestion.changeset(attrs)
    |> Repo.update()
  end

  def change_dm_qna_question(%DmQnaQuestion{} = dm_qna_question) do
    DmQnaQuestion.changeset(dm_qna_question, %{})
  end

  def delete_dm_qna_questions(md5s) do
    DmQnaQuestion
      |> where([q], q.md5 in ^md5s)
      |> Repo.delete_all()
  end

  """
  PDF问答
  """
  def find_pdf_qna_question(md5) do
    question =
      PdfQnaQuestion
      |> where(md5: ^md5)
      |> Repo.all()
      |> List.first()

    case question do
      nil -> {:error, "pdf_qna_question not found"}
      _ -> {:ok, question}
    end
  end

  def create_pdf_qna_question(attrs \\ %{}) do
    %PdfQnaQuestion{}
    |> PdfQnaQuestion.changeset(attrs)
    |> Repo.insert()
  end

  def update_pdf_qna_question(%PdfQnaQuestion{} = pdf_qna_question, attrs) do
    pdf_qna_question
    |> PdfQnaQuestion.changeset(attrs)
    |> Repo.update()
  end

  def change_pdf_qna_question(%PdfQnaQuestion{} = pdf_qna_question) do
    PdfQnaQuestion.changeset(pdf_qna_question, %{})
  end

  def delete_pdf_qna_questions(md5s) do
    PdfQnaQuestion
      |> where([q], q.md5 in ^md5s)
      |> Repo.delete_all()
  end

  ####
  @doc """
  Aimidas.Qa.list_pdf_page_pictures("601318", "2017", "2", "1", [108, 109])
  """
  def list_pdf_page_pictures(company_code, year, type_code, detail_code, page_nums) do
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    # |> where(start_page in ^page_nums)
    |> where([p], p.start_page in ^page_nums)
    |> order_by([asc: :start_page])
    |> Repo.all
    |> Enum.map(fn page ->
      page.image_url
    end)
  end

  def list_pdf_page_picture(company_code, year, type_code, detail_code, page_num) do
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    # |> where(start_page in ^page_nums)
    |> where(start_page: ^page_num)
    |> Repo.all
    |> Enum.map(fn page ->
      page.image_url
    end)
    |> List.first()
  end

  def list_pdf_group(company_code, year, type_code, detail_code, page_nums) do
    page_nums |> Enum.map(fn page_num -> get_pdf_group(company_code, year, type_code, detail_code, page_num) end)
  end

  def get_pdf_group(company_code, year, type_code, detail_code, page_num) do
    group = [page_num - 5, page_num - 4, page_num - 3, page_num - 2, page_num - 1, page_num, page_num + 1, page_num + 2, page_num + 3, page_num + 4, page_num + 5]
    group |> Enum.map(fn page -> list_pdf_page_picture(company_code, year, type_code, detail_code, page) end)
  end


  def list_pdf_page_pictures(company_code, year, type_code, detail_code, updated_at, page_nums) do
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    |> where(src_updated_at: ^updated_at)
    # |> where(start_page in ^page_nums)
    |> where([p], p.start_page in ^page_nums)
    |> order_by([asc: :start_page])
    |> Repo.all
    |> Enum.map(fn page ->
      page.image_url
    end)
  end

  def list_pdf_page_picture(company_code, year, type_code, detail_code, updated_at, page_num) do
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    |> where(src_updated_at: ^updated_at)
    # |> where(start_page in ^page_nums)
    |> where(start_page: ^page_num)
    |> Repo.all
    |> Enum.map(fn page ->
      page.image_url
    end)
    |> List.first()
  end

  def list_pdf_group(company_code, year, type_code, detail_code, updated_at, page_nums) do
  page_nums |> Enum.map(fn page_num -> get_pdf_group(company_code, year, type_code, detail_code, updated_at, page_num) end)
  end

  def get_pdf_group(company_code, year, type_code, detail_code, updated_at, page_num) do
  group = [page_num - 5, page_num - 4, page_num - 3, page_num - 2, page_num - 1, page_num, page_num + 1, page_num + 2, page_num + 3, page_num + 4, page_num + 5]
  group |> Enum.map(fn page -> list_pdf_page_picture(company_code, year, type_code, detail_code, updated_at, page) end)
  end

  def list_all_pdf_page_pictures(company_code, year, type_code, detail_code, updated_at) do
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    |> where(src_updated_at: ^updated_at)
    |> order_by([asc: :start_page])
    |> Repo.all
    |> Enum.map(fn page ->
      page.image_url
    end)
  end

  def list_all_pdf_page_pictures(company_code, year, type_code, detail_code) do
    PdfPage
    |> where(company_code: ^company_code)
    |> where(year: ^year)
    |> where(type_code: ^type_code)
    |> where(detail_code: ^detail_code)
    |> order_by([asc: :start_page])
    |> Repo.all
    |> Enum.map(fn page ->
      page.image_url
    end)
  end
end
