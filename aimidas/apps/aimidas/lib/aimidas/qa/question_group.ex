defmodule Aimidas.Qa.QuestionGroup do
  use Ecto.Schema
  import Ecto.Changeset

  alias Aimidas.Repo
  alias Aimidas.Accounts.User
  alias Aimidas.Qa.Question

  schema "question_groups" do
    field :name, :string

    has_many :questions, Question
    belongs_to :user, User, on_replace: :mark_as_invalid

    timestamps()
  end

  @doc false
  def changeset(question_group, attrs) do
    question_group
    |> cast(attrs, [:user_id, :name])
    |> validate_required([:user_id, :name])
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Qa.QuestionGroup do
    def equals(left, right) do
      left.id == right.id and
      left.user_id == right.user_id and
      left.name == right.name
    end
  end
end
