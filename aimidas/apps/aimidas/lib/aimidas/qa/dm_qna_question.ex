defmodule Aimidas.Qa.DmQnaQuestion do
  use Ecto.Schema
  import Ecto.Changeset

  schema "dm_qna_questions" do
    field :md5, :string
    field :query, :string
    field :answer, :string

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:md5, :answer, :query])
    |> validate_required([:md5, :answer, :query])
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Qa.DmQnaQuestion do
    def equals(left, right) do
      left.id == right.id and
      left.md5 == right.md5
    end
  end
end
