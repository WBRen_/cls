defmodule Aimidas.Qa.ProQnaQuestion do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pro_qna_questions" do
    field :md5, :string
    field :query, :string
    field :answer, :string

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:md5, :answer, :query])
    |> validate_required([:md5, :answer, :query])
  end

  defimpl Aimidas.ModelComparable, for: Aimidas.Qa.ProQnaQuestion do
    def equals(left, right) do
      left.id == right.id and
      left.md5 == right.md5
    end
  end
end
