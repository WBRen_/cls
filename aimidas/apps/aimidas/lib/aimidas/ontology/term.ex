defmodule Aimidas.Ontology.Term do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder,
           only: [:id, :name, :description, :inserted_at, :updated_at]}
  schema "terms" do
    field :description, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(term, attrs) do
    term
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
  end
end
