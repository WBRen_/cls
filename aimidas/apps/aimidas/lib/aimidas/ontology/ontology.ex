defmodule Aimidas.Ontology do
  @moduledoc """
  The Ontology context.
  """
  # use Appsignal.Instrumentation.Decorators

  import Ecto.Query, warn: false
  alias Aimidas.Repo

  alias Aimidas.Ontology.Term

  @doc """
  Returns the list of terms.

  ## Examples

      iex> list_terms()
      [%Term{}, ...]

  """
  def list_terms do
    Repo.all(Term)
  end

  @doc """
  Gets a single term.

  Raises `Ecto.NoResultsError` if the Term does not exist.

  ## Examples

      iex> get_term!(123)
      %Term{}

      iex> get_term!(456)
      ** (Ecto.NoResultsError)

  """
  def get_term!(id), do: Repo.get!(Term, id)

  # @decorate transaction()
  def find_term_by_name(name) do
    term =
      Term
      |> where(name: ^name)
      |> Repo.all()
      |> List.first()
    case term do
      nil ->
        {:error, "term #{name} not found"}
      _ ->
        {:ok, term}
    end
  end

  @doc """
  Creates a term.

  ## Examples

      iex> create_term(%{field: value})
      {:ok, %Term{}}

      iex> create_term(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_term(attrs \\ %{}) do
    %Term{}
    |> Term.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a term.

  ## Examples

      iex> update_term(term, %{field: new_value})
      {:ok, %Term{}}

      iex> update_term(term, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_term(%Term{} = term, attrs) do
    term
    |> Term.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Term.

  ## Examples

      iex> delete_term(term)
      {:ok, %Term{}}

      iex> delete_term(term)
      {:error, %Ecto.Changeset{}}

  """
  def delete_term(%Term{} = term) do
    Repo.delete(term)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking term changes.

  ## Examples

      iex> change_term(term)
      %Ecto.Changeset{source: %Term{}}

  """
  def change_term(%Term{} = term) do
    Term.changeset(term, %{})
  end

  alias Aimidas.Ontology.CompanyInfo

  @doc """
  Returns the list of company_infos.

  ## Examples

      iex> list_company_infos()
      [%CompanyInfo{}, ...]

  """
  def list_company_infos do
    Repo.all(CompanyInfo)
  end

  @doc """
  Gets a single company_info.

  Raises `Ecto.NoResultsError` if the Company info does not exist.

  ## Examples

      iex> get_company_info!(123)
      %CompanyInfo{}

      iex> get_company_info!(456)
      ** (Ecto.NoResultsError)

  """
  def get_company_info!(id), do: Repo.get!(CompanyInfo, id)

  # @decorate transaction_event()
  def find_company_info_by_code(code) do
    company =
      CompanyInfo
      |> where(company_code: ^code)
      |> Repo.all()
      |> List.first()
    case company do
      nil ->
        {:error, "company #{code} not found"}
      _ ->
        {:ok, company}
    end
  end

  @doc """
  Creates a company_info.

  ## Examples

      iex> create_company_info(%{field: value})
      {:ok, %CompanyInfo{}}

      iex> create_company_info(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_company_info(attrs \\ %{}) do
    %CompanyInfo{}
    |> CompanyInfo.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a company_info.

  ## Examples

      iex> update_company_info(company_info, %{field: new_value})
      {:ok, %CompanyInfo{}}

      iex> update_company_info(company_info, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_company_info(%CompanyInfo{} = company_info, attrs) do
    company_info
    |> CompanyInfo.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a CompanyInfo.

  ## Examples

      iex> delete_company_info(company_info)
      {:ok, %CompanyInfo{}}

      iex> delete_company_info(company_info)
      {:error, %Ecto.Changeset{}}

  """
  def delete_company_info(%CompanyInfo{} = company_info) do
    Repo.delete(company_info)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking company_info changes.

  ## Examples

      iex> change_company_info(company_info)
      %Ecto.Changeset{source: %CompanyInfo{}}

  """
  def change_company_info(%CompanyInfo{} = company_info) do
    CompanyInfo.changeset(company_info, %{})
  end

  alias Aimidas.Ontology.CompanyXbrl

  @doc """
  Returns the list of company_xbrls.

  ## Examples

      iex> list_company_xbrls()
      [%CompanyXbrl{}, ...]

  """
  def list_company_xbrls do
    Repo.all(CompanyXbrl)
  end

  @doc """
  Gets a single company_xbrl.

  Raises `Ecto.NoResultsError` if the Company xbrl does not exist.

  ## Examples

      iex> get_company_xbrl!(123)
      %CompanyXbrl{}

      iex> get_company_xbrl!(456)
      ** (Ecto.NoResultsError)

  """
  def get_company_xbrl!(id), do: Repo.get!(CompanyXbrl, id)

  # @decorate transaction_event()
  def find_company_xbrl_by_code(code) do
    xbrl =
      CompanyXbrl
      |> where(company_code: ^code)
      |> Repo.all()
      |> List.first()
    case xbrl do
      nil ->
        {:error, "xbrl #{code} not found"}
      _ ->
        # xbrl |> IO.inspect(label: ">>>> XBRL:")
        {:ok, xbrl}
    end
  end

  @doc """
  Creates a company_xbrl.

  ## Examples

      iex> create_company_xbrl(%{field: value})
      {:ok, %CompanyXbrl{}}

      iex> create_company_xbrl(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_company_xbrl(attrs \\ %{}) do
    %CompanyXbrl{}
    |> CompanyXbrl.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a company_xbrl.

  ## Examples

      iex> update_company_xbrl(company_xbrl, %{field: new_value})
      {:ok, %CompanyXbrl{}}

      iex> update_company_xbrl(company_xbrl, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_company_xbrl(%CompanyXbrl{} = company_xbrl, attrs) do
    company_xbrl
    |> CompanyXbrl.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a CompanyXbrl.

  ## Examples

      iex> delete_company_xbrl(company_xbrl)
      {:ok, %CompanyXbrl{}}

      iex> delete_company_xbrl(company_xbrl)
      {:error, %Ecto.Changeset{}}

  """
  def delete_company_xbrl(%CompanyXbrl{} = company_xbrl) do
    Repo.delete(company_xbrl)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking company_xbrl changes.

  ## Examples

      iex> change_company_xbrl(company_xbrl)
      %Ecto.Changeset{source: %CompanyXbrl{}}

  """
  def change_company_xbrl(%CompanyXbrl{} = company_xbrl) do
    CompanyXbrl.changeset(company_xbrl, %{})
  end
end
