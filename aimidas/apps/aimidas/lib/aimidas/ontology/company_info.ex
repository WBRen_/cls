defmodule Aimidas.Ontology.CompanyInfo do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder,
          only: [:id, :abbrev_name, :company_code, :details, :full_name,
                 :industry, :source, :info_udpated_at, :inserted_at,
                 :updated_at]}
  schema "company_infos" do
    field :abbrev_name, :string
    field :company_code, :string
    field :details, :map
    field :full_name, :string
    field :industry, :string
    field :season, :integer
    field :source, :string
    field :info_updated_at, :date
    field :year, :integer

    timestamps()
  end

  @doc false
  def changeset(company_info, attrs) do
    company_info
    |> cast(attrs, [:company_code, :year, :season, :industry, :source,
                    :info_updated_at, :abbrev_name, :full_name, :details])
    |> validate_required([:company_code, :year, :abbrev_name,
                          :full_name, :details])
  end
end
