defmodule ChinaDateTime do
  # import Ecto.Changeset

  @moduledoc """
  返回的值是 NaiveDateTime，当前时区
  ChinaDateTime.now
  """
  def now() do
    NaiveDateTime.utc_now
    |> Timex.shift(hours: 8)
  end

  @doc """
  ChinaDateTime.timestamp()
  """
  def timestamp() do
    {{year_, month_, day_}, {hour_, minute_, second_}} = now() |> NaiveDateTime.to_erl # today
    month = fix_zero(month_)
    day = fix_zero(day_)
    hour = fix_zero(hour_)
    minute = fix_zero(minute_)
    second = fix_zero(second_)
    "#{year_}#{month}#{day}#{hour}#{minute}#{second}"
  end
  defp fix_zero(origin) do
    if origin > 9 do origin else "0#{origin}" end
  end
  # @doc """
  # 给 Ecto Changeset 使用
  # ChinaDateTime.fix_time_zone(%{update_at: ""}, :updated_at)
  # """
  # def fix_time_zone(%Ecto.Changeset{} = changeset, key) do
  #   IO.inspect(changeset)
  #   if updated_at = get_change(changeset, key, nil) do
  #     %{changeset | "#{key}": updated_at |> Timex.shift(hours: 8)}
  #   else
  #     changeset
  #   end
  # end

  def put_updated_time(attr) do
    attr
    |> Map.put(:updated_at, ChinaDateTime.now)
    |> to_atom_map
  end
  def put_created_time(attr) do
    time = ChinaDateTime.now
    attr
    |> Map.put(:updated_at, time)
    |> Map.put(:inserted_at, time)
    |> to_atom_map
    |> IO.inspect
  end


  def to_atom_map(%{} = json_map) do
    json_map
    |> Map.new(fn {k, v} ->
      if is_atom(k) do
        {k, v}
      else
        {String.to_atom(k), v}
      end
    end)
  end
  def to_atom_map([]), do: []
  def to_atom_map([head|tail]) do
    [to_atom_map(head)] ++ to_atom_map(tail)
  end
  def to_atom_map(data), do: data
end