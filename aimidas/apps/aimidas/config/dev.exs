use Mix.Config

# Configure your database
config :aimidas, Aimidas.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_DB_HOST") || "localhost",
  username: System.get_env("AIMIDAS_DB_USER") || "postgres",
  password: System.get_env("AIMIDAS_DB_PASSWORD") || "postgres",
  database: System.get_env("AIMIDAS_DB_NAME") || "aimidas_dev",
  port: System.get_env("AIMIDAS_DB_PORT") || 5432,
  pool_size: 10,
  pool_timeout: 60_000,
  timeout: 60_000
