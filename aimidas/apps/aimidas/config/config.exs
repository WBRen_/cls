use Mix.Config

config :aimidas, ecto_repos: [Aimidas.Repo]

# config :aimidas, Aimidas.Repo,
#   loggers: [Appsignal.Ecto, Ecto.LogEntry]

import_config "#{Mix.env}.exs"
