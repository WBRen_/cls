use Mix.Config

# import_config "prod.secret.exs"

config :aimidas, Aimidas.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_DB_HOST"),
  username: System.get_env("AIMIDAS_DB_USER"),
  password: System.get_env("AIMIDAS_DB_PASSWORD"),
  database: System.get_env("AIMIDAS_DB_NAME"),
  port: System.get_env("AIMIDAS_DB_PORT"),
  pool_size: 10,
  pool_timeout: 30_000,
  timeout: 30_000

# config :aimidas, Aimidas.Repo,
#   adapter: Ecto.Adapters.Postgres,
#   hostname: "${AIMIDAS_DB_HOST}",
#   username: "${AIMIDAS_DB_USER}",
#   password: "${AIMIDAS_DB_PASSWORD}",
#   database: "aimidas",
#   pool_size: 1
