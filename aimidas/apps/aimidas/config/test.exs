use Mix.Config

# Configure your database
config :aimidas, Aimidas.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "aimidas_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  ownership_timeout: 60_000, # <- add this line
  pool: Ecto.Adapters.SQL.Sandbox
