defmodule Aimidas.StocksTest do
  use Aimidas.DataCase

  alias Aimidas.Stocks
  alias Aimidas.Accounts
  alias Aimidas.ModelComparable

  @user_attrs %{
    nickname: "test_user",
    avatar: "icon",
    password_digest: "pwd",
    password_salt: "salt"
  }
  
  @portfolio_attrs %{
    name: "my portfolio",
    user: nil
  }

  @stock_attrs %{
    code: "000001",
    exchange_type_code: "SZ",
    last_px: "120.5",
    last_px_at: "2010-04-17 14:00:00.000000Z",
    name: "some name",
    px_exchange_rate: "120.5"
  }

  defp with_user(_tags) do
    {:ok, user} =
      @user_attrs
      |> Accounts.create_user()

    {:ok, user: user}
  end

  defp with_portfolio(tags) do
    {:ok, portfolio} =
      tags
      |> Map.put(:user_id, tags.user.id)
      |> Enum.into(@portfolio_attrs)
      |> Stocks.create_portfolio()

    {:ok, portfolio: portfolio}
  end

  defp with_stock(_tags) do
    {:ok, stock} =
      @stock_attrs
      |> Stocks.create_stock()

    {:ok, stock: stock}
  end

  describe "portfolios" do
    alias Aimidas.Stocks.Portfolio

    @portfolio_valid_attrs %{name: "some name", user: nil}
    @update_attrs %{name: "some updated name", user: nil}
    @invalid_attrs %{name: nil, user: nil}
    
    def portfolio_fixture(attrs \\ %{}) do
      {:ok, portfolio} =
        attrs
        |> Map.put(:user_id, attrs.user.id)
        |> Enum.into(@portfolio_valid_attrs)
        |> Stocks.create_portfolio()

      portfolio
    end

    setup [:with_user]

    test "list_portfolios/0 returns all portfolios", tags do
      portfolio = portfolio_fixture(tags)
      assert ModelComparable.list_equals(Stocks.list_portfolios(), [portfolio])
    end

    test "get_portfolio!/1 returns the portfolio with given id", tags do
      portfolio = portfolio_fixture(tags)
      assert portfolio.id
              |> Stocks.get_portfolio!()
              |> ModelComparable.equals(portfolio)
    end

    test "create_portfolio/1 with valid data creates a portfolio", tags do
      assert {:ok, %Portfolio{} = portfolio} = 
        tags
        |> Map.put(:user_id, tags.user.id)
        |> Enum.into(@portfolio_valid_attrs)
        |> Stocks.create_portfolio()
      assert portfolio.name == "some name"
    end

    test "create_portfolio/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stocks.create_portfolio(@invalid_attrs)
    end

    test "update_portfolio/2 with valid data updates the portfolio", tags do
      portfolio = portfolio_fixture(tags)
      assert {:ok, portfolio} = Stocks.update_portfolio(portfolio, @update_attrs)
      assert %Portfolio{} = portfolio
      assert portfolio.name == "some updated name"
    end

    test "update_portfolio/2 with invalid data returns error changeset", tags do
      portfolio = portfolio_fixture(tags)
      assert {:error, %Ecto.Changeset{}} = Stocks.update_portfolio(portfolio, @invalid_attrs)
      assert portfolio.id
              |> Stocks.get_portfolio!()
              |> ModelComparable.equals(portfolio)
    end

    test "delete_portfolio/1 deletes the portfolio", tags do
      portfolio = portfolio_fixture(tags)
      assert {:ok, %Portfolio{}} = Stocks.delete_portfolio(portfolio)
      assert_raise Ecto.NoResultsError, fn -> Stocks.get_portfolio!(portfolio.id) end
    end

    test "change_portfolio/1 returns a portfolio changeset", tags do
      portfolio = portfolio_fixture(tags)
      assert %Ecto.Changeset{} = Stocks.change_portfolio(portfolio)
    end
  end

  describe "stocks" do
    alias Aimidas.Stocks.Stock

    @valid_attrs %{code: "some code", exchange_type_code: "SZ", last_px: "120.5", last_px_at: "2010-04-17 14:00:00.000000Z", name: "some name", px_exchange_rate: "120.5"}
    @update_attrs %{code: "some updated code", exchange_type_code: "SZ", last_px: "456.7", last_px_at: "2011-05-18 15:01:01.000000Z", name: "some updated name", px_exchange_rate: "456.7"}
    @invalid_attrs %{code: nil, exchange_type_code: nil, last_px: nil, last_px_at: nil, name: nil, px_exchange_rate: nil}

    def stock_fixture(attrs \\ %{}) do
      {:ok, stock} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Stocks.create_stock()

      stock
    end

    test "list_stocks/0 returns all stocks" do
      stock = stock_fixture()
      assert Stocks.list_stocks() == [stock]
    end

    test "get_stock!/1 returns the stock with given id" do
      stock = stock_fixture()
      assert Stocks.get_stock!(stock.id) == stock
    end

    test "create_stock/1 with valid data creates a stock" do
      assert {:ok, %Stock{} = stock} = Stocks.create_stock(@valid_attrs)
      assert stock.code == "some code"
      assert stock.exchange_type_code == "SZ"
      assert stock.last_px == Decimal.new("120.5")
      assert stock.last_px_at == DateTime.from_naive!(~N[2010-04-17 14:00:00.000000Z], "Etc/UTC")
      assert stock.name == "some name"
      assert stock.px_exchange_rate == Decimal.new("120.5")
    end

    test "create_stock/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stocks.create_stock(@invalid_attrs)
    end

    test "update_stock/2 with valid data updates the stock" do
      stock = stock_fixture()
      assert {:ok, stock} = Stocks.update_stock(stock, @update_attrs)
      assert %Stock{} = stock
      assert stock.code == "some updated code"
      assert stock.exchange_type_code == "SZ"
      assert stock.last_px == Decimal.new("456.7")
      assert stock.last_px_at == DateTime.from_naive!(~N[2011-05-18 15:01:01.000000Z], "Etc/UTC")
      assert stock.name == "some updated name"
      assert stock.px_exchange_rate == Decimal.new("456.7")
    end

    test "update_stock/2 with invalid data returns error changeset" do
      stock = stock_fixture()
      assert {:error, %Ecto.Changeset{}} = Stocks.update_stock(stock, @invalid_attrs)
      assert stock == Stocks.get_stock!(stock.id)
    end

    test "delete_stock/1 deletes the stock" do
      stock = stock_fixture()
      assert {:ok, %Stock{}} = Stocks.delete_stock(stock)
      assert_raise Ecto.NoResultsError, fn -> Stocks.get_stock!(stock.id) end
    end

    test "change_stock/1 returns a stock changeset" do
      stock = stock_fixture()
      assert %Ecto.Changeset{} = Stocks.change_stock(stock)
    end
  end

  describe "portfolio_items" do
    alias Aimidas.Stocks.PortfolioItem

    @valid_attrs %{
      user_id: nil,
      portfolio_id: nil,
      stock_id: nil
    }
    @update_attrs %{}
    @invalid_attrs %{user_id: nil}

    def portfolio_item_fixture(attrs \\ %{}) do
      {:ok, portfolio_item} =
        attrs
        |> Map.put(:user_id, attrs.user.id)
        |> Map.put(:portfolio_id, attrs.portfolio.id)
        |> Map.put(:stock_id, attrs.stock.id)
        |> Enum.into(@valid_attrs)
        |> Stocks.create_portfolio_item()

      portfolio_item
    end

    setup [:with_user, :with_portfolio, :with_stock]

    test "list_portfolio_items/0 returns all portfolio_items", tags do
      portfolio_item = portfolio_item_fixture(tags)
      assert [portfolio_item]
              |> ModelComparable.list_equals(Stocks.list_portfolio_items())
    end

    test "get_portfolio_item!/1 returns the portfolio_item with given id", tags do
      portfolio_item = portfolio_item_fixture(tags)
      assert portfolio_item.id
              |> Stocks.get_portfolio_item!()
              |> ModelComparable.equals(portfolio_item)
    end

    test "create_portfolio_item/1 with valid data creates a portfolio_item", tags do
      
      assert {:ok, %PortfolioItem{} = _portfolio_item} = 
        tags
        |> Map.put(:user_id, tags.user.id)
        |> Map.put(:portfolio_id, tags.portfolio.id)
        |> Map.put(:stock_id, tags.stock.id)
        |> Enum.into(@valid_attrs)
        |> Stocks.create_portfolio_item()
    end

    test "create_portfolio_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stocks.create_portfolio_item(@invalid_attrs)
    end

    test "update_portfolio_item/2 with valid data updates the portfolio_item", tags do
      portfolio_item = portfolio_item_fixture(tags)
      assert {:ok, portfolio_item} = Stocks.update_portfolio_item(portfolio_item, @update_attrs)
      assert %PortfolioItem{} = portfolio_item
    end

    test "update_portfolio_item/2 with invalid data returns error changeset", tags do
      portfolio_item = portfolio_item_fixture(tags)
      assert {:error, %Ecto.Changeset{}} = Stocks.update_portfolio_item(portfolio_item, @invalid_attrs)
      assert portfolio_item == Stocks.get_portfolio_item!(portfolio_item.id)
    end

    test "delete_portfolio_item/1 deletes the portfolio_item", tags do
      portfolio_item = portfolio_item_fixture(tags)
      assert {:ok, %PortfolioItem{}} = Stocks.delete_portfolio_item(portfolio_item)
      assert_raise Ecto.NoResultsError, fn -> Stocks.get_portfolio_item!(portfolio_item.id) end
    end

    test "change_portfolio_item/1 returns a portfolio_item changeset", tags do
      portfolio_item = portfolio_item_fixture(tags)
      assert %Ecto.Changeset{} = Stocks.change_portfolio_item(portfolio_item)
    end
  end
end
