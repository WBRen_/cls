defmodule Aimidas.QaTest do
  use Aimidas.DataCase

  alias Aimidas.Qa
  alias Aimidas.Accounts
  alias Aimidas.ModelComparable

  @user_attrs %{
    nickname: "test_user",
    avatar: "icon",
    password_digest: "pwd",
    password_salt: "salt"
  }

  @question_group_attrs %{
    name: "招商银行",
    user: nil
  }

  defp with_user(tags) do
    {:ok, user} =
      @user_attrs
      |> Accounts.create_user()

    {:ok, user: user}
  end

  defp with_question_group(tags) do
    {:ok, question_group} =
      tags
      |> Map.put(:user_id, tags.user.id)
      |> Enum.into(@question_group_attrs)
      |> Qa.create_question_group()

    {:ok, question_group: question_group}
  end

  describe "question_groups" do
    alias Aimidas.Qa.QuestionGroup

    @question_group_valid_attrs %{name: "some name", user: nil}
    @update_attrs %{name: "some updated name", user: nil}
    @invalid_attrs %{name: nil, user: nil}

    def question_group_fixture(attrs \\ %{}) do
      {:ok, question_group} =
        attrs
        |> Map.put(:user_id, attrs.user.id)
        |> Enum.into(@question_group_valid_attrs)
        |> IO.inspect(label: "创建对话", pretty: true)
        |> Qa.create_question_group()

      question_group
    end

    setup [:with_user]

    test "list_question_groups/0 returns all question_groups", tags do
      question_group = question_group_fixture(tags)
      assert ModelComparable.list_equals(Qa.list_question_groups(), [question_group])
    end

    test "get_question_group!/1 returns the question_group with given id", tags do
      question_group = question_group_fixture(tags)
      assert question_group.id
              |> Qa.get_question_group!()
              |> ModelComparable.equals(question_group)
    end

    test "create_question_group/1 with valid data creates a question_group", tags do
      assert {:ok, %QuestionGroup{} = question_group} =
        tags
        |> Map.put(:user_id, tags.user.id)
        |> Enum.into(@question_group_valid_attrs)
        |> Qa.create_question_group()
      assert question_group.name == "some name"
    end

    test "create_question_group/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Qa.create_question_group(@invalid_attrs)
    end

    test "update_question_group/2 with valid data updates the question_group", tags do
      question_group = question_group_fixture(tags)
      assert {:ok, question_group} = Qa.update_question_group(question_group, @update_attrs)
      assert %QuestionGroup{} = question_group
      assert question_group.name == "some updated name"
    end

    test "update_question_group/2 with invalid data returns error changeset", tags do
      question_group = question_group_fixture(tags)
      result =
        Qa.update_question_group(question_group, @invalid_attrs)
        |> IO.inspect(label: "update_question_group with invalid data: ", pretty: true)
      assert {:error, %Ecto.Changeset{}} = result
      assert question_group.id
              |> Qa.get_question_group!()
              |> ModelComparable.equals(question_group)
    end

    test "delete_question_group/1 deletes the question_group", tags do
      question_group = question_group_fixture(tags)
      assert {:ok, %QuestionGroup{}} = Qa.delete_question_group(question_group)
      assert_raise Ecto.NoResultsError, fn -> Qa.get_question_group!(question_group.id) end
    end

    test "change_question_group/1 returns a question_group changeset", tags do
      question_group = question_group_fixture(tags)
      assert %Ecto.Changeset{} = Qa.change_question_group(question_group)
    end
  end

  describe "questions" do
    alias Aimidas.Qa.Question

    @valid_attrs %{query: "question", answer: "answer", last_otpq: %{}, otpq: %{}, answer_level: 1, question_group_id: 1, user: nil}
    @update_attrs %{query: "question", answer: "answer", last_otpq: %{}, otpq: %{}, answer_level: 2, question_group_id: 2, user: nil}
    @invalid_attrs %{query: nil, answer: nil, last_otpq: nil, otpq: nil, answer_level: nil, question_group_id: nil, user: nil}

    def question_fixture(attrs \\ %{}) do
      {:ok, question} =
        attrs
        |> Map.put(:user_id, attrs.user.id)
        |> Enum.into(@valid_attrs)
        |> Qa.create_question()

      question

    end

    setup [:with_user, :with_question_group]

    test "list_questions/0 returns all questions", tags do
      question = question_fixture(tags)
      assert [question]
              |> ModelComparable.list_equals(Qa.list_questions())
    end

    test "get_question!/1 returns the question with given id", tags do
      question = question_fixture(tags)
      assert question.id
              |> Qa.get_question!()
              |> ModelComparable.equals(question)
    end

    test "create_question/1 with valid data creates a question", tags do
      assert {:ok, %Question{} = question} =
        tags
        |> IO.inspect(label: "tag输出:", pretty: true)
        |> Map.put(:user_id, tags.user.id)
        |> Enum.into(@valid_attrs)
        |> Qa.create_question()
      assert question.query == "question"
      assert question.answer == "answer"
    end

    test "create_question/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Qa.create_question(@invalid_attrs)
    end

    test "update_question/2 with valid data updates the question", tags do
      question = question_fixture(tags)
      assert {:ok, question} = Qa.update_question(question, @update_attrs)
      assert %Question{} = question

    end

    test "update_question/2 with invalid data returns error changeset", tags do
      question = question_fixture(tags)
      assert {:error, %Ecto.Changeset{}} = Qa.update_question(question, @invalid_attrs)
      assert question == Qa.get_question!(question.id)
    end

    test "delete_question/1 deletes the question", tags do
      question = question_fixture(tags)
      assert {:ok, %Question{}} = Qa.delete_question(question)
      assert_raise Ecto.NoResultsError, fn -> Qa.get_question!(question.id) end
    end

    test "change_question/1 returns a question changeset", tags do
      question = question_fixture(tags)
      assert %Ecto.Changeset{} = Qa.change_question(question)
    end
  end
end
