defmodule Aimidas.OntologyTest do
  use Aimidas.DataCase

  alias Aimidas.Ontology

  describe "terms" do
    alias Aimidas.Ontology.Term

    @valid_attrs %{description: "some description", name: "some name"}
    @update_attrs %{description: "some updated description", name: "some updated name"}
    @invalid_attrs %{description: nil, name: nil}

    def term_fixture(attrs \\ %{}) do
      {:ok, term} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Ontology.create_term()

      term
    end

    test "list_terms/0 returns all terms" do
      term = term_fixture()
      assert Ontology.list_terms() == [term]
    end

    test "get_term!/1 returns the term with given id" do
      term = term_fixture()
      assert Ontology.get_term!(term.id) == term
    end

    test "create_term/1 with valid data creates a term" do
      assert {:ok, %Term{} = term} = Ontology.create_term(@valid_attrs)
      assert term.description == "some description"
      assert term.name == "some name"
    end

    test "create_term/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Ontology.create_term(@invalid_attrs)
    end

    test "update_term/2 with valid data updates the term" do
      term = term_fixture()
      assert {:ok, term} = Ontology.update_term(term, @update_attrs)
      assert %Term{} = term
      assert term.description == "some updated description"
      assert term.name == "some updated name"
    end

    test "update_term/2 with invalid data returns error changeset" do
      term = term_fixture()
      assert {:error, %Ecto.Changeset{}} = Ontology.update_term(term, @invalid_attrs)
      assert term == Ontology.get_term!(term.id)
    end

    test "delete_term/1 deletes the term" do
      term = term_fixture()
      assert {:ok, %Term{}} = Ontology.delete_term(term)
      assert_raise Ecto.NoResultsError, fn -> Ontology.get_term!(term.id) end
    end

    test "change_term/1 returns a term changeset" do
      term = term_fixture()
      assert %Ecto.Changeset{} = Ontology.change_term(term)
    end
  end

  describe "company_infos" do
    alias Aimidas.Ontology.CompanyInfo

    @valid_attrs %{abbrev_name: "some abbrev_name", company_code: "some company_code", details: %{}, full_name: "some full_name", season: 42, year: 42}
    @update_attrs %{abbrev_name: "some updated abbrev_name", company_code: "some updated company_code", details: %{}, full_name: "some updated full_name", season: 43, year: 43}
    @invalid_attrs %{abbrev_name: nil, company_code: nil, details: nil, full_name: nil, season: nil, year: nil}

    def company_info_fixture(attrs \\ %{}) do
      {:ok, company_info} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Ontology.create_company_info()

      company_info
    end

    test "list_company_infos/0 returns all company_infos" do
      company_info = company_info_fixture()
      assert Ontology.list_company_infos() == [company_info]
    end

    test "get_company_info!/1 returns the company_info with given id" do
      company_info = company_info_fixture()
      assert Ontology.get_company_info!(company_info.id) == company_info
    end

    test "create_company_info/1 with valid data creates a company_info" do
      assert {:ok, %CompanyInfo{} = company_info} = Ontology.create_company_info(@valid_attrs)
      assert company_info.abbrev_name == "some abbrev_name"
      assert company_info.company_code == "some company_code"
      assert company_info.details == %{}
      assert company_info.full_name == "some full_name"
      assert company_info.season == 42
      assert company_info.year == 42
    end

    test "create_company_info/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Ontology.create_company_info(@invalid_attrs)
    end

    test "update_company_info/2 with valid data updates the company_info" do
      company_info = company_info_fixture()
      assert {:ok, company_info} = Ontology.update_company_info(company_info, @update_attrs)
      assert %CompanyInfo{} = company_info
      assert company_info.abbrev_name == "some updated abbrev_name"
      assert company_info.company_code == "some updated company_code"
      assert company_info.details == %{}
      assert company_info.full_name == "some updated full_name"
      assert company_info.season == 43
      assert company_info.year == 43
    end

    test "update_company_info/2 with invalid data returns error changeset" do
      company_info = company_info_fixture()
      assert {:error, %Ecto.Changeset{}} = Ontology.update_company_info(company_info, @invalid_attrs)
      assert company_info == Ontology.get_company_info!(company_info.id)
    end

    test "delete_company_info/1 deletes the company_info" do
      company_info = company_info_fixture()
      assert {:ok, %CompanyInfo{}} = Ontology.delete_company_info(company_info)
      assert_raise Ecto.NoResultsError, fn -> Ontology.get_company_info!(company_info.id) end
    end

    test "change_company_info/1 returns a company_info changeset" do
      company_info = company_info_fixture()
      assert %Ecto.Changeset{} = Ontology.change_company_info(company_info)
    end
  end

  describe "company_xbrls" do
    alias Aimidas.Ontology.CompanyXbrl

    @valid_attrs %{abbrev: "some abbrev", company_code: "some company_code", exchange_code: "some exchange_code", details: %{}, full_name: "some full_name", industry: "some industry", src_updated_at: ~D[2010-04-17]}
    @update_attrs %{abbrev: "some updated abbrev", company_code: "some updated company_code", details: %{}, exchange_code: "some updated exchange_code", full_name: "some updated full_name", industry: "some updated industry", src_updated_at: ~D[2011-05-18]}
    @invalid_attrs %{abbrev: nil, company_code: nil, details: nil, full_name: nil, industry: nil, exchange_code: nil, src_updated_at: nil}

    def company_xbrl_fixture(attrs \\ %{}) do
      {:ok, company_xbrl} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Ontology.create_company_xbrl()

      company_xbrl
    end

    test "list_company_xbrls/0 returns all company_xbrls" do
      company_xbrl = company_xbrl_fixture()
      assert Ontology.list_company_xbrls() == [company_xbrl]
    end

    test "get_company_xbrl!/1 returns the company_xbrl with given id" do
      company_xbrl = company_xbrl_fixture()
      assert Ontology.get_company_xbrl!(company_xbrl.id) == company_xbrl
    end

    test "create_company_xbrl/1 with valid data creates a company_xbrl" do
      assert {:ok, %CompanyXbrl{} = company_xbrl} = Ontology.create_company_xbrl(@valid_attrs)
      assert company_xbrl.abbrev == "some abbrev"
      assert company_xbrl.company_code == "some company_code"
      assert company_xbrl.details == %{}
      assert company_xbrl.exchange_code == "some exchange_code"
      assert company_xbrl.full_name == "some full_name"
      assert company_xbrl.industry == "some industry"
      assert company_xbrl.src_updated_at == ~D[2010-04-17]
    end

    test "create_company_xbrl/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Ontology.create_company_xbrl(@invalid_attrs)
    end

    test "update_company_xbrl/2 with valid data updates the company_xbrl" do
      company_xbrl = company_xbrl_fixture()
      assert {:ok, company_xbrl} = Ontology.update_company_xbrl(company_xbrl, @update_attrs)
      assert %CompanyXbrl{} = company_xbrl
      assert company_xbrl.abbrev == "some updated abbrev"
      assert company_xbrl.company_code == "some updated company_code"
      assert company_xbrl.details == %{}
      assert company_xbrl.exchange_code == "some updated exchange_code"
      assert company_xbrl.full_name == "some updated full_name"
      assert company_xbrl.industry == "some updated industry"
      assert company_xbrl.src_updated_at == ~D[2011-05-18]
    end

    test "update_company_xbrl/2 with invalid data returns error changeset" do
      company_xbrl = company_xbrl_fixture()
      assert {:error, %Ecto.Changeset{}} = Ontology.update_company_xbrl(company_xbrl, @invalid_attrs)
      assert company_xbrl == Ontology.get_company_xbrl!(company_xbrl.id)
    end

    test "delete_company_xbrl/1 deletes the company_xbrl" do
      company_xbrl = company_xbrl_fixture()
      assert {:ok, %CompanyXbrl{}} = Ontology.delete_company_xbrl(company_xbrl)
      assert_raise Ecto.NoResultsError, fn -> Ontology.get_company_xbrl!(company_xbrl.id) end
    end

    test "change_company_xbrl/1 returns a company_xbrl changeset" do
      company_xbrl = company_xbrl_fixture()
      assert %Ecto.Changeset{} = Ontology.change_company_xbrl(company_xbrl)
    end
  end
end
