# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :payment,
  namespace: Payment,
  ecto_repos: [Payment.Repo]

# Configures the endpoint
config :payment, PaymentWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XSkMZBRfA+PPQt6d0OwQcSAm7Z5J3QR9q7cjSeUsP4FyGZyAYPbCI+Acfds9pXPQ",
  render_errors: [view: PaymentWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Payment.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]
  
# http
config :payment, PaymentWeb.Endpoint,
  http: [port: 14030],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../assets", __DIR__)]]
# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.

# 商户号：1500374682 APPID：wxd907a379de9ccf45
# 设定 appid 和 商户id
config :payment, :mini_app_id, "wxd907a379de9ccf45"
config :payment, :mini_mch_id, "1500374682"
config :payment, :mini_mch_key, "192106250b4c09247ec02edce69f6a2d"# key为商户平台设置的密钥key

import_config "#{Mix.env}.exs"
