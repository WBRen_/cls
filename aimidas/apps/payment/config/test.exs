use Mix.Config

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :payment, Payment.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_PAYMENT_USER") || "postgres",
  password: System.get_env("POSTGRES_PAYMENT_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_PAYMENT_DB") || "aimidas_payment_test",
  hostname: System.get_env("POSTGRES_PAYMENT_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox