use Mix.Config

# Configure your database
config :payment, Payment.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_PAYMENT_DB_HOST") || "aim-postgresql",
  username: System.get_env("AIMIDAS_PAYMENT_DB_USER") || "postgres",
  password: System.get_env("AIMIDAS_PAYMENT_DB_PASSWORD") || "postgres",
  database: System.get_env("AIMIDAS_PAYMENT_DB_NAME") || "aimidas_payment_citest",
  pool_size: 10,
  pool_timeout: 20_000,
  timeout: 20_000
