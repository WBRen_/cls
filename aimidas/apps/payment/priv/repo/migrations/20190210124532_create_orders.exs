defmodule Payment.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :out_trade_no, :string
      add :user_id, :integer
      add :price, :integer
      add :event, :integer
      add :type, :integer
      add :pay_method, :integer
      add :pay_status, :integer
      add :pay_time, :naive_datetime
      add :created_time, :naive_datetime
      add :title, :string
      add :data, :map

      timestamps()
    end

  end
end
