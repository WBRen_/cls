defmodule Payment.Repo.Migrations.CreateWxTrade do
  use Ecto.Migration

  def change do
    create table(:wx_trade) do
      add :out_trade_no, :string, size: 32
      add :appid, :string
      add :mch_id, :string
      add :device_info, :string
      add :nonce_str, :string
      add :sign, :string
      add :result_code, :string
      add :err_code, :string
      add :err_code_des, :string
      add :openid, :string
      add :is_subscribe, :string
      add :trade_type, :string
      add :bank_type, :string
      add :total_fee, :integer
      add :fee_type, :string
      add :cash_fee, :integer
      add :cash_fee_type, :string
      add :coupon_fee, :integer
      add :coupon_count, :integer
      add :transaction_id, :string
      add :attach, :string
      add :time_end, :naive_datetime
      add :body, :string # 商品简单描述，该字段请按照规范传递
      add :spbill_create_ip, :string # 终端 IP，支持IPV4和IPV6
      add :time_start, :naive_datetime # 订单生成时间 yyyyMMddHHmmss
      add :time_expire, :naive_datetime # 订单失效时间 yyyyMMddHHmmss 建议：最短失效时间间隔大于1分钟
      add :limit_pay, :string # 上传此参数 no_credit --可限制用户不能使用信用卡支付
      add :receipt, :string # Y 传入Y时，支付成功消息和支付详情页将出现开票入口，需要在微信支付商户平台或微信公众平台开通电子发票功能，传此字段才可生效

      timestamps()
    end

    create index(:wx_trade, [:out_trade_no], unique: true)
  end
end
