defmodule Payment.WxSign do

  @mch_key Application.get_env(:payment, :mini_mch_key)

  def sign(data, key \\ @mch_key) do
    data_str =
    data
    |> Enum.map(fn {key, val} ->
      "#{key}=#{val}"
    end)
    |> Enum.reduce(fn el, result ->
      "#{result}&#{el}"
    end)
    final_data = "#{data_str}&key=#{key}"
    # 签名
    :crypto.hash(:md5, final_data) |> Base.encode16()
  end
end