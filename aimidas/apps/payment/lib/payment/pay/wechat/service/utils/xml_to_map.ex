defmodule Payment.XmlToMap do

  @doc """
  格式：(string)
  <xml>
    <key><![CDATA[value]]></key>
    <key>value</key>
    ...
  </xml>
  """
  def to_map(xml) do
    regex_all = ~r(\<\w+\>\<\!\[CDATA\[\w+\]\]\>\<\/\w+\>)
    regex_line = ~r/\<(\w+)\>\<\!\[CDATA\[(\w+)\]\]\>\<\/(\w+)\>/

    # result_map =
    regex_all
    |> Regex.scan(xml) # |> IO.inspect
    |> Enum.map(fn line_list ->
      # <return_code><![CDATA[SUCCESS]]></return_code>
      [line] = line_list
      [list] = Regex.scan(regex_line, line) # |> IO.inspect
      if length(list) == 4 do
        [_, key, value, _] = list
        {key, value} # |> IO.inspect
      else
        nil
      end
    end)
    |> Enum.filter(fn x -> x != nil end)
    |> Map.new()
  end
end