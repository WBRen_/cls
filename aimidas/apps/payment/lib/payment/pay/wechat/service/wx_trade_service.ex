defmodule Payment.WxTradeService do
  @moduledoc """
  处理微信服务器返回的数据，做以更新 repo 数据使用
  """

  alias Payment.XmlToMap
  alias Payment.WxSign
  alias Payment.Pay
  alias Payment.OrderService

  def create_trade(data) do
    Pay.create_wx_trade(data)
  end

  @doc """
  微信服务器返回数据处理
  wx_string:
  <xml>
    <key><![CDATA[value]]></key>
    ...
  </xml>
  """
  def inpdate(wx_string) do
    wx_map = XmlToMap.to_map(wx_string)
    out_trade_no = wx_map["out_trade_no"]
    if check_sign(wx_map) do
      if has_processed(out_trade_no) do
        {:ok, "processed"}
      else
        return_code = wx_map["return_code"]
        result_code = wx_map["result_code"]
        # update or insert trade info
        if trade = Pay.get_wx_trade(out_trade_no) do
          Pay.update_wx_trade(trade, wx_map)
        else
          Pay.create_wx_trade(wx_map)
        end
        # process order
        if return_code == "SUCCESS" and result_code == "SUCCESS" do
          success_order(out_trade_no)
        else
          fail_order(out_trade_no)
        end
        {:ok, "finished"}
      end
    else
      {:error, "wrong sign"}
    end
  end

  def check_sign(data) do
    your_sign = data["sign"] # |> IO.inspect
    pure_data = data |> Map.delete(:sign)
    correct_sign = WxSign.sign(pure_data) # |> IO.inspect
    your_sign == correct_sign
  end

  # ======== 以下是对 order 的处理 ======== #

  # 从数据库里查询，看订单是否被处理过
  def has_processed(out_trade_no) do
    OrderService.has_processed(out_trade_no)
  end

  def success_order(out_trade_no) do
    OrderService.success_order(out_trade_no)
  end

  def fail_order(out_trade_no) do
    OrderService.fail_order(out_trade_no)
  end
end