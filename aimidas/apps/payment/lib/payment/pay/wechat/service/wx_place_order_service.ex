defmodule Payment.WxPlaceOrderService do
  @moduledoc """
  下单使用，与业务逻辑无关，只与微信支付逻辑相关
  """

  alias Payment.XmlToMap
  alias Payment.WxTradeService
  
  @place_order_url "https://api.mch.weixin.qq.com/pay/unifiedorder"
  @place_order_callback_url "https://dev.aimidas.com/app/api/payment/callback/wxpay"
  @app_id Application.get_env(:payment, :mini_app_id)
  @mch_id Application.get_env(:payment, :mini_mch_id)
  @mch_key Application.get_env(:payment, :mini_mch_key)

  @random_seeds ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
  @request_headers ["Content-Type": "application/xml", "Accept": "application/xml"]
  @doc """
  统一下单接口
  """
  def place_order(user_openid, user_ip, trade_no, title, total_fee, detail) do
    request_data = request_model(user_openid, user_ip, trade_no, title, total_fee, detail)
    # request
    @place_order_url
    |> HTTPotion.post([body: request_data, headers: @request_headers])
    |> handle_response()
  end

  def handle_response(%HTTPotion.ErrorResponse{} = response) do
    Rollbax.report_message(:error,
      "WeChat Payment Server Error",
      _custom_data = %{},
      %{"HTTPotion.ErrorMessage" => response.message})
    {:error, response}
  end

  @doc """
  Payment.WxPlaceOrderService.handle_response(%{body: "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg><appid><![CDATA[wx2421b1c4370ec43b]]></appid><mch_id><![CDATA[10000100]]></mch_id><nonce_str><![CDATA[IITRi8Iabbblz1Jc]]></nonce_str><openid><![CDATA[oUpF8uMuAJO_M2pxb1Q9zNjWeS6o]]></openid><sign><![CDATA[7921E432F65EB8ED0CE9755F0E86D72F]]></sign><result_code><![CDATA[SUCCESS]]></result_code><prepay_id><![CDATA[wx201411101639507cbf6ffd8b0779950874]]></prepay_id><trade_type><![CDATA[JSAPI]]></trade_type></xml>"})
  """
  def handle_response(%{} = response) do
    # regex = ~r{(\<(?<key_pre>\w+)\>(?<value>\w+)\<\/(?<key_suf>\w+)\>)+}
    # Regex.named_captures(regex, data)
    result_map =
    response.body
    |> XmlToMap.to_map()
    |> handle_pay_response()
    {:ok, result_map}
  end

  # 处理微信返回逻辑，将必要的数据返回给小程序
  def handle_pay_response(resp_map) do
    if resp_map["return_code"] == "SUCCESS" do
      # 处理返回信息
      data = %{
        appId: resp_map["appid"],
        nonceStr: resp_map["nonce_str"],
        package: "prepay_id=#{resp_map["prepay_id"]}",
        signType: "MD5",
        timeStamp: "#{DateTime.utc_now |> DateTime.to_unix}" # in second
      }
      sign_str = sign_data(data)
      result = data |> Map.put(:paySign, sign_str) # 返回给小程序客户端使用
      {:ok, result}
    else
      {:fail, resp_map["return_msg"]}
    end
  end

  ####### prepare for request #######

  defp request_model(user_openid, user_ip, trade_no, title, total_fee, detail) do
    data = %{
      appid: @app_id,
      attach: "小程序支付",
      body: title,
      detail: detail, # string
      device_info: "miniapp",
      mch_id: @mch_id,
      nonce_str: random_string(),
      notify_url: @place_order_callback_url,
      openid: user_openid,
      out_trade_no: trade_no,
      spbill_create_ip: user_ip, # string
      total_fee: total_fee,
      trade_type: "JSAPI"
    }
    sign_str = sign_data(data)
    # 持久化 model
    WxTradeService.create_trade(data)
    # return
    """
    <xml>
      <appid>#{data.appid}</appid>
      <attach>#{data.attach}</attach>
      <body>#{data.body}</body>
      <detail><![CDATA[#{data.detail}]]></detail>
      <device_info>#{data.device_info}</device_info>
      <mch_id>#{data.mch_id}</mch_id>
      <nonce_str>#{data.nonce_str}</nonce_str>
      <notify_url>#{data.notify_url}</notify_url>
      <openid>#{data.openid}</openid>
      <out_trade_no>#{data.out_trade_no}</out_trade_no>
      <spbill_create_ip>#{data.spbill_create_ip}</spbill_create_ip>
      <total_fee>#{data.total_fee}</total_fee>
      <trade_type>#{data.trade_type}</trade_type>
      <sign>#{sign_str}</sign>
    </xml>
    """
  end

  defp random_string() do
    1..32
    |> Enum.reduce([], fn(_, acc) -> [Enum.random(@random_seeds) | acc] end)
    |> Enum.join("")
  end

  # 请确保 key 传入顺序为 ascii 序列
  defp sign_data(data) do
    data_str =
    data
    |> Enum.map(fn {key, val} ->
      "#{key}=#{val}"
    end)
    |> Enum.reduce(fn el, result ->
      "#{result}&#{el}"
    end)
    final_data = "#{data_str}&key=#{@mch_key}"
    # 签名
    :crypto.hash(:md5, final_data) |> Base.encode16()
  end
end