defmodule Payment.Pay.WxTrade do
  use Ecto.Schema
  import Ecto.Changeset


  schema "wx_trade" do
    field :out_trade_no, :string # 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
    field :transaction_id, :string # 微信支付订单号
    field :appid, :string
    field :mch_id, :string # 商户号
    field :device_info, :string # 设备，如：小程序、WEB、APP
    field :nonce_str, :string # 随机字符串
    field :sign, :string # 签名
    # field :sign_type, :string # 签名类型，默认为MD5，支持HMAC-SHA256和MD5，此处直接都使用 MD5
    field :result_code, :string # 业务结果（SUCCESS/FAIL）
    field :err_code, :string # 错误返回的信息描述
    field :err_code_des, :string # 错误返回的信息描述
    field :openid, :string # 用户 openid
    field :is_subscribe, :string # 用户是否关注公众号（Y）
    field :trade_type, :string # 交易类型（如：APP），小程序取值如下：JSAPI
    field :bank_type, :string # 付款银行
    field :total_fee, :integer # 订单金额，总金额（单位：分）
    field :settlement_total_fee, :integer # [request 不需要] 应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额
    field :fee_type, :string # 货币种类，符合ISO 4217标准的三位字母代码，默认人民币：CNY
    field :cash_fee, :integer # 现金支付金额
    field :cash_fee_type, :string # 现金支付种类
    field :coupon_fee, :integer # 代金券或立减优惠金额<=订单总金额，订单总金额-代金券或立减优惠金额=现金支付金额，详见支付金额
    field :coupon_count, :integer # 优惠券数量
    field :time_end, :naive_datetime # 支付完成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
    field :attach, :string # 商家数据包，原样返回

    # request model needs append this: #optional
    field :body, :string # 商品简单描述，该字段请按照规范传递
    field :spbill_create_ip, :string # 终端 IP，支持IPV4和IPV6
    field :time_start, :naive_datetime # 订单生成时间 yyyyMMddHHmmss
    field :time_expire, :naive_datetime # 订单失效时间 yyyyMMddHHmmss 建议：最短失效时间间隔大于1分钟
    field :limit_pay, :string # 上传此参数 no_credit --可限制用户不能使用信用卡支付
    field :receipt, :string # Y 传入Y时，支付成功消息和支付详情页将出现开票入口，需要在微信支付商户平台或微信公众平台开通电子发票功能，传此字段才可生效

    timestamps()
  end

  @doc false
  def changeset(wx_trade, attrs) do
    wx_trade
    |> cast(attrs, [
      :out_trade_no, :appid, :mch_id, :device_info, :nonce_str,
      :sign, :result_code, :err_code, :err_code_des, :openid, :is_subscribe,
      :trade_type, :bank_type, :total_fee, :fee_type, :cash_fee, :cash_fee_type,
      :coupon_fee, :coupon_count, :transaction_id, :attach, :time_end,
      :inserted_at, :updated_at, :settlement_total_fee, :body, :spbill_create_ip,
      :time_start, :time_expire, :limit_pay, :receipt])
    |> validate_required([:out_trade_no, :openid, :total_fee])
  end
end
