defmodule Payment.Pay.Order do
  use Ecto.Schema
  import Ecto.Changeset


  schema "orders" do
    field :out_trade_no, :string
    field :user_id, :integer
    field :price, :integer
    field :event, PaymentEvent
    field :type, PaymentType
    field :pay_method, PaymentMethod
    field :pay_status, PaymentStatus
    field :pay_time, :naive_datetime
    field :created_time, :naive_datetime
    field :title, :string
    field :data, :map

    timestamps()
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [:out_trade_no, :user_id, :price, :event, :type, :pay_method, :pay_status, :pay_time, :created_time, :inserted_at, :updated_at, :title, :data])
    |> validate_required([:out_trade_no, :user_id, :price, :event, :type, :pay_method, :pay_status])
  end
end
