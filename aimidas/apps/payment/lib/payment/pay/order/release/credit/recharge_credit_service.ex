defmodule Payment.OrderService.RechargeCredit do
  @moduledoc """
  支付示例：
  充值积分
  data: %{
    credit: 100,
    # price: 800, # 8.00 ¥CNY
  }
  """

  alias Payment.OrderService
  alias Payment.WxPlaceOrderService
  alias Payment.UserInfoService

  @doc """
  method: :wechat, :alipay
  user_id: #
  ipaddress: 用户 ip，支持：ipv4、ipv6
  credit_number: 充值积分数量
  price: 价格（单位：分，CNY）
  """
  def create(:wechat, user_id, ipaddress, credit_number, price) do
    # prepare data
    title = "积分充值【#{credit_number}分】"
    detail = "credit:#{credit_number}"
    trade_no = gen_trade_no()
    user_openid = get_openid(user_id)
    data = %{
      out_trade_no: trade_no,
      title: title,
      user_id: user_id,
      price: price,
      data: %{
        open_id: user_openid,
        credit: credit_number
      },
      event: :credit_recharge,
      type: :in,
      pay_method: :wechat,
      pay_status: :paying,
      created_time: NaiveDateTime.utc_now
    }
    OrderService.create_order(data) # 创建业务订单信息
    # 创建支付订单信息
    # place order into wechat server, and return result_map to user_endpoint_device
    case WxPlaceOrderService.place_order(user_openid, ipaddress, trade_no, title, price, detail) do
      {:error, _} -> {:error, "微信服务器错误"}
      {:fail, msg} -> {:error, msg}
      {:ok, result_map} -> {:ok, result_map}
    end
  end

  @doc """
  支付完成后执行该方法，否则不做处理（此处不校验支付逻辑）
  """
  def finish(:wechat, user_id, data) do
    %{open_id: user_openid, credit: credit_number} = data
    
  end

  defp get_openid(user_id) do
    UserInfoService.openid(user_id)
  end

  # 201801011833570000000000 [len: 24]
  defp gen_trade_no() do
    TradeNoService.gen_trade_no()
  end
end