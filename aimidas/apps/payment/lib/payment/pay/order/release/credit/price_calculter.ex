defmodule Payment.OrderService.RechargeCredit.Price do
  def get_price(credit) do
    credit * 1 * 100 # 1 个积分价值 100 分（1.00 ¥CNY）
  end
end