defmodule Payment.TradeNoService do
  @doc """
  获取交易号
  Payment.TradeNoService.gen_trade_no()
  e.g. 201801011833570000000000
  """
  def gen_trade_no() do
    timestamp = ChinaDateTime.timestamp() # "20190210233627"
    random_num = random_number()
    "#{timestamp}#{random_num}"
  end

  defp random_number() do
    1000000000 + :rand.uniform(8999999999)
  end
end