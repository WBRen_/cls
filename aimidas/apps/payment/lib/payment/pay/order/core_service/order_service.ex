defmodule Payment.OrderService do
  alias Payment.Pay
  alias Payment.Pay.Order

  def create_order(data) do
    Pay.create_order(data)
  end
  def has_processed(out_trade_no) do
    if order = Pay.get_order(out_trade_no) do
      order.pay_status != :success and order.pay_status != :timeout
    else
      false
    end
  end

  def success_order(out_trade_no) do
    if order = Pay.get_order(out_trade_no) do
      # 匹配对应支付事件，完成支付闭环
      case order.event do
        :credit_recharge -> {:ok, ""}
      end
      # 支付成功，修改订单状态
      Pay.update_order(order, %{pay_status: :success})
    end
  end

  def fail_order(out_trade_no) do
    if order = Pay.get_order(out_trade_no) do
      Pay.update_order(order, %{pay_status: :failure})
    end
  end
  
end