defmodule Payment.UserInfoService do
  alias Aimidas.Accounts

  def openid(user_id) do
    Accounts.get_openid(user_id)
  end
end