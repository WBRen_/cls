defmodule Payment.Pay do
  @moduledoc """
  The Pay context.
  """

  import Ecto.Query, warn: false
  alias Payment.Repo

  alias Payment.Pay.Order
  alias Payment.Pay.WxTrade

  @doc """
  Returns the page of orders.

  ## Examples

      iex> page_orders(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%Order{}, ...]
      }

  """
  def page_orders(page) do
    Order
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of orders.

  ## Examples

      iex> list_orders()
      [%Order{}, ...]

  """
  def list_orders do
    Repo.all(Order)
  end

  @doc """
  Gets a single order.

  Raises `Ecto.NoResultsError` if the Order does not exist.

  ## Examples

      iex> get_order!(123)
      %Order{}

      iex> get_order!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order!(id), do: Repo.get!(Order, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_order(id) when is_integer(id) do
    Order
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  def get_order(out_trade_no) when is_binary(out_trade_no) do
    Order
    |> where(out_trade_no: ^out_trade_no)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a order.

  ## Examples

      iex> create_order(%{field: value})
      {:ok, %Order{}}

      iex> create_order(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %Order{}
    |> Order.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a order.

  ## Examples

      iex> update_order(order, %{field: new_value})
      {:ok, %Order{}}

      iex> update_order(order, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order(%Order{} = order, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    order
    |> Order.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a Order.

  ## Examples

      iex> delete_order(order)
      {:ok, %Order{}}

      iex> delete_order(order)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order(%Order{} = order) do
    Repo.delete(order)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order changes.

  ## Examples

      iex> change_order(order)
      %Ecto.Changeset{source: %Order{}}

  """
  def change_order(%Order{} = order) do
    Order.changeset(order, %{})
  end

  @doc """
  Returns the page of wx_trade.

  ## Examples

      iex> page_wx_trade(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%WxTrade{}, ...]
      }

  """
  def page_wx_trade(page) do
    WxTrade
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of wx_trade.

  ## Examples

      iex> list_wx_trade()
      [%WxTrade{}, ...]

  """
  def list_wx_trade do
    Repo.all(WxTrade)
  end

  @doc """
  Gets a single wx_trade.

  Raises `Ecto.NoResultsError` if the Wx trade does not exist.

  ## Examples

      iex> get_wx_trade!(123)
      %WxTrade{}

      iex> get_wx_trade!(456)
      ** (Ecto.NoResultsError)

  """
  def get_wx_trade!(id), do: Repo.get!(WxTrade, id)

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_wx_trade(id) when is_integer(id) do
    WxTrade
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  def get_wx_trade(out_trade_no) when is_binary(out_trade_no) do
    WxTrade
    |> where(out_trade_no: ^out_trade_no)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a wx_trade.

  ## Examples

      iex> create_wx_trade(%{field: value})
      {:ok, %WxTrade{}}

      iex> create_wx_trade(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_wx_trade(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %WxTrade{}
    |> WxTrade.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a wx_trade.

  ## Examples

      iex> update_wx_trade(wx_trade, %{field: new_value})
      {:ok, %WxTrade{}}

      iex> update_wx_trade(wx_trade, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_wx_trade(%WxTrade{} = wx_trade, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    wx_trade
    |> WxTrade.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a WxTrade.

  ## Examples

      iex> delete_wx_trade(wx_trade)
      {:ok, %WxTrade{}}

      iex> delete_wx_trade(wx_trade)
      {:error, %Ecto.Changeset{}}

  """
  def delete_wx_trade(%WxTrade{} = wx_trade) do
    Repo.delete(wx_trade)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking wx_trade changes.

  ## Examples

      iex> change_wx_trade(wx_trade)
      %Ecto.Changeset{source: %WxTrade{}}

  """
  def change_wx_trade(%WxTrade{} = wx_trade) do
    WxTrade.changeset(wx_trade, %{})
  end
end
