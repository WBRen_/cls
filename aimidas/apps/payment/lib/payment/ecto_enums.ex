import EctoEnum
defenum PaymentMethod, alipay: 0, wechat: 1, bank: 2, admin_insert: 3
defenum PaymentEvent, credit_recharge: 1
defenum PaymentType, in: 1, out: 2 # 入aimidas账：1，出aimidas账：2
defenum PaymentStatus, paying: 3, success: 4, timeout: 5, failure: 6