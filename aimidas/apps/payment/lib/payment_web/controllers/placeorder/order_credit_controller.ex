defmodule PaymentWeb.PlaceOrderController do
  use PaymentWeb, :controller
  
  alias Payment.OrderService.RechargeCredit
  alias Payment.OrderService.RechargeCredit.Price

  action_fallback PaymentWeb.FallbackController

  @doc """
  积分充值，微信支付
  """
  def credit_wechat(conn, %{"credit" => credit_number} = params) do
    ipaddress = to_string(:inet_parse.ntoa(conn.remote_ip))
    user = get_session(conn, :user)
    credit = String.to_integer(credit_number)
    price = Price.get_price(credit)
    case RechargeCredit.create(:wechat, user.id, ipaddress, credit, price) do
      {:error, msg} -> conn |> put_status(400) |> json(%{message: msg})
      {:ok, data} -> conn |> put_status(200) |> json(%{data: data})
    end
  end
end
