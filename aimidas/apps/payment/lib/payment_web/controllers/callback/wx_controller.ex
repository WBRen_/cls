defmodule PaymentWeb.Callback.WxPayController do
  use PaymentWeb, :controller

  alias Payment.WxTradeService

  action_fallback PaymentWeb.FallbackController

  def callback(conn, params) do
    {:ok, body, _conn} = Plug.Conn.read_body(conn)
    # IO.inspect(body)
    case WxTradeService.inpdate(body) do
      {:ok, _} -> "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>"
      {:error, msg} -> "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[#{msg}]]></return_msg></xml>"
    end
  end
end
