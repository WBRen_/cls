defmodule PaymentWeb.OrderController do
  use PaymentWeb, :controller

  alias Payment.Pay
  alias Payment.Pay.Order

  action_fallback PaymentWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    orders = Pay.page_orders(page)
    render(conn, "page.json", page: orders)
  end
  
  def index(conn, _params) do
    orders = Pay.list_orders()
    render(conn, "index.json", orders: orders)
  end

  def create(conn, %{"order" => order_params}) do
    with {:ok, %Order{} = order} <- Pay.create_order(order_params) do
      conn
      |> put_status(:created)
      # |> put_resp_header("location", order_path(conn, :show, order))
      |> render("show.json", order: order)
    end
  end

  def show(conn, %{"id" => id}) do
    order = Pay.get_order!(id)
    render(conn, "show.json", order: order)
  end

  def update(conn, %{"id" => id, "order" => order_params}) do
    order = Pay.get_order!(id)

    with {:ok, %Order{} = order} <- Pay.update_order(order, order_params) do
      render(conn, "show.json", order: order)
    end
  end

  def delete(conn, %{"id" => id}) do
    order = Pay.get_order!(id)
    with {:ok, %Order{}} <- Pay.delete_order(order) do
      send_resp(conn, :no_content, "")
    end
  end
end
