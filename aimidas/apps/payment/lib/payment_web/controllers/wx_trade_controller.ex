defmodule PaymentWeb.WxTradeController do
  use PaymentWeb, :controller

  alias Payment.Pay
  alias Payment.Pay.WxTrade

  action_fallback PaymentWeb.FallbackController

  def index(conn, params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    wx_trade = Pay.page_wx_trade(page)
    render(conn, "page.json", page: wx_trade)
  end
  
  def index(conn, _params) do
    wx_trade = Pay.list_wx_trade()
    render(conn, "index.json", wx_trade: wx_trade)
  end

  def create(conn, %{"wx_trade" => wx_trade_params}) do
    with {:ok, %WxTrade{} = wx_trade} <- Pay.create_wx_trade(wx_trade_params) do
      conn
      |> put_status(:created)
      # |> put_resp_header("location", wx_trade_path(conn, :show, wx_trade))
      |> render("show.json", wx_trade: wx_trade)
    end
  end

  def show(conn, %{"id" => id}) do
    wx_trade = Pay.get_wx_trade!(id)
    render(conn, "show.json", wx_trade: wx_trade)
  end

  def update(conn, %{"id" => id, "wx_trade" => wx_trade_params}) do
    wx_trade = Pay.get_wx_trade!(id)

    with {:ok, %WxTrade{} = wx_trade} <- Pay.update_wx_trade(wx_trade, wx_trade_params) do
      render(conn, "show.json", wx_trade: wx_trade)
    end
  end

  def delete(conn, %{"id" => id}) do
    wx_trade = Pay.get_wx_trade!(id)
    with {:ok, %WxTrade{}} <- Pay.delete_wx_trade(wx_trade) do
      send_resp(conn, :no_content, "")
    end
  end
end
