defmodule PaymentWeb.Router do
  use PaymentWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug PaymentWeb.Context
  end

  pipeline :callback do
    plug :put_secure_browser_headers
  end

  scope "/app/api/payment/callback", PaymentWeb do
    pipe_through :callback

    # "/wxpay" # 微信支付回调接口，微信 server 触发
    get "/wxpay", Callback.WxPayController, :callback
    post "/wxpay", Callback.WxPayController, :callback
  end

  scope "/app/api/payment", PaymentWeb do
    pipe_through :api
    # "/orders" # 订单创建/修改/查询/删除
    # "/pay/:method/:order_id" # 支付订单
    post "/place_order/recharge_credit/wechat", PlaceOrderController, :credit_wechat # 充值积分，需要参数e.g.：credit=10
  end
end
