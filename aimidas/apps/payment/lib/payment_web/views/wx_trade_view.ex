defmodule PaymentWeb.WxTradeView do
  use PaymentWeb, :view
  alias PaymentWeb.WxTradeView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, WxTradeView, "wx_trade.json")
    }
  end

  def render("index.json", %{wx_trade: wx_trade}) do
    %{data: render_many(wx_trade, WxTradeView, "wx_trade.json")}
  end

  def render("show.json", %{wx_trade: wx_trade}) do
    %{data: render_one(wx_trade, WxTradeView, "wx_trade.json")}
  end

  def render("wx_trade.json", %{wx_trade: wx_trade}) do
    %{
      id: wx_trade.id,
      out_trade_no: wx_trade.out_trade_no,
      appid: wx_trade.appid,
      mch_id: wx_trade.mch_id,
      device_info: wx_trade.device_info,
      nonce_str: wx_trade.nonce_str,
      sign: wx_trade.sign,
      result_code: wx_trade.result_code,
      err_code: wx_trade.err_code,
      err_code_des: wx_trade.err_code_des,
      openid: wx_trade.openid,
      is_subscribe: wx_trade.is_subscribe,
      trade_type: wx_trade.trade_type,
      bank_type: wx_trade.bank_type,
      total_fee: wx_trade.total_fee,
      settlement_total_fee: wx_trade.settlement_total_fee,
      fee_type: wx_trade.fee_type,
      cash_fee: wx_trade.cash_fee,
      cash_fee_type: wx_trade.cash_fee_type,
      coupon_fee: wx_trade.coupon_fee,
      coupon_count: wx_trade.coupon_count,
      transaction_id: wx_trade.transaction_id,
      attach: wx_trade.attach,
      time_end: wx_trade.time_end,
      updated_at: wx_trade.updated_at,
      inserted_at: wx_trade.inserted_at
    }
  end
end
