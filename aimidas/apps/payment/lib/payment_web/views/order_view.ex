defmodule PaymentWeb.OrderView do
  use PaymentWeb, :view
  alias PaymentWeb.OrderView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, OrderView, "order.json")
    }
  end

  def render("index.json", %{orders: orders}) do
    %{data: render_many(orders, OrderView, "order.json")}
  end

  def render("show.json", %{order: order}) do
    %{data: render_one(order, OrderView, "order.json")}
  end

  def render("order.json", %{order: order}) do
    %{
      id: order.id,
      out_trade_no: order.out_trade_no,
      user_id: order.user_id,
      price: order.price,
      event: order.event,
      type: order.type,
      pay_method: order.pay_method,
      pay_status: order.pay_status,
      pay_time: order.pay_time,
      created_time: order.created_time,
      updated_at: order.updated_at,
      inserted_at: order.inserted_at
    }
  end
end
