defmodule IntegrationTestApi.AcFactories.DatabaseCleaner do

  alias Aimidas.Repo

  @tables [
    Aimidas.Accounts.Authentication,
    Aimidas.Ontology.Term,
    Aimidas.Ontology.CompanyInfo,
    Aimidas.Accounts.User
  ]

  def clean(params) do
    params
    |> Map.pop("db_clean")
    |> clean_db
  end

  def clean_db({"true", params}) do
    @tables
    |> Enum.each(&Repo.delete_all/1)

    params
  end
  def clean_db({_, params}), do: params
end
