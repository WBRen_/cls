defmodule IntegrationTestApi.AcFactories.AcCreator do

  def create(params) do
    params
    |> Map.pop("ac")
    |> run_ac()
  end

  def run_ac({factory, attrs}) do
    "Elixir.IntegrationTestApi.DataFactories." <> Macro.camelize(factory)
    |> String.to_existing_atom()
    |> apply(:create, [attrs])
  end
end
