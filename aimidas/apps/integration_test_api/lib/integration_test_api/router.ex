defmodule IntegrationTestApi.Router do
  use IntegrationTestApi, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  # scope "/", IntegrationTestApi do
  #   pipe_through :browser # Use the default browser stack

  # end

  # Other scopes may use custom stacks.
  scope "/ciapi", IntegrationTestApi do
    pipe_through :api
    post "/ac_factory/:ac", AcFactoryController, :create
  end
end
