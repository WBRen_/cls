defmodule IntegrationTestApi.PageController do
  use IntegrationTestApi, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
