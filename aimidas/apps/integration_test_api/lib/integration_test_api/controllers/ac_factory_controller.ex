defmodule IntegrationTestApi.AcFactoryController do
  use IntegrationTestApi, :controller

  alias IntegrationTestApi.AcFactories.AcCreator
  alias IntegrationTestApi.AcFactories.DatabaseCleaner

  def create(conn, params) do
    params
    |> DatabaseCleaner.clean()
    |> AcCreator.create()
    |> response(conn)
  end

  defp response({:error, _}, conn) do
    conn
    |> render("500")
  end

  defp response({:ok, data}, conn) do
    conn
    |> json(data)
  end
end
