defmodule IntegrationTestApi.DataFactories.Term do
  alias Aimidas.Ontology

  def create(attrs) do
    attrs
    |> Ontology.create_term()
  end
end