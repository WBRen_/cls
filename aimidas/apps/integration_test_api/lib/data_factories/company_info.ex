defmodule IntegrationTestApi.DataFactories.CompanyInfo do
  alias Aimidas.Ontology

  def create(attrs) do
    attrs
    |> Ontology.create_company_info()
  end
end