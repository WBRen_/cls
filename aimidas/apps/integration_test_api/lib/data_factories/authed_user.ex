defmodule IntegrationTestApi.DataFactories.AuthedUser do
  alias Aimidas.Accounts.Authenticator
  alias Aimidas.Utils.StrongParams

  @permitted_authed_user_attrs [
    "uid", "union_id", "provider", "name", "nickname", "telephone",
    "image", "email"]
  def create(attrs) do
    default_attrs = %{
      uid: gen_uid(),
      union_id: nil,
      provider: "wechat_miniapp",
      token: "empty",
      name: "user_" <> gen_uid(6),
      image: "http://avatar.com/non_exist.jpg"
    }

    new_attrs =
      attrs
      |> StrongParams.extract(@permitted_authed_user_attrs, true)

    {:ok, auth, user} =
      default_attrs
      |> Map.merge(new_attrs)
      |> Authenticator.authenticate

    {:ok, %{user: user, auth: auth}}
  end

  defp gen_uid(bytes \\ 16) do
    bytes
    |> :crypto.strong_rand_bytes()
    |> Base.url_encode64(padding: false)
  end
end