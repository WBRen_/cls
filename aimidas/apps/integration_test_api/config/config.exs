# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :integration_test_api,
  namespace: IntegrationTestApi,
  ecto_repos: [Aimidas.Repo]

# Configures the endpoint
config :integration_test_api, IntegrationTestApi.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "s7YG61ijyUriXazUZpG4gQVVonYWhbV+YP6Wmiz0I7TlqnRDA/U/R+cQ+ow2RohE",
  render_errors: [view: IntegrationTestApi.ErrorView, accepts: ~w(html json)],
  pubsub: [name: IntegrationTestApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :integration_test_api, :generators,
  context_app: false

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
