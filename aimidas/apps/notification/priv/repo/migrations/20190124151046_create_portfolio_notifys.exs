defmodule Notification.Repo.Migrations.CreatePortfolioNotifys do
  use Ecto.Migration

  def change do
    create table(:portfolio_notifys) do
      add :user_id, :integer
      add :portfolio_item_id, :integer
      add :company_code, :string
      add :company_name, :string
      add :exchange_code, :integer
      add :type, :integer
      add :param, :string
      add :value, :float
      add :has_notify, :boolean, default: false, null: false

      timestamps()
    end

  end
end
