defmodule Notification.Repo.Migrations.CreateBoardMessages do
  use Ecto.Migration

  def change do
    create table(:board_messages) do
      add :message_type, :integer
      add :title, :string
      add :content, :text
      add :detail, :map
      
      add :priority, :integer, default: 0
      add :user_type, :integer, default: 0

      add :has_canceled, :boolean, default: false, null: false
      add :cancel_time, :naive_datetime
      add :has_deleted, :boolean, default: false, null: false
      add :delete_time, :naive_datetime

      timestamps()
    end

    # create index(:board_messages, [:message_type])

  end
end
