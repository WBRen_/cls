defmodule Notification.Repo.Migrations.CreateBoardReaders do
  use Ecto.Migration

  def change do
    create table(:board_readers) do
      add :message_id, :integer
      add :uid, :integer
      add :has_read, :boolean, default: false, null: false
      add :read_time, :naive_datetime

      timestamps()
    end
    
    create index(:board_readers, [:message_id, :uid, :has_read])

  end
end
