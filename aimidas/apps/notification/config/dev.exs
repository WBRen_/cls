use Mix.Config

# Watch static and templates for browser reloading.
config :notification, NotificationWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{lib/notification_web/views/.*(ex)$},
      ~r{lib/notification_web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :notification, Notification.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_NOTIFICATION_DB_HOST") || "localhost",
  username: System.get_env("AIMIDAS_NOTIFICATION_DB_USER") || "postgres",
  password: System.get_env("AIMIDAS_NOTIFICATION_DB_PASSWORD") || "postgres",
  database: System.get_env("AIMIDAS_NOTIFICATION_DB_NAME") || "aimidas_notification_dev",
  port: System.get_env("AIMIDAS_NOTIFICATION_DB_PORT") || 5432,
  pool_size: 10,
  pool_timeout: 60_000,
  timeout: 60_000