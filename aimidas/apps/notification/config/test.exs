use Mix.Config

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :notification, Notification.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_NOTIFICATION_USER") || "postgres",
  password: System.get_env("POSTGRES_NOTIFICATION_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_NOTIFICATION_DB") || "aimidas_notification_test",
  hostname: System.get_env("POSTGRES_NOTIFICATION_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox