use Mix.Config

# Do not print debug messages in production
config :logger, level: :info

# Configure your database
config :notification, Notification.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("AIMIDAS_NOTIFICATION_DB_HOST") || "localhost",
  username: System.get_env("AIMIDAS_NOTIFICATION_DB_USER") || "postgres",
  password: System.get_env("AIMIDAS_NOTIFICATION_DB_PASSWORD") || "postgres",
  database: System.get_env("AIMIDAS_NOTIFICATION_DB_NAME") || "aimidas_notification",
  port: System.get_env("AIMIDAS_NOTIFICATION_DB_PORT") || 5432,
  pool_size: 15

