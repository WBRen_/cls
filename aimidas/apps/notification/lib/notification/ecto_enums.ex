import EctoEnum
defenum UserType, normal: 0, vip: 1, all: 2 # 用户类型，发送消息通知时可以据此全局发送
defenum MessageType, normal: 0, pay: 1, answer_apply: 2, stock_apply: 3 # 消息类型，可扩展增加
defenum ExchangeCode, SH: 0, SZ: 1, HK: 2 # 交易所代码
defenum PortfolioNotifyType, up: 0, down: 1