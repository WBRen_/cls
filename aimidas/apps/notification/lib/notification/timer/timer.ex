defmodule Notification.Timer do
  use GenServer
  alias Notification.Timer.StockMonitor

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    schedule_work() # Schedule work to be performed at some point
    StockMonitor.alert_param
    # PostCrawler.start_craw(0, 4387)
    # PostCrawler.start_craw(4486, 100)
    {:ok, state}
  end

  def handle_info(:work, state) do
    # Do the work you desire here
    schedule_work() # Reschedule once more
    StockMonitor.alert_param
    # here to process local method:
    IO.inspect ">>>>>> #{DateTime.utc_now} >>>>>>"

    {:noreply, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :work, 10 * 60 * 1000) # In 10 mins
  end
end
