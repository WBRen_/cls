defmodule Notification.Timer.StockMonitor do
  alias Notification.Pusher
  alias Notification.Portfolio
  alias AimidasWeb.Stock.Stock
  def alert_param do
    Portfolio.get_portfolio_notify_no_notify() |> run()
  end
  def alert_param_async do
    Task.async(fn -> alert_param end)
  end

  def run([]) do
  end

  def run(list) do
    list
      |> Enum.map(fn item -> check(item) end)
  end

  def check(item) do
    set_value = item.value
    now_value = item |> get_value(item.param)
    case item.type do
      :up ->
        if set_value < now_value do
          Pusher.send(:stock_apply, item.user_id, "#{item.company_name}指标变动提醒", "您的#{item.company_name}的指标，已涨至提醒值！", %{company: item.company_name, company_code: item.company_code, type: "上涨", param: "股价", value: item.value, user_id: item.user_id, portfolio_item_id: item.portfolio_item_id})
          item |> Portfolio.update_portfolio_notify(%{has_notify: true})
        end
      :down ->
        if set_value > now_value do
          Pusher.send(:stock_apply, item.user_id, "#{item.company_name}指标变动提醒", "您的#{item.company_name}的指标，已跌至提醒值！", %{company: item.company_name, company_code: item.company_code, type: "下跌", param: "股价", value: item.value, user_id: item.user_id, portfolio_item_id: item.portfolio_item_id})
          item |> Portfolio.update_portfolio_notify(%{has_notify: true})
        end
    end
  end

  def get_value(item, "stock_price") do
    {:ok, value} = get_stock(item)
    {now_value, _} = value["当前价格"] |> Float.parse
    now_value
  end

  def get_stock(item) do
    case item.exchange_code do
      :SH ->
        result = Stock.analyze("SH", item.company_code)
      :SZ ->
        result = Stock.analyze("SZ", item.company_code)
    end
    result
  end

  def get_value(item, param) do
    0.0
  end
end
