defmodule Notification.Receiver do
  alias Notification.BlackBoard.Service

  @doc """
  获取所有消息
  uid: 用户 id
  message_type: 消息类型
  page: %{page: 1, page_size: 10}
  return ->
    %Scrivener.Page{
      entries: [
        %{
          content: "content",
          detail: %{},
          has_read: false,
          message_id: 6,
          notification_time: ~N[2019-01-11 00:39:54.853269],
          priority: 0,
          title: "title",
          type: :normal,
          uid: 1
        }
        ...
      ],
      page_number: 1,
      page_size: 10,
      total_entries: 1,
      total_pages: 1
    }
  """
  def page_all(uid, message_type, page) do
    Service.fetch(uid, message_type, page)
  end
  def page_unread(uid, message_type, page) do
    Service.fetch_unread(uid, message_type, page)
  end

  @doc """
  获取未读消息数量
  uid: 用户 id
  message_type: 消息类型
  return ->
  {:ok, 98}
  """
  def count_unread(uid, message_type) do
    Service.count_unread(uid, message_type)
  end

  @doc """
  设为已读
  uid: 用户 id
  message_id: 消息 id
  return ->
  {:ok, "Success"}
  """
  def mark_as_read(uid, message_id) do
    Service.mark_read(uid, message_id)
  end
  
  # 设为全部已读
  def mark_as_read(uid) do
    Service.mark_read_all(uid)
  end
end