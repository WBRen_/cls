defmodule Notification.BlackBoard do
  @moduledoc """
  The BlackBoard context.
  """

  import Ecto.Query, warn: false
  alias Notification.Repo

  alias Notification.BlackBoard.Message
  alias Notification.BlackBoard.Reader


  @doc """
  Notification.BlackBoard.page_message(1, :normal, %{page: 1, page_size: 10})
  """
  def page_message(uid, message_type, page) do
    query =
    from r in Reader,
      where: r.uid == ^uid,
      left_join: m in Message,
      where: m.id == r.message_id and m.message_type == ^message_type,
      select: %{
        uid: r.uid,
        message_id: r.message_id,
        has_read: r.has_read,
        notification_time: r.inserted_at,
        title: m.title,
        content: m.content,
        message_type: m.message_type,
        priority: m.priority,
        detail: m.detail
      }
    query |> Repo.paginate
  end
  def page_message(uid, message_type, has_read, page) do
    query =
    from r in Reader,
      where: r.uid == ^uid and r.has_read == ^has_read,
      left_join: m in Message,
      where: m.id == r.message_id and m.message_type == ^message_type,
      select: %{
        uid: r.uid,
        message_id: r.message_id,
        has_read: r.has_read,
        notification_time: r.inserted_at,
        title: m.title,
        content: m.content,
        message_type: m.message_type,
        priority: m.priority,
        detail: m.detail
      }
    query |> Repo.paginate
  end
  @doc """
    Notification.BlackBoard.count_unread(1, :normal)
  """
  def count_unread(uid, message_type) do
    query =
    from r in Reader,
      where: r.uid == ^uid and r.has_read == false,
      left_join: m in Message,
      where: m.id == r.message_id and m.message_type == ^message_type,
      select: count(r.id, :distinct)
    [entries] = query |> Repo.all
    entries
  end

  #### ⚠️ 注意！ 暂不支持 publish 级别消息的 mark
  def mark_read_all(uid) do
    sql =
    from r in Reader,
      where: r.uid == ^uid and r.has_read == false
    sql |> Repo.update_all(set: [has_read: true])
  end
  @doc """
  Returns the page of board_messages.

  ## Examples

      iex> page_board_messages(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%Message{}, ...]
      }

  """
  def page_board_messages(page) do
    Message
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of board_messages.

  ## Examples

      iex> list_board_messages()
      [%Message{}, ...]

  """
  def list_board_messages do
    Repo.all(Message)
  end

  @doc """
  Gets a single message.

  Raises `Ecto.NoResultsError` if the Message does not exist.

  ## Examples

      iex> get_message!(123)
      %Message{}

      iex> get_message!(456)
      ** (Ecto.NoResultsError)

  """
  def get_message!(id), do: Repo.get!(Message, id)

  def get_message(id) do
    Message
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a message.

  ## Examples

      iex> create_message(%{field: value})
      {:ok, %Message{}}

      iex> create_message(%{field: bad_value})
      {:error, %Ecto.Changeset{}}
  {:ok, a} = Notification.BlackBoard.create_message(%{
    title: "xxx",
    content: "xxx"
  })
  Notification.BlackBoard.update_message(a, %{
    title: "yyy",
    content: "xxxx"
  })
  """
  def create_message(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %Message{}
    |> Message.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a message.

  ## Examples

      iex> update_message(message, %{field: new_value})
      {:ok, %Message{}}

      iex> update_message(message, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_message(%Message{} = message, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    message
    |> Message.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a Message.

  ## Examples

      iex> delete_message(message)
      {:ok, %Message{}}

      iex> delete_message(message)
      {:error, %Ecto.Changeset{}}

  """
  def delete_message(%Message{} = message) do
    Repo.delete(message)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking message changes.

  ## Examples

      iex> change_message(message)
      %Ecto.Changeset{source: %Message{}}

  """
  def change_message(%Message{} = message) do
    Message.changeset(message, %{})
  end



  @doc """
  Returns the page of board_readers.

  ## Examples

      iex> page_board_readers(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%Reader{}, ...]
      }

  """
  def page_board_readers(page) do
    Reader
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of board_readers.

  ## Examples

      iex> list_board_readers()
      [%Reader{}, ...]

  """
  def list_board_readers do
    Repo.all(Reader)
  end

  @doc """
  Gets a single reader.

  Raises `Ecto.NoResultsError` if the Reader does not exist.

  ## Examples

      iex> get_reader!(123)
      %Reader{}

      iex> get_reader!(456)
      ** (Ecto.NoResultsError)

  """
  def get_reader!(id), do: Repo.get!(Reader, id)

  ## return nil if not exist
  def get_reader(uid, message_id) do
    Reader
    |> where(uid: ^uid)
    |> where(message_id: ^message_id)
    |> Repo.all
    |> List.first
  end

  @doc """
  Creates a reader.

  ## Examples

      iex> create_reader(%{field: value})
      {:ok, %Reader{}}

      iex> create_reader(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_reader(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %Reader{}
    |> Reader.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a reader.

  ## Examples

      iex> update_reader(reader, %{field: new_value})
      {:ok, %Reader{}}

      iex> update_reader(reader, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_reader(%Reader{} = reader, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    reader
    |> Reader.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a Reader.

  ## Examples

      iex> delete_reader(reader)
      {:ok, %Reader{}}

      iex> delete_reader(reader)
      {:error, %Ecto.Changeset{}}

  """
  def delete_reader(%Reader{} = reader) do
    Repo.delete(reader)
  end

  def delete_readers(message_id) do
    Reader
    |> where(message_id: ^message_id)
    |> Repo.delete_all
  end
  @doc """
  Returns an `%Ecto.Changeset{}` for tracking reader changes.

  ## Examples

      iex> change_reader(reader)
      %Ecto.Changeset{source: %Reader{}}

  """
  def change_reader(%Reader{} = reader) do
    Reader.changeset(reader, %{})
  end
end
