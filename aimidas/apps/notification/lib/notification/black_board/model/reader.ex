defmodule Notification.BlackBoard.Reader do
  use Ecto.Schema
  import Ecto.Changeset


  schema "board_readers" do
    field :message_id, :integer
    field :uid, :integer
    field :has_read, :boolean, default: false
    field :read_time, :naive_datetime

    timestamps()
  end

  @doc false
  def changeset(reader, attrs) do
    reader
    |> cast(attrs, [:message_id, :uid, :has_read, :read_time, :inserted_at, :updated_at])
    |> validate_required([:message_id, :uid, :has_read])
  end

end
