defmodule Notification.BlackBoard.Message do
  use Ecto.Schema
  import Ecto.Changeset


  schema "board_messages" do
    field :message_type, MessageType
    field :title, :string
    field :content, :string
    field :detail, :map
    field :priority, :integer, default: 0
    field :user_type, UserType, default: :normal

    field :has_canceled, :boolean, default: false
    field :has_deleted, :boolean, default: false
    field :cancel_time, :naive_datetime
    field :delete_time, :naive_datetime

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:message_type, :title, :content, :has_canceled, :cancel_time, :has_deleted, :delete_time, :priority, :detail, :user_type, :inserted_at, :updated_at])
    |> validate_required([:message_type, :title, :content, :has_canceled, :has_deleted, :priority, :user_type])
  end

end
