defmodule Notification.BlackBoard.Service do
  alias Notification.Repo # only for transaction
  alias Notification.BlackBoard
  alias Notification.BlackBoard.Message
  alias Notification.BlackBoard.Reader
  alias Notification.BlackBoard.Service.Socket


  @doc """
  获取消息列表 by: type = 某一类型
  """
  def fetch(uid, type, %{} = page) do
    BlackBoard.page_message(uid, type, page)
  end

  @doc """
  获取消息列表 by: has_read: false, type = 某一类型
  """
  def fetch_unread(uid, type, %{} = page) do
    BlackBoard.page_message(uid, type, false, page)
  end

  @doc """
  获取未读消息数量
  """
  def count_unread(uid, message_type) do
    if count = BlackBoard.count_unread(uid, message_type) do
      {:ok, count}
    else
      {:error, "Fetch Fail"}
    end
  end
  
  @doc """
  单个通知，通知给某一个用户
    field :title, :string
    field :content, :string
    field :detail, :map
    field :priority, :integer, default: 0
  Notification.BlackBoard.Service.send(:normal, 1, "title", "content")
  Notification.BlackBoard.Service.send(:normal, 1, "title", "content", %{time: "now", blabla: "ddda"})
  """
  def send(message_type, uid, title, content, detail \\ %{}, priority \\ 0) do
    # 创建消息
    message = %{
      message_type: message_type,
      title: title,
      content: content,
      detail: detail,
      priority: priority,
      user_type: :normal
    }
    Repo.transaction(fn ->
      {:ok, msg_record} = BlackBoard.create_message(message)
      # 创建接收者
      reader = %{
        uid: uid,
        message_id: msg_record.id
      }
      {:ok, _} = BlackBoard.create_reader(reader)
    end)
    ## 在此异步通知前端 websocket
    Socket.send(uid, message)
    {:ok, "消息通知发布成功"}
  end

  @doc """
  全局通知，通知给符合条件的用户（根据用户类型）
    field :title, :string
    field :content, :string
    field :detail, :map
    field :priority, :integer, default: 0
    field :user_type, UserType, default: :normal

  !!! 该通知不会立即加入用户 Reader 列表，当用户请求消息系统时再加入
  """
  def publish(message_type, title, content, detail \\ %{}, priority \\ 0, user_type \\ :normal) do
    # 创建消息
    Repo.transaction(fn ->
      message = %{
        message_type: message_type,
        title: title,
        content: content,
        detail: detail,
        priority: priority,
        user_type: user_type
      }
      {:ok, msg_record} = BlackBoard.create_message(message)
    end)
    {:ok, "消息通知发布成功"}
  end

  @doc """
  读消息列表，若已阅读该消息，则通过该接口将消息设为：已读状态
  """
  def mark_read(uid, message_id) do
    if reader_record = BlackBoard.get_reader(message_id, uid) do
      reader_record |> BlackBoard.update_reader(%{
        has_read: true,
        read_time: ChinaDateTime.now()
      })
      {:ok, "Success"}
    else
      {:error, "该用户无此消息 [ID: #{message_id}] 可被读取"}
    end
  end

  @doc """
  全部已读
  """
  def mark_read_all(uid) do
    BlackBoard.mark_read_all(uid)
  end

  @doc """
  撤销发布的通知 // 考虑事物
  """
  def cancel_message(id) do
    if message_record = BlackBoard.get_message(id) do
      message_record |> BlackBoard.update_message(%{
        has_canceled: true,
        cancel_time: ChinaDateTime.now()
      })
      remove_readers(id)
      {:ok, "Success"}
    else
      {:error, "无此消息 [ID: #{id}] 可被撤销"}
    end
  end

  @doc """
  删除发布的通知 // 考虑事物
  """
  def delete_message(id) do
    BlackBoard.delete_message(%{id: id})
    remove_readers(id)
    {:ok, "Success"}
  end

  def remove_readers(message_id) do
    BlackBoard.delete_readers(message_id)
  end
end