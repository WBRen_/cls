defmodule Notification.Pusher do
  alias Notification.BlackBoard.Service

  @doc """
  发送答案审核通知
  uid: 用户 id
  title: 标题
  content: 概述 / 最大长度 255
  priority: 优先级，越高越快被发送
  Notification.Pusher.send(:answer_apply, 1, "标题内容", "描述", %{})
  Notification.Pusher.send(:answer_apply, 1, "标题内容", "描述", ${}, 9)
  return ->
  {:ok, "消息通知发布成功"}
  """
  def send(:answer_apply, uid, title, content, detail) do
    Service.send(:answer_apply, uid, title, content, detail)
  end

  @doc """
  异步发送，不必关心返回结果
  """
  def send_async(:answer_apply, uid, title, content, detail) do
    Task.async(fn -> send(:answer_apply, uid, title, content, detail) end)
  end

  @doc """
  发送股价变动通知
  type: "stock_apply",
  title: "您的股票价格已达到提醒值",
  content: "招商银行的股价已发生变动，点击查看详情。",
  detail: {
    company: "招商银行",
    company_code: "600036",
    type: "stock_price",
    value: "25.00",
    portfolioItem_id: 1
  }
  return ->
  {:ok, "消息通知发布成功"}
  """
  def send(:stock_apply, uid, title, content, detail) do
    Service.send(:stock_apply, uid, title, content, detail)
  end

  @doc """
  异步发送，不必关心返回结果
  """
  def send_async(:stock_apply, uid, title, content, detail) do
    Task.async(fn -> send(:stock_apply, uid, title, content, detail) end)
  end

  @doc """
  删除通知消息
  return ->
  {:ok, "Success"}
  """
  def delete(message_id) do
    Service.delete_message(message_id)
  end
end