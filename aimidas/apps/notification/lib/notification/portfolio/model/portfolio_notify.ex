defmodule Notification.Portfolio.PortfolioNotify do
  use Ecto.Schema
  import Ecto.Changeset


  schema "portfolio_notifys" do
    field :user_id, :integer
    field :portfolio_item_id, :integer
    field :company_code, :string
    field :company_name, :string
    field :exchange_code, ExchangeCode # :SH, :SZ, :HK 或者 "SH", "SZ", "HK"
    field :param, :string
    field :value, :float
    field :type, PortfolioNotifyType # :up, :down 或者 "up", "down"
    field :has_notify, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(portfolio_notify, attrs) do
    portfolio_notify
    |> cast(attrs, [:user_id, :portfolio_item_id, :company_code, :company_name, :exchange_code, :type, :param, :value, :has_notify, :inserted_at, :updated_at])
    |> validate_required([:user_id, :portfolio_item_id, :company_code, :company_name, :exchange_code, :type, :has_notify])
  end
end
