defmodule Notification.Portfolio do
  @moduledoc """
  The Portfolio context.
  """

  import Ecto.Query, warn: false
  alias Notification.Repo

  alias Notification.Portfolio.PortfolioNotify


  # page_xxx 使用见 page_portfolio_notifys/1 方法 doc 说明
  def page_notifys_by_user_id(user_id, page) do
    PortfolioNotify
    |> where(user_id: ^user_id)
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end

  def page_notifys_by_portfolio_item_id(portfolio_item_id, page) do
    PortfolioNotify
    |> where(portfolio_item_id: ^portfolio_item_id)
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end

  @doc """
  Returns the page of portfolio_notifys.

  ## Examples

      iex> page_portfolio_notifys(%{page: 1, page_size: 10}})
      %{
        page_number: 1,
        page_size: 10,
        total_entries: 1234,
        total_pages: 13,
        data: [%PortfolioNotify{}, ...]
      }

  """
  def page_portfolio_notifys(page) do
    PortfolioNotify
    |> order_by(desc: :inserted_at)
    |> Repo.paginate(page)
  end
  @doc """
  Returns the list of portfolio_notifys.

  ## Examples

      iex> list_portfolio_notifys()
      [%PortfolioNotify{}, ...]

  """
  def list_portfolio_notifys do
    Repo.all(PortfolioNotify)
  end

  @doc """
  Gets a single portfolio_notify.

  Raises `Ecto.NoResultsError` if the Portfolio notify does not exist.

  ## Examples

      iex> get_portfolio_notify!(123)
      %PortfolioNotify{}

      iex> get_portfolio_notify!(456)
      ** (Ecto.NoResultsError)

  """
  def get_portfolio_notify!(id), do: Repo.get!(PortfolioNotify, id)
  def get_portfolio_notify(id) do
    PortfolioNotify
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end

  def get_portfolio_notify_no_notify() do
    PortfolioNotify
    |> where(has_notify: false)
    |> Repo.all
  end

  @doc """
  获取某一条记录，return nil if not exist
  """
  def get_portfolio_notify(id) do
    PortfolioNotify
    |> where(id: ^id)
    |> Repo.all
    |> List.first
  end
  @doc """
  Creates a portfolio_notify.

  ## Examples

      iex> create_portfolio_notify(%{field: value})
      {:ok, %PortfolioNotify{}}

      iex> create_portfolio_notify(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_portfolio_notify(attrs \\ %{}) do
    attrs_with_time = ChinaDateTime.put_created_time(attrs)
    %PortfolioNotify{}
    |> PortfolioNotify.changeset(attrs_with_time)
    |> Repo.insert()
  end

  @doc """
  Updates a portfolio_notify.

  ## Examples

      iex> update_portfolio_notify(portfolio_notify, %{field: new_value})
      {:ok, %PortfolioNotify{}}

      iex> update_portfolio_notify(portfolio_notify, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_portfolio_notify(%PortfolioNotify{} = portfolio_notify, attrs) do
    attrs_with_time = ChinaDateTime.put_updated_time(attrs)
    portfolio_notify
    |> PortfolioNotify.changeset(attrs_with_time)
    |> Repo.update()
  end

  @doc """
  Deletes a PortfolioNotify.

  ## Examples

      iex> delete_portfolio_notify(portfolio_notify)
      {:ok, %PortfolioNotify{}}

      iex> delete_portfolio_notify(portfolio_notify)
      {:error, %Ecto.Changeset{}}

  """
  def delete_portfolio_notify(%PortfolioNotify{} = portfolio_notify) do
    Repo.delete(portfolio_notify)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking portfolio_notify changes.

  ## Examples

      iex> change_portfolio_notify(portfolio_notify)
      %Ecto.Changeset{source: %PortfolioNotify{}}

  """
  def change_portfolio_notify(%PortfolioNotify{} = portfolio_notify) do
    PortfolioNotify.changeset(portfolio_notify, %{})
  end
end
