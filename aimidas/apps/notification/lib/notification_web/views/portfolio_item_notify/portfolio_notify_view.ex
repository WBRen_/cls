defmodule NotificationWeb.PortfolioNotifyView do
  use NotificationWeb, :view
  alias NotificationWeb.PortfolioNotifyView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, PortfolioNotifyView, "portfolio_notify.json")
    }
  end

  def render("index.json", %{portfolio_notifys: portfolio_notifys}) do
    %{data: render_many(portfolio_notifys, PortfolioNotifyView, "portfolio_notify.json")}
  end

  def render("show.json", %{portfolio_notify: portfolio_notify}) do
    %{data: render_one(portfolio_notify, PortfolioNotifyView, "portfolio_notify.json")}
  end

  def render("portfolio_notify.json", %{portfolio_notify: portfolio_notify}) do
    # 可在此定制输出给前端的内容，调用方式参考 Controller 内的 render 操作
    %{
      id: portfolio_notify.id,
      user_id: portfolio_notify.user_id,
      portfolio_item_id: portfolio_notify.portfolio_item_id,
      company_code: portfolio_notify.company_code,
      company_name: portfolio_notify.company_name,
      exchange_code: portfolio_notify.exchange_code,
      type: portfolio_notify.type,
      param: portfolio_notify.param,
      value: portfolio_notify.value,
      has_notify: portfolio_notify.has_notify,
      updated_at: portfolio_notify.updated_at,
      inserted_at: portfolio_notify.inserted_at
    }
  end
end
