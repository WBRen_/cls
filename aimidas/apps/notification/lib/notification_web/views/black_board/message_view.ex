defmodule NotificationWeb.MessageView do
  use NotificationWeb, :view
  alias NotificationWeb.MessageView

  def render("page.json", %{page: page}) do
    %{
      page_number: page.page_number,
      page_size: page.page_size,
      total_entries: page.total_entries,
      total_pages: page.total_pages,
      data: render_many(page.entries, MessageView, "message.json")
    }
  end

  def render("index.json", %{board_messages: board_messages}) do
    %{data: render_many(board_messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, MessageView, "message.json")}
  end

  def render("message.json", %{message: message}) do
    %{
      uid: message.uid,
      message_id: message.message_id,
      has_read: message.has_read,
      notification_time: message.notification_time,
      title: message.title,
      content: message.content,
      type: message.message_type,
      priority: message.priority,
      detail: message.detail
    }
  end
end
