defmodule NotificationWeb.PortfolioNotifyController do
  use NotificationWeb, :controller

  alias Notification.Portfolio
  alias Notification.Portfolio.PortfolioNotify
  alias Notification.Timer.StockMonitor

  action_fallback NotificationWeb.FallbackController

  def index(conn, params) do
    user = get_session(conn, :user)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    portfolio_notifys = Portfolio.page_notifys_by_user_id(user.id, page)
    render(conn, "page.json", page: portfolio_notifys)
  end

  def index2(conn, %{"portfolio_item_id" => portfolio_item_id} = params) do
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    portfolio_notifys = Portfolio.page_notifys_by_portfolio_item_id(portfolio_item_id, page)
    render(conn, "page.json", page: portfolio_notifys)
  end

  def create(conn, params) do
    user = get_session(conn, :user)
    params_with_user_id = params |> Map.put("user_id", user.id)
    with {:ok, %PortfolioNotify{} = portfolio_notify} <- Portfolio.create_portfolio_notify(params_with_user_id) do
      StockMonitor.check(portfolio_notify)
      conn
      |> put_status(:created)
      |> put_resp_header("location", portfolio_notify_path(conn, :show, portfolio_notify))
      |> render("show.json", portfolio_notify: portfolio_notify)
    end
  end

  def show(conn, %{"id" => id}) do
    portfolio_notify = Portfolio.get_portfolio_notify!(id)
    user = get_session(conn, :user)
    if portfolio_notify.user_id == user.id do
      render(conn, "show.json", portfolio_notify: portfolio_notify)
    else
      conn |> put_status(404) |> json(%{message: "Not Found"})
    end
  end

  def update(conn, %{"id" => id} = params) do
    portfolio_notify = Portfolio.get_portfolio_notify!(id)
    user = get_session(conn, :user)
    if portfolio_notify.user_id == user.id do
      params_with_user_id = params |> Map.put("user_id", user.id)
      with {:ok, %PortfolioNotify{} = portfolio_notify} <- Portfolio.update_portfolio_notify(portfolio_notify, params_with_user_id) do
        StockMonitor.check(portfolio_notify)
        render(conn, "show.json", portfolio_notify: portfolio_notify)
      end
    else
      conn |> put_status(404) |> json(%{message: "Not Found"})
    end
  end

  def delete(conn, %{"id" => id}) do
    user = get_session(conn, :user)
    portfolio_notify = Portfolio.get_portfolio_notify(id)
    if portfolio_notify do
      if portfolio_notify.user_id == user.id do
        with {:ok, %PortfolioNotify{}} <- Portfolio.delete_portfolio_notify(portfolio_notify) do
          conn |> put_status(200) |> json(%{message: "删除成功"})
        end
      else
        conn |> put_status(400) |> json(%{message: "无可删除数据"})
      end
    else
      conn |> put_status(400) |> json(%{message: "无可删除数据"})
    end
  end
end
