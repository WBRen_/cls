defmodule NotificationWeb.Controller do
  use NotificationWeb, :controller

  alias Notification.Receiver
  alias NotificationWeb.MessageView
  alias Notification.BlackBoard

  action_fallback AdminWeb.FallbackController

  def index(conn, %{"type" => type} = params) do
    user = get_session(conn, :user)
    page_number = params |> Map.get("page", "1") |> String.to_integer
    page_size = params |> Map.get("size", "10") |> String.to_integer
    page =
    %{
      page: page_number,
      page_size: page_size
    }
    render(conn, MessageView, "page.json", page: Receiver.page_all(user.id, type, page))
  end

  def delete(conn, %{"message_id" => id}) do
    BlackBoard.delete_readers(id)
    conn |> json(%{message: "Success"})
  end

  def read_one(conn, %{"message_id" => id}) do
    user = get_session(conn, :user)
    result = Receiver.mark_as_read(user.id, id)
    conn |> json(result)
  end

  def read_all(conn, _params) do
    user = get_session(conn, :user)
    result = Receiver.mark_as_read(user.id)
    conn |> json(result)
  end

  def unread_count(conn, %{"type" => type}) do
    user = get_session(conn, :user)
    case Receiver.count_unread(user.id, type) do
      {:ok, c} -> conn |> json(%{data: %{unread_count: c}})
      _ -> conn |> put_status(400) |> json(%{message: "获取失败，请稍后重新尝试"})
    end
  end
end
