defmodule NotificationWeb.PageController do
  use NotificationWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
