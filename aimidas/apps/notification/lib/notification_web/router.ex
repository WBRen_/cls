defmodule NotificationWeb.Router do
  use NotificationWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug NotificationWeb.Context
  end

  # scope "/", NotificationWeb do
  #   pipe_through :browser # Use the default browser stack
  # end

  # Other scopes may use custom stacks.
  scope "/app/api/notifications", NotificationWeb do
    pipe_through :api
    get "/:type", Controller, :index
    delete "/:message_id", Controller, :delete
    put "/read/:message_id", Controller, :read_one
    put "/read", Controller, :read_all
    get "/unread/count", Controller, :unread_count
    resources "/portfolio/notifys", PortfolioNotifyController, only: [:index, :show, :create, :update, :delete]
    get "/portfolio/notifys_by_portfolio_item_id/:portfolio_item_id", PortfolioNotifyController, :index2
  end
end
