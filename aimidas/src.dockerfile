FROM bitwalker/alpine-elixir:1.6.5 as builder
# FROM aimidas-prebuild:1.0 as builder
# FROM elixir:1.6-alpine as builder

ENV REPLACE_OS_VARS true
ENV PORT 4000
ENV LC_ALL=zh_CN.UTF-8
ARG MIX_ENV
# ENV MIX_ENV=${MIX_ENV}
ARG NLP_URL

ARG ENVIRONMENT

EXPOSE 14000
EXPOSE 14010

RUN sed -i "s/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g" /etc/apk/repositories \
 && sed -i "s/nl.alpinelinux.org/mirrors.aliyun.com/g" /etc/apk/repositories \
 && apk upgrade \
 && apk update \
 && apk add \
    bash \
    openssl \
    curl \
    build-base \
    alpine-sdk \
    coreutils

RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd

ADD . /app
WORKDIR /app
# RUN mkdir -p /app/deps \
#     && mkdir -p /app/_build/dev/lib \
#     && ls / \
#     && ls -la /app/_build/dev/lib/ \
#     && cp -R /app-prebuild/deps/appsignal /app/deps \
#     && cp -R /app-prebuild/_build/dev/lib/appsignal /app/_build/dev/lib 

RUN export HEX_HTTP_TIMEOUT=30000 \
    && export HEX_MIRROR="https://cdn.jsdelivr.net/hex" \
    && env \
    && echo "MIX_ENV = > $MIX_ENV" \
    && rm -rf $(find / -name ".hex") \
    && mix local.rebar --force \
    && mix local.hex --force \
    && mix deps.get \
    && mix deps.compile \
    && mix compile \
    && tar zcvf /aimidas.tar.gz .

FROM bitwalker/alpine-elixir:1.6.5 as release
# FROM elixir:1.6-alpine as release

# ENV REPLACE_OS_VARS true
# ENV PORT 4000
# ENV LC_ALL=zh_CN.UTF-8
# ARG MIX_ENV
# # ENV MIX_ENV=${MIX_ENV}
# ARG NLP_URL

# ARG ENVIRONMENT
# # ENV ENVIRONMENT=${ENVIRONMENT}
# #ENV AIMIDAS_DB_NAME="aimidas_aaaa"

# EXPOSE 14000

WORKDIR /app
COPY --from=builder /aimidas.tar.gz .

RUN tar xvfz aimidas.tar.gz > /dev/null && rm aimidas.tar.gz

COPY ./rel/scripts/docker-entrypoint.sh .
COPY ./rel/scripts/migrate-and-run.sh .
RUN ["chmod", "+x", "/app/docker-entrypoint.sh"]
RUN ["chmod", "+x", "/app/migrate-and-run.sh"]

# ENTRYPOINT ["/bin/bash"]
ENTRYPOINT ["/app/docker-entrypoint.sh"]

CMD ["/app/migrate-and-run.sh"]
