use Mix.Config

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

config :rollbax,
  access_token: "7fdb93470cec475ea1651c626d671a60",
  environment: "dev"

config :rollbax, enable_crash_reports: true
