use Mix.Config

# Do not print debug messages in production
config :logger, level: :info

config :rollbax,
  access_token: "7fdb93470cec475ea1651c626d671a60",
  environment: "prod"

config :rollbax, enable_crash_reports: true

config :httpotion, default_timeout: 50000