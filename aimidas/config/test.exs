use Mix.Config

# Print only warnings and errors during test
config :logger, level: :warn

config :rollbax, enabled: false

# config httppotion default timeout to 15s
config :httpotion, default_timeout: 15000