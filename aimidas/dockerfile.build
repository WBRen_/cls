# Build assets
FROM node:10-alpine as assets
RUN mkdir -p /priv/static
RUN mkdir -p /deps/phoenix
RUN mkdir -p /deps/phoenix_html
WORKDIR /build/apps/aimidas_web/assets
COPY apps/aimidas_web/assets .
COPY deps/phoenix /build/deps/phoenix
COPY deps/phoenix_html /build/deps/phoenix_html
RUN npm install 
RUN yarn install \
 && yarn run deploy

# Elixir build container
FROM bitwalker/alpine-elixir:1.6.5 as builder

ENV SECRET_KEY_BASE ""
ARG MIX_ENV
ENV MIX_ENV=${MIX_ENV}

RUN apk upgrade \
 && apk update \
 && apk add --upgrade apk-tools@edge \
 && apk upgrade \
 && apk update \
 && apk add \
   git \
   build-base \
   alpine-sdk \
   coreutils \
   curl \
   openssh

RUN mix local.hex --force && mix local.rebar --force
RUN mkdir /build
ADD . /build
WORKDIR /build
COPY --from=assets /priv/static apps/aimidas_web/priv/static
RUN mix deps.clean --all && mix deps.get
RUN mix do phx.digest, release --env=${MIX_ENV}

# Release Container
FROM alpine:3.7 as release

ENV REPLACE_OS_VARS true
ENV PORT 4000
ENV LC_ALL=zh_CN.UTF-8
ARG MIX_ENV
ENV MIX_ENV=${MIX_ENV}
ARG ENVIRONMENT
ENV ENVIRONMENT=${ENVIRONMENT}

EXPOSE 4000

RUN apk update && apk upgrade
RUN apk add bash openssl curl
RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd

WORKDIR /app
COPY --from=builder /build/_build/$MIX_ENV/rel/aimidas/releases/0.1.0/aimidas.tar.gz .
RUN tar xvfz aimidas.tar.gz > /dev/null && rm aimidas.tar.gz

COPY --from=builder /build/rel/scripts/docker-entrypoint.sh .
RUN ["chmod", "+x", "/app/docker-entrypoint.sh"]

ENTRYPOINT ["/app/docker-entrypoint.sh"]

CMD ["/app/bin/aimidas", "foreground"]
