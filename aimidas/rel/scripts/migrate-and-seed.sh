#!/bin/sh

echo "Starting migration of aimidas"
$RELEASE_ROOT_DIR/bin/aimidas command Elixir.Aimidas.Release.Tasks migrate
echo "Finished migration of aimidas"

echo "Starting seeds of aimidas"
$RELEASE_ROOT_DIR/bin/aimidas command Elixir.Aimidas.Release.Tasks seed
echo "Finished seeds of aimidas"
